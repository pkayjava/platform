package com.angkorteam.pki.master.model;

import org.apache.wicket.request.resource.ResourceReference;
import org.apache.wicket.util.lang.Args;

import java.io.Serializable;

public class NavbarMenu implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1283110730375482419L;

    private String text;

    private ResourceReference image;

    private Icon icon;

    private Badge badge;

    private Dropdown dropdown;

    public NavbarMenu(String text) {
        this.text = text;
        Args.notNull(text, "text must not null");
    }

    public NavbarMenu(Icon icon) {
        Args.notNull(icon, "icon must not null");
        this.icon = icon;
    }

    public NavbarMenu(ResourceReference image) {
        Args.notNull(image, "image must not null");
        this.image = image;
    }

    public ResourceReference getImage() {
        return image;
    }

    public void setImage(ResourceReference image) {
        this.image = image;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public Badge getBadge() {
        return badge;
    }

    public void setBadge(Badge badge) {
        this.badge = badge;
    }

    public Dropdown getDropdown() {
        return dropdown;
    }

    public void setDropdown(NavbarDropdown dropdown) {
        this.dropdown = dropdown;
    }

    public void setDropdown(NavbarUserDropdown dropdown) {
        this.dropdown = dropdown;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
