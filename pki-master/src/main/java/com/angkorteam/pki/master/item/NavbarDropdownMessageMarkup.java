package com.angkorteam.pki.master.item;

import com.angkorteam.core.webui.Emoji;
import com.angkorteam.pki.master.model.NavbarDropdownItemMarkup;
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.ExternalImage;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import java.util.ArrayList;
import java.util.List;

public class NavbarDropdownMessageMarkup extends NavbarDropdownItemMarkup {

    /**
     *
     */
    private static final long serialVersionUID = 6667950204689927258L;

    protected NavbarDropdownMessageModel model;

    public NavbarDropdownMessageMarkup(String id, IModel<?> model) {
        super(id, model);
    }

    @Override
    protected void initData() {
        this.model = (NavbarDropdownMessageModel) getDefaultModelObject();
    }

    @Override
    protected void initComponent() {
        BookmarkablePageLink<Void> link = new BookmarkablePageLink<Void>("link", this.model.getPage(),
                this.model.getParameters());
        add(link);

        ExternalImage image = new ExternalImage("image", urlFor(this.model.getImage(), null).toString());
        link.add(image);

        Label h4 = new Label("h4", new PropertyModel<>(this.model, "h4"));
        link.add(h4);

        List<String> clazz = new ArrayList<>(2);
        if (this.model.getSmallIcon().getType() == Emoji.FA) {
            clazz.add("fa");
        }
        if (this.model.getSmallIcon().getType() == Emoji.ION) {
            clazz.add("ion");
        }
        clazz.add(this.model.getSmallIcon().getLiteral());
        WebMarkupContainer smallIcon = new WebMarkupContainer("smallIcon");
        smallIcon.add(AttributeModifier.replace("class", StringUtils.join(clazz, " ")));
        link.add(smallIcon);

        Label smallText = new Label("smallText", new PropertyModel<>(this.model, "smallText"));
        link.add(smallText);

        Label paragraph = new Label("paragraph", new PropertyModel<>(this.model, "paragraph"));
        link.add(paragraph);
    }

    @Override
    protected void configureMetaData() {
    }

}
