package com.angkorteam.pki.master.model;

import com.angkorteam.core.webui.BadgeType;

import java.io.Serializable;

public class Badge implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3437706617228585189L;

    private String text;

    private BadgeType type;

    public Badge(String text) {
        this.text = text;
    }

    public Badge(String text, BadgeType type) {
        this.text = text;
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public BadgeType getType() {
        return type;
    }

    public void setType(BadgeType type) {
        this.type = type;
    }

}
