package com.angkorteam.pki.master.model;

import java.io.Serializable;

public class NavbarDropdownItem implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5617796878240923739L;

    private NavbarDropdownItemModel model;

    private Class<? extends NavbarDropdownItemMarkup> markup;

    public NavbarDropdownItem(NavbarDropdownItemModel model, Class<? extends NavbarDropdownItemMarkup> markup) {
        this.model = model;
        this.markup = markup;
    }

    public NavbarDropdownItemModel getModel() {
        return model;
    }

    public void setModel(NavbarDropdownItemModel model) {
        this.model = model;
    }

    public Class<? extends NavbarDropdownItemMarkup> getMarkup() {
        return markup;
    }

    public void setMarkup(Class<? extends NavbarDropdownItemMarkup> markup) {
        this.markup = markup;
    }

}
