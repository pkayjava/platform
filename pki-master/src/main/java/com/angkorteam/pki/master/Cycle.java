package com.angkorteam.pki.master;

import com.angkorteam.boot.webui.ICycle;
import com.angkorteam.core.webui.WebUiProperties;
import com.angkorteam.pki.master.pages.HomePage;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.protocol.http.WebSession;

public class Cycle implements ICycle {

    /**
     *
     */
    private static final long serialVersionUID = 5356829316167162608L;

    @Override
    public Class<? extends WebPage> getHomePageClass(WebUiProperties webUiProperties, WebSession session) {
        return HomePage.class;
    }

    @Override
    public void onInitializedApplication(WebUiProperties webUiProperties, WebApplication application) {
        application.mountPage("/home", com.angkorteam.pki.master.pages.HomePage.class);
    }

}
