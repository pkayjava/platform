package com.angkorteam.pki.master.item;

import com.angkorteam.pki.master.model.NavbarDropdownItemMarkup;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.ExternalImage;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

public class NavbarDropdownUserHeaderMarkup extends NavbarDropdownItemMarkup {

    /**
     *
     */
    private static final long serialVersionUID = 6667950204689927258L;

    protected NavbarDropdownUserHeaderModel model;

    public NavbarDropdownUserHeaderMarkup(String id, IModel<?> model) {
        super(id, model);
    }

    @Override
    protected void initData() {
        this.model = (NavbarDropdownUserHeaderModel) getDefaultModelObject();
    }

    @Override
    protected void initComponent() {
        ExternalImage image = new ExternalImage("image", urlFor(this.model.getImage(), null).toString());
        add(image);

        Label paragraph = new Label("paragraph", new PropertyModel<>(this.model, "paragraph"));
        add(paragraph);

        Label smallText = new Label("smallText", new PropertyModel<>(this.model, "smallText"));
        add(smallText);
    }

    @Override
    protected void configureMetaData() {
    }

}
