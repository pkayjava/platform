package com.angkorteam.pki.master.model;

import com.angkorteam.core.webui.Emoji;
import com.angkorteam.core.webui.TextColor;
import org.apache.wicket.util.lang.Args;

import java.io.Serializable;

public class Icon implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3325962448107267154L;

    private Emoji emoji;

    private TextColor color;

    public Icon(Emoji emoji) {
        Args.notNull(emoji, "emoji must not null");
        this.emoji = emoji;
    }

    public Icon(Emoji emoji, TextColor color) {
        Args.notNull(emoji, "emoji must not null");
        Args.notNull(color, "color must not null");
        this.emoji = emoji;
        this.color = color;
    }

    public Emoji getEmoji() {
        return emoji;
    }

    public void setEmoji(Emoji emoji) {
        Args.notNull(emoji, "emoji must not null");
        this.emoji = emoji;
    }

    public TextColor getColor() {
        return color;
    }

    public void setColor(TextColor color) {
        Args.notNull(color, "color must not null");
        this.color = color;
    }

}
