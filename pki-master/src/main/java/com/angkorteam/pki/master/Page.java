package com.angkorteam.pki.master;

import com.angkorteam.boot.webui.HomePage;
import com.angkorteam.core.webui.Emoji;
import com.angkorteam.core.webui.ReferenceUtilities;
import com.angkorteam.core.webui.models.*;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import java.util.ArrayList;
import java.util.List;

public abstract class Page extends DashboardPage {

    @Override
    protected IModel<PageLogo> buildPageLogo() {
        return null;
    }

    @Override
    protected IModel<PageFooter> buildPageFooter() {
        return null;
    }

    @Override
    protected IModel<PageHeader> buildPageHeader() {
        return null;
    }

    @Override
    protected IModel<List<PageBreadcrumb>> buildPageBreadcrumb() {
        return null;
    }

    @Override
    protected IModel<List<SideMenu>> buildSideMenu() {
        return null;
    }

    @Override
    protected IModel<List<NavBarMenu>> buildNavBarMenu() {
        NavBarMenu logoutMenu01 = new NavBarMenu.Builder()
                .withImage(
                        urlFor(ReferenceUtilities.loadResourceReference(
                                ReferenceUtilities.AdminLTE + "/dist/img/user2-160x160.jpg"), null).toString(),
                        "Logout", HomePage.class)
                .build();
        NavBarMenu logoutMenu02 = new NavBarMenu.Builder().withIcon(Emoji.ion_log_out, "Logout", HomePage.class)
                .build();
        NavBarMenu logoutMenu03 = new NavBarMenu.Builder().withIcon(Emoji.ion_log_out, "Logout", HomePage.class)
                .build();
        NavBarMenu logoutMenu04 = new NavBarMenu.Builder().withIcon(Emoji.ion_log_out, "Logout", HomePage.class)
                .build();
        List<NavBarMenu> menus = new ArrayList<>();
        menus.add(logoutMenu01);
        menus.add(logoutMenu02);
        menus.add(logoutMenu03);
        menus.add(logoutMenu04);
        return Model.ofList(menus);
    }

    @Override
    protected IModel<UserInfo> buildUserInfo() {
        return null;
    }

    @Override
    protected IModel<Boolean> hasSearchForm() {
        return null;
    }

    @Override
    protected void onSearchClick(String searchValue) {

    }

}
