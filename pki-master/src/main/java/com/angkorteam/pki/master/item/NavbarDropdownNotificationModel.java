package com.angkorteam.pki.master.item;

import com.angkorteam.pki.master.model.Icon;
import com.angkorteam.pki.master.model.NavbarDropdownItemModel;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class NavbarDropdownNotificationModel extends NavbarDropdownItemModel {

    private Class<? extends WebPage> page;

    private PageParameters parameters;

    private Icon icon;

    private String text;

    /**
     *
     */
    private static final long serialVersionUID = 6173694912988735850L;

    public NavbarDropdownNotificationModel(Class<? extends WebPage> page, PageParameters parameters) {
        super(NavbarDropdownNotificationMarkup.class);
        this.page = page;
        this.parameters = parameters;
    }

    public Class<? extends WebPage> getPage() {
        return page;
    }

    public void setPage(Class<? extends WebPage> page) {
        this.page = page;
    }

    public PageParameters getParameters() {
        return parameters;
    }

    public void setParameters(PageParameters parameters) {
        this.parameters = parameters;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
