package com.angkorteam.pki.master.model;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.io.Serializable;

public class Branding implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5059273688615591171L;

    private BrandingText mini;

    private BrandingText large;

    private Class<? extends WebPage> linkPage;

    private PageParameters parameters;

    public BrandingText getMini() {
        return mini;
    }

    public void setMini(BrandingText mini) {
        this.mini = mini;
    }

    public BrandingText getLarge() {
        return large;
    }

    public void setLarge(BrandingText large) {
        this.large = large;
    }

    public Class<? extends WebPage> getLinkPage() {
        return linkPage;
    }

    public void setLinkPage(Class<? extends WebPage> linkPage) {
        this.linkPage = linkPage;
    }

    public PageParameters getParameters() {
        return parameters;
    }

    public void setParameters(PageParameters parameters) {
        this.parameters = parameters;
    }

}
