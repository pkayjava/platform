package com.angkorteam.pki.master.item;

import com.angkorteam.pki.master.model.NavbarDropdownItemModel;
import org.apache.wicket.request.resource.ResourceReference;

public class NavbarDropdownUserHeaderModel extends NavbarDropdownItemModel {

    /**
     *
     */
    private static final long serialVersionUID = 6173694912988735850L;

    private ResourceReference image;

    private String paragraph;

    private String smallText;

    public NavbarDropdownUserHeaderModel(ResourceReference image) {
        super(NavbarDropdownUserHeaderMarkup.class);
        this.image = image;
    }

    public ResourceReference getImage() {
        return image;
    }

    public void setImage(ResourceReference image) {
        this.image = image;
    }

    public String getParagraph() {
        return paragraph;
    }

    public void setParagraph(String paragraph) {
        this.paragraph = paragraph;
    }

    public String getSmallText() {
        return smallText;
    }

    public void setSmallText(String smallText) {
        this.smallText = smallText;
    }

}
