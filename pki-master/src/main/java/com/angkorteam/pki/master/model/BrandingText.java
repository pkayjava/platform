package com.angkorteam.pki.master.model;

import java.io.Serializable;

public class BrandingText implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -6257598186581216254L;

    private String boldText;

    private String skinText;

    public String getBoldText() {
        return boldText;
    }

    public void setBoldText(String boldText) {
        this.boldText = boldText;
    }

    public String getSkinText() {
        return skinText;
    }

    public void setSkinText(String skinText) {
        this.skinText = skinText;
    }

}
