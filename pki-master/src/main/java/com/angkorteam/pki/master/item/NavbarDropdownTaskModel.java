package com.angkorteam.pki.master.item;

import com.angkorteam.core.webui.ProgressBarColor;
import com.angkorteam.pki.master.model.NavbarDropdownItemModel;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class NavbarDropdownTaskModel extends NavbarDropdownItemModel {

    private Class<? extends WebPage> page;

    private PageParameters parameters;

    private String h3;

    private int progressValue;

    private String progressSmallText;

    private String progressDescriptionText;

    private String style;

    private ProgressBarColor progressBarColor;

    /**
     *
     */
    private static final long serialVersionUID = 6173694912988735850L;

    public NavbarDropdownTaskModel(Class<? extends WebPage> page, PageParameters parameters) {
        super(NavbarDropdownTaskMarkup.class);
        this.page = page;
        this.parameters = parameters;
        setProgressValue(0);
    }

    public Class<? extends WebPage> getPage() {
        return page;
    }

    public void setPage(Class<? extends WebPage> page) {
        this.page = page;
    }

    public PageParameters getParameters() {
        return parameters;
    }

    public void setParameters(PageParameters parameters) {
        this.parameters = parameters;
    }

    public String getH3() {
        return h3;
    }

    public void setH3(String h3) {
        this.h3 = h3;
    }

    public ProgressBarColor getProgressBarColor() {
        return progressBarColor;
    }

    public void setProgressBarColor(ProgressBarColor progressBarColor) {
        this.progressBarColor = progressBarColor;
    }

    public int getProgressValue() {
        return progressValue;
    }

    public void setProgressValue(int progressValue) {
        this.progressValue = Math.abs(progressValue);
        this.progressSmallText = this.progressValue + "%";
        this.progressDescriptionText = this.progressValue + "% Completed";
        this.style = "width: " + this.progressValue + "%";
    }

    public String getProgressSmallText() {
        return progressSmallText;
    }

    public String getProgressDescriptionText() {
        return progressDescriptionText;
    }

    public String getStyle() {
        return style;
    }

}
