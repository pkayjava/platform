package com.angkorteam.pki.master.item;

import com.angkorteam.pki.master.model.NavbarDropdownItemMarkup;
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import java.util.ArrayList;
import java.util.List;

public class NavbarDropdownTaskMarkup extends NavbarDropdownItemMarkup {

    /**
     *
     */
    private static final long serialVersionUID = 6667950204689927258L;

    protected NavbarDropdownTaskModel model;

    public NavbarDropdownTaskMarkup(String id, IModel<?> model) {
        super(id, model);
    }

    @Override
    protected void initData() {
        this.model = (NavbarDropdownTaskModel) getDefaultModelObject();
    }

    @Override
    protected void initComponent() {
        BookmarkablePageLink<Void> link = new BookmarkablePageLink<Void>("link", this.model.getPage(),
                this.model.getParameters());
        add(link);

        Label h3 = new Label("h3", new PropertyModel<>(this.model, "h3"));
        link.add(h3);

        Label smallText = new Label("smallProgress", new PropertyModel<>(this.model, "progressSmallText"));
        link.add(smallText);

        WebMarkupContainer progressBar = new WebMarkupContainer("progressBar");
        progressBar.add(AttributeModifier.replace("style", new PropertyModel<>(this.model, "style")));
        progressBar.add(AttributeModifier.replace("aria-valuenow", new PropertyModel<>(this.model, "progressValue")));

        List<String> clazz = new ArrayList<>(2);
        clazz.add("progress-bar");
        if (this.model.getProgressBarColor() != null) {
            clazz.add(this.model.getProgressBarColor().getLiteral());
        }
        progressBar.add(AttributeModifier.replace("class", StringUtils.join(clazz, " ")));

        link.add(progressBar);

        Label progressBarSr = new Label("progressBarSr", new PropertyModel<>(this.model, "progressDescriptionText"));
        progressBar.add(progressBarSr);
    }

    @Override
    protected void configureMetaData() {
    }

}
