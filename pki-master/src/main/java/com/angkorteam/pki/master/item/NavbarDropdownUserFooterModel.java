package com.angkorteam.pki.master.item;

import com.angkorteam.pki.master.model.Link;
import com.angkorteam.pki.master.model.NavbarDropdownItemModel;

public class NavbarDropdownUserFooterModel extends NavbarDropdownItemModel {

    /**
     *
     */
    private static final long serialVersionUID = 6173694912988735850L;

    private Link leftLink;

    private Link rightLink;

    public NavbarDropdownUserFooterModel() {
        super(NavbarDropdownUserFooterMarkup.class);
    }

    public Link getLeftLink() {
        return leftLink;
    }

    public void setLeftLink(Link leftLink) {
        this.leftLink = leftLink;
    }

    public Link getRightLink() {
        return rightLink;
    }

    public void setRightLink(Link rightLink) {
        this.rightLink = rightLink;
    }

}
