package com.angkorteam.pki.master.item;

import com.angkorteam.core.webui.Emoji;
import com.angkorteam.pki.master.model.NavbarDropdownItemModel;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.ResourceReference;

public class NavbarDropdownMessageModel extends NavbarDropdownItemModel {

    private Class<? extends WebPage> page;

    private PageParameters parameters;

    private ResourceReference image;

    private String h4;

    private Emoji smallIcon;

    private String smallText;

    private String paragraph;

    /**
     *
     */
    private static final long serialVersionUID = 6173694912988735850L;

    public NavbarDropdownMessageModel(Class<? extends WebPage> page, PageParameters parameters) {
        super(NavbarDropdownMessageMarkup.class);
        this.page = page;
        this.parameters = parameters;
    }

    public Class<? extends WebPage> getPage() {
        return page;
    }

    public void setPage(Class<? extends WebPage> page) {
        this.page = page;
    }

    public PageParameters getParameters() {
        return parameters;
    }

    public void setParameters(PageParameters parameters) {
        this.parameters = parameters;
    }

    public ResourceReference getImage() {
        return image;
    }

    public void setImage(ResourceReference image) {
        this.image = image;
    }

    public String getH4() {
        return h4;
    }

    public void setH4(String h4) {
        this.h4 = h4;
    }

    public Emoji getSmallIcon() {
        return smallIcon;
    }

    public void setSmallIcon(Emoji smallIcon) {
        this.smallIcon = smallIcon;
    }

    public String getSmallText() {
        return smallText;
    }

    public void setSmallText(String smallText) {
        this.smallText = smallText;
    }

    public String getParagraph() {
        return paragraph;
    }

    public void setParagraph(String paragraph) {
        this.paragraph = paragraph;
    }

}
