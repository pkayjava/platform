package com.angkorteam.pki.master.item;

import com.angkorteam.pki.master.model.NavbarDropdownItemMarkup;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

public class NavbarDropdownUserFooterMarkup extends NavbarDropdownItemMarkup {

    /**
     *
     */
    private static final long serialVersionUID = 6667950204689927258L;

    protected NavbarDropdownUserFooterModel model;

    public NavbarDropdownUserFooterMarkup(String id, IModel<?> model) {
        super(id, model);
    }

    @Override
    protected void initData() {
        this.model = (NavbarDropdownUserFooterModel) getDefaultModelObject();
    }

    @Override
    protected void initComponent() {
        WebMarkupContainer leftLink = null;
        if (this.model.getLeftLink() != null) {
            leftLink = new BookmarkablePageLink<>("leftLink", this.model.getLeftLink().getPage(),
                    this.model.getLeftLink().getParameters());
            leftLink.add(new Label("text", new PropertyModel<>(this.model, "leftLink.text")));
        } else {
            leftLink = new WebMarkupContainer("leftLink");
            leftLink.setVisible(false);
        }
        add(leftLink);

        WebMarkupContainer rightLink = null;
        if (this.model.getRightLink() != null) {
            rightLink = new BookmarkablePageLink<>("rightLink", this.model.getRightLink().getPage(),
                    this.model.getRightLink().getParameters());
            rightLink.add(new Label("text", new PropertyModel<>(this.model, "rightLink.text")));
        } else {
            rightLink = new WebMarkupContainer("rightLink");
            rightLink.setVisible(false);
        }
        add(rightLink);

    }

    @Override
    protected void configureMetaData() {
    }

}
