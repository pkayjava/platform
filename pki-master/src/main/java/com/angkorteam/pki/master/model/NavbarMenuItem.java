package com.angkorteam.pki.master.model;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.ResourceReference;
import org.apache.wicket.util.lang.Args;

import java.io.Serializable;

public class NavbarMenuItem implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2939335335791719818L;

    private String text;

    private ResourceReference image;

    private Icon icon;

    private Badge badge;

    private Class<? extends WebPage> page;

    private PageParameters parameters;

    public NavbarMenuItem(String text, Class<? extends WebPage> page, PageParameters parameters) {
        this.text = text;
        Args.notNull(text, "text must not null");
        Args.notNull(page, "page must not null");
        this.page = page;
        this.parameters = parameters;
    }

    public NavbarMenuItem(Icon icon, Class<? extends WebPage> page, PageParameters parameters) {
        Args.notNull(icon, "icon must not null");
        Args.notNull(page, "page must not null");
        this.icon = icon;
        this.page = page;
        this.parameters = parameters;
    }

    public NavbarMenuItem(ResourceReference image, Class<? extends WebPage> page, PageParameters parameters) {
        Args.notNull(image, "image must not null");
        Args.notNull(page, "page must not null");
        this.image = image;
        this.page = page;
        this.parameters = parameters;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Class<? extends WebPage> getPage() {
        return page;
    }

    public void setPage(Class<? extends WebPage> page) {
        this.page = page;
    }

    public PageParameters getParameters() {
        return parameters;
    }

    public void setParameters(PageParameters parameters) {
        this.parameters = parameters;
    }

    public ResourceReference getImage() {
        return image;
    }

    public void setImage(ResourceReference image) {
        this.image = image;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public Badge getBadge() {
        return badge;
    }

    public void setBadge(Badge badge) {
        this.badge = badge;
    }

}
