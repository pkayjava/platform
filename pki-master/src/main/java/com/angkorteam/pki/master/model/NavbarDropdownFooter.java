package com.angkorteam.pki.master.model;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.io.Serializable;

public class NavbarDropdownFooter implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5969297634125752326L;

    private String text;

    private Class<? extends WebPage> page;

    private PageParameters parameters;

    public NavbarDropdownFooter(String text) {
        super();
        this.text = text;
    }

    public NavbarDropdownFooter(String text, Class<? extends WebPage> page, PageParameters parameters) {
        super();
        this.text = text;
        this.page = page;
        this.parameters = parameters;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Class<? extends WebPage> getPage() {
        return page;
    }

    public void setPage(Class<? extends WebPage> page) {
        this.page = page;
    }

    public PageParameters getParameters() {
        return parameters;
    }

    public void setParameters(PageParameters parameters) {
        this.parameters = parameters;
    }

}
