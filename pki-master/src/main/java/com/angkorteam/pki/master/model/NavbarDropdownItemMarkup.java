package com.angkorteam.pki.master.model;

import com.angkorteam.core.webui.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

public abstract class NavbarDropdownItemMarkup extends Panel {

    /**
     *
     */
    private static final long serialVersionUID = 6667950204689927258L;

    public NavbarDropdownItemMarkup(String id, IModel<?> model) {
        super(id, model);
    }

}
