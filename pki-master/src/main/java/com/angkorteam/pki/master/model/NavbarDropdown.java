package com.angkorteam.pki.master.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NavbarDropdown implements Dropdown, Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1283110730375482419L;

    private NavbarDropdownFooter footer;

    private NavbarDropdownHeader header;

    private List<NavbarDropdownItem> items = new ArrayList<>(0);

    public List<NavbarDropdownItem> getItems() {
        return Collections.unmodifiableList(this.items);
    }

    public void addItem(NavbarDropdownItemModel model) {
        addItem(model, model.getMarkup());
    }

    public void addItem(NavbarDropdownItemModel model, Class<? extends NavbarDropdownItemMarkup> markup) {
        NavbarDropdownItem item = new NavbarDropdownItem(model, markup);
        this.items.add(item);
    }

    public NavbarDropdownFooter getFooter() {
        return footer;
    }

    public void setFooter(NavbarDropdownFooter footer) {
        this.footer = footer;
    }

    public NavbarDropdownHeader getHeader() {
        return header;
    }

    public void setHeader(NavbarDropdownHeader header) {
        this.header = header;
    }

}
