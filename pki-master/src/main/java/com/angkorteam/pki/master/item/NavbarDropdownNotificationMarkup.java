package com.angkorteam.pki.master.item;

import com.angkorteam.core.webui.Emoji;
import com.angkorteam.pki.master.model.NavbarDropdownItemMarkup;
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;

import java.util.ArrayList;
import java.util.List;

public class NavbarDropdownNotificationMarkup extends NavbarDropdownItemMarkup {

    /**
     *
     */
    private static final long serialVersionUID = 6667950204689927258L;

    protected NavbarDropdownNotificationModel model;

    public NavbarDropdownNotificationMarkup(String id, IModel<?> model) {
        super(id, model);
    }

    @Override
    protected void initData() {
        this.model = (NavbarDropdownNotificationModel) getDefaultModelObject();
    }

    @Override
    protected void initComponent() {
        BookmarkablePageLink<Void> link = new BookmarkablePageLink<Void>("link", this.model.getPage(),
                this.model.getParameters());
        add(link);

        List<String> clazz = new ArrayList<>(2);
        if (this.model.getIcon().getEmoji().getType() == Emoji.FA) {
            clazz.add("fa");
        }
        if (this.model.getIcon().getEmoji().getType() == Emoji.ION) {
            clazz.add("ion");
        }
        clazz.add(this.model.getIcon().getEmoji().getLiteral());

        if (this.model.getIcon().getColor() != null) {
            clazz.add(this.model.getIcon().getColor().getLiteral());
        }

        WebMarkupContainer icon = new WebMarkupContainer("icon");
        icon.add(AttributeModifier.replace("class", StringUtils.join(clazz, " ")));
        link.add(icon);

        Label text = new Label("text", new PropertyModel<>(this.model, "text"));
        link.add(text);
    }

    @Override
    protected void configureMetaData() {
    }

}
