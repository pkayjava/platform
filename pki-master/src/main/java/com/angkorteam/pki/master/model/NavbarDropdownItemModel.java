package com.angkorteam.pki.master.model;

import java.io.Serializable;

public abstract class NavbarDropdownItemModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2036903553004785822L;

    private final Class<? extends NavbarDropdownItemMarkup> markup;

    public NavbarDropdownItemModel(Class<? extends NavbarDropdownItemMarkup> markup) {
        this.markup = markup;
    }

    public Class<? extends NavbarDropdownItemMarkup> getMarkup() {
        return this.markup;
    }

}
