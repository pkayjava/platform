package com.angkorteam.pki.master;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ServletInitializer extends SpringBootServletInitializer {

    protected ServletContext servletContext;

    protected File propertiesFile;

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        this.servletContext = servletContext;
        this.propertiesFile = new File(this.servletContext.getRealPath(".") + ".properties");
        super.onStartup(servletContext);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        if (this.propertiesFile.exists() && this.propertiesFile.isFile() && this.propertiesFile.canRead()) {
            try (FileInputStream stream = new FileInputStream(this.propertiesFile)) {
                Properties prop = new Properties();
                prop.load(stream);
                application.properties(prop);
            } catch (IOException e) {
            }
        }
        return application.sources(BootApplication.class);
    }

}