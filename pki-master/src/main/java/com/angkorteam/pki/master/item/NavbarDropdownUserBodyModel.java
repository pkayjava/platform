package com.angkorteam.pki.master.item;

import com.angkorteam.pki.master.model.Link;
import com.angkorteam.pki.master.model.NavbarDropdownItemModel;

public class NavbarDropdownUserBodyModel extends NavbarDropdownItemModel {

    /**
     *
     */
    private static final long serialVersionUID = 6173694912988735850L;

    private Link leftLink;

    private Link centerLink;

    private Link rightLink;

    public NavbarDropdownUserBodyModel() {
        super(NavbarDropdownUserBodyMarkup.class);
    }

    public Link getLeftLink() {
        return leftLink;
    }

    public void setLeftLink(Link leftLink) {
        this.leftLink = leftLink;
    }

    public Link getCenterLink() {
        return centerLink;
    }

    public void setCenterLink(Link centerLink) {
        this.centerLink = centerLink;
    }

    public Link getRightLink() {
        return rightLink;
    }

    public void setRightLink(Link rightLink) {
        this.rightLink = rightLink;
    }

}
