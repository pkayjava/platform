package com.angkorteam.pki.master.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Navbar implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6891513677239118375L;

    private Branding brand;

    private List<Object> items = new ArrayList<Object>();

    public void addItem(NavbarMenu menu) {
        this.items.add(menu);
    }

    public void addItem(NavbarMenuItem menuItem) {
        this.items.add(menuItem);
    }

    public List<Object> getItems() {
        return Collections.unmodifiableList(this.items);
    }

}
