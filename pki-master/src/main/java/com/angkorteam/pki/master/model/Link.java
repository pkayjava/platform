package com.angkorteam.pki.master.model;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.io.Serializable;

public class Link implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -278695642669722573L;

    private Class<? extends WebPage> page;

    private PageParameters parameters;

    private String text;

    public Link(String text, Class<? extends WebPage> page, PageParameters parameters) {
        super();
        this.page = page;
        this.parameters = parameters;
        this.text = text;
    }

    public Class<? extends WebPage> getPage() {
        return page;
    }

    public void setPage(Class<? extends WebPage> page) {
        this.page = page;
    }

    public PageParameters getParameters() {
        return parameters;
    }

    public void setParameters(PageParameters parameters) {
        this.parameters = parameters;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
