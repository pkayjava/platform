package com.angkorteam.pki.master.model;

import java.io.Serializable;

public class NavbarDropdownHeader implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3441189954351780227L;

    private String text;

    public NavbarDropdownHeader(String text) {
        super();
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
