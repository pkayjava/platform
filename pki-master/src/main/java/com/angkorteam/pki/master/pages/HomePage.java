package com.angkorteam.pki.master.pages;

import com.angkorteam.core.webui.*;
import com.angkorteam.pki.master.Page;
import com.angkorteam.pki.master.item.*;
import com.angkorteam.pki.master.model.*;

public class HomePage extends Page {

    @Override
    protected void initData() {
        Branding branding = new Branding();
        BrandingText brandingText = new BrandingText();
        brandingText.setSkinText("LTE");
        brandingText.setBoldText("Admin");
        branding.setLarge(brandingText);
        setBranding(branding);

        Navbar navbar = new Navbar();
        {
            NavbarMenu menu = new NavbarMenu(new Icon(Emoji.fa_envelope_o));
            menu.setBadge(new Badge("4", BadgeType.Success));
            navbar.addItem(menu);
            NavbarDropdown dropdown = new NavbarDropdown();
            menu.setDropdown(dropdown);
            dropdown.setHeader(new NavbarDropdownHeader("You have 4 messages"));
            dropdown.setFooter(new NavbarDropdownFooter("See All Messages"));
            {
                NavbarDropdownMessageModel model = new NavbarDropdownMessageModel(HomePage.class, null);
                model.setImage(ReferenceUtilities
                        .loadResourceReference(ReferenceUtilities.AdminLTE + "/dist/img/user2-160x160.jpg"));
                model.setH4("Support Team");
                model.setSmallIcon(Emoji.fa_clock_o);
                model.setSmallText("5 mins");
                model.setParagraph("Why not buy a new awesome theme?");
                dropdown.addItem(model);
            }
            {
                NavbarDropdownMessageModel model = new NavbarDropdownMessageModel(HomePage.class, null);
                model.setImage(ReferenceUtilities
                        .loadResourceReference(ReferenceUtilities.AdminLTE + "/dist/img/user3-128x128.jpg"));
                model.setH4("AdminLTE Design Team");
                model.setSmallIcon(Emoji.fa_clock_o);
                model.setSmallText("2 hours");
                model.setParagraph("Why not buy a new awesome theme?");
                dropdown.addItem(model);
            }
            {
                NavbarDropdownMessageModel model = new NavbarDropdownMessageModel(HomePage.class, null);
                model.setImage(ReferenceUtilities
                        .loadResourceReference(ReferenceUtilities.AdminLTE + "/dist/img/user4-128x128.jpg"));
                model.setH4("Developers");
                model.setSmallIcon(Emoji.fa_clock_o);
                model.setSmallText("Today");
                model.setParagraph("Why not buy a new awesome theme?");
                dropdown.addItem(model);
            }
            {
                NavbarDropdownMessageModel model = new NavbarDropdownMessageModel(HomePage.class, null);
                model.setImage(ReferenceUtilities
                        .loadResourceReference(ReferenceUtilities.AdminLTE + "/dist/img/user3-128x128.jpg"));
                model.setH4("Sales Department");
                model.setSmallIcon(Emoji.fa_clock_o);
                model.setSmallText("Yesterday");
                model.setParagraph("Why not buy a new awesome theme?");
                dropdown.addItem(model);
            }
            {
                NavbarDropdownMessageModel model = new NavbarDropdownMessageModel(HomePage.class, null);
                model.setImage(ReferenceUtilities
                        .loadResourceReference(ReferenceUtilities.AdminLTE + "/dist/img/user4-128x128.jpg"));
                model.setH4("Reviewers");
                model.setSmallIcon(Emoji.fa_clock_o);
                model.setSmallText("2 Days");
                model.setParagraph("Why not buy a new awesome theme?");
                dropdown.addItem(model);
            }
        }
        {
            NavbarMenu menu = new NavbarMenu(new Icon(Emoji.fa_bell_o));
            menu.setBadge(new Badge("10", BadgeType.Warning));
            navbar.addItem(menu);
            NavbarDropdown dropdown = new NavbarDropdown();
            menu.setDropdown(dropdown);
            dropdown.setHeader(new NavbarDropdownHeader("You have 10 notifications"));
            dropdown.setFooter(new NavbarDropdownFooter("View all"));
            {
                NavbarDropdownNotificationModel model = new NavbarDropdownNotificationModel(HomePage.class, null);
                model.setIcon(new Icon(Emoji.fa_users, TextColor.Aqua));
                model.setText("5 new members joined today");
                dropdown.addItem(model);
            }
            {
                NavbarDropdownNotificationModel model = new NavbarDropdownNotificationModel(HomePage.class, null);
                model.setIcon(new Icon(Emoji.fa_warning, TextColor.Yellow));
                model.setText(
                        "Very long description here that may not fit into the page and may cause design problems");
                dropdown.addItem(model);
            }
            {
                NavbarDropdownNotificationModel model = new NavbarDropdownNotificationModel(HomePage.class, null);
                model.setIcon(new Icon(Emoji.fa_user, TextColor.Red));
                model.setText("5 new members joined");
                dropdown.addItem(model);
            }
            {
                NavbarDropdownNotificationModel model = new NavbarDropdownNotificationModel(HomePage.class, null);
                model.setIcon(new Icon(Emoji.fa_shopping_cart, TextColor.Green));
                model.setText("25 sales made");
                dropdown.addItem(model);
            }
            {
                NavbarDropdownNotificationModel model = new NavbarDropdownNotificationModel(HomePage.class, null);
                model.setIcon(new Icon(Emoji.fa_user, TextColor.Red));
                model.setText("You changed your username");
                dropdown.addItem(model);
            }
        }
        {
            NavbarMenu menu = new NavbarMenu(new Icon(Emoji.fa_flag_o));
            menu.setBadge(new Badge("9", BadgeType.Danger));
            navbar.addItem(menu);
            NavbarDropdown dropdown = new NavbarDropdown();
            menu.setDropdown(dropdown);
            dropdown.setHeader(new NavbarDropdownHeader("You have 9 tasks"));
            dropdown.setFooter(new NavbarDropdownFooter("View all tasks"));
            {
                NavbarDropdownTaskModel model = new NavbarDropdownTaskModel(HomePage.class, null);
                model.setH3("Design some buttons");
                model.setProgressBarColor(ProgressBarColor.Aqua);
                model.setProgressValue(20);
                dropdown.addItem(model);
            }
            {
                NavbarDropdownTaskModel model = new NavbarDropdownTaskModel(HomePage.class, null);
                model.setH3("Create a nice theme");
                model.setProgressBarColor(ProgressBarColor.Green);
                model.setProgressValue(40);
                dropdown.addItem(model);
            }
            {
                NavbarDropdownTaskModel model = new NavbarDropdownTaskModel(HomePage.class, null);
                model.setH3("Some task i need to do");
                model.setProgressBarColor(ProgressBarColor.Red);
                model.setProgressValue(60);
                dropdown.addItem(model);
            }
            {
                NavbarDropdownTaskModel model = new NavbarDropdownTaskModel(HomePage.class, null);
                model.setH3("Make beautiful transitions");
                model.setProgressBarColor(ProgressBarColor.Yellow);
                model.setProgressValue(80);
                dropdown.addItem(model);
            }
        }
        {
            NavbarMenu menu = new NavbarMenu(ReferenceUtilities
                    .loadResourceReference(ReferenceUtilities.AdminLTE + "/dist/img/user2-160x160.jpg"));
            menu.setText("Alexander Pierce");
            navbar.addItem(menu);
            NavbarUserDropdown dropdown = new NavbarUserDropdown();
            menu.setDropdown(dropdown);
            {
                NavbarDropdownUserHeaderModel model = new NavbarDropdownUserHeaderModel(ReferenceUtilities
                        .loadResourceReference(ReferenceUtilities.AdminLTE + "/dist/img/user2-160x160.jpg"));
                model.setParagraph("Alexander Pierce - Web Developer");
                model.setSmallText("Member since Nov. 2012");
                dropdown.addItem(model);
            }
            {
                NavbarDropdownUserBodyModel model = new NavbarDropdownUserBodyModel();
                model.setLeftLink(new Link("Followers", HomePage.class, null));
                model.setCenterLink(new Link("Sales", HomePage.class, null));
                model.setRightLink(new Link("Friends", HomePage.class, null));
                dropdown.addItem(model);
            }
            {
                NavbarDropdownUserFooterModel model = new NavbarDropdownUserFooterModel();
                model.setLeftLink(new Link("Profile", HomePage.class, null));
                model.setRightLink(new Link("Logout", HomePage.class, null));
                dropdown.addItem(model);
            }
        }
        setNavbar(navbar);
    }

    @Override
    protected void initComponent() {
    }

    @Override
    protected void configureMetaData() {
    }

}
