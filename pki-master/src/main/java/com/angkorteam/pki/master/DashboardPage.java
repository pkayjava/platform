package com.angkorteam.pki.master;

import com.angkorteam.core.webui.AdminLTEOption;
import com.angkorteam.core.webui.Emoji;
import com.angkorteam.core.webui.ReferenceUtilities;
import com.angkorteam.core.webui.models.*;
import com.angkorteam.pki.master.model.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.ExternalImage;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.EmptyPanel;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by socheatkhauv on 6/16/17.
 */
public abstract class DashboardPage extends WebPage {

    /**
     *
     */
    private static final long serialVersionUID = -7990394497716007617L;

    private String pageTitle;

    private Branding branding;

    private Sidebar sidebar;

    private Navbar navbar;

    private Kickbar kickbar;

//    private PageHeaderPanel pageHeaderPanel;
//
//    private PageBreadcrumbPanel pageBreadcrumbPanel;
//
//    private NavBarMenuPanel navBarMenuPanel;
//
//    private UserInfoPanel userInfoPanel;
//
//    private UserSearchPanel userSearchPanel;
//
//    private SideMenuPanel sideMenuPanel;
//
//    private PageLogoPanel pageLogoPanel;
//
//    private PageFooterPanel pageFooterPanel;
//
//    private WebMarkupContainer mainSidebar;
//
//    private WebMarkupContainer toogleButton;
//
//    protected FeedbackPanel feedbackPanel;
//
//    private boolean leftSideBar;

    public DashboardPage() {
    }

    public DashboardPage(IModel<?> model) {
        super(model);
    }

    public DashboardPage(PageParameters parameters) {
        super(parameters);
    }

    private void initBranding() {
        WebMarkupContainer fragmentBranding = null;
        if (getBranding() == null) {
            fragmentBranding = new EmptyPanel("brandingWidget");
            fragmentBranding.setVisible(false);
        } else {
            fragmentBranding = new Fragment("brandingWidget", "fragmentBranding", this);
            WebMarkupContainer linkPage = null;
            if (getBranding().getLinkPage() != null) {
                linkPage = new BookmarkablePageLink<>("linkPage", getBranding().getLinkPage(),
                        getBranding().getParameters());
            } else {
                linkPage = new WebMarkupContainer("linkPage");
                linkPage.add(AttributeModifier.replace("href", "#"));
            }
            fragmentBranding.add(linkPage);

            Label miniBold = new Label("miniBold", new PropertyModel<>(this, "branding.mini.boldText"));
            linkPage.add(miniBold);
            Label mini = new Label("mini", new PropertyModel<>(this, "branding.mini.skinText"));
            linkPage.add(mini);

            Label largeBold = new Label("largeBold", new PropertyModel<>(this, "branding.large.boldText"));
            linkPage.add(largeBold);
            Label large = new Label("large", new PropertyModel<>(this, "branding.large.skinText"));
            linkPage.add(large);
        }
        add(fragmentBranding);
    }

    private void initHamburger() {
        WebMarkupContainer fragmentHamburger = null;
        if (getSidebar() == null) {
            fragmentHamburger = new WebMarkupContainer("hamburgerWidget");
            fragmentHamburger.setVisible(false);
        } else {
            fragmentHamburger = new Fragment("hamburgerWidget", "fragmentHamburger", this);
        }
        add(fragmentHamburger);
    }

    private void initNavbarMenuItem(RepeatingView navbarRoot, NavbarMenuItem navbarMenuItem) {
        String itemId = navbarRoot.newChildId();
        Fragment fragmentNavbarMenuItem = new Fragment(itemId, "fragmentNavbarMenuItem", this);
        navbarRoot.add(fragmentNavbarMenuItem);

        BookmarkablePageLink<Void> link = new BookmarkablePageLink<>("link", navbarMenuItem.getPage(),
                navbarMenuItem.getParameters());
        fragmentNavbarMenuItem.add(link);

        Component icon = new WebMarkupContainer("icon");
        if (navbarMenuItem.getIcon() != null) {
            List<String> clazz = new ArrayList<>(4);
            if (navbarMenuItem.getIcon().getEmoji() != null) {
                if (navbarMenuItem.getIcon().getEmoji().getType() == Emoji.FA) {
                    clazz.add("fa");
                } else if (navbarMenuItem.getIcon().getEmoji().getType() == Emoji.ION) {
                    clazz.add("ion");
                }
                clazz.add(navbarMenuItem.getIcon().getEmoji().getLiteral());
            }
            if (navbarMenuItem.getIcon().getColor() != null) {
                clazz.add(navbarMenuItem.getIcon().getColor().getLiteral());
            }
            if (!clazz.isEmpty()) {
                icon.add(AttributeModifier.replace("class", StringUtils.join(clazz, " ")));
            }
        } else {
            icon.setVisible(false);
        }
        link.add(icon);

        Component text = null;
        if (navbarMenuItem.getText() != null) {
            text = new Label("text", navbarMenuItem.getText());
        } else {
            text = new WebMarkupContainer("text");
            text.setVisible(false);
        }
        link.add(text);

        Component image = null;
        if (navbarMenuItem.getImage() != null) {
            image = new ExternalImage("image", urlFor(navbarMenuItem.getImage(), null).toString());
        } else {
            image = new WebMarkupContainer("image");
            image.setVisible(false);
        }
        link.add(image);

        Component badge = null;
        if (navbarMenuItem.getBadge() != null) {
            badge = new Label("badge", navbarMenuItem.getBadge().getText());
            List<String> clazz = new ArrayList<>(2);
            clazz.add("label");
            if (navbarMenuItem.getBadge().getType() != null) {
                clazz.add(navbarMenuItem.getBadge().getType().getLiteral());
            }
            if (!clazz.isEmpty()) {
                badge.add(AttributeModifier.replace("class", StringUtils.join(clazz, " ")));
            }
        } else {
            badge = new WebMarkupContainer("badge");
            badge.setVisible(false);
        }
        link.add(badge);
    }

    private void initNavbarMenu(RepeatingView navbarRoot, NavbarMenu navbarMenu) {
        String itemId = navbarRoot.newChildId();
        Fragment fragmentNavbarMenu = new Fragment(itemId, "fragmentNavbarMenu", this);
        navbarRoot.add(fragmentNavbarMenu);

        Component icon = new WebMarkupContainer("icon");
        if (navbarMenu.getIcon() != null) {
            List<String> clazz = new ArrayList<>(4);
            if (navbarMenu.getIcon().getEmoji() != null) {
                if (navbarMenu.getIcon().getEmoji().getType() == Emoji.FA) {
                    clazz.add("fa");
                } else if (navbarMenu.getIcon().getEmoji().getType() == Emoji.ION) {
                    clazz.add("ion");
                }
                clazz.add(navbarMenu.getIcon().getEmoji().getLiteral());
            }
            if (navbarMenu.getIcon().getColor() != null) {
                clazz.add(navbarMenu.getIcon().getColor().getLiteral());
            }
            if (!clazz.isEmpty()) {
                icon.add(AttributeModifier.replace("class", StringUtils.join(clazz, " ")));
            }
        } else {
            icon.setVisible(false);
        }
        fragmentNavbarMenu.add(icon);

        Component text = null;
        if (navbarMenu.getText() != null) {
            text = new Label("text", navbarMenu.getText());
        } else {
            text = new WebMarkupContainer("text");
            text.setVisible(false);
        }
        fragmentNavbarMenu.add(text);

        Component image = null;
        if (navbarMenu.getImage() != null) {
            image = new ExternalImage("image", urlFor(navbarMenu.getImage(), null).toString());
        } else {
            image = new WebMarkupContainer("image");
            image.setVisible(false);
        }
        fragmentNavbarMenu.add(image);

        Component badge = null;
        if (navbarMenu.getBadge() != null) {
            badge = new Label("badge", navbarMenu.getBadge().getText());
            List<String> clazz = new ArrayList<>(2);
            clazz.add("label");
            if (navbarMenu.getBadge().getType() != null) {
                clazz.add(navbarMenu.getBadge().getType().getLiteral());
            }
            if (!clazz.isEmpty()) {
                badge.add(AttributeModifier.replace("class", StringUtils.join(clazz, " ")));
            }
        } else {
            badge = new WebMarkupContainer("badge");
            badge.setVisible(false);
        }
        fragmentNavbarMenu.add(badge);

        WebMarkupContainer fragmentDropdown = null;
        if (navbarMenu.getDropdown() != null) {
            if (navbarMenu.getDropdown() instanceof NavbarDropdown) {
                NavbarDropdown dropdown = (NavbarDropdown) navbarMenu.getDropdown();
                fragmentDropdown = new Fragment("dropdown", "fragmentNavbarDropdown", this);
                // fragmentNavbarDropdownHeader
                WebMarkupContainer fragmentNavbarDropdownHeader = null;
                if (dropdown.getHeader() == null) {
                    fragmentNavbarDropdownHeader = new WebMarkupContainer("header");
                    fragmentNavbarDropdownHeader.setVisible(false);
                } else {
                    fragmentNavbarDropdownHeader = new Fragment("header", "fragmentNavbarDropdownHeader", this);
                    Label headerText = new Label("text", new PropertyModel<>(navbarMenu, "dropdown.header.text"));
                    fragmentNavbarDropdownHeader.add(headerText);
                }
                fragmentDropdown.add(fragmentNavbarDropdownHeader);

                WebMarkupContainer body = null;
                if (dropdown.getItems() == null || dropdown.getItems().isEmpty()) {
                    body = new WebMarkupContainer("body");
                    body.setVisible(false);
                } else {
                    body = new Fragment("body", "fragmentNavbarDropdownBody", this);
                    RepeatingView items = new RepeatingView("items");
                    body.add(items);
                    for (NavbarDropdownItem item : dropdown.getItems()) {
                        String childId = items.newChildId();
                        NavbarDropdownItemMarkup markup = null;
                        try {
                            markup = item.getMarkup().getDeclaredConstructor(String.class, IModel.class)
                                    .newInstance(childId, new PropertyModel<>(item, "model"));
                        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                                | InvocationTargetException | NoSuchMethodException | SecurityException e) {
                            throw new WicketRuntimeException(e);
                        }
                        items.add(markup);
                    }
                }
                fragmentDropdown.add(body);

                WebMarkupContainer fragmentNavbarDropdownFooter = null;
                if (dropdown.getFooter() == null) {
                    fragmentNavbarDropdownFooter = new WebMarkupContainer("footer");
                    fragmentNavbarDropdownFooter.setVisible(false);
                } else {
                    fragmentNavbarDropdownFooter = new Fragment("footer", "fragmentNavbarDropdownFooter", this);
                    WebMarkupContainer link = null;
                    if (dropdown.getFooter().getPage() != null) {
                        link = new BookmarkablePageLink<>("link", dropdown.getFooter().getPage(),
                                dropdown.getFooter().getParameters());
                    } else {
                        link = new WebMarkupContainer("link");
                        link.add(AttributeModifier.replace("href", "#"));
                    }
                    fragmentNavbarDropdownFooter.add(link);
                    link.add(new Label("text", new PropertyModel<>(navbarMenu, "dropdown.footer.text")));
                }
                fragmentDropdown.add(fragmentNavbarDropdownFooter);
            } else if (navbarMenu.getDropdown() instanceof NavbarUserDropdown) {
                NavbarUserDropdown dropdown = (NavbarUserDropdown) navbarMenu.getDropdown();
                fragmentDropdown = new Fragment("dropdown", "fragmentNavbarUserDropdown", this);

                WebMarkupContainer body = null;
                if (dropdown.getItems() == null || dropdown.getItems().isEmpty()) {
                    body = new WebMarkupContainer("body");
                    body.setVisible(false);
                } else {
                    body = new Fragment("body", "fragmentNavbarUserDropdownBody", this);
                    RepeatingView items = new RepeatingView("items");
                    body.add(items);
                    for (NavbarDropdownItem item : dropdown.getItems()) {
                        String childId = items.newChildId();
                        NavbarDropdownItemMarkup markup = null;
                        try {
                            markup = item.getMarkup().getDeclaredConstructor(String.class, IModel.class)
                                    .newInstance(childId, new PropertyModel<>(item, "model"));
                        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                                | InvocationTargetException | NoSuchMethodException | SecurityException e) {
                            throw new WicketRuntimeException(e);
                        }
                        items.add(markup);
                    }
                }
                fragmentDropdown.add(body);
            }
        } else {
            fragmentDropdown = new WebMarkupContainer("dropdown");
            fragmentDropdown.setVisible(false);
        }
        fragmentNavbarMenu.add(fragmentDropdown);
    }

    private void initNavbar() {

        WebMarkupContainer fragmentNavbar = null;

        if ((getNavbar() == null || getNavbar().getItems() == null || getNavbar().getItems().isEmpty())
                && getKickbar() == null) {
            fragmentNavbar = new WebMarkupContainer("navbarWidget");
            fragmentNavbar.setVisible(false);
        } else {
            fragmentNavbar = new Fragment("navbarWidget", "fragmentNavbar", this);
            RepeatingView navbarRoot = new RepeatingView("navbarRoot");
            fragmentNavbar.add(navbarRoot);
            if (getNavbar() == null || getNavbar().getItems() == null || getNavbar().getItems().isEmpty()) {
                for (Object item : getNavbar().getItems()) {
                    if (item instanceof NavbarMenuItem) {
                        initNavbarMenuItem(navbarRoot, (NavbarMenuItem) item);
                    } else if (item instanceof NavbarMenu) {
                        initNavbarMenu(navbarRoot, (NavbarMenu) item);
                    }
                }
            }
            if (getSidebar() != null) {
                String itemId = navbarRoot.newChildId();
                Fragment fragmentNavbarSidebarItem = new Fragment(itemId, "fragmentNavbarSidebarItem", this);
                navbarRoot.add(fragmentNavbarSidebarItem);
            }
        }
        add(fragmentNavbar);
    }

    @Override
    protected void onInitialize() {
        initData();

        super.onInitialize();

        Label pageTitleLabel = new Label("pageTitleLabel", new PropertyModel<>(this, "pageTitle"));
        add(pageTitleLabel);

        initBranding();

        initHamburger();

        initNavbar();

//        this.feedbackPanel = new FeedbackPanel("feedbackPanel", this::report);
//        this.add(this.feedbackPanel);
//        this.feedbackPanel.setOutputMarkupId(true);
//
//        IModel<PageHeader> pageHeader = buildPageHeader();
//        this.pageHeaderPanel = new PageHeaderPanel("pageHeaderPanel", pageHeader);
//        this.add(this.pageHeaderPanel);
//
//        this.pageBreadcrumbPanel = new PageBreadcrumbPanel("pageBreadcrumbPanel", buildPageBreadcrumb());
//        this.add(this.pageBreadcrumbPanel);
//
//        this.navBarMenuPanel = new NavBarMenuPanel("navBarMenuPanel", buildNavBarMenu());
//        this.add(this.navBarMenuPanel);
//
//        this.mainSidebar = new WebMarkupContainer("mainSidebar");
//        this.add(this.mainSidebar);
//
//        IModel<UserInfo> userInfo = buildUserInfo();
//        this.userInfoPanel = new UserInfoPanel("userInfoPanel", userInfo);
//        this.mainSidebar.add(this.userInfoPanel);
//
//        IModel<Boolean> searchForm = hasSearchForm();
//        this.userSearchPanel = new UserSearchPanel("userSearchPanel", this::onSearchClick, searchForm);
//        this.mainSidebar.add(this.userSearchPanel);
//
//        IModel<List<SideMenu>> sideMenu = buildSideMenu();
//        this.sideMenuPanel = new SideMenuPanel("sideMenuPanel", sideMenu);
//        this.mainSidebar.add(this.sideMenuPanel);
//
//        this.pageLogoPanel = new PageLogoPanel("pageLogoPanel", buildPageLogo());
//        this.add(this.pageLogoPanel);
//        this.pageFooterPanel = new PageFooterPanel("pageFooterPanel", buildPageFooter());
//        this.add(this.pageFooterPanel);
//
//        Label pageTitle = new Label("pageTitle",
//                () -> pageHeader == null || pageHeader.getObject() == null || pageHeader.getObject().getTitle() == null
//                        || (pageHeader.getObject().getTitle() == null || "".equals(pageHeader.getObject().getTitle()))
//                                ? String.valueOf(LocalDateTime.now().getYear())
//                                : pageHeader.getObject().getTitle());
//        this.add(pageTitle);
//
//        if ((userInfo == null || userInfo.getObject() == null)
//                && (searchForm == null || searchForm.getObject() == null || !searchForm.getObject())
//                && (sideMenu == null || sideMenu.getObject() == null) || sideMenu.getObject().isEmpty()) {
//            this.leftSideBar = false;
//        } else {
//            this.leftSideBar = true;
//        }
//        this.mainSidebar.setVisible(this.leftSideBar);
//
//        this.toogleButton = new WebMarkupContainer("toogleButton");
//        this.add(this.toogleButton);
//        this.toogleButton.setVisible(this.leftSideBar);

//        initComponent();
//        configureMetaData();
    }

    protected abstract void initData();

    protected abstract void initComponent();

    protected abstract void configureMetaData();

    private boolean report(FeedbackMessage feedbackMessage) {
        if (feedbackMessage.getReporter() instanceof org.apache.wicket.Page) {
            return true;
        }
        return false;
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);
        response.render(CssHeaderItem.forCSS(".dropdown-menu {  z-index: 100060 !important; }", "menu"));

//        if (!this.leftSideBar) {
//            response.render(
//                    CssHeaderItem.forCSS(".content-wrapper {  margin-left:0px !important; }", "content-wrapper"));
//            response.render(CssHeaderItem.forCSS(".main-footer {  margin-left:0px !important; }", "main-footer"));
//        }
        AdminLTEOption option = new AdminLTEOption();
        option.Bootstrap = true;
        option.FontAwesome = true;
        option.Ionicons = true;
        option.AdminLTE = true;
        option.HTML5ShimAndRespondJS = true;
        option.GoogleFont = true;
        option.JQuery = true;
        ReferenceUtilities.render(option, response);
    }

    protected abstract IModel<PageLogo> buildPageLogo();

    protected abstract IModel<PageFooter> buildPageFooter();

    protected abstract IModel<PageHeader> buildPageHeader();

    protected abstract IModel<List<PageBreadcrumb>> buildPageBreadcrumb();

    protected abstract IModel<List<SideMenu>> buildSideMenu();

    protected abstract IModel<List<NavBarMenu>> buildNavBarMenu();

    protected abstract IModel<UserInfo> buildUserInfo();

    protected abstract IModel<Boolean> hasSearchForm();

    protected abstract void onSearchClick(String searchValue);

    public String getPageTitle() {
        return pageTitle;
    }

    public void setPageTitle(String pageTitle) {
        this.pageTitle = pageTitle;
    }

    public Branding getBranding() {
        return branding;
    }

    public void setBranding(Branding branding) {
        this.branding = branding;
    }

    public Sidebar getSidebar() {
        return sidebar;
    }

    public void setSidebar(Sidebar sidebar) {
        this.sidebar = sidebar;
    }

    public Navbar getNavbar() {
        return navbar;
    }

    public void setNavbar(Navbar navbar) {
        this.navbar = navbar;
    }

    public Kickbar getKickbar() {
        return kickbar;
    }

    public void setKickbar(Kickbar kickbar) {
        this.kickbar = kickbar;
    }

}
