DELIMITER ;;
CREATE PROCEDURE test_proc(
    IN in_attr_text VARCHAR(255),
    IN in_attr_boolean BOOLEAN,
    IN in_attr_date DATE,
    IN in_attr_time TIME,
    IN in_attr_datetime DATETIME,
    IN in_attr_integer INT(11),
    IN in_attr_float DECIMAL(15,4),
    OUT out_attr_text VARCHAR(255),
    OUT out_attr_boolean BOOLEAN,
    OUT out_attr_date DATE,
    OUT out_attr_time TIME,
    OUT out_attr_datetime DATETIME,
    OUT out_attr_integer INT(11),
    OUT out_attr_float DECIMAL(15,4),
    INOUT inout_attr_text VARCHAR(255),
    INOUT inout_attr_boolean BOOLEAN,
    INOUT inout_attr_date DATE,
    INOUT inout_attr_time TIME,
    INOUT inout_attr_datetime DATETIME,
    INOUT inout_attr_integer INT(11),
    INOUT inout_attr_float DECIMAL(15,4))
 BEGIN

SELECT attr_text FROM tbl_hello LIMIT 0,1;
SELECT attr_boolean FROM tbl_hello LIMIT 0,1;
SELECT attr_date FROM tbl_hello LIMIT 0,1;
SELECT attr_time FROM tbl_hello LIMIT 0,1;
SELECT attr_datetime FROM tbl_hello LIMIT 0,1;
SELECT attr_integer FROM tbl_hello LIMIT 0,1;
SELECT attr_float FROM tbl_hello LIMIT 0,1;
SELECT * FROM tbl_hello LIMIT 0,1;

SELECT attr_text FROM tbl_hello;
SELECT attr_boolean FROM tbl_hello;
SELECT attr_date FROM tbl_hello;
SELECT attr_time FROM tbl_hello;
SELECT attr_datetime FROM tbl_hello;
SELECT attr_integer FROM tbl_hello;
SELECT attr_float FROM tbl_hello;
SELECT * FROM tbl_hello;

 END ;;
DELIMITER ;

insert into tbl_proc(proc_id,name) values(UUID(),'test_proc');

insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'in_attr_text', 'IN', 'Text', '', 11 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'in_attr_boolean', 'IN', 'Boolean', '', 12 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'in_attr_date', 'IN', 'Date', '', 13 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'in_attr_time', 'IN', 'Time', '', 14 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'in_attr_datetime', 'IN', 'DateTime', '', 15 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'in_attr_integer', 'IN', 'Integer', '', 16 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'in_attr_float', 'IN', 'Float', '', 17 );

insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'out_attr_text', 'OUT', 'Text', '', 21 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'out_attr_boolean', 'OUT', 'Boolean', '', 22 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'out_attr_date', 'OUT', 'Date', '', 23 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'out_attr_time', 'OUT', 'Time', '', 24 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'out_attr_datetime', 'OUT', 'DateTime', '', 25 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'out_attr_integer', 'OUT', 'Integer', '', 26 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'out_attr_float', 'OUT', 'Float', '', 27 );

insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'inout_attr_text', 'INOUT', 'Text', '', 31 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'inout_attr_boolean', 'INOUT', 'Boolean', '', 32 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'inout_attr_date', 'INOUT', 'Date', '', 33 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'inout_attr_time', 'INOUT', 'Time', '', 34 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'inout_attr_datetime', 'INOUT', 'DateTime', '', 35 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'inout_attr_integer', 'INOUT', 'Integer', '', 36 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'inout_attr_float', 'INOUT', 'Float', '', 37 );

insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'select_attr_text', 'SELECT', 'Text', '', 41 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'select_attr_boolean', 'SELECT', 'Boolean', '', 42 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'select_attr_date', 'SELECT', 'Date', '', 43 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'select_attr_time', 'SELECT', 'Time', '', 44 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'select_attr_datetime', 'SELECT', 'DateTime', '', 45 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'select_attr_integer', 'SELECT', 'Integer', '', 46 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'select_attr_float', 'SELECT', 'Float', '', 47 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'select_attr_map', 'SELECT', 'Map', '', 48 );

insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'select_list_text', 'SELECT', 'List', 'Text', 51 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'select_list_boolean', 'SELECT', 'List', 'Boolean', 52 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'select_list_date', 'SELECT', 'List', 'Date', 53 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'select_list_time', 'SELECT', 'List', 'Time', 54 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'select_list_datetime', 'SELECT', 'List', 'DateTime', 55 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'select_list_integer', 'SELECT', 'List', 'Integer', 56 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'select_list_float', 'SELECT', 'List', 'Float', 57 );
insert into tbl_proc_param(proc_param_id, proc_id, name, direction, type, sub_type, param_order) values(uuid(), (select proc_id from tbl_proc where name = 'test_proc'), 'select_list_map', 'SELECT', 'List', 'Map', 58 );

