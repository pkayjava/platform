CREATE TABLE tbl_table (
	table_id   VARCHAR(100) NOT NULL,
	name       VARCHAR(200) NOT NULL,
	PRIMARY KEY (table_id)
);

CREATE UNIQUE INDEX tbl_table_001 ON tbl_table(name);

CREATE TABLE tbl_column (
	column_id  VARCHAR(100) NOT NULL,
	table_id   VARCHAR(100) NOT NULL,
	name       VARCHAR(200) NOT NULL,
	type       VARCHAR(10)  NOT NULL,
	PRIMARY KEY (column_id)
);

CREATE UNIQUE INDEX tbl_column_001 ON tbl_column(table_id, name);
CREATE INDEX tbl_column_002 ON tbl_column(type);

CREATE TABLE tbl_query (
    query_id   VARCHAR(100) NOT NULL,
	table_id   VARCHAR(100) NOT NULL,
	name       VARCHAR(200) NOT NULL,
	PRIMARY KEY (query_id)
);

CREATE UNIQUE INDEX tbl_query_001 ON tbl_query(table_id, name);

CREATE TABLE tbl_query_param (
    query_param_id VARCHAR(100) NOT NULL,
    query_id       VARCHAR(100) NOT NULL,
	name           VARCHAR(200) NOT NULL,
	type           VARCHAR(10)  NOT NULL,
	PRIMARY KEY (query_param_id)
);

CREATE TABLE tbl_proc (
    proc_id     VARCHAR(100) NOT NULL,
	name        VARCHAR(200) NOT NULL,
	PRIMARY KEY (proc_id)
);

CREATE UNIQUE INDEX tbl_proc_001 ON tbl_proc(name);

CREATE TABLE tbl_proc_param (
    proc_param_id  VARCHAR(100) NOT NULL,
    proc_id        VARCHAR(100) NOT NULL,
	name           VARCHAR(200) NOT NULL,
	direction      VARCHAR(10)  NOT NULL,
	type           VARCHAR(10)  NULL,
	sub_type       VARCHAR(10)  NULL,
	param_order    INT(11)  NOT NULL,
	PRIMARY KEY (proc_param_id)
);

CREATE UNIQUE INDEX tbl_proc_param_001 ON tbl_proc_param(proc_id, name);

