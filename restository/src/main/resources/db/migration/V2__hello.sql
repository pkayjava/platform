CREATE TABLE tbl_hello (
	hello_id      VARCHAR(100) NOT NULL,
	attr_text     VARCHAR(255) NULL,
	attr_boolean  BOOLEAN NULL,
	attr_datetime DATETIME NULL,
	attr_date     DATE NULL,
	attr_time     TIME NULL,
	attr_integer  INT(11) NULL,
	attr_float    DECIMAL(15,4) NULL,
	PRIMARY KEY (hello_id)
);

CREATE INDEX tbl_hello_001 ON tbl_hello(attr_text);
CREATE INDEX tbl_hello_002 ON tbl_hello(attr_boolean);
CREATE INDEX tbl_hello_003 ON tbl_hello(attr_datetime);
CREATE INDEX tbl_hello_004 ON tbl_hello(attr_date);
CREATE INDEX tbl_hello_005 ON tbl_hello(attr_time);
CREATE INDEX tbl_hello_006 ON tbl_hello(attr_integer);
CREATE INDEX tbl_hello_007 ON tbl_hello(attr_float);

INSERT INTO tbl_table(table_id, name) values(UUID(), 'hello');

INSERT INTO tbl_column(column_id, table_id, name, type) values(UUID(), (select table_id from tbl_table where name = 'hello'), 'hello_id', 'Text');
INSERT INTO tbl_column(column_id, table_id, name, type) values(UUID(), (select table_id from tbl_table where name = 'hello'), 'attr_text', 'Text');
INSERT INTO tbl_column(column_id, table_id, name, type) values(UUID(), (select table_id from tbl_table where name = 'hello'), 'attr_boolean', 'Boolean');
INSERT INTO tbl_column(column_id, table_id, name, type) values(UUID(), (select table_id from tbl_table where name = 'hello'), 'attr_datetime', 'DateTime');
INSERT INTO tbl_column(column_id, table_id, name, type) values(UUID(), (select table_id from tbl_table where name = 'hello'), 'attr_date', 'Date');
INSERT INTO tbl_column(column_id, table_id, name, type) values(UUID(), (select table_id from tbl_table where name = 'hello'), 'attr_time', 'Time');
INSERT INTO tbl_column(column_id, table_id, name, type) values(UUID(), (select table_id from tbl_table where name = 'hello'), 'attr_integer', 'Integer');
INSERT INTO tbl_column(column_id, table_id, name, type) values(UUID(), (select table_id from tbl_table where name = 'hello'), 'attr_float', 'Float');

insert into tbl_hello(hello_id, attr_text, attr_boolean, attr_datetime, attr_date, attr_time, attr_integer, attr_float) values(uuid(), 'hello1', true, now(), now(), now(), 1, 1.10);
insert into tbl_hello(hello_id, attr_text, attr_boolean, attr_datetime, attr_date, attr_time, attr_integer, attr_float) values(uuid(), 'hello2', true, now(), now(), now(), 1, 1.10);
insert into tbl_hello(hello_id, attr_text, attr_boolean, attr_datetime, attr_date, attr_time, attr_integer, attr_float) values(uuid(), 'hello3', true, now(), now(), now(), 1, 1.10);
