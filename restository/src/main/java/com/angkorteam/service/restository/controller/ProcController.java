package com.angkorteam.service.restository.controller;

import com.angkorteam.core.query.SelectQuery;
import com.angkorteam.core.query.SortType;
import com.angkorteam.core.spring.jdbc.*;
import com.angkorteam.core.spring.jdbc.ColumnMapRowMapper;
import com.angkorteam.service.restository.consts.Type;
import com.angkorteam.service.restository.ddl.Column;
import com.angkorteam.service.restository.ddl.Proc;
import com.angkorteam.service.restository.ddl.ProcParam;
import com.angkorteam.service.restository.gson.SqlDateTypeAdapter;
import com.angkorteam.service.restository.gson.SqlTimeTypeAdapter;
import com.angkorteam.service.restository.gson.SqlTimestampTypeAdapter;
import com.angkorteam.service.restository.response.ProcResponseBody;
import org.springframework.jdbc.core.*;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.ParseException;
import java.util.*;

@RestController
@RequestMapping(path = "/proc")
public class ProcController {

    private final DataSource dataSource;

    public ProcController(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @RequestMapping(path = "/{proc}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public @ResponseBody
    ProcResponseBody proc(@PathVariable(name = "proc") String proc, @RequestBody Map<String, Object> params) throws ParseException {

        JdbcNamed named = new JdbcNamed(this.dataSource, new ColumnMapRowMapper(true));

        Map<String, Map<String, Object>> procParams = lookupParam(named, proc);

        Map<String, Object> jdbcCallParams = new HashMap<>();

        List<SqlParameter> jdbcProcParams = new ArrayList<>();

        for (Map<String, Object> procParam : procParams.values()) {
            String name = (String) procParam.get(ProcParam.Field.NAME);
            String direction = (String) procParam.get(ProcParam.Field.DIRECTION);
            String type = (String) procParam.get(ProcParam.Field.TYPE);
            String subType = (String) procParam.get(ProcParam.Field.SUB_TYPE);
            int jdbcType = 0;
            if (Type.TEXT.equalsIgnoreCase(type)) {
                jdbcType = Types.VARCHAR;
            } else if (Type.BOOLEAN.equalsIgnoreCase(type)) {
                jdbcType = Types.BOOLEAN;
            } else if (Type.INTEGER.equalsIgnoreCase(type)) {
                jdbcType = Types.INTEGER;
            } else if (Type.FLOAT.equalsIgnoreCase(type)) {
                jdbcType = Types.DOUBLE;
            } else if (Type.DATE.equalsIgnoreCase(type)) {
                jdbcType = Types.DATE;
            } else if (Type.DATE_TIME.equalsIgnoreCase(type)) {
                jdbcType = Types.TIMESTAMP;
            } else if (Type.TIME.equalsIgnoreCase(type)) {
                jdbcType = Types.TIME;
            }

            if ("IN".equalsIgnoreCase(direction)) {
                jdbcProcParams.add(new SqlParameter(name, jdbcType));
            } else if ("OUT".equalsIgnoreCase(direction)) {
                jdbcProcParams.add(new SqlOutParameter(name, jdbcType));
            } else if ("INOUT".equalsIgnoreCase(direction)) {
                jdbcProcParams.add(new SqlInOutParameter(name, jdbcType));
            }

            if ("IN".equalsIgnoreCase(direction) || "INOUT".equalsIgnoreCase(direction)) {
                if (Type.TEXT.equalsIgnoreCase(type)) {
                    jdbcCallParams.put(name, (String) params.get(name));
                } else if (Type.BOOLEAN.equalsIgnoreCase(type)) {
                    jdbcCallParams.put(name, (boolean) params.get(name));
                } else if (Type.INTEGER.equalsIgnoreCase(type)) {
                    jdbcCallParams.put(name, (long) (double) params.get(name));
                } else if (Type.FLOAT.equalsIgnoreCase(type)) {
                    jdbcCallParams.put(name, (double) params.get(name));
                } else if (Type.DATE.equalsIgnoreCase(type)) {
                    jdbcCallParams.put(name, SqlDateTypeAdapter.parse((String) params.get(name)));
                } else if (Type.DATE_TIME.equalsIgnoreCase(type)) {
                    jdbcCallParams.put(name, SqlTimestampTypeAdapter.parse((String) params.get(name)));
                } else if (Type.TIME.equalsIgnoreCase(type)) {
                    jdbcCallParams.put(name, SqlTimeTypeAdapter.parse((String) params.get(name)));
                }
            }

            if ("SELECT".equalsIgnoreCase(direction)) {
                if (Type.TEXT.equalsIgnoreCase(type)) {
                    jdbcProcParams.add(new SqlReturnResultSet(name, new StringResultSetExtractor()));
                } else if (Type.BOOLEAN.equalsIgnoreCase(type)) {
                    jdbcProcParams.add(new SqlReturnResultSet(name, new BooleanResultSetExtractor()));
                } else if (Type.INTEGER.equalsIgnoreCase(type)) {
                    jdbcProcParams.add(new SqlReturnResultSet(name, new LongResultSetExtractor()));
                } else if (Type.FLOAT.equalsIgnoreCase(type)) {
                    jdbcProcParams.add(new SqlReturnResultSet(name, new DoubleResultSetExtractor()));
                } else if (Type.DATE.equalsIgnoreCase(type)) {
                    jdbcProcParams.add(new SqlReturnResultSet(name, new DateResultSetExtractor()));
                } else if (Type.DATE_TIME.equalsIgnoreCase(type)) {
                    jdbcProcParams.add(new SqlReturnResultSet(name, new TimestampResultSetExtractor()));
                } else if (Type.TIME.equalsIgnoreCase(type)) {
                    jdbcProcParams.add(new SqlReturnResultSet(name, new TimeResultSetExtractor()));
                } else if ("Map".equalsIgnoreCase(type)) {
                    jdbcProcParams.add(new SqlReturnResultSet(name, new MapResultSetExtractor()));
                } else if ("List".equalsIgnoreCase(type)) {
                    if (Type.TEXT.equalsIgnoreCase(subType)) {
                        jdbcProcParams.add(new SqlReturnResultSet(name, new SingleColumnRowMapper<>(String.class)));
                    } else if (Type.BOOLEAN.equalsIgnoreCase(subType)) {
                        jdbcProcParams.add(new SqlReturnResultSet(name, new SingleColumnRowMapper<>(Boolean.class)));
                    } else if (Type.INTEGER.equalsIgnoreCase(subType)) {
                        jdbcProcParams.add(new SqlReturnResultSet(name, new SingleColumnRowMapper<>(Long.class)));
                    } else if (Type.FLOAT.equalsIgnoreCase(subType)) {
                        jdbcProcParams.add(new SqlReturnResultSet(name, new SingleColumnRowMapper<>(Double.class)));
                    } else if (Type.DATE.equalsIgnoreCase(subType)) {
                        jdbcProcParams.add(new SqlReturnResultSet(name, new SingleColumnRowMapper<>(Date.class)));
                    } else if (Type.DATE_TIME.equalsIgnoreCase(subType)) {
                        jdbcProcParams.add(new SqlReturnResultSet(name, new SingleColumnRowMapper<>(Timestamp.class)));
                    } else if (Type.TIME.equalsIgnoreCase(subType)) {
                        jdbcProcParams.add(new SqlReturnResultSet(name, new SingleColumnRowMapper<>(Time.class)));
                    } else if ("Map".equalsIgnoreCase(subType)) {
                        jdbcProcParams.add(new SqlReturnResultSet(name, new ColumnMapRowMapper(true)));
                    }
                }
            }
        }

        String uuid = "UPDATE" + UUID.randomUUID().toString();

        jdbcProcParams.add(new SqlReturnUpdateCount(uuid));

        SimpleJdbcCall jdbcCall = new SimpleJdbcCall(dataSource);
        jdbcCall.setAccessCallParameterMetaData(false);
        jdbcCall.declareParameters(jdbcProcParams.toArray(new SqlParameter[jdbcProcParams.size()]));
        jdbcCall.withProcedureName(proc);

        Map<String, Object> result = jdbcCall.execute(jdbcCallParams);

        ProcResponseBody response = new ProcResponseBody();

        for (Map.Entry<String, Object> item : result.entrySet()) {
            String key = item.getKey();
            if (uuid.equals(key)) {
                response.setUpdateCount((int) item.getValue());
            }
            Map<String, Object> procParam = procParams.get(key);
            if (procParam != null) {
                String direction = (String) procParam.get(ProcParam.Field.DIRECTION);
                if ("OUT".equalsIgnoreCase(direction) || "INOUT".equalsIgnoreCase(direction)) {
                    response.getParams().put(item.getKey(), item.getValue());
                } else if ("SELECT".equalsIgnoreCase(direction)) {
                    response.getSelectResult().put(item.getKey(), item.getValue());
                }
            }
        }

        return response;
    }

    protected Map<String, Map<String, Object>> lookupParam(JdbcNamed named, String proc) {

        SelectQuery selectQuery = null;
        selectQuery = new SelectQuery(Proc.NAME);
        selectQuery.addField(Proc.Field.ID);
        selectQuery.addWhere(Proc.Field.NAME + " = :" + Proc.Field.NAME, proc);

        Map<String, Object> procObject = named.queryForMap(selectQuery.toSQL(), selectQuery.getParam());

        selectQuery = new SelectQuery(ProcParam.NAME);
        selectQuery.addField(ProcParam.Field.ID);
        selectQuery.addField(ProcParam.Field.NAME);
        selectQuery.addField(ProcParam.Field.DIRECTION);
        selectQuery.addField(ProcParam.Field.PARAM_ORDER);
        selectQuery.addField(ProcParam.Field.PROC_ID);
        selectQuery.addField(ProcParam.Field.TYPE);
        selectQuery.addField(ProcParam.Field.SUB_TYPE);
        selectQuery.addOrderBy(ProcParam.Field.PARAM_ORDER, SortType.Asc);
        selectQuery.addWhere(ProcParam.Field.PROC_ID + " = :" + ProcParam.Field.PROC_ID, procObject.get(Proc.Field.ID));
        Map<String, Map<String, Object>> params = new LinkedHashMap<>();
        for (Map<String, Object> paramObject : named.queryForList(selectQuery.toSQL(), selectQuery.getParam())) {
            params.put((String) paramObject.get(Column.Field.NAME), paramObject);
        }
        return params;
    }

}
