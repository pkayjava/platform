package com.angkorteam.service.restository.gson;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;

public class SqlTimestampTypeAdapter extends TypeAdapter<Timestamp> {

    public static final TypeAdapterFactory FACTORY = new TypeAdapterFactory() {
        @Override
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
            return type.getRawType() == Timestamp.class ? (TypeAdapter<T>) new SqlTimestampTypeAdapter() : null;
        }
    };

    public SqlTimestampTypeAdapter() {
    }

    public synchronized void write(JsonWriter out, Timestamp value) throws IOException {
        if (value == null) {
            out.nullValue();
        } else {
            out.value(format(value));
        }
    }

    public Timestamp read(JsonReader in) throws IOException {
        if (in.peek() == JsonToken.NULL) {
            in.nextNull();
            return null;
        } else {
            try {
                return parse(in.nextString());
            } catch (ParseException e) {
                throw new IOException(e);
            }
        }
    }

    public static String format(Timestamp value) {
        return DateFormatUtils.ISO_8601_EXTENDED_DATETIME_FORMAT.format(value);
    }

    public static Timestamp parse(String value) throws ParseException {
        return new Timestamp(DateFormatUtils.ISO_8601_EXTENDED_DATETIME_FORMAT.parse(value).getTime());
    }

}
