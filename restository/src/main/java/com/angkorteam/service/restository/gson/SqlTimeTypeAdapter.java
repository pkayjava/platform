package com.angkorteam.service.restository.gson;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.io.IOException;
import java.sql.Time;
import java.text.ParseException;

public class SqlTimeTypeAdapter extends TypeAdapter<Time> {

    public static final TypeAdapterFactory FACTORY = new TypeAdapterFactory() {
        @Override
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
            return type.getRawType() == Time.class ? (TypeAdapter<T>) new SqlTimeTypeAdapter() : null;
        }
    };

    public SqlTimeTypeAdapter() {
    }

    public synchronized void write(JsonWriter out, Time value) throws IOException {
        if (value == null) {
            out.nullValue();
        } else {
            out.value(format(value));
        }
    }

    public Time read(JsonReader in) throws IOException {
        if (in.peek() == JsonToken.NULL) {
            in.nextNull();
            return null;
        } else {
            try {
                return parse(in.nextString());
            } catch (ParseException e) {
                throw new IOException(e);
            }
        }
    }

    public static String format(Time value) {
        return DateFormatUtils.ISO_8601_EXTENDED_TIME_FORMAT.format(value);
    }

    public static Time parse(String value) throws ParseException {
        return new Time(DateFormatUtils.ISO_8601_EXTENDED_TIME_FORMAT.parse(value).getTime());
    }

}
