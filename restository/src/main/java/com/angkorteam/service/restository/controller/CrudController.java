package com.angkorteam.service.restository.controller;

import com.angkorteam.core.query.DeleteQuery;
import com.angkorteam.core.query.InsertQuery;
import com.angkorteam.core.query.SelectQuery;
import com.angkorteam.core.query.UpdateQuery;
import com.angkorteam.core.spring.jdbc.ColumnMapRowMapper;
import com.angkorteam.core.spring.jdbc.JdbcNamed;
import com.angkorteam.service.restository.consts.Type;
import com.angkorteam.service.restository.ddl.Column;
import com.angkorteam.service.restository.ddl.Table;
import com.angkorteam.service.restository.error.BadRequest;
import com.angkorteam.service.restository.error.Conflict;
import com.angkorteam.service.restository.error.NotFound;
import com.angkorteam.service.restository.gson.SqlDateTypeAdapter;
import com.angkorteam.service.restository.gson.SqlTimeTypeAdapter;
import com.angkorteam.service.restository.gson.SqlTimestampTypeAdapter;
import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;
import java.text.ParseException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/crud")
public class CrudController {

    private final DataSource dataSource;

    public CrudController(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @RequestMapping(path = "/{table}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public @ResponseBody
    Map<String, Object> create(@PathVariable(name = "table") String table, @RequestBody Map<String, Object> body) throws ParseException {

        String pkField = table + "_id";

        JdbcNamed named = new JdbcNamed(this.dataSource, new ColumnMapRowMapper(true));

        String id = null;

        if (body.containsKey(pkField)) {
            if (countRecord(named, "tbl_" + table, pkField, Collections.singletonList((String) body.get(pkField))) > 0) {
                throw new Conflict();
            }
            id = (String) body.get(pkField);
        } else {
            id = lookupUUID(named);
            body.put(pkField, id);
        }

        Map<String, String> column = lookupColumn(named, "tbl_" + table);

        InsertQuery insertQuery = new InsertQuery("tbl_" + table);

        for (Map.Entry<String, Object> i : body.entrySet()) {
            if (i.getValue() != null) {
                if (Type.TEXT.equalsIgnoreCase(column.get(i.getKey()))) {
                    insertQuery.addValue(i.getKey() + " = :" + i.getKey(), (String) i.getValue());
                } else if (Type.DATE.equalsIgnoreCase(column.get(i.getKey()))) {
                    insertQuery.addValue(i.getKey() + " = :" + i.getKey(), SqlDateTypeAdapter.parse((String) i.getValue()));
                } else if (Type.DATE_TIME.equalsIgnoreCase(column.get(i.getKey()))) {
                    insertQuery.addValue(i.getKey() + " = :" + i.getKey(), SqlTimestampTypeAdapter.parse((String) i.getValue()));
                } else if (Type.TIME.equalsIgnoreCase(column.get(i.getKey()))) {
                    insertQuery.addValue(i.getKey() + " = :" + i.getKey(), SqlTimeTypeAdapter.parse((String) i.getValue()));
                } else if (Type.FLOAT.equalsIgnoreCase(column.get(i.getKey()))) {
                    insertQuery.addValue(i.getKey() + " = :" + i.getKey(), (double) i.getValue());
                } else if (Type.INTEGER.equalsIgnoreCase(column.get(i.getKey()))) {
                    insertQuery.addValue(i.getKey() + " = :" + i.getKey(), (long) (double) i.getValue());
                } else if (Type.BOOLEAN.equalsIgnoreCase(column.get(i.getKey()))) {
                    insertQuery.addValue(i.getKey() + " = :" + i.getKey(), (boolean) i.getValue());
                } else {
                    throw new BadRequest();
                }
            } else {
                insertQuery.addValue(i.getKey() + " = :" + i.getKey(), null);
            }
        }
        named.update(insertQuery.toSQL(), insertQuery.getParam());

        SelectQuery selectQuery = new SelectQuery("tbl_" + table);
        selectQuery.addWhere(pkField + " = :" + pkField, id);

        return named.queryForMap(selectQuery.toSQL(), selectQuery.getParam());
    }

    @RequestMapping(path = "/{table}/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Map<String, Object> retrieve(@PathVariable(name = "table") String table, @PathVariable(name = "id") String id) {
        String pkField = table + "_id";

        JdbcNamed named = new JdbcNamed(this.dataSource, new ColumnMapRowMapper(true));

        Map<String, String> column = lookupColumn(named, "tbl_" + table);

        SelectQuery selectQuery = null;

        selectQuery = new SelectQuery("tbl_" + table);
        for (String name : column.keySet()) {
            selectQuery.addField(name);
        }

        selectQuery.addWhere(pkField + " = :pkField", id);

        Map<String, Object> documentObject = named.queryForMap(selectQuery.toSQL(), selectQuery.getParam());

        if (documentObject == null) {
            throw new NotFound();
        }

        return documentObject;
    }

    @RequestMapping(path = "/{table}/{id}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    public @ResponseBody
    Map<String, Object> update(@PathVariable(name = "table") String table, @PathVariable(name = "id") String id, @RequestBody Map<String, Object> body) throws ParseException {
        String pkField = table + "_id";

        JdbcNamed named = new JdbcNamed(this.dataSource, new ColumnMapRowMapper(true));

        Map<String, String> column = lookupColumn(named, "tbl_" + table);

        UpdateQuery updateQuery = null;

        updateQuery = new UpdateQuery("tbl_" + table);

        for (Map.Entry<String, Object> i : body.entrySet()) {
            if (i.getValue() != null) {
                if (Type.TEXT.equalsIgnoreCase(column.get(i.getKey()))) {
                    updateQuery.addValue(i.getKey() + " = :" + i.getKey(), (String) i.getValue());
                } else if (Type.DATE.equalsIgnoreCase(column.get(i.getKey()))) {
                    updateQuery.addValue(i.getKey() + " = :" + i.getKey(), SqlDateTypeAdapter.parse((String) i.getValue()));
                } else if (Type.DATE_TIME.equalsIgnoreCase(column.get(i.getKey()))) {
                    updateQuery.addValue(i.getKey() + " = :" + i.getKey(), SqlTimestampTypeAdapter.parse((String) i.getValue()));
                } else if (Type.TIME.equalsIgnoreCase(column.get(i.getKey()))) {
                    updateQuery.addValue(i.getKey() + " = :" + i.getKey(), SqlTimeTypeAdapter.parse((String) i.getValue()));
                } else if (Type.FLOAT.equalsIgnoreCase(column.get(i.getKey()))) {
                    updateQuery.addValue(i.getKey() + " = :" + i.getKey(), (double) i.getValue());
                } else if (Type.INTEGER.equalsIgnoreCase(column.get(i.getKey()))) {
                    updateQuery.addValue(i.getKey() + " = :" + i.getKey(), (long) (double) i.getValue());
                } else if (Type.BOOLEAN.equalsIgnoreCase(column.get(i.getKey()))) {
                    updateQuery.addValue(i.getKey() + " = :" + i.getKey(), (boolean) i.getValue());
                } else {
                    throw new BadRequest();
                }
            } else {
                updateQuery.addValue(i.getKey() + " = :" + i.getKey(), null);
            }
        }

        updateQuery.addWhere(pkField + " = :" + pkField, id);

        named.update(updateQuery.toSQL(), updateQuery.getParam());

        SelectQuery selectQuery = new SelectQuery("tbl_" + table);
        selectQuery.addWhere(pkField + " = :" + pkField, id);

        return named.queryForMap(selectQuery.toSQL(), selectQuery.getParam());
    }

    @RequestMapping(path = "/{table}/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public
    @ResponseBody
    Map<String, Object> delete(@PathVariable(name = "table") String table, @PathVariable(name = "id") String id) {
        String pkField = table + "_id";

        JdbcNamed named = new JdbcNamed(this.dataSource, new ColumnMapRowMapper(true));

        SelectQuery selectQuery = new SelectQuery("tbl_" + table);
        selectQuery.addWhere(pkField + " = :" + pkField, id);

        Map<String, Object> record = named.queryForMap(selectQuery.toSQL(), selectQuery.getParam());
        if (record == null) {
            throw new NotFound();
        }

        DeleteQuery deleteQuery = null;
        deleteQuery = new DeleteQuery("tbl_" + table);
        deleteQuery.addWhere(pkField + " = :pkField", id);

        named.update(deleteQuery.toSQL(), deleteQuery.getParam());

        return record;
    }


    protected Map<String, String> lookupColumn(JdbcNamed named, String table) {
        SelectQuery selectQuery = null;

        selectQuery = new SelectQuery(Table.NAME);
        selectQuery.addField(Table.Field.ID);
        selectQuery.addField(Table.Field.NAME);
        selectQuery.addWhere(Table.Field.NAME + " = :" + Table.Field.NAME, table.substring(4));

        Map<String, Object> tableObject = named.queryForMap(selectQuery.toSQL(), selectQuery.getParam());

        selectQuery = new SelectQuery(Column.NAME);
        selectQuery.addField(Column.Field.NAME);
        selectQuery.addField(Column.Field.TYPE);
        selectQuery.addWhere(Column.Field.TABLE_ID + " = :" + Column.Field.TABLE_ID, tableObject.get(Table.Field.ID));
        Map<String, String> column = new HashMap<>();
        for (Map<String, Object> columnObject : named.queryForList(selectQuery.toSQL(), selectQuery.getParam())) {
            column.put((String) columnObject.get(Column.Field.NAME), (String) columnObject.get(Column.Field.TYPE));
        }
        return column;
    }


    protected int countRecord(JdbcNamed named, String table, String pkField, List<String> ids) {
        SelectQuery selectQuery = new SelectQuery(table);
        selectQuery.addField("count(*)");
        selectQuery.addWhere(pkField + " IN (:" + pkField + ")", pkField, ids);
        Integer result = named.queryForObject(selectQuery.toSQL(), selectQuery.getParam(), int.class);
        return result == null ? 0 : result;
    }

    protected String lookupUUID(JdbcNamed named) {
        return named.queryForObject("SELECT UUID()", String.class);
    }

}
