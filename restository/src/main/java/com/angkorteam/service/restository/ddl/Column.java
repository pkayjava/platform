package com.angkorteam.service.restository.ddl;

public interface Column {

    String NAME = "tbl_column";

    interface Field {

        String ID = "column_id";

        String TABLE_ID = Table.Field.ID;

        String NAME = "name";

        String TYPE = "type";

    }

}
