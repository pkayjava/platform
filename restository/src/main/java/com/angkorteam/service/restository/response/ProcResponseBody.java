package com.angkorteam.service.restository.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ProcResponseBody implements Serializable {

    @Expose
    @SerializedName("params")
    private Map<String, Object> params = new HashMap<>();

    @Expose
    @SerializedName("updateCount")
    private int updateCount;

    @Expose
    @SerializedName("selectResult")
    private Map<String, Object> selectResult = new HashMap<>();

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public int getUpdateCount() {
        return updateCount;
    }

    public void setUpdateCount(int updateCount) {
        this.updateCount = updateCount;
    }

    public Map<String, Object> getSelectResult() {
        return selectResult;
    }

    public void setSelectResult(Map<String, Object> selectResult) {
        this.selectResult = selectResult;
    }
}
