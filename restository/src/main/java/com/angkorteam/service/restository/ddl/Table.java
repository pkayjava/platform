package com.angkorteam.service.restository.ddl;

public interface Table {

    String NAME = "tbl_table";

    interface Field {

        String ID = "table_id";

        String NAME = "name";

    }

}
