package com.angkorteam.service.restository.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class BulkRetrieveRequestBody implements Serializable {

    @Expose
    @SerializedName("field")
    private List<String> field;

    @Expose
    @SerializedName("groupBy")
    private List<String> groupBy;

    @Expose
    @SerializedName("orderBy")
    private List<String> orderBy;

    @Expose
    @SerializedName("where")
    private List<String> where;

    @Expose
    @SerializedName("having")
    private List<String> having;

    @Expose
    @SerializedName("offset")
    private Integer offset;

    @Expose
    @SerializedName("limit")
    private Integer limit;

    @Expose
    @SerializedName("params")
    private Map<String, Object> params;

    public List<String> getField() {
        return field;
    }

    public void setField(List<String> field) {
        this.field = field;
    }

    public List<String> getGroupBy() {
        return groupBy;
    }

    public void setGroupBy(List<String> groupBy) {
        this.groupBy = groupBy;
    }

    public List<String> getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(List<String> orderBy) {
        this.orderBy = orderBy;
    }

    public List<String> getWhere() {
        return where;
    }

    public void setWhere(List<String> where) {
        this.where = where;
    }

    public List<String> getHaving() {
        return having;
    }

    public void setHaving(List<String> having) {
        this.having = having;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }
}
