package com.angkorteam.service.restository.gson;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;

public class SqlDateTypeAdapter extends TypeAdapter<Date> {

    public static final TypeAdapterFactory FACTORY = new TypeAdapterFactory() {
        @Override
        public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
            return type.getRawType() == Date.class ? (TypeAdapter<T>) new SqlDateTypeAdapter() : null;
        }
    };

    public SqlDateTypeAdapter() {
    }

    public synchronized void write(JsonWriter out, Date value) throws IOException {
        if (value == null) {
            out.nullValue();
        } else {
            out.value(format(value));
        }
    }

    public Date read(JsonReader in) throws IOException {
        if (in.peek() == JsonToken.NULL) {
            in.nextNull();
            return null;
        } else {
            try {
                return parse(in.nextString());
            } catch (ParseException e) {
                throw new IOException(e);
            }
        }
    }

    public static String format(Date value) {
        return DateFormatUtils.ISO_8601_EXTENDED_DATE_FORMAT.format(value);
    }

    public static Date parse(String value) throws ParseException {
        return new Date(DateFormatUtils.ISO_8601_EXTENDED_DATE_FORMAT.parse(value).getTime());
    }

}
