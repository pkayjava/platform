package com.angkorteam.service.restository.controller;

import com.angkorteam.core.query.DeleteQuery;
import com.angkorteam.core.query.InsertQuery;
import com.angkorteam.core.query.SelectQuery;
import com.angkorteam.core.query.UpdateQuery;
import com.angkorteam.core.spring.jdbc.ColumnMapRowMapper;
import com.angkorteam.core.spring.jdbc.JdbcNamed;
import com.angkorteam.service.restository.consts.Type;
import com.angkorteam.service.restository.ddl.Column;
import com.angkorteam.service.restository.ddl.Table;
import com.angkorteam.service.restository.error.BadRequest;
import com.angkorteam.service.restository.error.Conflict;
import com.angkorteam.service.restository.gson.SqlDateTypeAdapter;
import com.angkorteam.service.restository.gson.SqlTimeTypeAdapter;
import com.angkorteam.service.restository.gson.SqlTimestampTypeAdapter;
import com.angkorteam.service.restository.request.BulkRetrieveRequestBody;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/bulk")
public class BulkController {

    private static final List<String> CRITERIA_OP = new ArrayList<>();

    static {
        CRITERIA_OP.add(" not");
        CRITERIA_OP.add(" like");
        CRITERIA_OP.add(" regexp");
        CRITERIA_OP.add(" rlike");
        CRITERIA_OP.add(" between");
        CRITERIA_OP.add(" div");
        CRITERIA_OP.add(" mod");
        CRITERIA_OP.add("<");
        CRITERIA_OP.add(">");
        CRITERIA_OP.add("+");
        CRITERIA_OP.add("-");
        CRITERIA_OP.add("*");
        CRITERIA_OP.add("/");
        CRITERIA_OP.add("!");
        CRITERIA_OP.add("=");
    }

    private final DataSource dataSource;

    public BulkController(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @RequestMapping(path = "/{table}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public @ResponseBody
    List<Map<String, Object>> create(@PathVariable(name = "table") String table, @RequestBody List<Map<String, Object>> bodies) throws ParseException {

        String pkField = table + "_id";

        JdbcNamed named = new JdbcNamed(this.dataSource, new ColumnMapRowMapper(true));

        List<String> ids = new ArrayList<>();
        for (Map<String, Object> body : bodies) {
            if (body.containsKey(pkField)) {
                String id = (String) body.get(pkField);
                if (ids.contains(id)) {
                    throw new BadRequest();
                }
                ids.add(id);
            } else {
                String id = lookupUUID(named);
                body.put(pkField, id);
                if (ids.contains(id)) {
                    throw new BadRequest();
                }
                ids.add(id);
            }
        }

        if (countRecord(named, "tbl_" + table, pkField, ids) > 0) {
            throw new Conflict();
        }

        Map<String, String> column = lookupColumn(named, "tbl_" + table);

        List<InsertQuery> insertQueries = new ArrayList<>();

        for (Map<String, Object> body : bodies) {
            InsertQuery insertQuery = new InsertQuery("tbl_" + table);

            for (Map.Entry<String, Object> i : body.entrySet()) {
                if (i.getValue() != null) {
                    if (Type.TEXT.equalsIgnoreCase(column.get(i.getKey()))) {
                        insertQuery.addValue(i.getKey() + " = :" + i.getKey(), (String) i.getValue());
                    } else if (Type.DATE.equalsIgnoreCase(column.get(i.getKey()))) {
                        insertQuery.addValue(i.getKey() + " = :" + i.getKey(), SqlDateTypeAdapter.parse((String) i.getValue()));
                    } else if (Type.DATE_TIME.equalsIgnoreCase(column.get(i.getKey()))) {
                        insertQuery.addValue(i.getKey() + " = :" + i.getKey(), SqlTimestampTypeAdapter.parse((String) i.getValue()));
                    } else if (Type.TIME.equalsIgnoreCase(column.get(i.getKey()))) {
                        insertQuery.addValue(i.getKey() + " = :" + i.getKey(), SqlTimeTypeAdapter.parse((String) i.getValue()));
                    } else if (Type.FLOAT.equalsIgnoreCase(column.get(i.getKey()))) {
                        insertQuery.addValue(i.getKey() + " = :" + i.getKey(), (double) i.getValue());
                    } else if (Type.INTEGER.equalsIgnoreCase(column.get(i.getKey()))) {
                        insertQuery.addValue(i.getKey() + " = :" + i.getKey(), (long) (double) i.getValue());
                    } else if (Type.BOOLEAN.equalsIgnoreCase(column.get(i.getKey()))) {
                        insertQuery.addValue(i.getKey() + " = :" + i.getKey(), (boolean) i.getValue());
                    } else {
                        throw new BadRequest();
                    }
                } else {
                    insertQuery.addValue(i.getKey() + " = :" + i.getKey(), null);
                }
            }
            insertQueries.add(insertQuery);
        }

        for (InsertQuery insertQuery : insertQueries) {
            named.update(insertQuery.toSQL(), insertQuery.getParam());
        }

        SelectQuery selectQuery = new SelectQuery("tbl_" + table);
        selectQuery.addWhere(pkField + " IN ( :" + pkField + ")", String.class, ids);

        return named.queryForList(selectQuery.toSQL(), selectQuery.getParam());
    }

    @RequestMapping(path = "/{table}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    public List<Map<String, Object>> update(@PathVariable(name = "table") String table, @RequestBody Map<String, Map<String, Object>> bodies) throws ParseException {
        String pkField = table + "_id";

        JdbcNamed named = new JdbcNamed(this.dataSource, new ColumnMapRowMapper(true));

        Map<String, String> column = lookupColumn(named, "tbl_" + table);

        List<UpdateQuery> updateQueries = new ArrayList<>();

        List<String> ids = new ArrayList<>();
        for (Map.Entry<String, Map<String, Object>> body : bodies.entrySet()) {
            ids.add(body.getKey());
        }

        int count = countRecord(named, "tbl_" + table, pkField, ids);
        if (count != ids.size()) {
            throw new BadRequest();
        }

        for (Map.Entry<String, Map<String, Object>> body : bodies.entrySet()) {
            UpdateQuery updateQuery = new UpdateQuery("tbl_" + table);
            for (Map.Entry<String, Object> i : body.getValue().entrySet()) {
                if (i.getValue() != null) {
                    if (Type.TEXT.equalsIgnoreCase(column.get(i.getKey()))) {
                        updateQuery.addValue(i.getKey() + " = :" + i.getKey(), (String) i.getValue());
                    } else if (Type.DATE.equalsIgnoreCase(column.get(i.getKey()))) {
                        updateQuery.addValue(i.getKey() + " = :" + i.getKey(), SqlDateTypeAdapter.parse((String) i.getValue()));
                    } else if (Type.DATE_TIME.equalsIgnoreCase(column.get(i.getKey()))) {
                        updateQuery.addValue(i.getKey() + " = :" + i.getKey(), SqlTimestampTypeAdapter.parse((String) i.getValue()));
                    } else if (Type.TIME.equalsIgnoreCase(column.get(i.getKey()))) {
                        updateQuery.addValue(i.getKey() + " = :" + i.getKey(), SqlTimeTypeAdapter.parse((String) i.getValue()));
                    } else if (Type.FLOAT.equalsIgnoreCase(column.get(i.getKey()))) {
                        updateQuery.addValue(i.getKey() + " = :" + i.getKey(), (double) i.getValue());
                    } else if (Type.INTEGER.equalsIgnoreCase(column.get(i.getKey()))) {
                        updateQuery.addValue(i.getKey() + " = :" + i.getKey(), (long) (double) i.getValue());
                    } else if (Type.BOOLEAN.equalsIgnoreCase(column.get(i.getKey()))) {
                        updateQuery.addValue(i.getKey() + " = :" + i.getKey(), (boolean) i.getValue());
                    } else {
                        throw new BadRequest();
                    }
                } else {
                    updateQuery.addValue(i.getKey() + " = :" + i.getKey(), null);
                }
            }
            updateQuery.addWhere(pkField + " = :" + pkField, body.getKey());
            updateQueries.add(updateQuery);
        }

        for (UpdateQuery updateQuery : updateQueries) {
            named.update(updateQuery.toSQL(), updateQuery.getParam());
        }

        SelectQuery selectQuery = new SelectQuery("tbl_" + table);
        selectQuery.addWhere(pkField + " IN ( :" + pkField + ")", String.class, ids);

        return named.queryForList(selectQuery.toSQL(), selectQuery.getParam());
    }

    @RequestMapping(path = "/{table}/delete", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public List<Map<String, Object>> delete(@PathVariable(name = "table") String table, @RequestBody List<String> ids) {
        String pkField = table + "_id";

        List<String> temp = new ArrayList<>();
        for (String id : ids) {
            if (temp.contains(id)) {
                throw new BadRequest();
            }
            temp.add(id);
        }

        JdbcNamed named = new JdbcNamed(this.dataSource, new ColumnMapRowMapper(true));

        SelectQuery selectQuery = new SelectQuery("tbl_" + table);
        selectQuery.addWhere(pkField + " IN ( :" + pkField + ")", String.class, ids);

        List<Map<String, Object>> records = named.queryForList(selectQuery.toSQL(), selectQuery.getParam());

        if (records.size() != ids.size()) {
            throw new BadRequest();
        }

        DeleteQuery deleteQuery = null;
        deleteQuery = new DeleteQuery("tbl_" + table);
        deleteQuery.addWhere(pkField + " IN (:" + pkField + ")", "pkField", ids);

        named.update(deleteQuery.toSQL(), deleteQuery.getParam());

        return records;
    }

    @RequestMapping(path = "/{table}/retrieve", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public @ResponseBody
    List<Map<String, Object>> retrieve(@PathVariable(name = "table") String table, @RequestBody BulkRetrieveRequestBody body) throws ParseException {

        JdbcNamed named = new JdbcNamed(this.dataSource, new ColumnMapRowMapper(true));

        Map<String, String> column = lookupColumn(named, "tbl_" + table);

        List<String> segments = new ArrayList<>();
        if (body.getWhere() != null) {
            segments.addAll(body.getWhere());
        }
        if (body.getHaving() != null) {
            segments.addAll(body.getHaving());
        }
        List<String> params = lookupParams(segments);

        if (!params.isEmpty() && (body.getParams() == null || body.getParams().isEmpty())) {
            throw new BadRequest();
        }

        for (String param : params) {
            if (!body.getParams().containsKey(param)) {
                throw new BadRequest();
            }
        }

        Map<String, String> types = new HashMap<>();
        if (!params.isEmpty()) {
            master:
            for (String param : params) {
                if (body.getWhere() != null && !body.getWhere().isEmpty()) {
                    for (String where : body.getWhere()) {
                        String type = lookupType(column, where, param);
                        types.put(param, type);
                        continue master;
                    }
                }
                if (body.getHaving() != null && !body.getHaving().isEmpty()) {
                    for (String having : body.getHaving()) {
                        String type = lookupType(column, having, param);
                        types.put(param, type);
                        continue master;
                    }
                }
            }
        }

        Map<String, Object> queryParams = new HashMap<>();
        if (body.getParams() != null && !body.getParams().isEmpty()) {
            for (Map.Entry<String, Object> entry : body.getParams().entrySet()) {
                if (entry.getValue() == null) {
                    queryParams.put(entry.getKey(), null);
                } else {
                    String type = types.get(entry.getKey());
                    if (Type.TEXT.equalsIgnoreCase(type)) {
                        queryParams.put(entry.getKey(), (String) entry.getValue());
                    } else if (Type.BOOLEAN.equalsIgnoreCase(type)) {
                        queryParams.put(entry.getKey(), (boolean) entry.getValue());
                    } else if (Type.DATE.equalsIgnoreCase(type)) {
                        queryParams.put(entry.getKey(), SqlDateTypeAdapter.parse((String) entry.getValue()));
                    } else if (Type.TIME.equalsIgnoreCase(type)) {
                        queryParams.put(entry.getKey(), SqlTimeTypeAdapter.parse((String) entry.getValue()));
                    } else if (Type.DATE_TIME.equalsIgnoreCase(type)) {
                        queryParams.put(entry.getKey(), SqlTimestampTypeAdapter.parse((String) entry.getValue()));
                    } else if (Type.FLOAT.equalsIgnoreCase(type)) {
                        queryParams.put(entry.getKey(), (double) entry.getValue());
                    } else if (Type.INTEGER.equalsIgnoreCase(type)) {
                        queryParams.put(entry.getKey(), (long) (double) entry.getValue());
                    } else {
                        throw new BadRequest();
                    }
                }
            }
        }

        StringBuffer buffer = new StringBuffer();

        buffer.append("SELECT");

        SelectQuery selectQuery = new SelectQuery("tbl_" + table);

        if (body.getField() != null) {
            buffer.append(" ").append(StringUtils.join(body.getField(), ", "));
        } else {
            buffer.append(" *");
        }

        buffer.append(" FROM tbl_" + table);

        if (body.getWhere() != null) {
            buffer.append(" WHERE ").append(StringUtils.join(body.getWhere(), " AND "));
        }

        if (body.getGroupBy() != null) {
            buffer.append(" GROUP BY ").append(StringUtils.join(body.getGroupBy(), ", "));
        }

        if (body.getHaving() != null) {
            buffer.append(" HAVING ").append(StringUtils.join(body.getHaving(), " AND "));
        }

        if (body.getOrderBy() != null) {
            buffer.append(" ORDER BY ").append(StringUtils.join(body.getOrderBy(), ", "));
        }

        int offset = 0;
        if (body.getOffset() != null && body.getOffset() >= 0) {
            offset = body.getOffset();
        }

        int limit = 100;
        if (body.getLimit() != null && body.getLimit() >= 1) {
            limit = body.getLimit();
        }

        buffer.append(" LIMIT " + offset + "," + limit);

        return named.queryForList(buffer.toString(), queryParams);
    }

    public List<String> lookupParams(List<String> segments) {
        List<String> params = new ArrayList<>();
        for (String segment : segments) {
            if (segment == null || "".equals(segment)) {
                throw new BadRequest();
            }
            for (int i = 0; i < segment.length(); i++) {
                if (segment.charAt(i) == ':') {
                    StringBuffer param = new StringBuffer();
                    for (int j = i + 1; j < segment.length(); j++) {
                        if (segment.charAt(j) == ',' || segment.charAt(j) == ' ' || segment.charAt(j) == ')') {
                            break;
                        } else {
                            param.append(segment.charAt(j));
                        }
                    }
                    String temp = param.toString();
                    if (!params.contains(temp)) {
                        params.add(temp);
                    }
                }
            }
        }
        return params;
    }

    protected String lookupType(Map<String, String> attrs, String criteria, String param) {
        int countMatches = StringUtils.countMatches(criteria, ":");
        if (countMatches == 0) {
            return null;
        } else {
            criteria = criteria.substring(0, criteria.indexOf(":" + param)).trim();
            for (String op : CRITERIA_OP) {
                int id = StringUtils.lastIndexOfIgnoreCase(criteria, op);
                if (id > 0) {
                    criteria = criteria.substring(0, id).trim();
                    String temps[] = criteria.split(" ");
                    String temp = null;
                    for (int i = temps.length - 1; i >= 0; i--) {
                        if (temps[i] != null && !"".equals(temps[i])) {
                            temp = temps[i];
                            break;
                        }
                    }
                    while (temp.startsWith("(")) {
                        temp = temp.substring(1);
                    }
                    temp = temp.trim();
                    return attrs.get(temp);
                }
            }
        }
        return null;
    }

    protected Map<String, String> lookupColumn(JdbcNamed named, String table) {
        SelectQuery selectQuery = null;

        selectQuery = new SelectQuery(Table.NAME);
        selectQuery.addField(Table.Field.ID);
        selectQuery.addField(Table.Field.NAME);
        selectQuery.addWhere(Table.Field.NAME + " = :" + Table.Field.NAME, table.substring(4));

        Map<String, Object> tableObject = named.queryForMap(selectQuery.toSQL(), selectQuery.getParam());

        selectQuery = new SelectQuery(Column.NAME);
        selectQuery.addField(Column.Field.NAME);
        selectQuery.addField(Column.Field.TYPE);
        selectQuery.addWhere(Column.Field.TABLE_ID + " = :" + Column.Field.TABLE_ID, tableObject.get(Table.Field.ID));
        Map<String, String> column = new HashMap<>();
        for (Map<String, Object> columnObject : named.queryForList(selectQuery.toSQL(), selectQuery.getParam())) {
            column.put((String) columnObject.get(Column.Field.NAME), (String) columnObject.get(Column.Field.TYPE));
        }
        return column;
    }


    protected int countRecord(JdbcNamed named, String table, String pkField, List<String> ids) {
        SelectQuery selectQuery = new SelectQuery(table);
        selectQuery.addField("count(*)");
        selectQuery.addWhere(pkField + " IN (:" + pkField + ")", pkField, ids);
        Integer result = named.queryForObject(selectQuery.toSQL(), selectQuery.getParam(), int.class);
        return result == null ? 0 : result;
    }

    protected String lookupUUID(JdbcNamed named) {
        return named.queryForObject("SELECT UUID()", String.class);
    }

}
