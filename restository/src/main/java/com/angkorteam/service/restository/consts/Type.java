package com.angkorteam.service.restository.consts;

public interface Type {

    String TEXT = "Text";

    String BOOLEAN = "Boolean";

    String DATE = "Date";

    String DATE_TIME = "DateTime";

    String TIME = "Time";

    String INTEGER = "Integer";

    String FLOAT = "Float";

}
