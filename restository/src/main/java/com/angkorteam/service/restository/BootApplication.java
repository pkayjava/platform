package com.angkorteam.service.restository;

import com.angkorteam.service.restository.boot.AppProperties;
import com.angkorteam.service.restository.gson.SqlDateTypeAdapter;
import com.angkorteam.service.restository.gson.SqlTimeTypeAdapter;
import com.angkorteam.service.restository.gson.SqlTimestampTypeAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class BootApplication {

    public static void main(String[] args) {
        SpringApplicationBuilder builder = new SpringApplicationBuilder(BootApplication.class);
        SpringApplication application = builder.build(args);
        ApplicationContext applicationContext = application.run(args);
    }

    @Bean
    public Gson createGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.setDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        builder.setPrettyPrinting();
        builder.excludeFieldsWithoutExposeAnnotation();
        builder.disableHtmlEscaping();
        builder.registerTypeAdapterFactory(SqlDateTypeAdapter.FACTORY);
        builder.registerTypeAdapterFactory(SqlTimeTypeAdapter.FACTORY);
        builder.registerTypeAdapterFactory(SqlTimestampTypeAdapter.FACTORY);
        return builder.create();
    }

}