package com.angkorteam.service.restository.ddl;

public interface ProcParam {

    String NAME = "tbl_proc_param";

    interface Field {

        String ID = "proc_param_id";

        String PROC_ID = Proc.Field.ID;

        String NAME = "name";

        String DIRECTION = "direction";

        String TYPE = "type";

        String SUB_TYPE = "sub_type";

        String PARAM_ORDER = "param_order";

    }

}
