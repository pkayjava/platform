package com.angkorteam.service.restository.ddl;

public interface Proc {

    String NAME = "tbl_proc";

    interface Field {

        String ID = "proc_id";

        String NAME = "name";

    }

}
