package com.angkorteam.core.webui.wicket.markup.html.form;

import com.angkorteam.core.query.SelectQuery;
import com.angkorteam.core.spring.jdbc.JdbcTemplate;
import com.angkorteam.core.webui.wicket.markup.html.form.select2.Option;
import org.apache.wicket.model.util.ListModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by socheatkhauv on 20/1/17.
 */
public class OptionListModel extends ListModel<Option> {

    /**
     *
     */
    private static final long serialVersionUID = -2868650190922370946L;

    private List<String> where = null;

    public OptionListModel(JdbcTemplate jdbcTemplate, String table, String idField, String textField) {
        this(jdbcTemplate, table, idField, textField, new ArrayList<>());
    }

    public OptionListModel(JdbcTemplate jdbcTemplate, String table, String idField, String textField,
                           List<String> where) {
        this.where = where;
        SelectQuery selectQuery = new SelectQuery(table);
        selectQuery.addField(idField + " AS id");
        selectQuery.addField(textField + " AS text");
        selectQuery.addOrderBy(textField + " ASC");
        if (this.where != null && !this.where.isEmpty()) {
            for (String wh : this.where) {
                selectQuery.addWhere(wh);
            }
        }
        List<Option> options = jdbcTemplate.queryForList(selectQuery.toSQL(), Option.class);
        setObject(options);
    }

}
