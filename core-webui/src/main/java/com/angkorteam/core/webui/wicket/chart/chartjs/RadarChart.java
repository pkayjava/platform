package com.angkorteam.core.webui.wicket.chart.chartjs;

import com.angkorteam.core.webui.ReferenceUtilities;
import com.google.gson.Gson;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebComponent;
import org.apache.wicket.markup.parser.XmlTag.TagType;
import org.apache.wicket.model.IModel;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class RadarChart extends WebComponent {

    /**
     *
     */
    private static final long serialVersionUID = 8927640931364707241L;

    private IModel<RadarDataset> dataset;

    public RadarChart(String id, IModel<RadarDataset> model) {
        super(id);
        setOutputMarkupId(true);
        this.dataset = model;
    }

    @Override
    protected void onComponentTag(final ComponentTag tag) {
        checkComponentTag(tag, "canvas");
        super.onComponentTag(tag);
        if (tag.isOpenClose()) {
            tag.setType(TagType.OPEN);
        }
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);

        ReferenceUtilities.renderJavascript(response, "<!-- ChartJS 1.0.2 -->");
        ReferenceUtilities.renderJavascript(response,
                ReferenceUtilities.AdminLTE + "/bower_components/chart.js/Chart.min.js");

        Gson gson = new Gson();

        String markupId = getMarkupId();
        String chart = String.format(
                "new Chart(document.getElementById('%s').getContext('2d')).Radar(%s, { responsive : true })", markupId,
                gson.toJson(this.dataset.getObject()));
        response.render(OnDomReadyHeaderItem.forScript(chart));
    }

    public static class RadarItem implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = 5582716545107944446L;

        private String pointHighlightStroke;

        private String pointHighlightFill;

        private String pointStrokeColor;

        private String pointColor;

        private String strokeColor;

        private String fillColor;

        private List<Integer> data = new LinkedList<>();

        public List<Integer> getData() {
            return data;
        }

        public String getPointHighlightStroke() {
            return pointHighlightStroke;
        }

        public void setPointHighlightStroke(String pointHighlightStroke) {
            this.pointHighlightStroke = pointHighlightStroke;
        }

        public String getPointHighlightFill() {
            return pointHighlightFill;
        }

        public void setPointHighlightFill(String pointHighlightFill) {
            this.pointHighlightFill = pointHighlightFill;
        }

        public String getPointStrokeColor() {
            return pointStrokeColor;
        }

        public void setPointStrokeColor(String pointStrokeColor) {
            this.pointStrokeColor = pointStrokeColor;
        }

        public String getPointColor() {
            return pointColor;
        }

        public void setPointColor(String pointColor) {
            this.pointColor = pointColor;
        }

        public String getStrokeColor() {
            return strokeColor;
        }

        public void setStrokeColor(String strokeColor) {
            this.strokeColor = strokeColor;
        }

        public String getFillColor() {
            return fillColor;
        }

        public void setFillColor(String fillColor) {
            this.fillColor = fillColor;
        }

    }

    public static class RadarDataset implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = 5107795266394949273L;

        private List<String> labels = new LinkedList<>();

        private List<RadarItem> datasets = new LinkedList<>();

        public List<String> getLabels() {
            return labels;
        }

        public List<RadarItem> getDatasets() {
            return datasets;
        }

    }

}