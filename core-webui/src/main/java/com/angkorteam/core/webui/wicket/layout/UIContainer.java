package com.angkorteam.core.webui.wicket.layout;

import com.angkorteam.core.webui.wicket.widget.TextFeedbackPanel;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.FormComponent;

public class UIContainer extends WebMarkupContainer {

    /**
     *
     */
    private static final long serialVersionUID = 2599850482317800532L;

    protected UIContainer(final String id) {
        super(id);
        setOutputMarkupId(true);
    }

    public TextFeedbackPanel newFeedback(String id, FormComponent<?> formComponent) {
        TextFeedbackPanel feedback = new TextFeedbackPanel(id, formComponent);
        add(feedback);
        return feedback;
    }

}