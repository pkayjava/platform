package com.angkorteam.core.webui.wicket.markup.html.form;

import com.angkorteam.core.webui.ReferenceUtilities;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.resource.CssResourceReference;
import org.apache.wicket.request.resource.ResourceReference;

import java.util.Date;

/**
 * Created by socheat on 6/1/16.
 */
public class DateTextField extends org.apache.wicket.extensions.markup.html.form.DateTextField {

    /**
     *
     */
    private static final long serialVersionUID = 3349883874886789180L;

    public static final ResourceReference CSS = new CssResourceReference(Form.class, "datepicker.css");

    private static final String JAVA_PARTTERN = "dd/MM/yyyy";
    private static final String JAVASCRIPT_PARTTERN = "dd/mm/yyyy";

    public static final String REMOVE_POPUP_UP_SCRIPT = "$('.datepicker').each(function() { if ( $(this).hasClass('dropdown-menu') && $(this).hasClass('datepicker-dropdown')) {$(this).remove(); }});";

    public DateTextField(String id, IModel<Date> model) {
        super(id, model, JAVA_PARTTERN);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        setOutputMarkupId(true);
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);
        String markupId = this.getMarkupId(true);
        response.render(CssHeaderItem.forReference(DateTextField.CSS));
        response.render(CssHeaderItem
                .forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.DATE_PICKER_CSS)));
        response.render(JavaScriptHeaderItem
                .forReference(getApplication().getJavaScriptLibrarySettings().getJQueryReference()));
        response.render(JavaScriptHeaderItem
                .forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.BOOTSTRAP_JS)));
        response.render(JavaScriptHeaderItem
                .forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.DATE_PICKER_JS)));
        response.render(OnDomReadyHeaderItem.forScript(
                "$('#" + markupId + "').datepicker({ todayBtn:'linked', todayHighlight:true, autoclose:true, format:'"
                        + JAVASCRIPT_PARTTERN + "', weekStart:1 })"));
    }
}
