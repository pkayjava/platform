package com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.filter;

import com.angkorteam.core.webui.wicket.functional.WicketBiFunction;
import com.angkorteam.core.webui.wicket.functional.WicketTriConsumer;
import com.angkorteam.core.webui.wicket.markup.html.form.DateTextField;
import com.angkorteam.core.webui.wicket.markup.html.form.select2.Select2SingleChoice;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheat on 12/7/16.
 */
public class ItemTextLink<T> extends Panel {

    /**
     *
     */
    private static final long serialVersionUID = 566391360952711919L;

    public ItemTextLink(String id, IModel<T> rowModel, IModel<?> model, WicketBiFunction<String, T, ItemCss> itemCss,
                        WicketTriConsumer<String, T, AjaxRequestTarget> itemClick, String identity) {
        super(id);

        AjaxLink<T> link = new AjaxLink<T>("link", rowModel) {
            /**
             *
             */
            private static final long serialVersionUID = -8413623235924109139L;

            @Override
            public void onClick(AjaxRequestTarget target) {
                target.appendJavaScript(Select2SingleChoice.REMOVE_POPUP_UP_SCRIPT);
                target.appendJavaScript(DateTextField.REMOVE_POPUP_UP_SCRIPT);
                if (itemClick != null) {
                    itemClick.accept(identity, rowModel.getObject(), target);
                }
            }
        };
        add(link);

        Label text = new Label("text", model);
        text.setRenderBodyOnly(true);
        link.add(text);

        ItemCss htmlLambdaCss = itemCss.apply(identity, rowModel.getObject());
        if (htmlLambdaCss == ItemCss.SUCCESS) {
            link.add(AttributeModifier.replace("class", "btn-xs btn-success"));
        } else if (htmlLambdaCss == ItemCss.PRIMARY) {
            link.add(AttributeModifier.replace("class", "btn-xs btn-primary"));
        } else if (htmlLambdaCss == ItemCss.WARNING) {
            link.add(AttributeModifier.replace("class", "btn-xs btn-warning"));
        } else if (htmlLambdaCss == ItemCss.DANGER) {
            link.add(AttributeModifier.replace("class", "btn-xs btn-danger"));
        } else if (htmlLambdaCss == ItemCss.INFO) {
            link.add(AttributeModifier.replace("class", "btn-xs btn-info"));
        }
    }

    public ItemTextLink(String id, IModel<T> rowModel, IModel<?> model, ItemCss itemCss,
                        WicketTriConsumer<String, T, AjaxRequestTarget> itemClick, String identity) {
        super(id);

        AjaxLink<T> link = new AjaxLink<T>("link", rowModel) {
            /**
             *
             */
            private static final long serialVersionUID = 3891810044928474771L;

            @Override
            public void onClick(AjaxRequestTarget target) {
                target.appendJavaScript(Select2SingleChoice.REMOVE_POPUP_UP_SCRIPT);
                target.appendJavaScript(DateTextField.REMOVE_POPUP_UP_SCRIPT);
                if (itemClick != null) {
                    itemClick.accept(identity, rowModel.getObject(), target);
                }
            }
        };
        add(link);

        Label text = new Label("text", model);
        text.setRenderBodyOnly(true);
        link.add(text);

        ItemCss htmlLambdaCss = itemCss;
        if (htmlLambdaCss == ItemCss.SUCCESS) {
            link.add(AttributeModifier.replace("class", "btn-xs btn-success"));
        } else if (htmlLambdaCss == ItemCss.PRIMARY) {
            link.add(AttributeModifier.replace("class", "btn-xs btn-primary"));
        } else if (htmlLambdaCss == ItemCss.WARNING) {
            link.add(AttributeModifier.replace("class", "btn-xs btn-warning"));
        } else if (htmlLambdaCss == ItemCss.DANGER) {
            link.add(AttributeModifier.replace("class", "btn-xs btn-danger"));
        } else if (htmlLambdaCss == ItemCss.INFO) {
            link.add(AttributeModifier.replace("class", "btn-xs btn-info"));
        }
    }

}
