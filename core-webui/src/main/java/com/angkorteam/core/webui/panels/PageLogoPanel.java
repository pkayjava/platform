package com.angkorteam.core.webui.panels;

import com.angkorteam.core.webui.models.PageLogo;
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class PageLogoPanel extends Panel {

    /**
     *
     */
    private static final long serialVersionUID = 7198852413613423409L;

    private WebMarkupContainer wicketContainer = null;

    public PageLogoPanel(String id, IModel<PageLogo> model) {
        super(id, model);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        this.wicketContainer = new WebMarkupContainer("wicketContainer");
        this.add(this.wicketContainer);

        PageLogo pageLogo = (PageLogo) getDefaultModelObject();
        if (pageLogo == null) {
            this.wicketContainer.setVisible(false);
        }
        MarkupContainer container = null;
        if (pageLogo != null && pageLogo.getPage() != null) {
            Fragment fragment = new Fragment("root", "fragmentLink", this);
            this.wicketContainer.add(fragment);
            container = new BookmarkablePageLink<Void>("link", pageLogo.getPage(), pageLogo.getParameters());
            fragment.add(container);
        } else {
            container = new Fragment("root", "fragmentText", this);
            this.wicketContainer.add(container);
        }
        parseItem(container, pageLogo);
    }

    protected void parseItem(MarkupContainer container, PageLogo pageLogo) {
        Label mini = new Label("mini",
                pageLogo == null || pageLogo.getSmall() == null || StringUtils.isEmpty(pageLogo.getSmall())
                        ? "<b>A</b>LT"
                        : pageLogo.getSmall());
        mini.setEscapeModelStrings(false);
        container.add(mini);
        Label large = new Label("large",
                pageLogo == null || pageLogo.getLarge() == null || StringUtils.isEmpty(pageLogo.getLarge())
                        ? "<b>Admin</b>LTE"
                        : pageLogo.getLarge());
        large.setEscapeModelStrings(false);
        container.add(large);
    }
}
