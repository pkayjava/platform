package com.angkorteam.core.webui.wicket.ajax.form;

import com.angkorteam.core.webui.wicket.functional.WicketBiFunction;
import com.angkorteam.core.webui.wicket.functional.WicketFunction;
import com.angkorteam.core.webui.wicket.markup.html.form.DateTextField;
import com.angkorteam.core.webui.wicket.markup.html.form.select2.Select2SingleChoice;
import org.apache.wicket.ajax.AjaxRequestTarget;

/**
 * Created by socheatkhauv on 7/15/17.
 */
public class OnChangeAjaxBehavior extends org.apache.wicket.ajax.form.OnChangeAjaxBehavior {

    /**
     *
     */
    private static final long serialVersionUID = -993933875386168322L;

    private WicketFunction<AjaxRequestTarget, Boolean> update;

    private WicketBiFunction<AjaxRequestTarget, RuntimeException, Boolean> error;

    public OnChangeAjaxBehavior() {
    }

    public OnChangeAjaxBehavior(WicketFunction<AjaxRequestTarget, Boolean> update) {
        this.update = update;
    }

    public OnChangeAjaxBehavior(WicketFunction<AjaxRequestTarget, Boolean> update,
                                WicketBiFunction<AjaxRequestTarget, RuntimeException, Boolean> error) {
        this.update = update;
        this.error = error;
    }

    @Override
    protected final void onUpdate(AjaxRequestTarget target) {
        if (this.update != null) {
            boolean clear = this.update.apply(target);
            if (clear) {
                target.appendJavaScript(Select2SingleChoice.REMOVE_POPUP_UP_SCRIPT);
                target.appendJavaScript(DateTextField.REMOVE_POPUP_UP_SCRIPT);
            }
        }
    }

    @Override
    protected final void onError(AjaxRequestTarget target, RuntimeException e) {
        if (this.error != null) {
            boolean clear = this.error.apply(target, e);
            if (clear) {
                target.appendJavaScript(Select2SingleChoice.REMOVE_POPUP_UP_SCRIPT);
                target.appendJavaScript(DateTextField.REMOVE_POPUP_UP_SCRIPT);
            }
        } else {
            super.onError(target, e);
        }
    }

}
