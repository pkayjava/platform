package com.angkorteam.core.webui;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public enum BoxColor {
    Success("box-success"), Solid("box-solid"), Default("box-default"), Warning("box-warning"), Danger("box-danger"),
    Info("box-info"), Primary("box-primary");

    private String literal;

    BoxColor(String literal) {
        this.literal = literal;
    }

    public String getLiteral() {
        return literal;
    }
}
