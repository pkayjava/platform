package com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.filter;

//package com.angkorteam.framework.wicket.extensions.markup.html.repeater.data.table.filter;
//
//import java.util.Date;
//import java.util.List;
//import java.util.Map;
//
//import org.apache.commons.lang3.time.DateFormatUtils;
//import org.apache.wicket.Component;
//import org.apache.wicket.WicketRuntimeException;
//import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
//import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
//import org.apache.wicket.extensions.markup.html.repeater.data.table.export.IExportableColumn;
//import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilterForm;
//import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.IFilteredColumn;
//import org.apache.wicket.markup.html.basic.Label;
//import org.apache.wicket.markup.html.form.IChoiceRenderer;
//import org.apache.wicket.markup.repeater.Item;
//import org.apache.wicket.model.IModel;
//import org.apache.wicket.model.Model;
//import org.apache.wicket.model.PropertyModel;
//
//import com.angkorteam.framework.share.provider.TableProvider;
//import com.angkorteam.framework.wicket.functional.WicketTriFunction;
//import com.angkorteam.framework.wicket.markup.html.form.select2.Option;
//
///**
// * Created by socheat on 12/7/16.
// */
//public class ChoiceFilterColumn extends AbstractColumn<Map<String, Object>, String>
//        implements IFilteredColumn<Map<String, Object>, String>, IExportableColumn<Map<String, Object>, String> {
//
//    /**
//     * 
//     */
//    private static final long serialVersionUID = -226653817584457150L;
//
//    private final IModel<List<String>> filterChoices;
//
//    private final String propertyExpression;
//
//    private final ItemClass itemClass;
//
//    private WicketTriFunction<String, IModel<String>, Map<String, Object>, ItemPanel> function;
//
//    private WicketTriFunction<String, IModel<String>, Map<String, Object>, String> export;
//
//    private String valueColumn;
//
//    public ChoiceFilterColumn(TableProvider provider, ItemClass itemClass, IModel<String> displayModel,
//            String valueColumn, IModel<List<String>> filterChoices, String propertyExpression,
//            WicketTriFunction<String, IModel<String>, Map<String, Object>, ItemPanel> function) {
//        this(provider, itemClass, displayModel, valueColumn, filterChoices, propertyExpression, function, null);
//    }
//
//    public ChoiceFilterColumn(TableProvider provider, ItemClass itemClass, IModel<String> displayModel,
//            String valueColumn, IModel<List<String>> filterChoices, String propertyExpression,
//            WicketTriFunction<String, IModel<String>, Map<String, Object>, ItemPanel> function,
//            WicketTriFunction<String, IModel<String>, Map<String, Object>, String> export) {
//        super(displayModel, propertyExpression);
//        if (export == null) {
//            this.export = this::exportColumn;
//        } else {
//            this.export = export;
//        }
//        this.propertyExpression = propertyExpression;
//        this.itemClass = itemClass;
//        this.function = function;
//        this.valueColumn = valueColumn;
//        if (itemClass == ItemClass.Byte) {
//            provider.selectField(valueColumn, Byte.class);
//        } else if (itemClass == ItemClass.Short) {
//            provider.selectField(valueColumn, Short.class);
//        } else if (itemClass == ItemClass.Integer) {
//            provider.selectField(valueColumn, Integer.class);
//        } else if (itemClass == ItemClass.Long) {
//            provider.selectField(valueColumn, Long.class);
//        } else if (itemClass == ItemClass.Float) {
//            provider.selectField(valueColumn, Float.class);
//        } else if (itemClass == ItemClass.Double) {
//            provider.selectField(valueColumn, Double.class);
//        } else if (itemClass == ItemClass.Boolean) {
//            provider.selectField(valueColumn, Boolean.class);
//        } else if (itemClass == ItemClass.String) {
//            provider.selectField(valueColumn, String.class);
//        } else if (itemClass == ItemClass.DateTime) {
//            provider.selectField(valueColumn, "yyyy-MM-dd HH:mm:ss", Calendar.DateTime);
//        } else if (itemClass == ItemClass.Date) {
//            provider.selectField(valueColumn, "yyyy-MM-dd", Calendar.Date);
//        } else if (itemClass == ItemClass.Time) {
//            provider.selectField(valueColumn, "HH:mm:ss", Calendar.Time);
//        }
//        this.filterChoices = filterChoices;
//    }
//
//    @Override
//    public Component getFilter(final String componentId, final FilterForm<?> form) {
//        ChoiceFilter<String> filter = new ChoiceFilter<>(componentId, getFilterModel(form), form, getFilterChoices(),
//                enableAutoSubmit());
//        IChoiceRenderer<String> renderer = getChoiceRenderer();
//        if (renderer != null) {
//            filter.getChoice().setChoiceRenderer(renderer);
//        }
//        return filter;
//    }
//
//    @Override
//    public void populateItem(Item<ICellPopulator<Map<String, Object>>> item, String componentId,
//            IModel<Map<String, Object>> rowModel) {
//        ItemPanel itemPanel = this.function.apply(this.valueColumn, this.getDisplayModel(), rowModel.getObject());
//        if (itemPanel == null) {
//            item.add(new Label(componentId, ""));
//        } else {
//            CellPanel cell = new CellPanel(componentId);
//            item.add(cell);
//            cell.add(itemPanel);
//        }
//    }
//
//    protected IModel<String> getFilterModel(final FilterForm<?> form) {
//        return new PropertyModel<>(form.getDefaultModel(), this.propertyExpression);
//    }
//
//    protected IChoiceRenderer<String> getChoiceRenderer() {
//        return null;
//    }
//
//    protected final IModel<List<String>> getFilterChoices() {
//        return filterChoices;
//    }
//
//    public void detach() {
//        super.detach();
//        if (filterChoices != null) {
//            filterChoices.detach();
//        }
//    }
//
//    protected boolean enableAutoSubmit() {
//        return true;
//    }
//
//    protected String exportColumn(String column, IModel<String> display, Map<String, Object> model) {
//        Object object = model.get(column);
//        if (object == null) {
//            return "";
//        }
//        if (object instanceof Number) {
//            return String.valueOf(object);
//        } else if (object instanceof Boolean) {
//            return String.valueOf(object);
//        } else if (object instanceof Character) {
//            return String.valueOf(object);
//        } else if (object instanceof String) {
//            return (String) object;
//        } else if (object instanceof Date) {
//            if (this.itemClass == ItemClass.Date) {
//                return DateFormatUtils.ISO_8601_EXTENDED_DATE_FORMAT.format((Date) object);
//            } else if (this.itemClass == ItemClass.DateTime) {
//                return DateFormatUtils.ISO_8601_EXTENDED_DATETIME_TIME_ZONE_FORMAT.format((Date) object);
//            } else if (this.itemClass == ItemClass.Time) {
//                return DateFormatUtils.ISO_8601_EXTENDED_TIME_FORMAT.format((Date) object);
//            } else {
//                return DateFormatUtils.ISO_8601_EXTENDED_DATETIME_TIME_ZONE_FORMAT.format((Date) object);
//            }
//        } else if (object instanceof Option) {
//            return ((Option) object).getText();
//        } else {
//            throw new WicketRuntimeException(object.getClass().getName() + " is not supported");
//        }
//    }
//
//    @Override
//    public IModel<?> getDataModel(IModel<Map<String, Object>> rowModel) {
//        String data = this.export.apply(this.valueColumn, this.getDisplayModel(), rowModel.getObject());
//        return Model.of(data);
//    }
//
//}
