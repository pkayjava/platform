package com.angkorteam.core.webui.models;

import com.angkorteam.core.webui.AlertType;
import com.angkorteam.core.webui.Emoji;

import java.io.Serializable;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class Alert implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8921264027633728449L;

    private AlertType type;

    private Emoji icon;

    private String title;

    private String description;

    private Alert() {
    }

    public AlertType getType() {
        return type;
    }

    public Emoji getIcon() {
        return icon;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public static class Builder {

        private AlertType type;

        private Emoji icon;

        private String title;

        private String description;

        public Builder() {
        }

        public Builder withType(AlertType type) {
            this.type = type;
            return this;
        }

        public Builder withEmoji(Emoji icon) {
            this.icon = icon;
            return this;
        }

        public Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Alert build() {
            Alert alert = new Alert();
            alert.type = type;
            alert.icon = icon;
            alert.title = title;
            alert.description = description;
            return alert;
        }

    }

}
