package com.angkorteam.core.webui;

/**
 * Created by socheatkhauv on 6/13/17.
 */
public enum ProgressBarColor {

    LightBlue("progress-bar-light-blue"),

    Green("progress-bar-green"),

    Aqua("progress-bar-aqua"),

    Yellow("progress-bar-yellow"),

    Red("progress-bar-red");

    private String literal;

    ProgressBarColor(String literal) {
        this.literal = literal;
    }

    public String getLiteral() {
        return literal;
    }
}
