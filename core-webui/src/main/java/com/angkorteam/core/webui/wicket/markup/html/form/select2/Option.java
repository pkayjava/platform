package com.angkorteam.core.webui.wicket.markup.html.form.select2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by socheat on 5/25/16.
 */
public class Option implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -8984541544744301572L;

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("text")
    private String text;

    public Option() {
    }

    public Option(String id, String text) {
        this.id = id;
        this.text = text;
    }

    public static final OptionMapper MAPPER = new OptionMapper();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((text == null) ? 0 : text.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Option other = (Option) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (text == null) {
            if (other.text != null)
                return false;
        } else if (!text.equals(other.text))
            return false;
        return true;
    }

}
