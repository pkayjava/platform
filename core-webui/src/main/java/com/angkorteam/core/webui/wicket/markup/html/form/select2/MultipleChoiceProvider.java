package com.angkorteam.core.webui.wicket.markup.html.form.select2;

import java.util.List;

/**
 * Created by socheat on 5/25/16.
 */
public abstract class MultipleChoiceProvider<T> extends IChoiceProvider<T> {

    /**
     *
     */
    private static final long serialVersionUID = 3573561370363154954L;

    public abstract List<T> toChoices(List<String> ids);

}
