package com.angkorteam.core.webui.models;

import java.io.Serializable;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class PageFooter implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -5177095116326795843L;

    private String company;

    private PageFooter() {
    }

    public String getCompany() {
        return company;
    }

    public static class Builder {

        private String company;

        public PageFooter build() {
            PageFooter item = new PageFooter();
            item.company = this.company;
            return item;
        }

        public Builder withCompany(String company) {
            this.company = company;
            return this;
        }

    }

}
