package com.angkorteam.core.webui.models;

import com.angkorteam.core.webui.Emoji;
import com.angkorteam.core.webui.SideMenuType;
import com.angkorteam.core.webui.TextColor;
import org.apache.wicket.Page;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * Created by socheatkhauv on 6/14/17.
 */
public class SideMenu implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7259380266500041560L;

    private SideMenuType type;

    private SideMenu parent;

    private List<SideMenu> children;

    private List<Badge> badges;

    private String label;

    private Class<? extends Page> page;

    private PageParameters parameters;

    private Emoji icon;

    private TextColor iconColor;

    private boolean active;

    public boolean isActive() {
        return active;
    }

    public SideMenuType getType() {
        return type;
    }

    public SideMenu getParent() {
        return parent;
    }

    public List<SideMenu> getChildren() {
        return children;
    }

    public List<Badge> getBadges() {
        return badges;
    }

    public String getLabel() {
        return label;
    }

    public Class<? extends Page> getPage() {
        return page;
    }

    public PageParameters getParameters() {
        return parameters;
    }

    public Emoji getIcon() {
        return icon;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public TextColor getIconColor() {
        return iconColor;
    }

    public static class Builder {

        private SideMenuType type;

        private SideMenu parent;

        private List<SideMenu> children;

        private List<Badge> badges;

        private String label;

        private Class<? extends Page> page;

        private PageParameters parameters;

        private Emoji icon;

        private TextColor iconColor;

        private boolean active;

        public SideMenu build() {
            SideMenu item = new SideMenu();
            item.type = this.type;
            item.parent = this.parent;
            item.children = this.children;
            item.badges = this.badges;
            item.label = this.label;
            item.page = this.page;
            item.parameters = this.parameters;
            item.icon = this.icon;
            item.iconColor = this.iconColor;
            item.active = this.active;

            if (this.children != null && !this.children.isEmpty()) {
                for (SideMenu ch : this.children) {
                    this.children.add(ch);
                    ch.parent = item;
                }
            }
            return item;
        }

        public Builder withHeader(String label) {
            this.type = SideMenuType.Header;
            this.label = label;
            return this;
        }

        public Builder withMenu(Emoji icon, String label, Class<? extends Page> page) {
            return withMenu(icon, label, page, null);
        }

        public Builder withMenu(Emoji icon, String label, Class<? extends Page> page, Badge badge) {
            return withMenu(icon, label, page, badge == null ? Arrays.asList() : Arrays.asList(badge),
                    new PageParameters(), TextColor.Red);
        }

        public Builder withMenu(Emoji icon, String label, Class<? extends Page> page, List<Badge> badges,
                                PageParameters parameters, TextColor color) {
            this.type = SideMenuType.Menu;
            this.page = page;
            this.icon = icon;
            this.parameters = parameters;
            this.label = label;
            this.iconColor = color;
            this.badges = badges;
            return this;
        }

        public Builder withMenu(Emoji icon, String label, List<SideMenu> children) {
            return withMenu(icon, label, children, null);
        }

        public Builder withMenu(Emoji icon, String label, List<SideMenu> children, Badge badge) {
            return withMenu(icon, label, children, badge == null ? Arrays.asList() : Arrays.asList(badge),
                    TextColor.Red);
        }

        public Builder withMenu(Emoji icon, String label, List<SideMenu> children, List<Badge> badges,
                                TextColor color) {
            this.type = SideMenuType.Menu;
            this.icon = icon;
            this.page = null;
            this.parameters = null;
            this.label = label;
            this.children = children;
            this.badges = badges;
            this.iconColor = color;
            return this;
        }

        public Builder withActive(boolean active) {
            this.active = active;
            return this;
        }

    }

}
