package com.angkorteam.core.webui.panels;

import com.angkorteam.core.webui.models.PageHeader;
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/11/17.
 */
public class PageHeaderPanel extends Panel {

    /**
     *
     */
    private static final long serialVersionUID = -3793400293468691473L;

    private Label wicketTitle;

    private Label wicketDescription;

    private WebMarkupContainer wicketContainer;

    public PageHeaderPanel(String id, IModel<PageHeader> model) {
        super(id, model);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        this.wicketContainer = new WebMarkupContainer("wicketContainer");
        add(this.wicketContainer);

        this.wicketTitle = new Label("wicketTitle", () -> {
            PageHeader pageHeader = (PageHeader) getDefaultModelObject();
            if (pageHeader == null || pageHeader.getTitle() == null || StringUtils.isEmpty(pageHeader.getTitle())) {
                return "&nbsp;";
            } else {
                return pageHeader.getTitle();
            }
        });
        this.wicketTitle.setEscapeModelStrings(false);
        this.wicketContainer.add(this.wicketTitle);

        this.wicketDescription = new Label("wicketDescription", () -> {
            PageHeader pageHeader = (PageHeader) getDefaultModelObject();
            if (pageHeader == null || pageHeader.getDescription() == null
                    || StringUtils.isEmpty(pageHeader.getDescription())) {
                return "&nbsp;";
            } else {
                return pageHeader.getDescription();
            }
        });
        this.wicketDescription.setEscapeModelStrings(false);
        this.wicketContainer.add(this.wicketDescription);
    }

    @Override
    protected void onBeforeRender() {
        super.onBeforeRender();
        PageHeader pageHeader = (PageHeader) getDefaultModelObject();
        if (pageHeader == null) {
            this.wicketContainer.setVisible(false);
        } else {
            if (pageHeader.getTitle() == null || StringUtils.isEmpty(pageHeader.getTitle())) {
                this.wicketTitle.setVisible(false);
            }
            if (pageHeader.getDescription() == null || StringUtils.isEmpty(pageHeader.getDescription())) {
                this.wicketDescription.setVisible(false);
            }
        }
    }

}
