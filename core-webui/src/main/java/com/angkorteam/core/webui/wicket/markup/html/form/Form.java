package com.angkorteam.core.webui.wicket.markup.html.form;

import com.angkorteam.core.webui.wicket.functional.WicketConsumer;
import org.apache.wicket.model.IModel;

/**
 * Created by socheat on 3/4/16.
 */
public class Form<T> extends org.apache.wicket.markup.html.form.Form<T> {

    /**
     *
     */
    private static final long serialVersionUID = -7730821057811138569L;

    private WicketConsumer<Form<T>> onSubmit;
    private WicketConsumer<Form<T>> onError;

    public Form(String id) {
        super(id);
    }

    public Form(String id, IModel<T> model) {
        super(id, model);
    }

    @Override
    protected final void onSubmit() {
        if (this.onSubmit != null) {
            this.onSubmit.accept(this);
        }
    }

    @Override
    protected final void onError() {
        if (this.onError != null) {
            this.onError.accept(this);
        }
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        setOutputMarkupId(true);
    }

    public void setOnSubmit(WicketConsumer<Form<T>> onSubmit) {
        this.onSubmit = onSubmit;
    }

    public void setOnError(WicketConsumer<Form<T>> onError) {
        this.onError = onError;
    }

}
