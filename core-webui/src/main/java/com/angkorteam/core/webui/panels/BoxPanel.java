package com.angkorteam.core.webui.panels;

import com.angkorteam.core.webui.models.Box;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class BoxPanel extends Panel {

    /**
     *
     */
    private static final long serialVersionUID = 9183074361007819524L;

    private WebMarkupContainer root;

    public BoxPanel(String id) {
        super(id);
    }

    public BoxPanel(String id, IModel<Box> model) {
        super(id, model);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        this.root = new WebMarkupContainer("root");
        this.add(this.root);

        Box box = (Box) getDefaultModelObject();
        if (box == null || (box.getBody() == null && box.getHeader() == null && box.getFooter() == null)) {
            this.root.setVisible(false);
        }

        if (box != null && box.getColor() != null) {
            this.root.add(AttributeModifier.append("class", box.getColor().getLiteral()));
        }

        if (box != null && box.getHeader() != null) {
            this.root.add(box.getHeader());
        } else {
            Fragment fragment = new Fragment("header", "fragmentEmpty", this);
            fragment.setVisible(false);
            this.root.add(fragment);
        }

        if (box != null && box.getFooter() != null) {
            this.root.add(box.getFooter());
        } else {
            Fragment fragment = new Fragment("footer", "fragmentEmpty", this);
            fragment.setVisible(false);
            this.root.add(fragment);
        }

        if (box != null && box.getBody() != null) {
            this.root.add(box.getBody());
        } else {
            Fragment fragment = new Fragment("body", "fragmentEmpty", this);
            fragment.setVisible(false);
            this.root.add(fragment);
        }

    }
}
