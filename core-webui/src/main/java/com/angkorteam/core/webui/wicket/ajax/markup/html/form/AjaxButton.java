package com.angkorteam.core.webui.wicket.ajax.markup.html.form;

import com.angkorteam.core.webui.wicket.functional.WicketBiFunction;
import com.angkorteam.core.webui.wicket.markup.html.form.DateTextField;
import com.angkorteam.core.webui.wicket.markup.html.form.select2.Select2SingleChoice;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.IModel;

/**
 * Created by socheat on 10/14/15.
 */
public class AjaxButton extends org.apache.wicket.ajax.markup.html.form.AjaxButton {

    /**
     *
     */
    private static final long serialVersionUID = 2125579879601239551L;

    private WicketBiFunction<AjaxButton, AjaxRequestTarget, Boolean> onSubmit;

    private WicketBiFunction<AjaxButton, AjaxRequestTarget, Boolean> onError;

    public AjaxButton(String id) {
        super(id);
    }

    public AjaxButton(String id, IModel<String> model) {
        super(id, model);
    }

    public AjaxButton(String id, Form<?> form) {
        super(id, form);
    }

    public AjaxButton(String id, IModel<String> model, Form<?> form) {
        super(id, model, form);
    }

    @Override
    protected void onSubmit(AjaxRequestTarget target) {
        if (this.onSubmit != null) {
            boolean clear = this.onSubmit.apply(this, target);
            if (clear) {
                target.appendJavaScript(Select2SingleChoice.REMOVE_POPUP_UP_SCRIPT);
                target.appendJavaScript(DateTextField.REMOVE_POPUP_UP_SCRIPT);
            }
        }
    }

    @Override
    protected final void onError(AjaxRequestTarget target) {
        if (this.onError != null) {
            boolean clear = this.onError.apply(this, target);
            if (clear) {
                target.appendJavaScript(Select2SingleChoice.REMOVE_POPUP_UP_SCRIPT);
                target.appendJavaScript(DateTextField.REMOVE_POPUP_UP_SCRIPT);
            }
        }
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        setOutputMarkupId(true);
    }

    public void setOnSubmit(WicketBiFunction<AjaxButton, AjaxRequestTarget, Boolean> onSubmit) {
        this.onSubmit = onSubmit;
    }

    public void setOnError(WicketBiFunction<AjaxButton, AjaxRequestTarget, Boolean> onError) {
        this.onError = onError;
    }
}
