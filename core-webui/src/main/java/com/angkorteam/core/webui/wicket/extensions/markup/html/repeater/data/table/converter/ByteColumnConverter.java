package com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.converter;

import com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.filter.Operator;
import org.apache.wicket.util.convert.ConversionException;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import java.util.Map;

public class ByteColumnConverter implements IColumnConverter<Byte> {

    /**
     *
     */
    private static final long serialVersionUID = -8146555542559151414L;

    @Override
    public Byte convertToObject(String key, String value) throws ConversionException {
        if (value == null || "".equals(value)) {
            return null;
        }
        try {
            return Byte.valueOf(value);
        } catch (NumberFormatException e) {
            throw new ConversionException(e);
        }
    }

    @Override
    public String convertToString(String key, Byte value) {
        if (value == null) {
            return "";
        }
        return String.valueOf(value);
    }

    @Override
    public String buildJdbcQuery(String key, String column, Operator operator, Byte firstOperand, Byte secondOperand,
                                 Map<String, Object> params) {
        String condition = null;
        String firstParam = key + "_1st";
        String secondParam = key + "_2nd";
        if (operator == Operator.Equal || operator == Operator.Like) {
            condition = column + " = :" + firstParam;
            params.put(firstParam, firstOperand);
        } else if (operator == Operator.NotEqual || operator == Operator.NotLike) {
            condition = column + " != :" + firstParam;
            params.put(firstParam, firstOperand);
        } else if (operator == Operator.GreaterThan) {
            condition = column + " > :" + firstParam;
            params.put(firstParam, firstOperand);
        } else if (operator == Operator.GreaterThanOrEqual) {
            condition = column + " >= :" + firstParam;
            params.put(firstParam, firstOperand);
        } else if (operator == Operator.LessThan) {
            condition = column + " < :" + firstParam;
            params.put(firstParam, firstOperand);
        } else if (operator == Operator.LessThanOrEqual) {
            condition = column + " <= :" + firstParam;
            params.put(firstParam, firstOperand);
        } else if (operator == Operator.Between) {
            if (firstOperand < secondOperand) {
                condition = column + " BETWEEN :" + firstParam + " AND :" + secondParam;
                params.put(firstParam, firstOperand);
                params.put(secondParam, secondOperand);
            } else {
                condition = column + " BETWEEN :" + secondParam + " AND :" + firstParam;
                params.put(firstParam, firstOperand);
                params.put(secondParam, secondOperand);
            }
        } else if (operator == Operator.NotBetween) {
            if (firstOperand < secondOperand) {
                condition = column + " NOT BETWEEN :" + firstParam + " AND :" + secondParam;
                params.put(firstParam, firstOperand);
                params.put(secondParam, secondOperand);
            } else {
                condition = column + " NOT BETWEEN :" + secondParam + " AND :" + firstParam;
                params.put(firstParam, firstOperand);
                params.put(secondParam, secondOperand);
            }
        }
        return condition;
    }

    @Override
    public QueryBuilder buildElasticQuery(String key, String column, Operator operator, Byte firstOperand,
                                          Byte secondOperand) {
        if (operator == Operator.Equal || operator == Operator.Like) {
            return QueryBuilders.termQuery(key, Integer.valueOf(firstOperand));
        } else if (operator == Operator.NotEqual || operator == Operator.NotLike) {
            return QueryBuilders.rangeQuery(key).gt(Integer.valueOf(firstOperand)).lt(Integer.valueOf(firstOperand));
        } else if (operator == Operator.GreaterThan) {
            return QueryBuilders.rangeQuery(key).gt(Integer.valueOf(firstOperand));
        } else if (operator == Operator.GreaterThanOrEqual) {
            return QueryBuilders.rangeQuery(key).gte(Integer.valueOf(firstOperand));
        } else if (operator == Operator.LessThan) {
            return QueryBuilders.rangeQuery(key).lt(Integer.valueOf(firstOperand));
        } else if (operator == Operator.LessThanOrEqual) {
            return QueryBuilders.rangeQuery(key).lte(Integer.valueOf(firstOperand));
        } else if (operator == Operator.Between) {
            if (firstOperand < secondOperand) {
                return QueryBuilders.rangeQuery(key).from(Integer.valueOf(firstOperand))
                        .to(Integer.valueOf(secondOperand));
            } else {
                return QueryBuilders.rangeQuery(key).from(Integer.valueOf(secondOperand))
                        .to(Integer.valueOf(firstOperand));
            }
        } else if (operator == Operator.NotBetween) {
            if (firstOperand < secondOperand) {
                return QueryBuilders.rangeQuery(key).lt(Integer.valueOf(Integer.valueOf(firstOperand)))
                        .gt(Integer.valueOf(secondOperand));
            } else {
                return QueryBuilders.rangeQuery(key).lt(Integer.valueOf(secondOperand))
                        .gt(Integer.valueOf(firstOperand));
            }
        }
        return null;
    }

}
