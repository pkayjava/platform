package com.angkorteam.core.webui.wicket.markup.html.form;

import com.angkorteam.core.spring.jdbc.JdbcTemplate;
import com.angkorteam.core.webui.wicket.markup.html.form.select2.Option;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.model.IModel;

import java.util.List;

/**
 * Created by socheatkhauv on 20/1/17.
 */
public class OptionDropDownChoice extends DropDownChoice<Option> {

    /**
     *
     */
    private static final long serialVersionUID = -424744219900488693L;

    public OptionDropDownChoice(String id, IModel<Option> model, JdbcTemplate jdbcTemplate, String table,
                                String idField, String textField) {
        super(id, model, new OptionListModel(jdbcTemplate, table, idField, textField), new OptionChoiceRenderer());
    }

    public OptionDropDownChoice(String id, IModel<Option> model, JdbcTemplate jdbcTemplate, String table,
                                String idField, String textField, List<String> filter) {
        super(id, model, new OptionListModel(jdbcTemplate, table, idField, textField, filter),
                new OptionChoiceRenderer());
    }

}