package com.angkorteam.core.webui;

/**
 * Created by socheatkhauv on 6/14/17.
 */
public enum SideMenuType {

    Header, Menu
}
