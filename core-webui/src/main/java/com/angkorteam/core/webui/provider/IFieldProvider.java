package com.angkorteam.core.webui.provider;

import com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.converter.IColumnConverter;

public interface IFieldProvider {

    void selectField(String column);

    void selectField(String key, String column);

    void selectField(String column, IColumnConverter<?> converter);

    void selectField(String key, String column, IColumnConverter<?> converter);

}
