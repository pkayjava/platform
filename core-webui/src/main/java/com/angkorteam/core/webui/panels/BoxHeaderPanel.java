package com.angkorteam.core.webui.panels;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class BoxHeaderPanel extends Panel {

    /**
     *
     */
    private static final long serialVersionUID = 7621263971382940392L;
    private boolean border;

    public BoxHeaderPanel(boolean border) {
        super("header");
        this.border = border;
    }

    public BoxHeaderPanel(boolean border, IModel<?> model) {
        super("header", model);
        this.border = border;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        if (this.border) {
            this.add(AttributeModifier.append("class", "with-border"));
        }
    }
}
