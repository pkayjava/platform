package com.angkorteam.core.webui.wicket.extensions.markup.html.tabs;

public abstract class ITab implements org.apache.wicket.extensions.markup.html.tabs.ITab {

    /**
     *
     */
    private static final long serialVersionUID = 7639000470363335507L;

    protected AjaxTabbedPanel<? extends ITab> tab;

    public AjaxTabbedPanel<? extends ITab> getTab() {
        return this.tab;
    }

    public abstract boolean isEnabled();

}
