package com.angkorteam.core.webui.panels;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/13/17.
 */
public class UserHeaderPanel extends Panel {

    /**
     *
     */
    private static final long serialVersionUID = -964706526880887144L;

    public UserHeaderPanel() {
        super("header");
    }

    public UserHeaderPanel(IModel<?> model) {
        super("header", model);
    }
}
