package com.angkorteam.core.webui;

import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.StringHeaderItem;
import org.apache.wicket.request.Url;
import org.apache.wicket.request.resource.PackageResourceReference;
import org.apache.wicket.request.resource.ResourceReference;
import org.apache.wicket.request.resource.UrlResourceReference;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by socheatkhauv on 6/11/17.
 */
public class ReferenceUtilities {

    public static final String AdminLTE = "AdminLTE-2.4.5";

    public static final String CODEMIRROR = "codemirror-5.16.0/";

    public static final String MOMENT = AdminLTE + "/bower_components/moment/min/moment-with-locales.min.js";

    public static final String DATE_PICKER_CSS = AdminLTE
            + "/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css";

    public static final String DATE_PICKER_JS = AdminLTE
            + "/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js";

    public static final String BOOTSTRAP_JS = AdminLTE + "/bower_components/bootstrap/dist/js/bootstrap.min.js";

    public static final String SELECT2_CSS = AdminLTE + "/bower_components/select2/dist/css/select2.min.css";

    public static final String SELECT2_JS = AdminLTE + "/bower_components/select2/dist/js/select2.full.min.js";

    public static final String J_QUERY_JS = AdminLTE + "/bower_components/jquery/dist/jquery.min.js";

    public static Map<String, ResourceReference> CACHES = new HashMap<>();

    public static void render(AdminLTEOption option, IHeaderResponse response) {
        if (option.Bootstrap) {
            renderCss(response, "<!-- Bootstrap 3.3.7 -->");
            renderCss(response, AdminLTE + "/bower_components/bootstrap/dist/css/bootstrap.min.css");
        }
        if (option.FontAwesome) {
            renderCss(response, "<!-- Font Awesome -->");
            renderCss(response, AdminLTE + "/bower_components/font-awesome/css/font-awesome.min.css");
        }
        if (option.Ionicons) {
            renderCss(response, "<!-- Ionicons -->");
            renderCss(response, AdminLTE + "/bower_components/Ionicons/css/ionicons.min.css");

        }
        if (option.JVectorMap) {
            renderCss(response, "<!-- jvectormap -->");
            renderCss(response, AdminLTE + "/plugins/jvectormap/jquery-jvectormap-1.2.2.css");
        }
        if (option.AdminLTE) {
            renderCss(response, "<!-- Theme style -->");
            renderCss(response, AdminLTE + "/dist/css/AdminLTE.min.css");
            renderCss(response,
                    "<!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->");
            renderCss(response, AdminLTE + "/dist/css/skins/_all-skins.min.css");
        }
        if (option.DatePicker) {
            renderCss(response, "<!-- Date Picker -->");
            renderCss(response, ReferenceUtilities.DATE_PICKER_CSS);
        }
        if (option.DateRangePicker) {
            renderCss(response, "<!-- Daterange picker -->");
            renderCss(response, AdminLTE + "/bower_components/bootstrap-daterangepicker/daterangepicker.css");
        }
        if (option.BootstrapWYSIHTML5) {
            renderCss(response, "<!-- bootstrap wysihtml5 - text editor -->");
            renderCss(response, AdminLTE + "/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css");
        }

        if (option.ICheck) {
            renderCss(response, "<!-- iCheck -->");
            renderCss(response, AdminLTE + "/plugins/iCheck/square/blue.css");
        }
        if (option.GoogleFont) {
            renderCss(response, "<!-- Google Font -->");
            renderCss(response, "cdn/google/google.css");
            renderCss(response, AdminLTE + "/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css");
        }
        if (option.Morris) {
            renderCss(response, "<!-- Morris -->");
            renderCss(response, AdminLTE + "/bower_components/morris.js/morris.css");
        }

        if (option.HTML5ShimAndRespondJS) {
            renderJavascript(response,
                    "<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->");
            renderJavascript(response, "<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->");
            renderJavascript(response, "<!--[if lt IE 9]>");
            renderJavascript(response, "cdn/html5shiv.min.js");
            renderJavascript(response, "cdn/respond.min.js");
            renderJavascript(response, "<![endif]-->");
        }
        if (option.JQuery) {
            renderJavascript(response, "<!-- jQuery 3.3.1 -->");
            renderJavascript(response, ReferenceUtilities.J_QUERY_JS);
        }
        if (option.JQueryUI) {
            renderJavascript(response, "<!-- jQuery UI 1.11.4 -->");
            renderJavascript(response, AdminLTE + "/bower_components/jquery-ui/jquery-ui.min.js");
            renderJavascript(response, "$.widget.bridge('uibutton', $.ui.button);");
        }
        if (option.Bootstrap) {
            renderJavascript(response, "<!-- Bootstrap 3.3.7 -->");
            renderJavascript(response, ReferenceUtilities.BOOTSTRAP_JS);
        }
        if (option.ICheck) {
            renderJavascript(response, "<!-- iCheck -->");
            renderJavascript(response, AdminLTE + "/plugins/iCheck/icheck.min.js");
            renderJavascript(response,
                    "$(function () { $('input').iCheck({ checkboxClass: 'icheckbox_square-blue', radioClass: 'iradio_square-blue', increaseArea: '20%' }); });");
        }
        if (option.MorrisJSChart) {
            renderJavascript(response, "<!-- Morris.js charts -->");
            renderJavascript(response, AdminLTE + "/bower_components/raphael/raphael.min.js");
            renderJavascript(response, AdminLTE + "/bower_components/morris.js/morris.min.js");
        }
        if (option.JQueryKnobChart) {
            renderJavascript(response, "<!-- jQuery Knob Chart -->");
            renderJavascript(response, AdminLTE + "/bower_components/jquery-knob/dist/jquery.knob.min.js");
        }
        if (option.DateRangePicker) {
            renderJavascript(response, "<!-- daterangepicker -->");
            renderJavascript(response, MOMENT);
            renderJavascript(response, AdminLTE + "/bower_components/bootstrap-daterangepicker/daterangepicker.js");
        }
        if (option.DatePicker) {
            renderJavascript(response, "<!-- datepicker -->");
            renderJavascript(response, ReferenceUtilities.DATE_PICKER_JS);
        }
        if (option.BootstrapWYSIHTML5) {
            renderJavascript(response, "<!-- Bootstrap WYSIHTML5 -->");
            renderJavascript(response, AdminLTE + "/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js");
        }
        if (option.FastClick) {
            renderJavascript(response, "<!-- FastClick -->");
            renderJavascript(response, AdminLTE + "/bower_components/fastclick/lib/fastclick.js");
        }
        if (option.AdminLTE) {
            renderJavascript(response, "<!-- AdminLTE App -->");
            renderJavascript(response, AdminLTE + "/dist/js/adminlte.min.js");
        }
        if (option.Sparkline) {
            renderJavascript(response, "<!-- Sparkline 2.1.3 -->");
            renderJavascript(response, AdminLTE + "/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js");
        }
        if (option.JVectorMap) {
            renderJavascript(response, "<!-- jvectormap -->");
            renderJavascript(response, AdminLTE + "/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js");
            renderJavascript(response, AdminLTE + "/plugins/jvectormap/jquery-jvectormap-world-mill-en.js");
        }
        if (option.SlimScroll) {
            renderJavascript(response, "<!-- SlimScroll 1.3.8 -->");
            renderJavascript(response, AdminLTE + "/bower_components/jquery-slimscroll/jquery.slimscroll.min.js");
        }
        if (option.Dashboard2) {
            renderJavascript(response, "<!-- AdminLTE dashboard demo (This is only for demo purposes) -->");
            renderJavascript(response, AdminLTE + "/dist/js/pages/dashboard2.js");
        }
        if (option.Dashboard) {
            renderJavascript(response, "<!-- AdminLTE dashboard demo (This is only for demo purposes) -->");
            renderJavascript(response, AdminLTE + "/dist/js/pages/dashboard.js");
        }
        if (option.Demo) {
            renderJavascript(response, "<!-- AdminLTE for demo purposes -->");
            renderJavascript(response, AdminLTE + "/dist/js/demo.js");
        }
        if (option.ChartJS) {
            renderJavascript(response, "<!-- ChartJS 1.0.2 -->");
            renderJavascript(response, AdminLTE + "/bower_components/chart.js/Chart.min.js");
        }
        if (option.Morris) {
            renderJavascript(response, "<!-- Morris -->");
            renderJavascript(response, AdminLTE + "/bower_components/raphael/raphael.min.js");
            renderJavascript(response, AdminLTE + "/bower_components/morris.js/morris.min.js");
        }
        if (option.Flot) {
            renderJavascript(response, "<!-- Flot -->");
            renderJavascript(response, AdminLTE + "/bower_components/Flot/jquery.flot.js");
            renderJavascript(response, AdminLTE + "/bower_components/Flot/jquery.flot.resize.js");
            renderJavascript(response, AdminLTE + "/bower_components/Flot/jquery.flot.resize.js");
            renderJavascript(response, AdminLTE + "/bower_components/Flot/jquery.flot.pie.js");
            renderJavascript(response, AdminLTE + "/bower_components/Flot/jquery.flot.categories.js");
        }
        if (option.Inline) {
            renderJavascript(response, "<!-- Inline -->");
            renderJavascript(response, AdminLTE + "/bower_components/jquery-knob/js/jquery.knob.js");
            renderJavascript(response, AdminLTE + "/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js");
        }
    }

    public static void renderJavascript(IHeaderResponse response, String js) {
        if (js.startsWith(AdminLTE) || js.startsWith("codemirror") || js.startsWith("ckeditor")) {
            ResourceReference reference = loadResourceReference(js);
            response.render(JavaScriptHeaderItem.forReference(reference));
        } else if (js.startsWith("http://") || js.startsWith("https://")) {
            ResourceReference reference = loadResourceReference(js);
            response.render(JavaScriptHeaderItem.forReference(reference));
        } else if (js.startsWith("<!")) {
            response.render(StringHeaderItem.forString(js + "\n"));
        } else {
            response.render(JavaScriptHeaderItem.forScript(js, ""));
        }
    }

    public static ResourceReference loadResourceReference(String resource) {
        if (resource.startsWith("http://") || resource.startsWith("https://")) {
            return CACHES.computeIfAbsent(resource, k -> new UrlResourceReference(Url.parse(k)));
        } else {
            return CACHES.computeIfAbsent(resource, k -> new PackageResourceReference(ResourceScope.class, k));
        }
    }

    public static void renderCss(IHeaderResponse response, String css) {
        if (css.startsWith(AdminLTE) || css.startsWith("codemirror") || css.startsWith("ckeditor")) {
            ResourceReference reference = loadResourceReference(css);
            response.render(CssHeaderItem.forReference(reference));
        } else if (css.startsWith("http://") || css.startsWith("https://")) {
            ResourceReference reference = loadResourceReference(css);
            response.render(CssHeaderItem.forReference(reference));
        } else if (css.startsWith("<!")) {
            response.render(StringHeaderItem.forString(css + "\n"));
        } else {
            response.render(CssHeaderItem.forCSS(css, ""));
        }
    }

    public static void renderIndexPage(IHeaderResponse response) {
        AdminLTEOption option = new AdminLTEOption();
        option.Bootstrap = true;
        option.FontAwesome = true;
        option.JVectorMap = true;
        option.Ionicons = true;
        option.AdminLTE = true;
        option.MorrisJSChart = true;
        option.DatePicker = true;
        option.DateRangePicker = true;
        option.JQueryUI = true;
        option.JQueryKnobChart = true;
        option.BootstrapWYSIHTML5 = true;
        option.HTML5ShimAndRespondJS = true;
        option.GoogleFont = true;
        option.JQuery = true;
        option.FastClick = true;
        option.Sparkline = true;
        option.SlimScroll = true;
        option.ChartJS = true;
        option.Dashboard = true;
        option.Demo = true;
        render(option, response);
    }

    public static void renderStarterPage(IHeaderResponse response) {
        AdminLTEOption option = new AdminLTEOption();
        option.Bootstrap = true;
        option.FontAwesome = true;
        option.Ionicons = true;
        option.AdminLTE = true;
        option.HTML5ShimAndRespondJS = true;
        option.GoogleFont = true;
        option.JQuery = true;
        render(option, response);
    }

    public static void renderIndex2Page(IHeaderResponse response) {
        AdminLTEOption option = new AdminLTEOption();
        option.Bootstrap = true;
        option.FontAwesome = true;
        option.JVectorMap = true;
        option.Ionicons = true;
        option.AdminLTE = true;
        option.HTML5ShimAndRespondJS = true;
        option.GoogleFont = true;
        option.JQuery = true;
        option.FastClick = true;
        option.Sparkline = true;
        option.SlimScroll = true;
        option.ChartJS = true;
        option.Dashboard2 = true;
        option.Demo = true;
        render(option, response);
    }

}
