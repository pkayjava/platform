package com.angkorteam.core.webui.panels;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/13/17.
 */
public class UserItemPanel extends Panel {

    /**
     *
     */
    private static final long serialVersionUID = 3323716284246860958L;

    public UserItemPanel() {
        super("item");
    }

    public UserItemPanel(IModel<?> model) {
        super("item", model);
    }
}
