package com.angkorteam.core.webui;

public class Finance {

    // deposit x amount + compound interest 10% per year
    // find year duration to get 200% of deposit amount
    public static long durationDepositCompound(double principal, double futurePrincipal, double interestRate) {
        if (futurePrincipal < principal) {
            throw new RuntimeException("invalid future principal");
        }
        if (interestRate <= 0 || interestRate > 100) {
            throw new RuntimeException("invalid interest rate");
        }
        double ratio = futurePrincipal / principal;
        double times = Math.log10(ratio) / Math.log10((interestRate / 100d) + 1);
        return (long) Math.ceil(times);
    }

    public static double amountLoanSimple(double principal, int duration, double interestRate) {
        return principal * (1 + (duration * (interestRate / 100d)));
    }

    public static double amountLoanCompound(double principal, int duration, double interestRate) {
        return principal * Math.pow((1 + (interestRate / 100d)), duration);
    }

    public static void main(String[] args) {
        System.out.println(durationDepositCompound(500, 1000, 10));
        System.out.println(amountLoanCompound(500, 8, 10));
    }

}
