package com.angkorteam.core.webui.models;

import com.angkorteam.core.webui.CalloutType;

import java.io.Serializable;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class Callout implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5339920296091689810L;

    private CalloutType type;

    private String title;

    private String description;

    private Callout() {
    }

    public CalloutType getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public static class Builder {

        private CalloutType type;

        private String title;

        private String description;

        public Builder withType(CalloutType type) {
            this.type = type;
            return this;
        }

        public Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Callout build() {
            Callout item = new Callout();
            item.type = this.type;
            item.title = this.title;
            item.description = this.description;
            return item;
        }

    }

}
