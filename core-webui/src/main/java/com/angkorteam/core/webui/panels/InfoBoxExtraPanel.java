package com.angkorteam.core.webui.panels;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class InfoBoxExtraPanel extends Panel {

    /**
     *
     */
    private static final long serialVersionUID = -7897851717614386593L;

    public InfoBoxExtraPanel() {
        super("extra");
    }

    public InfoBoxExtraPanel(IModel<?> model) {
        super("extra", model);
    }

}
