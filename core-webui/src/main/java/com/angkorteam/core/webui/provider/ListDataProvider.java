package com.angkorteam.core.webui.provider;

import org.apache.wicket.extensions.markup.html.repeater.data.sort.ISortState;
import org.apache.wicket.extensions.markup.html.repeater.data.sort.ISortStateLocator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.IFilterStateLocator;
import org.apache.wicket.extensions.markup.html.repeater.util.SingleSortState;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import java.util.*;

public class ListDataProvider implements IDataProvider<Map<String, Object>>, IFilterStateLocator<Map<String, String>>,
        ISortStateLocator<String> {

    private static final long serialVersionUID = 1L;

    private Map<String, String> state;

    private final SingleSortState<String> sort = new SingleSortState<>();

    /**
     * reference to the list used as dataprovider for the dataview
     */
    private final List<Map<String, Object>> list;

    /**
     * Constructs an empty provider. Useful for lazy loading together with
     * {@linkplain #getData()}
     */
    public ListDataProvider() {
        this(new ArrayList<>());
    }

    /**
     * @param list the list used as dataprovider for the dataview
     */
    public ListDataProvider(List<Map<String, Object>> list) {
        if (list == null) {
            throw new IllegalArgumentException("argument [list] cannot be null");
        }
        this.list = list;
        this.state = new HashMap<>();
    }

    /**
     * Subclass to lazy load the list
     *
     * @return The list
     */
    protected List<Map<String, Object>> getData() {
        return list;
    }

    @Override
    public Iterator<Map<String, Object>> iterator(final long first, final long count) {
        List<Map<String, Object>> list = getData();

        long toIndex = first + count;
        if (toIndex > list.size()) {
            toIndex = list.size();
        }
        return list.subList((int) first, (int) toIndex).listIterator();
    }

    @Override
    public Map<String, String> getFilterState() {
        return this.state;
    }

    @Override
    public ISortState<String> getSortState() {
        return this.sort;
    }

    @Override
    public void setFilterState(Map<String, String> state) {
        this.state = state;
    }

    @Override
    public long size() {
        return getData().size();
    }

    @Override
    public IModel<Map<String, Object>> model(Map<String, Object> object) {
        return Model.ofMap(object);
    }

    public List<Map<String, Object>> getList() {
        return this.list;
    }

}
