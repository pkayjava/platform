package com.angkorteam.core.webui.panels;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/13/17.
 */
public class UserFooterPanel extends Panel {

    /**
     *
     */
    private static final long serialVersionUID = -6806536529978332923L;

    public UserFooterPanel() {
        super("footer");
    }

    public UserFooterPanel(IModel<?> model) {
        super("footer", model);
    }
}
