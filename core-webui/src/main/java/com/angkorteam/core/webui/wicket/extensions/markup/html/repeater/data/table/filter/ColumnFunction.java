package com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.filter;

import org.apache.wicket.model.IModel;
import org.apache.wicket.util.io.IClusterable;

import java.util.Map;

public interface ColumnFunction extends IClusterable {

    default ItemPanel function(String key, IModel<String> display, String value, Map<String, Object> object) {
        return null;
    }

}
