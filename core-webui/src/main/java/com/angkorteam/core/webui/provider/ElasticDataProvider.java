package com.angkorteam.core.webui.provider;

import com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.converter.IColumnConverter;
import com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.filter.Expression;
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.IFilterStateLocator;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.util.MapModel;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

public abstract class ElasticDataProvider extends SortableDataProvider<Map<String, Object>, String>
        implements IFilterStateLocator<Map<String, Expression<Object>>>, IFieldProvider {

    /**
     *
     */
    private static final long serialVersionUID = 2453015465083001162L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ElasticDataProvider.class);

    private final String index;

    private final String document;

    private Map<String, Expression<Object>> filterState;

    private Map<String, String> negotiator;

    private String groupBy;

    private List<String> fields;

    protected String from;

    private final Map<String, String> join;

    private final Map<String, QueryBuilder> userWhere;

    private final Map<String, QueryBuilder> userHaving;

    private final List<String> aggregate;

    public ElasticDataProvider(String index, String document) {
//		this.index = index + "_" + document;
        this.index = index;
        this.document = document;
        this.join = new LinkedHashMap<>();
        this.aggregate = new ArrayList<>();
        this.negotiator = new LinkedHashMap<>();
        this.fields = new LinkedList<>();
        this.filterState = new LinkedHashMap<>();
        this.userWhere = new LinkedHashMap<>();
        this.userHaving = new LinkedHashMap<>();
    }

    protected String getFrom() {
        return this.from;
    }

    private List<QueryBuilder> buildWhere() {
        List<QueryBuilder> where = new ArrayList<>();
        if (this.filterState != null && !this.filterState.isEmpty()) {
            for (Map.Entry<String, Expression<Object>> entry : this.filterState.entrySet()) {
                if (!this.aggregate.contains(entry.getKey())) {
                    Expression<Object> expression = entry.getValue();
                    if (expression == null) {
                        continue;
                    }
                    IColumnConverter<Object> converter = expression.getConverter();
                    QueryBuilder query = converter.buildElasticQuery(entry.getKey(),
                            this.negotiator.get(entry.getKey()), expression.getOperator(), expression.getFirstOperand(),
                            expression.getSecondOperand());
                    if (query != null) {
                        where.add(query);
                    }
                }
            }
        }
        List<QueryBuilder> userWhere = where();
        if (userWhere != null && !userWhere.isEmpty()) {
            where.addAll(userWhere);
        }
        return where;
    }

    public void applyWhere(String key, QueryBuilder filter) {
        this.userWhere.put(key, filter);
    }

    public QueryBuilder removeWhere(String key) {
        return this.userWhere.remove(key);
    }

    public void applyHaving(String key, QueryBuilder filter) {
        this.userHaving.put(key, filter);
    }

    public QueryBuilder removeHaving(String key) {
        return this.userHaving.remove(key);
    }

    public void applyJoin(String key, String join) {
        this.join.put(key, join);
    }

    public String removeJoin(String key) {
        return this.join.remove(key);
    }

    private List<QueryBuilder> buildHaving() {
        List<QueryBuilder> having = new ArrayList<>();
//        if (this.filterState != null && !this.filterState.isEmpty()) {
//            for (Map.Entry<String, Expression<Object>> entry : this.filterState.entrySet()) {
//                if (this.aggregate.contains(entry.getKey())) {
//                    Expression<Object> expression = entry.getValue();
//                    if (expression == null) {
//                        continue;
//                    }
//                    IColumnConverter<Object> converter = expression.getConverter();
//                    String condition = converter.convertToSQL(entry.getKey(), this.negotiator.get(entry.getKey()),
//                            expression.getOperator(), expression.getFirstOperand(), expression.getSecondOperand(),
//                            params);
//                    if (!Strings.isNullOrEmpty(condition)) {
//                        having.add(condition);
//                    }
//                }
//            }
//        }
//        List<String> userHaving = having();
//        if (userHaving != null && !userHaving.isEmpty()) {
//            having.addAll(userHaving);
//        }
        return having;
    }

    protected String buildOrderBy() {
        String orderBy = null;
        if (getSort() != null) {
            if (getSort().isAscending()) {
                orderBy = "`" + getSort().getProperty() + "` ASC";
            } else {
                orderBy = "`" + getSort().getProperty() + "` DESC";
            }
        }
        return orderBy;
    }

    public List<String> getFields() {
        List<String> fields = new ArrayList<>();
        for (Map.Entry<String, String> entry : this.negotiator.entrySet()) {
            String alias = entry.getKey();
            String value = entry.getValue();
            if (this.fields.contains(alias) && !fields.contains(value)) {
                fields.add(value + " " + "`" + alias + "`");
            }
        }
        return fields;
    }

    public void selectField(String key, String column) {
        this.fields.add(key);
        this.negotiator.put(key, column);
        if (isAggregate(column)) {
            this.aggregate.add(key);
        }
    }

    public void selectField(String key, String column, IColumnConverter<?> converter) {
        this.fields.add(key);
        this.negotiator.put(key, column);
        if (isAggregate(column)) {
            this.aggregate.add(key);
        }
    }

    private boolean isAggregate(String jdbcColumn) {
        String column = StringUtils.upperCase(jdbcColumn);
        if (StringUtils.startsWith(column, "AVG(") || StringUtils.startsWith(column, "AVG (")
                || StringUtils.startsWith(column, "BIT_AND(") || StringUtils.startsWith(column, "BIT_AND (")
                || StringUtils.startsWith(column, "BIT_OR(") || StringUtils.startsWith(column, "BIT_OR (")
                || StringUtils.startsWith(column, "BIT_XOR(") || StringUtils.startsWith(column, "BIT_XOR (")
                || StringUtils.startsWith(column, "COUNT(") || StringUtils.startsWith(column, "COUNT (")
                || StringUtils.startsWith(column, "GROUP_CONCAT(") || StringUtils.startsWith(column, "GROUP_CONCAT (")
                || StringUtils.startsWith(column, "MAX(") || StringUtils.startsWith(column, "MAX (")
                || StringUtils.startsWith(column, "MIN(") || StringUtils.startsWith(column, "MIN (")
                || StringUtils.startsWith(column, "STD(") || StringUtils.startsWith(column, "STD (")
                || StringUtils.startsWith(column, "STDDEV(") || StringUtils.startsWith(column, "STDDEV (")
                || StringUtils.startsWith(column, "STDDEV_POP(") || StringUtils.startsWith(column, "STDDEV_POP (")
                || StringUtils.startsWith(column, "STDDEV_SAMP(") || StringUtils.startsWith(column, "STDDEV_SAMP (")
                || StringUtils.startsWith(column, "SUM(") || StringUtils.startsWith(column, "SUM (")
                || StringUtils.startsWith(column, "VAR_POP(") || StringUtils.startsWith(column, "VAR_POP (")
                || StringUtils.startsWith(column, "VAR_SAMP(") || StringUtils.startsWith(column, "VAR_SAMP (")
                || StringUtils.startsWith(column, "VARIANCE(") || StringUtils.startsWith(column, "VARIANCE (")) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public final IModel<Map<String, Object>> model(Map<String, Object> object) {
        return new MapModel<>(object);
    }

    @Override
    public final Map<String, Expression<Object>> getFilterState() {
        return this.filterState;
    }

    @Override
    public final void setFilterState(Map<String, Expression<Object>> filterState) {
        this.filterState = filterState;
    }

    protected QueryBuilder buildQuery() {
        return buildQuery(null);
    }

    protected QueryBuilder buildQuery(String orderBy) {
        List<QueryBuilder> where = buildWhere();
        List<QueryBuilder> having = buildHaving();
        QueryBuilder builder = null;

        if (where != null && !where.isEmpty()) {
            BoolQueryBuilder bqb = QueryBuilders.boolQuery();
            for (QueryBuilder qb : where) {
                bqb.filter(qb);
            }
            builder = bqb;
        } else {
            builder = QueryBuilders.matchAllQuery();
        }
        if (!having.isEmpty()) {

        }
        if (this.groupBy != null && !"".equals(this.groupBy)) {

        }
        if (orderBy != null && !"".equals(orderBy)) {

        }
        return builder;
    }

    protected List<QueryBuilder> where() {
        return new ArrayList<>(this.userWhere.values());
    }

    protected List<QueryBuilder> having() {
        return new ArrayList<>(this.userHaving.values());
    }

    @Override
    public Iterator<Map<String, Object>> iterator(long first, long count) {
        try {
            SearchSourceBuilder source = new SearchSourceBuilder();
            source.size((int) count);
            source.from((int) first);
            QueryBuilder query = buildQuery();
            System.out.println(query.toString());
            source.query(query);
            source.fetchSource(this.fields.toArray(new String[this.fields.size()]), null);
            SearchRequest request = new SearchRequest(this.index);
            request.types(this.document);
            request.source(source);

            RestHighLevelClient client = getClient();
            SearchResponse response = client.search(request, RequestOptions.DEFAULT);
            List<Map<String, Object>> items = new ArrayList<>((int) response.getHits().getTotalHits());
            for (SearchHit hit : response.getHits()) {
                items.add(hit.getSourceAsMap());
            }
            return items.listIterator();
        } catch (IOException e) {
            throw new WicketRuntimeException(e);
        }
    }

    @Override
    public long size() {
        try {
            SearchSourceBuilder source = new SearchSourceBuilder();
            source.fetchSource(false);
            QueryBuilder query = buildQuery();
            System.out.println(query.toString());
            source.query(query);

            SearchRequest request = new SearchRequest(this.index);
            request.types(this.document);
            request.source(source);

            RestHighLevelClient client = getClient();
            SearchResponse response = client.search(request, RequestOptions.DEFAULT);
            return response.getHits().getTotalHits();
        } catch (IOException e) {
            throw new WicketRuntimeException(e);
        }
    }

    public void setGroupBy(String groupBy) {
        this.groupBy = groupBy;
    }

    public String getGroupBy() {
        return groupBy;
    }

    protected abstract RestHighLevelClient getClient();

    @Override
    public void selectField(String column) {
        this.selectField(column, column);
    }

    @Override
    public void selectField(String column, IColumnConverter<?> converter) {
        this.selectField(column, column, converter);
    }

}