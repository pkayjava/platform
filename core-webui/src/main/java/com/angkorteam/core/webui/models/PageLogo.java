package com.angkorteam.core.webui.models;

import org.apache.wicket.Page;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.io.Serializable;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class PageLogo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2825466353754212430L;

    private Class<? extends Page> page;

    private PageParameters parameters;

    private String small;

    private String large;

    private PageLogo() {
    }

    public Class<? extends Page> getPage() {
        return page;
    }

    public PageParameters getParameters() {
        return parameters;
    }

    public String getSmall() {
        return small;
    }

    public String getLarge() {
        return large;
    }

    public static class Builder {

        private Class<? extends Page> page;

        private PageParameters parameters;

        private String small;

        private String large;

        public PageLogo build() {
            PageLogo item = new PageLogo();
            item.page = this.page;
            item.parameters = this.parameters;
            item.small = this.small;
            item.large = this.large;
            return item;
        }

        public Builder with(String small, String large) {
            return with(small, large, null);
        }

        public Builder with(String small, String large, Class<? extends Page> page) {
            return with(small, large, page, null);
        }

        public Builder with(String small, String large, Class<? extends Page> page, PageParameters parameters) {
            this.small = small;
            this.large = large;
            this.page = page;
            this.parameters = parameters;
            return this;
        }

    }

}
