package com.angkorteam.core.webui.wicket.extensions.ajax.markup.html.modal;

import com.angkorteam.core.webui.wicket.functional.WicketTriConsumer;
import com.angkorteam.core.webui.wicket.markup.html.form.DateTextField;
import com.angkorteam.core.webui.wicket.markup.html.form.select2.Select2SingleChoice;
import org.apache.wicket.Component;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.model.IModel;

public class ModalWindow extends org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow
        implements org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow.CloseButtonCallback,
        org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow.WindowClosedCallback {

    /**
     *
     */
    private static final long serialVersionUID = 816531705170182749L;

    private WicketTriConsumer<String, String, AjaxRequestTarget> onClose;
    private boolean callback = true;
    private String signalId;
    private PopupPanel popup;

    private boolean hasInitialHeight = false;

    public ModalWindow(String id) {
        super(id);
        setCloseButtonCallback(this);
        setWindowClosedCallback(this);
    }

    public ModalWindow(String id, IModel<?> model) {
        super(id, model);
        setCloseButtonCallback(this);
        setWindowClosedCallback(this);
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);
        if (!this.hasInitialHeight) {
            response.render(
                    CssHeaderItem.forCSS("div.w_content_container {height:auto !important}", "ModalWindow-AUTO"));
        }
    }

    @Override
    public org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow setInitialHeight(int initialHeight) {
        this.hasInitialHeight = true;
        return super.setInitialHeight(initialHeight);
    }

    public void setSignalId(String signalId) {
        this.signalId = signalId;
    }

    public void setOnClose(WicketTriConsumer<String, String, AjaxRequestTarget> onClose) {
        this.onClose = onClose;
    }

    @Override
    public final boolean onCloseButtonClicked(AjaxRequestTarget target) {
        target.appendJavaScript(Select2SingleChoice.REMOVE_POPUP_UP_SCRIPT);
        target.appendJavaScript(DateTextField.REMOVE_POPUP_UP_SCRIPT);
        this.callback = false;
        return true;
    }

    @Override
    public ModalWindow setContent(Component component) {
        if (component instanceof PopupPanel) {
            this.popup = (PopupPanel) component;
            this.popup.window = this;
        } else {
            throw new WicketRuntimeException("content must be extends from " + PopupPanel.class.getName());
        }
        return (ModalWindow) super.setContent(component);
    }

    @Override
    public final void onClose(AjaxRequestTarget target) {
        target.appendJavaScript(Select2SingleChoice.REMOVE_POPUP_UP_SCRIPT);
        target.appendJavaScript(DateTextField.REMOVE_POPUP_UP_SCRIPT);
        if (this.callback) {
            if (this.onClose != null) {
                this.onClose.accept(this.popup.name, this.signalId, target);
            }
        }
        this.callback = true;
    }

}
