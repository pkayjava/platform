package com.angkorteam.core.webui;

/**
 * Created by socheatkhauv on 6/13/17.
 */
public enum BackgroundColor {
    Gray("bg-gray"), GrayLight("bg-gray-light"), Black("bg-black"), Red("bg-red"), Yellow("bg-yellow"), Aqua("bg-aqua"),
    Blue("bg-blue"), LightBlue("bg-light-blue"), Green("bg-green"), Navy("bg-navy"), Teal("bg-teal"), Olive("bg-olive"),
    Lime("bg-lime"), Orange("bg-orange"), Fuchsia("bg-fuchsia "), Purple("bg-purple"), Maroon("bg-maroon"),
    GrayActive("bg-gray-active"), BlackActive("bg-black-active"), RedActive("bg-red-active"),
    YellowActive("bg-yellow-active"), AquaActive("bg-aqua-active"), BlueActive("bg-blue-active"),
    LightBlueActive("bg-light-blue-active"), GreenActive("bg-green-active"), NavyActive("bg-navy-active"),
    TealActive("bg-teal-active"), OliveActive("bg-olive-active"), LimeActive("bg-lime-active"),
    OrangeActive("bg-orange-active"), FuchsiaActive("bg-fuchsia-active"), PurpleActive("bg-purple-active"),
    MaroonActive("bg-maroon-active");

    private String literal;

    BackgroundColor(String literal) {
        this.literal = literal;
    }

    public String getLiteral() {
        return literal;
    }
}
