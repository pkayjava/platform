package com.angkorteam.core.webui.wicket.chart.morris;

import com.angkorteam.core.webui.ReferenceUtilities;
import com.google.gson.Gson;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebComponent;
import org.apache.wicket.markup.parser.XmlTag.TagType;
import org.apache.wicket.model.IModel;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class DonutChart extends WebComponent {

    /**
     *
     */
    private static final long serialVersionUID = -2903910336246590831L;

    private IModel<BarDataset> dataset;

    public DonutChart(String id, IModel<BarDataset> model) {
        super(id);
        setOutputMarkupId(true);
        this.dataset = model;
    }

    @Override
    protected void onComponentTag(final ComponentTag tag) {
        checkComponentTag(tag, "div");
        super.onComponentTag(tag);
        if (tag.isOpenClose()) {
            tag.setType(TagType.OPEN);
        }
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);

        ReferenceUtilities.renderJavascript(response, "<!-- Morris -->");
        ReferenceUtilities.renderJavascript(response,
                ReferenceUtilities.AdminLTE + "/bower_components/raphael/raphael.min.js");
        ReferenceUtilities.renderJavascript(response,
                ReferenceUtilities.AdminLTE + "/bower_components/morris.js/morris.min.js");

        String markupId = getMarkupId();
        this.dataset.getObject().setElement(markupId);

        Gson gson = new Gson();

        String chart = String.format("new Morris.Donut(%s)", gson.toJson(this.dataset.getObject()));
        response.render(OnDomReadyHeaderItem.forScript(chart));
    }

    public static class BarItem implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = -5796275283522032532L;

        private String label;

        private int value;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

    }

    public static class BarDataset implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = 6960809461861064409L;

        private String element;

        private String xkey;

        private List<String> ykeys = new LinkedList<>();

        private List<String> labels = new LinkedList<>();

        private List<String> colors = new LinkedList<>();

        private String hideOver = "auto";

        private boolean resize = true;

        private List<BarItem> data = new LinkedList<>();

        public String getElement() {
            return element;
        }

        public void setElement(String element) {
            this.element = element;
        }

        public String getXkey() {
            return xkey;
        }

        public void setXkey(String xkey) {
            this.xkey = xkey;
        }

        public String getHideOver() {
            return hideOver;
        }

        public boolean isResize() {
            return resize;
        }

        public List<String> getYkeys() {
            return ykeys;
        }

        public List<String> getLabels() {
            return labels;
        }

        public List<String> getColors() {
            return colors;
        }

        public List<BarItem> getData() {
            return data;
        }

    }

}