package com.angkorteam.core.webui.wicket.markup.html.form;

import com.angkorteam.core.webui.ReferenceUtilities;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.IModel;

/**
 * Created by socheat on 6/1/16.
 */
public class ColorTextField extends TextField<String> {

    /**
     *
     */
    private static final long serialVersionUID = 3933130617714702467L;

    public ColorTextField(String id) {
        super(id, String.class);
    }

    public ColorTextField(String id, IModel<String> model) {
        super(id, model, String.class);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        setOutputMarkupId(true);
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);
        String markupId = this.getMarkupId(true);
        response.render(CssHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.AdminLTE
                + "/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css")));
        response.render(JavaScriptHeaderItem
                .forReference(getApplication().getJavaScriptLibrarySettings().getJQueryReference()));
        response.render(JavaScriptHeaderItem
                .forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.BOOTSTRAP_JS)));
        response.render(
                JavaScriptHeaderItem.forReference(ReferenceUtilities.loadResourceReference(ReferenceUtilities.AdminLTE
                        + "/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js")));
        response.render(OnDomReadyHeaderItem.forScript("$('#" + markupId + "').colorpicker()"));
    }
}