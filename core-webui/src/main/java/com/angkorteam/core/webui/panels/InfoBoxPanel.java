package com.angkorteam.core.webui.panels;

import com.angkorteam.core.webui.Emoji;
import com.angkorteam.core.webui.models.InfoBox;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class InfoBoxPanel extends Panel {

    /**
     *
     */
    private static final long serialVersionUID = 6467838372824255203L;

    public InfoBoxPanel(String id) {
        super(id);
    }

    public InfoBoxPanel(String id, IModel<InfoBox> model) {
        super(id, model);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        InfoBox infoBox = (InfoBox) getDefaultModelObject();

        WebMarkupContainer bg = new WebMarkupContainer("bg");
        this.add(bg);

        if (infoBox.getBackgroundColor() != null) {
            bg.add(AttributeModifier.append("class", infoBox.getBackgroundColor().getLiteral()));
        }

        Label icon = new Label("icon");
        bg.add(icon);
        if (infoBox.getIcon().getType() == Emoji.FA) {
            icon.add(AttributeModifier.append("class", "fa " + infoBox.getIcon().getLiteral()));
        } else {
            icon.add(AttributeModifier.append("class", "ion " + infoBox.getIcon().getLiteral()));
        }

        if (infoBox.getPage() == null) {
            Fragment fragment = new Fragment("titleBlock", "fragmentTitleText", this);
            bg.add(fragment);
            Label title = new Label("title", infoBox.getTitle());
            fragment.add(title);
        } else {
            Fragment fragment = new Fragment("titleBlock", "fragmentTitleLink", this);
            bg.add(fragment);
            BookmarkablePageLink<Void> link = new BookmarkablePageLink<Void>("link", infoBox.getPage(),
                    infoBox.getParameters());
            fragment.add(link);
            if (infoBox.getBackgroundColor() != null) {
                link.add(AttributeModifier.replace("style", "color: white"));
            } else {
                link.add(AttributeModifier.replace("style", "color: black"));
            }
            Label title = new Label("title", infoBox.getTitle());
            link.add(title);
        }

        Label description = new Label("description", infoBox.getDescription());
        bg.add(description);

        if (infoBox.getExtra() != null) {
            bg.add(infoBox.getExtra());
        } else {
            Fragment fragment = new Fragment("extra", "fragmentEmpty", this);
            bg.add(fragment);
        }
    }

}
