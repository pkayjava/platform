package com.angkorteam.core.webui.wicket.markup.html.form;

import org.apache.wicket.extensions.ajax.markup.html.autocomplete.AbstractAutoCompleteBehavior;
import org.apache.wicket.extensions.ajax.markup.html.autocomplete.AutoCompleteSettings;
import org.apache.wicket.extensions.ajax.markup.html.autocomplete.AutoCompleteTextField;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.resource.CssResourceReference;
import org.apache.wicket.request.resource.ResourceReference;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class SmartTextField extends AutoCompleteTextField<String> {

    /**
     *
     */
    private static final long serialVersionUID = 6024731169531918099L;

    public static final ResourceReference CSS = new CssResourceReference(Form.class, "smarttext.css");

    private SmartTextProvider provider;

    public SmartTextField(String id, IModel<String> model, SmartTextProvider provider) {
        super(id, model, new AutoCompleteSettings().setShowListOnEmptyInput(false).setUseSmartPositioning(true)
                .setMinInputLength(1));
        this.provider = provider;
    }

    public SmartTextField(String id, int inputLength, IModel<String> model, SmartTextProvider provider) {
        super(id, model, new AutoCompleteSettings().setShowListOnEmptyInput(false).setUseSmartPositioning(true)
                .setMinInputLength(inputLength));
        this.provider = provider;
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);
        response.render(JavaScriptHeaderItem.forReference(AbstractAutoCompleteBehavior.AUTOCOMPLETE_JS));
        response.render(CssHeaderItem.forReference(CSS));
    }

    @Override
    protected Iterator<String> getChoices(String input) {
        if (this.provider != null) {
            List<String> choices = this.provider.toChoices(input);
            if (choices != null) {
                return choices.listIterator();
            } else {
                return Collections.emptyIterator();
            }
        } else {
            return Collections.emptyIterator();
        }
    }
}
