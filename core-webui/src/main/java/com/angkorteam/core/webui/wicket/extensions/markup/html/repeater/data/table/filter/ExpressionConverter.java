package com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.filter;

import com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.converter.IColumnConverter;
import org.apache.wicket.util.convert.ConversionException;
import org.apache.wicket.util.convert.IConverter;

import java.util.Locale;

public class ExpressionConverter<T> implements IConverter<Expression<T>> {

    /**
     *
     */
    private static final long serialVersionUID = -4460814600995037209L;

    private final String key;

    private final IColumnConverter<T> converter;

    public ExpressionConverter(String key, IColumnConverter<T> converter) {
        this.key = key;
        this.converter = converter;
    }

    @Override
    public Expression<T> convertToObject(String value, Locale locale) throws ConversionException {
        return new Expression<>(this.key, value, this.converter);
    }

    @Override
    public String convertToString(Expression<T> value, Locale locale) {
        if (value == null) {
            return null;
        } else {
            return value.getFilter();
        }
    }

    public IColumnConverter<T> getConverter() {
        return converter;
    }

}
