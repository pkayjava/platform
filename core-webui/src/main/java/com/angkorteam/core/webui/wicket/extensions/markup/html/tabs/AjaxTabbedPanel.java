package com.angkorteam.core.webui.wicket.extensions.markup.html.tabs;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.model.IModel;

import java.util.List;

/**
 * Created by socheat on 7/4/16.
 */
public class AjaxTabbedPanel<T extends ITab>
        extends org.apache.wicket.extensions.ajax.markup.html.tabs.AjaxTabbedPanel<T> {

    /**
     *
     */
    private static final long serialVersionUID = -8919634113963483093L;

    private List<T> tabs;

    public AjaxTabbedPanel(String id, List<T> tabs) {
        super(id, tabs);
        this.tabs = tabs;
    }

    public AjaxTabbedPanel(String id, List<T> tabs, IModel<Integer> model) {
        super(id, tabs, model);
        this.tabs = tabs;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        setOutputMarkupId(true);
        add(AttributeModifier.replace("class", "nav-tabs-custom"));
        if (this.tabs != null && !this.tabs.isEmpty()) {
            for (T tab : this.tabs) {
                tab.tab = this;
            }
        }
    }

    @Override
    protected String getSelectedTabCssClass() {
        return "active";
    }

    @Override
    protected WebMarkupContainer newTabsContainer(String id) {
        WebMarkupContainer container = super.newTabsContainer(id);
        container.setRenderBodyOnly(true);
        return container;
    }

    @Override
    protected WebMarkupContainer newLink(String linkId, int index) {
        if (this.tabs.get(index).isEnabled()) {
            return super.newLink(linkId, index);
        } else {
            WebMarkupContainer link = new WebMarkupContainer(linkId);
            link.add(AttributeModifier.replace("href", "#"));
            return link;
        }
    }

}
