package com.angkorteam.core.webui;

import java.io.Serializable;

/**
 * Created by socheatkhauv on 6/11/17.
 */
public class AdminLTEOption implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6598688634214260662L;

    public boolean JQuery;
    public boolean JQueryUI;
    public boolean Bootstrap;
    public boolean MorrisJSChart;
    public boolean Sparkline;
    public boolean JVectorMap;
    public boolean JQueryKnobChart;
    public boolean DateRangePicker;
    public boolean DatePicker;
    public boolean BootstrapWYSIHTML5;
    public boolean SlimScroll;
    public boolean FastClick;
    public boolean AdminLTE;
    public boolean Dashboard;
    public boolean Dashboard2;
    public boolean Demo;
    public boolean FontAwesome;
    public boolean Ionicons;
    public boolean HTML5ShimAndRespondJS;
    public boolean GoogleFont;
    public boolean ICheck;
    // chart
    public boolean ChartJS;
    public boolean Flot;
    public boolean Morris;
    public boolean Inline;

}
