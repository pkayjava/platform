package com.angkorteam.core.webui.models;

import com.angkorteam.core.webui.Emoji;
import com.angkorteam.core.webui.NavBarMenuItemType;
import com.angkorteam.core.webui.ProgressBarColor;
import com.angkorteam.core.webui.TextColor;
import com.angkorteam.core.webui.panels.UserItemPanel;
import org.apache.wicket.Page;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.io.Serializable;

/**
 * Created by socheatkhauv on 6/12/17.
 */
public class NavBarMenuItem implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 3672306226483586186L;

    private Emoji icon;

    private TextColor iconColor;

    private String label;

    private String description;

    private String image;

    private String small;

    private Emoji smallIcon;

    private TextColor smallIconColor;

    private Integer progressBarValue;

    private ProgressBarColor progressBarColor;

    private Class<? extends Page> page;

    private PageParameters parameters;

    private Panel panel;

    private NavBarMenuItemType type;

    private NavBarMenuItem() {
    }

    public Emoji getIcon() {
        return icon;
    }

    public ProgressBarColor getProgressBarColor() {
        return progressBarColor;
    }

    public Class<? extends Page> getPage() {
        return page;
    }

    public PageParameters getParameters() {
        return parameters;
    }

    public TextColor getIconColor() {
        return iconColor;
    }

    public String getLabel() {
        return label;
    }

    public Panel getPanel() {
        return panel;
    }

    public String getDescription() {
        return description;
    }

    public NavBarMenuItem setDescription(String description) {
        this.description = description;
        return this;
    }

    public Integer getProgressBarValue() {
        return progressBarValue;
    }

    public String getImage() {
        return image;
    }

    public String getSmall() {
        return small;
    }

    public Emoji getSmallIcon() {
        return smallIcon;
    }

    public TextColor getSmallIconColor() {
        return smallIconColor;
    }

    public NavBarMenuItemType getType() {
        return type;
    }

    public static class Builder {

        private Emoji icon;

        private TextColor iconColor;

        private String label;

        private String description;

        private String image;

        private String small;

        private Emoji smallIcon;

        private TextColor smallIconColor;

        private Integer progressBarValue;

        private ProgressBarColor progressBarColor;

        private Class<? extends Page> page;

        private PageParameters parameters;

        private Panel panel;

        private NavBarMenuItemType type;

        public Builder() {
        }

        public NavBarMenuItem build() {
            NavBarMenuItem item = new NavBarMenuItem();
            item.icon = this.icon;
            item.iconColor = this.iconColor;
            item.label = this.label;
            item.description = this.description;
            item.image = this.image;
            item.small = this.small;
            item.smallIcon = this.smallIcon;
            item.smallIconColor = this.smallIconColor;
            item.progressBarValue = this.progressBarValue;
            item.progressBarColor = this.progressBarColor;
            item.page = this.page;
            item.parameters = this.parameters;
            item.panel = this.panel;
            item.type = this.type;
            return item;
        }

        public Builder withImage(String image, String label, Class<? extends Page> page) {
            return withImage(image, label, page, new PageParameters(), "");
        }

        public Builder withImage(String image, String label, Class<? extends Page> page, PageParameters parameters,
                                 String description) {
            this.type = NavBarMenuItemType.ItemImage;
            this.label = label;
            this.page = page;
            this.image = image;
            this.parameters = parameters;
            this.description = description;
            return this;
        }

        public Builder withSmall(String small, Emoji icon, TextColor color) {
            this.small = small;
            this.smallIcon = icon;
            this.smallIconColor = color;
            return this;
        }

        public Builder withIcon(Emoji icon, String label, Class<? extends Page> page) {
            return withIcon(icon, label, page, new PageParameters(), TextColor.Red);
        }

        public Builder withIcon(Emoji icon, String label, Class<? extends Page> page, PageParameters parameters,
                                TextColor iconColor) {
            this.type = NavBarMenuItemType.ItemIcon;
            this.label = label;
            this.page = page;
            this.icon = icon;
            this.iconColor = iconColor;
            this.parameters = parameters;
            return this;
        }

        public Builder withPanel(UserItemPanel panel) {
            this.type = NavBarMenuItemType.ItemPanel;
            this.panel = panel;
            return this;
        }

        public Builder withProgressBar(Integer progressBarValue, String label, Class<? extends Page> page) {
            return withProgressBar(progressBarValue, label, page, new PageParameters(), ProgressBarColor.Red);
        }

        public Builder withProgressBar(Integer progressBarValue, String label, Class<? extends Page> page,
                                       PageParameters parameters, ProgressBarColor progressBarColor) {
            this.type = NavBarMenuItemType.ItemProgressBar;
            this.label = label;
            this.page = page;
            this.parameters = parameters;
            this.progressBarValue = progressBarValue;
            this.progressBarColor = progressBarColor;
            return this;
        }

    }

}
