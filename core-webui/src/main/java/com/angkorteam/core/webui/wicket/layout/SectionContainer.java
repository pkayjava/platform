package com.angkorteam.core.webui.wicket.layout;

import org.apache.wicket.markup.html.WebMarkupContainer;

public class SectionContainer extends WebMarkupContainer {

    /**
     *
     */
    private static final long serialVersionUID = -7141734928467790924L;

    protected SectionContainer(String id) {
        super(id);
        setOutputMarkupId(true);
    }

    public UIRow newUIRow(String id) {
        UIRow row = new UIRow(id);
        add(row);
        return row;
    }

}
