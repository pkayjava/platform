package com.angkorteam.core.webui.wicket.extensions.markup.html.tabs;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.extensions.markup.html.tabs.ITab;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.model.IModel;

import java.util.List;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class TabbedPanel<T extends ITab> extends org.apache.wicket.extensions.markup.html.tabs.TabbedPanel<T> {

    /**
     *
     */
    private static final long serialVersionUID = 6277003017054964423L;

    public TabbedPanel(String id, List<T> tabs) {
        super(id, tabs);
    }

    public TabbedPanel(String id, List<T> tabs, IModel<Integer> model) {
        super(id, tabs, model);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(AttributeModifier.replace("class", "nav-tabs-custom"));
    }

    @Override
    protected String getSelectedTabCssClass() {
        return "active";
    }

    @Override
    protected WebMarkupContainer newTabsContainer(String id) {
        WebMarkupContainer container = super.newTabsContainer(id);
        container.setRenderBodyOnly(true);
        return container;
    }
}
