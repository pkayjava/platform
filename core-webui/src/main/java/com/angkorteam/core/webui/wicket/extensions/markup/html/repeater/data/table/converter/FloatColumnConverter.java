package com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.converter;

import com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.filter.Operator;
import org.apache.wicket.util.convert.ConversionException;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import java.util.Map;

public class FloatColumnConverter implements IColumnConverter<Float> {

    /**
     *
     */
    private static final long serialVersionUID = -2948664583865434869L;

    @Override
    public Float convertToObject(String key, String value) throws ConversionException {
        if (value == null || "".equals(value)) {
            return null;
        }
        try {
            return Float.valueOf(value);
        } catch (NumberFormatException e) {
            throw new ConversionException(e);
        }
    }

    @Override
    public String convertToString(String key, Float value) {
        if (value == null) {
            return "";
        }
        return String.valueOf(value);
    }

    @Override
    public String buildJdbcQuery(String key, String column, Operator operator, Float firstOperand, Float secondOperand,
                                 Map<String, Object> params) {
        String condition = null;
        String firstParam = key + "_1st";
        String secondParam = key + "_2nd";
        if (operator == Operator.Equal || operator == Operator.Like) {
            condition = column + " = :" + firstParam;
            params.put(firstParam, firstOperand);
        } else if (operator == Operator.NotEqual || operator == Operator.NotLike) {
            condition = column + " != :" + firstParam;
            params.put(firstParam, firstOperand);
        } else if (operator == Operator.GreaterThan) {
            condition = column + " > :" + firstParam;
            params.put(firstParam, firstOperand);
        } else if (operator == Operator.GreaterThanOrEqual) {
            condition = column + " >= :" + firstParam;
            params.put(firstParam, firstOperand);
        } else if (operator == Operator.LessThan) {
            condition = column + " < :" + firstParam;
            params.put(firstParam, firstOperand);
        } else if (operator == Operator.LessThanOrEqual) {
            condition = column + " <= :" + firstParam;
            params.put(firstParam, firstOperand);
        } else if (operator == Operator.Between) {
            if (firstOperand < secondOperand) {
                condition = column + " BETWEEN :" + firstParam + " AND :" + secondParam;
                params.put(firstParam, firstOperand);
                params.put(secondParam, secondOperand);
            } else {
                condition = column + " BETWEEN :" + secondParam + " AND :" + firstParam;
                params.put(firstParam, firstOperand);
                params.put(secondParam, secondOperand);
            }
        } else if (operator == Operator.NotBetween) {
            if (firstOperand < secondOperand) {
                condition = column + " NOT BETWEEN :" + firstParam + " AND :" + secondParam;
                params.put(firstParam, firstOperand);
                params.put(secondParam, secondOperand);
            } else {
                condition = column + " NOT BETWEEN :" + secondParam + " AND :" + firstParam;
                params.put(firstParam, firstOperand);
                params.put(secondParam, secondOperand);
            }
        }
        return condition;
    }

    @Override
    public QueryBuilder buildElasticQuery(String key, String column, Operator operator, Float firstOperand,
                                          Float secondOperand) {
        if (operator == Operator.Equal || operator == Operator.Like) {
            return QueryBuilders.termQuery(key, firstOperand);
        } else if (operator == Operator.NotEqual || operator == Operator.NotLike) {
            return QueryBuilders.rangeQuery(key).gt(firstOperand).lt(firstOperand);
        } else if (operator == Operator.GreaterThan) {
            return QueryBuilders.rangeQuery(key).gt(firstOperand);
        } else if (operator == Operator.GreaterThanOrEqual) {
            return QueryBuilders.rangeQuery(key).gte(firstOperand);
        } else if (operator == Operator.LessThan) {
            return QueryBuilders.rangeQuery(key).lt(firstOperand);
        } else if (operator == Operator.LessThanOrEqual) {
            return QueryBuilders.rangeQuery(key).lte(firstOperand);
        } else if (operator == Operator.Between) {
            if (firstOperand < secondOperand) {
                return QueryBuilders.rangeQuery(key).from(firstOperand).to(secondOperand);
            } else {
                return QueryBuilders.rangeQuery(key).from(secondOperand).to(firstOperand);
            }
        } else if (operator == Operator.NotBetween) {
            if (firstOperand < secondOperand) {
                return QueryBuilders.rangeQuery(key).lt(firstOperand).gt(secondOperand);
            } else {
                return QueryBuilders.rangeQuery(key).lt(secondOperand).gt(firstOperand);
            }
        }
        return null;
    }

}
