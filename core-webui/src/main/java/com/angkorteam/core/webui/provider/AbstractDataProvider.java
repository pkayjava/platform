package com.angkorteam.core.webui.provider;

import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.IFilterStateLocator;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class AbstractDataProvider extends SortableDataProvider<Map<String, Object>, String>
        implements IFilterStateLocator<Map<String, String>> {

    /**
     *
     */
    private static final long serialVersionUID = 4230853566957215155L;

    private Map<String, String> filterState;

    protected AbstractDataProvider() {
        this.filterState = new HashMap<>();
    }

    @Override
    public Map<String, String> getFilterState() {
        return this.filterState;
    }

    @Override
    public void setFilterState(Map<String, String> filterState) {
        this.filterState = filterState;
    }

    @Override
    public Iterator<? extends Map<String, Object>> iterator(long first, long count) {
        return null;
    }

    @Override
    public long size() {
        return 0;
    }

    @Override
    public IModel<Map<String, Object>> model(Map<String, Object> object) {
        if (object != null) {
            return Model.ofMap(object);
        } else {
            return null;
        }
    }
}
