package com.angkorteam.core.webui.wicket.markup.html.form;

import java.io.Serializable;
import java.util.List;

public interface SmartTextProvider extends Serializable {

    List<String> toChoices(String input);

}
