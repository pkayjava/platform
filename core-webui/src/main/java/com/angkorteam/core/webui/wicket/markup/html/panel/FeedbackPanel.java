package com.angkorteam.core.webui.wicket.markup.html.panel;

import org.apache.wicket.feedback.IFeedbackMessageFilter;

/**
 * Created by socheat on 3/16/15.
 */
public class FeedbackPanel extends org.apache.wicket.markup.html.panel.FeedbackPanel {

    /**
     *
     */
    private static final long serialVersionUID = 2638799339364446222L;

    public FeedbackPanel(String id) {
        super(id);
    }

    public FeedbackPanel(String id, IFeedbackMessageFilter filter) {
        super(id, filter);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        setOutputMarkupId(true);
    }
}
