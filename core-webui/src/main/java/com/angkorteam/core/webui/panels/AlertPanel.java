package com.angkorteam.core.webui.panels;

import com.angkorteam.core.webui.Emoji;
import com.angkorteam.core.webui.models.Alert;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class AlertPanel extends Panel {

    /**
     *
     */
    private static final long serialVersionUID = -4846114056529590125L;

    public AlertPanel(String id) {
        super(id);
    }

    public AlertPanel(String id, IModel<Alert> model) {
        super(id, model);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        WebMarkupContainer root = new WebMarkupContainer("root");
        add(root);

        Alert model = (Alert) getDefaultModelObject();

        WebMarkupContainer alert = new WebMarkupContainer("alert");
        root.add(alert);

        if (model.getType() != null) {
            alert.add(AttributeModifier.append("class", model.getType().getLiteral()));
        }

        Label icon = new Label("icon");
        root.add(icon);
        if (model.getIcon().getType() == Emoji.FA) {
            icon.add(AttributeModifier.append("class", "fa " + model.getIcon().getLiteral()));
        } else {
            icon.add(AttributeModifier.append("class", "ion " + model.getIcon().getLiteral()));
        }

        Label title = new Label("title", model.getTitle());
        root.add(title);

        Label description = new Label("description", model.getDescription());
        root.add(description);

    }
}
