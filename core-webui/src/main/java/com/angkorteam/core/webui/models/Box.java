package com.angkorteam.core.webui.models;

import com.angkorteam.core.webui.BoxColor;
import com.angkorteam.core.webui.panels.BoxBodyPanel;
import com.angkorteam.core.webui.panels.BoxFooterPanel;
import com.angkorteam.core.webui.panels.BoxHeaderPanel;

import java.io.Serializable;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class Box implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7490679009995901368L;

    private BoxHeaderPanel header;

    private BoxFooterPanel footer;

    private BoxBodyPanel body;

    private BoxColor color;

    private Box() {
    }

    public BoxColor getColor() {
        return color;
    }

    public BoxHeaderPanel getHeader() {
        return header;
    }

    public BoxFooterPanel getFooter() {
        return footer;
    }

    public BoxBodyPanel getBody() {
        return body;
    }

    public static class Builder {

        private BoxHeaderPanel header;

        private BoxFooterPanel footer;

        private BoxBodyPanel body;

        private BoxColor color;

        public Builder() {
        }

        public Builder withHeader(BoxHeaderPanel header) {
            this.header = header;
            return this;
        }

        public Builder withBody(BoxBodyPanel body) {
            this.body = body;
            return this;
        }

        public Builder withColor(BoxColor color) {
            this.color = color;
            return this;
        }

        public Builder withFooter(BoxFooterPanel footer) {
            this.footer = footer;
            return this;
        }

        public Box build() {
            Box item = new Box();
            item.header = this.header;
            item.footer = this.footer;
            item.body = this.body;
            item.color = this.color;
            return item;
        }

    }

}
