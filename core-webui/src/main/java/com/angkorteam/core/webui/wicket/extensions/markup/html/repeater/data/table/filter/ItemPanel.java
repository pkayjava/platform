package com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.filter;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public abstract class ItemPanel extends Panel {

    /**
     *
     */
    private static final long serialVersionUID = -3045848430586617998L;

    public ItemPanel(IModel<?> model) {
        super("item", model);
    }

}
