package com.angkorteam.core.webui.wicket.functional;

import org.apache.wicket.util.io.IClusterable;

import java.util.Objects;
import java.util.function.Function;

@FunctionalInterface
public interface WicketTriFunction<A, B, C, R> extends IClusterable {

    R apply(A a, B b, C c);

    default <V> WicketTriFunction<A, B, C, V> andThen(Function<? super R, ? extends V> after) {
        Objects.requireNonNull(after);
        return (A a, B b, C c) -> after.apply(apply(a, b, c));
    }
}
