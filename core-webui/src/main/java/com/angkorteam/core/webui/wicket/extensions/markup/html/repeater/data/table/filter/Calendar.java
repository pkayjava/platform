package com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.filter;

public enum Calendar {
    Date, DateTime, Time;
}