package com.angkorteam.core.webui.models;

import com.angkorteam.core.webui.ProgressBarColor;
import com.angkorteam.core.webui.ProgressBarSize;

import java.io.Serializable;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class ProgressBar implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6888351362844234344L;

    private boolean active;

    private boolean vertical;

    private ProgressBarColor color;

    private ProgressBarSize size;

    private Integer value;

    private ProgressBar() {
    }

    public boolean isActive() {
        return active;
    }

    public boolean isVertical() {
        return vertical;
    }

    public ProgressBarColor getColor() {
        return color;
    }

    public ProgressBarSize getSize() {
        return size;
    }

    public Integer getValue() {
        return value;
    }

    public static class Builder {

        private boolean active;

        private boolean vertical;

        private ProgressBarColor color;

        private ProgressBarSize size;

        private Integer value;

        public ProgressBar build() {
            ProgressBar item = new ProgressBar();
            item.active = this.active;
            item.vertical = this.active;
            item.color = this.color;
            item.size = this.size;
            item.value = this.value;
            item.vertical = this.vertical;
            return item;
        }

        public Builder withActive(boolean active) {
            this.active = active;
            return this;
        }

        public Builder withVertical(boolean vertical) {
            this.vertical = vertical;
            return this;
        }

        public Builder withColor(ProgressBarColor color) {
            this.color = color;
            return this;
        }

        public Builder withSize(ProgressBarSize size) {
            this.size = size;
            return this;
        }

        public Builder withValue(Integer value) {
            this.value = value;
            return this;
        }

    }
}
