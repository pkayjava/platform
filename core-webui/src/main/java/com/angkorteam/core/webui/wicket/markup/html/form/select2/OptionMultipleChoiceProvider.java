package com.angkorteam.core.webui.wicket.markup.html.form.select2;

import com.angkorteam.core.query.SelectQuery;
import com.angkorteam.core.spring.jdbc.JdbcNamed;
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.model.IModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by socheat on 12/5/16.
 */
public abstract class OptionMultipleChoiceProvider extends MultipleChoiceProvider<Option> {

    /**
     *
     */
    private static final long serialVersionUID = 266367473515642887L;

    private final String table;

    private final String idField;

    private final String queryField;

    private final String labelField;

    private final Map<String, String> where;

    private final Map<String, String> join;

    private boolean disabled = false;

    private final String orderBy;

    public OptionMultipleChoiceProvider(String table, String idField) {
        this(table, idField, idField);
    }

    public OptionMultipleChoiceProvider(String table, String idField, String queryField) {
        this(table, idField, queryField, queryField + " ASC");
    }

    public OptionMultipleChoiceProvider(String table, String idField, String queryField, String orderBy) {
        this(table, idField, queryField, orderBy, queryField);
    }

    public OptionMultipleChoiceProvider(String table, String idField, String queryField, String orderBy,
                                        String labelField) {
        this.table = table;
        this.idField = idField;
        this.labelField = labelField;
        this.queryField = queryField;
        this.where = new HashMap<String, String>();
        this.join = new HashMap<String, String>();
        this.orderBy = orderBy;
    }

    public void applyJoin(String key, String join) {
        this.join.put(key, join);
    }

    public String removeJoin(String key) {
        return this.join.remove(key);
    }

    public void applyWhere(String key, String filter) {
        this.where.put(key, filter);
    }

    public String removeWhere(String key) {
        return this.where.remove(key);
    }

    @Override
    public List<Option> toChoices(List<String> ids) {
        if (isDisabled()) {
            return new ArrayList<>();
        }
        JdbcNamed named = getNamed();
        SelectQuery selectQuery = new SelectQuery(this.table);
        if (!this.join.isEmpty()) {
            for (String join : this.join.values()) {
                selectQuery.addJoin(join);
            }
        }
        selectQuery.addField(this.idField + " AS id");
        selectQuery.addField(this.labelField + " AS text");
        selectQuery.addWhere(this.idField + " in (:id)", String.class, ids);
        return named.queryForList(selectQuery.toSQL(), selectQuery.getParam(), Option.class);
    }

    @Override
    public List<Option> query(String s, int i) {
        if (isDisabled()) {
            return new ArrayList<>();
        }
        JdbcNamed named = getNamed();
        SelectQuery selectQuery = new SelectQuery(this.table);
        if (!this.join.isEmpty()) {
            for (String join : this.join.values()) {
                selectQuery.addJoin(join);
            }
        }
        selectQuery.addField(this.idField + " AS id");
        selectQuery.addField(this.labelField + " AS text");
        s = StringUtils.trimToEmpty(s);
        if (s != null && !"".equals(s)) {
            selectQuery.addWhere("LOWER(" + this.queryField + ") like LOWER(:value)", "value", s + "%");
        }
        if (this.where != null && !this.where.isEmpty()) {
            for (String where : this.where.values()) {
                if (where != null && !"".equals(where)) {
                    selectQuery.addWhere(where);
                }
            }
        }
        selectQuery.addOrderBy(this.orderBy);
        return named.queryForList(selectQuery.toSQL(), selectQuery.getParam(), Option.class);
    }

    @Override
    public boolean hasMore(String s, int i) {
        return false;
    }

    @Override
    public Object getDisplayValue(Option object) {
        return object.getText();
    }

    @Override
    public String getIdValue(Option object, int index) {
        return object.getId();
    }

    @Override
    public Option getObject(String id, IModel<? extends List<? extends Option>> choices) {
        if (isDisabled()) {
            return null;
        }
        JdbcNamed named = getNamed();
        SelectQuery selectQuery = new SelectQuery(this.table);
        if (!this.join.isEmpty()) {
            for (String join : this.join.values()) {
                selectQuery.addJoin(join);
            }
        }
        selectQuery.addField(this.idField + " AS id");
        selectQuery.addField(this.labelField + " AS text");
        selectQuery.addWhere(this.idField + " = :id", id);
        selectQuery.addOrderBy(this.orderBy);
        return named.queryForObject(selectQuery.toSQL(), selectQuery.getParam(), Option.class);
    }

    protected abstract JdbcNamed getNamed();

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }
}
