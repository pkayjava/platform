package com.angkorteam.core.webui.wicket.widget;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.markup.html.form.FormComponent;

public class TextFeedbackPanel extends com.angkorteam.core.webui.wicket.markup.html.panel.TextFeedbackPanel {

    /**
     *
     */
    private static final long serialVersionUID = -5258412752926599546L;

    public TextFeedbackPanel(String id, FormComponent<?> formComponent) {
        super(id, formComponent);
        add(AttributeModifier.replace("style", "color:red"));
    }

}
