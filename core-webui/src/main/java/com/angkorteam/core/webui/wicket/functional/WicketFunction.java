package com.angkorteam.core.webui.wicket.functional;

import org.apache.wicket.util.io.IClusterable;

import java.util.function.Function;

public interface WicketFunction<T, R> extends Function<T, R>, IClusterable {
}
