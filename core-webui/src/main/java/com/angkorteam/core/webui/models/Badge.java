package com.angkorteam.core.webui.models;

import com.angkorteam.core.webui.BackgroundColor;
import org.apache.wicket.model.IModel;

import java.io.Serializable;
import java.util.Comparator;

public class Badge implements Serializable, Comparator<Badge>, Comparable<Badge> {

    /**
     *
     */
    private static final long serialVersionUID = -2538470433588610701L;

    private IModel<String> badgeText;

    private BackgroundColor badgeColor = BackgroundColor.Blue;

    private Integer order;

    private Badge() {
    }

    public IModel<String> getBadgeText() {
        return badgeText;
    }

    public BackgroundColor getBadgeColor() {
        return badgeColor;
    }

    @Override
    public int compareTo(Badge o) {
        return this.order.compareTo(o.order);
    }

    @Override
    public int compare(Badge o1, Badge o2) {
        return o1.order.compareTo(o2.order);
    }

    public static class Builder {

        private IModel<String> badgeText;

        private BackgroundColor badgeColor = BackgroundColor.Blue;

        private Integer order;

        public Badge build() {
            Badge item = new Badge();
            item.order = this.order;
            item.badgeText = this.badgeText;
            item.badgeColor = this.badgeColor;
            return item;
        }

        public Builder with(int order, IModel<String> badgeText, BackgroundColor badgeColor) {
            this.badgeText = badgeText;
            this.badgeColor = badgeColor;
            this.order = order;
            return this;
        }
    }
}