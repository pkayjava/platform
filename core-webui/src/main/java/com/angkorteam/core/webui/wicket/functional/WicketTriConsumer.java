package com.angkorteam.core.webui.wicket.functional;

import org.apache.wicket.util.io.IClusterable;

import java.util.Objects;

@FunctionalInterface
public interface WicketTriConsumer<A, B, C> extends IClusterable {

    void accept(A a, B b, C c);

    default WicketTriConsumer<A, B, C> andThen(WicketTriConsumer<? super A, ? super B, ? super C> after) {
        Objects.requireNonNull(after);

        return (l, r, p) -> {
            accept(l, r, p);
            after.accept(l, r, p);
        };
    }
}