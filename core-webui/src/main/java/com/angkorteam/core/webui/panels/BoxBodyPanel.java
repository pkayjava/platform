package com.angkorteam.core.webui.panels;

import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class BoxBodyPanel extends Panel {

    /**
     *
     */
    private static final long serialVersionUID = 1251978641885809965L;

    public BoxBodyPanel() {
        super("body");
    }

    public BoxBodyPanel(IModel<?> model) {
        super("body", model);
    }
}
