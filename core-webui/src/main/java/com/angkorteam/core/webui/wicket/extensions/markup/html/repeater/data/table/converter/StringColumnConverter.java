package com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.converter;

import com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.filter.Operator;
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.util.convert.ConversionException;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import java.util.Map;

public class StringColumnConverter implements IColumnConverter<String> {

    /**
     *
     */
    private static final long serialVersionUID = -3281449504991406512L;

    private static final char HIDDEN_SPACE = '\u200B';

    @Override
    public String convertToObject(String key, String value) throws ConversionException {
        return value;
    }

    @Override
    public String convertToString(String key, String value) {
        return value;
    }

    @Override
    public String buildJdbcQuery(String key, String column, Operator operator, String firstOperand,
                                 String secondOperand, Map<String, Object> params) {
        String condition = null;
        String firstParam = key + "_1st";
        if (operator == Operator.Equal) {
            if (firstOperand == null || "".equals(firstOperand)) {
                condition = "(" + column + " = :" + firstParam + " OR " + column + " IS NULL)";
                params.put(firstParam, "");
            } else {
                condition = column + " = :" + firstParam;
                params.put(firstParam, firstOperand);
            }
        } else if (operator == Operator.NotEqual) {
            if (firstOperand == null || "".equals(firstOperand)) {
                condition = "(" + column + " != :" + firstParam + " AND " + column + " IS NOT NULL)";
                params.put(firstParam, "");
            } else {
                condition = column + " != :" + firstParam;
                params.put(firstParam, firstOperand);
            }
        } else if (operator == Operator.Like) {
            if (hasLikeRegx(firstOperand)) {
                condition = column + " RLIKE :" + firstParam;
                params.put(firstParam, buildLikeRegxExpression(firstOperand));
            } else {
                condition = column + " LIKE :" + firstParam;
                params.put(firstParam, buildLikeExpression(firstOperand));
            }
        } else if (operator == Operator.NotLike) {
            if (hasLikeRegx(firstOperand)) {
                condition = column + " NOT RLIKE :" + firstParam;
                params.put(firstParam, buildLikeRegxExpression(firstOperand));
            } else {
                condition = column + " NOT LIKE :" + firstParam;
                params.put(firstParam, buildLikeExpression(firstOperand));
            }
        }
        return condition;
    }

    protected boolean hasLikeRegx(String searchText) {
        for (char tmp : StringUtils.trimToEmpty(searchText).toCharArray()) {
            if (tmp == ' ' || tmp == HIDDEN_SPACE) {
                return true;
            }
        }
        return false;
    }

    protected String buildLikeRegxExpression(String searchText) {
        StringBuffer result = new StringBuffer(searchText.length());
        boolean space = false;
        for (char tmp : StringUtils.trimToEmpty(searchText).toCharArray()) {
            if (tmp == ' ' || tmp == HIDDEN_SPACE) {
                if (!space) {
                    result.append('|');
                    space = true;
                }
            } else {
                space = false;
                result.append(Character.toLowerCase(tmp));
            }
        }
        return "^.*(" + result.toString() + ").*$";
    }

    protected String buildLikeExpression(String searchText) {
        return StringUtils.trimToEmpty(searchText) + "%";
    }

    @Override
    public QueryBuilder buildElasticQuery(String key, String column, Operator operator, String firstOperand,
                                          String secondOperand) {
        if (operator == Operator.Equal) {
            if (firstOperand == null || "".equals(firstOperand)) {
                return QueryBuilders.boolQuery().should(QueryBuilders.matchQuery(key, ""))
                        .should(QueryBuilders.boolQuery().mustNot(QueryBuilders.existsQuery(key)));
            } else {
                return QueryBuilders.matchQuery(key, firstOperand);
            }
        } else if (operator == Operator.Like) {
            if (firstOperand == null || "".equals(firstOperand)) {
                return QueryBuilders.boolQuery().should(QueryBuilders.matchQuery(key, ""))
                        .should(QueryBuilders.boolQuery().mustNot(QueryBuilders.existsQuery(key)));
            } else {
                return QueryBuilders.matchPhrasePrefixQuery(key, firstOperand);
            }
        } else if (operator == Operator.NotEqual) {
            if (firstOperand == null || "".equals(firstOperand)) {
                return QueryBuilders.boolQuery().mustNot(QueryBuilders.matchQuery(key, ""))
                        .mustNot(QueryBuilders.existsQuery(key));

            } else {
                return QueryBuilders.boolQuery().mustNot(QueryBuilders.matchQuery(key, firstOperand));
            }
        } else if (operator == Operator.NotLike) {
            if (firstOperand == null || "".equals(firstOperand)) {
                return QueryBuilders.boolQuery().mustNot(QueryBuilders.matchQuery(key, ""))
                        .mustNot(QueryBuilders.existsQuery(key));
            } else {
                return QueryBuilders.boolQuery().mustNot(QueryBuilders.matchPhrasePrefixQuery(key, firstOperand));
            }
        }
        return null;
    }

}
