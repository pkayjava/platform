package com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.converter;

import com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.filter.Operator;
import org.apache.wicket.util.convert.ConversionException;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import java.util.Map;

public class BooleanColumnConverter implements IColumnConverter<Boolean> {

    /**
     *
     */
    private static final long serialVersionUID = 4926507893727375889L;

    @Override
    public Boolean convertToObject(String key, String value) throws ConversionException {
        if (value == null || "".equals(value)) {
            return null;
        }
        try {
            if ("yes".equalsIgnoreCase(value) || "on".equalsIgnoreCase(value) || "true".equalsIgnoreCase(value)
                    || "1".equals(value) || "y".equalsIgnoreCase(value) || "t".equalsIgnoreCase(value)) {
                return true;
            } else if ("no".equalsIgnoreCase(value) || "off".equalsIgnoreCase(value) || "false".equalsIgnoreCase(value)
                    || "0".equals(value) || "n".equalsIgnoreCase(value) || "f".equalsIgnoreCase(value)) {
                return false;
            } else {
                return Boolean.valueOf(value);
            }
        } catch (NumberFormatException e) {
            throw new ConversionException(e);
        }
    }

    @Override
    public String convertToString(String key, Boolean value) {
        if (value == null) {
            return "";
        }
        return String.valueOf(value);
    }

    @Override
    public String buildJdbcQuery(String key, String column, Operator operator, Boolean firstOperand,
                                 Boolean secondOperand, Map<String, Object> params) {
        String condition = null;
        String firstParam = key + "_1st";
        if (operator == Operator.Equal || operator == Operator.Like) {
            condition = column + " = :" + firstParam;
            params.put(firstParam, firstOperand);
        } else if (operator == Operator.NotEqual || operator == Operator.NotLike) {
            condition = column + " != :" + firstParam;
            params.put(firstParam, firstOperand);
        }
        return condition;
    }

    @Override
    public QueryBuilder buildElasticQuery(String key, String column, Operator operator, Boolean firstOperand,
                                          Boolean secondOperand) {
        if (operator == Operator.Equal || operator == Operator.Like) {
            QueryBuilders.termQuery(key, firstOperand);
        } else if (operator == Operator.NotEqual || operator == Operator.NotLike) {
            QueryBuilders.termQuery(key, !firstOperand);
        }
        return null;
    }

}
