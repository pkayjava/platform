package com.angkorteam.core.webui.panels;

import com.angkorteam.core.webui.models.PageFooter;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import java.time.LocalDateTime;

/**
 * Created by socheatkhauv on 6/17/17.
 */
public class PageFooterPanel extends Panel {

    /**
     *
     */
    private static final long serialVersionUID = -1575511950128423403L;

    private WebMarkupContainer wicketContainer = null;

    public PageFooterPanel(String id, IModel<PageFooter> model) {
        super(id, model);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        this.wicketContainer = new WebMarkupContainer("wicketContainer");
        this.add(this.wicketContainer);

        PageFooter pageFooter = (PageFooter) getDefaultModelObject();

        Label label = new Label("year", LocalDateTime.now().getYear());
        this.wicketContainer.add(label);

        Label company = new Label("company", pageFooter == null ? Model.of("") : pageFooter.getCompany());
        this.wicketContainer.add(company);

        if (pageFooter == null) {
            this.wicketContainer.setVisible(false);
        }
    }
}
