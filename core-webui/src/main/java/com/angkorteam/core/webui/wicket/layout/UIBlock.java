package com.angkorteam.core.webui.wicket.layout;

import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.util.lang.Args;

public class UIBlock extends WebMarkupContainer {

    /**
     *
     */
    private static final long serialVersionUID = -4973030497551512588L;

    protected Size size;

    public UIBlock(String id, Size size) {
        super(id);
        this.size = size;
        Args.notNull(size, "size");
        setOutputMarkupId(true);
    }

    @Override
    protected void onComponentTag(ComponentTag tag) {
        super.onComponentTag(tag);
        String clazz = tag.getAttribute("class");
        if (clazz == null || "".equals(clazz)) {
            clazz = this.size.getCss();
        } else {
            if (!clazz.contains(this.size.getCss())) {
                clazz = clazz + " " + this.size.getCss();
            }
        }
        if (clazz != null && !"".equals(clazz)) {
            tag.put("class", clazz);
        }
    }

    public UIContainer newUIContainer(String id) {
        UIContainer container = new UIContainer(id);
        add(container);
        return container;
    }

}
