package com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.filter;

import com.angkorteam.core.webui.wicket.functional.WicketBiFunction;
import com.angkorteam.core.webui.wicket.functional.WicketTriConsumer;
import com.angkorteam.core.webui.wicket.markup.html.form.DateTextField;
import com.angkorteam.core.webui.wicket.markup.html.form.select2.Select2SingleChoice;
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.AttributeModifier;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;

import java.util.List;

/**
 * Created by socheat on 12/7/16.
 */
public class ItemIconLink<T> extends Panel {

    /**
     *
     */
    private static final long serialVersionUID = 1564296998080424794L;

    public ItemIconLink(String id, IModel<T> rowModel, WicketBiFunction<String, T, ItemCss> itemCss,
                        WicketTriConsumer<String, T, AjaxRequestTarget> itemClick, String identity) {
        super(id);

        AjaxLink<T> link = new AjaxLink<T>("link", rowModel) {
            /**
             *
             */
            private static final long serialVersionUID = -7424883804625007512L;

            @Override
            public void onClick(AjaxRequestTarget target) {
                target.appendJavaScript(Select2SingleChoice.REMOVE_POPUP_UP_SCRIPT);
                target.appendJavaScript(DateTextField.REMOVE_POPUP_UP_SCRIPT);
                if (itemClick != null) {
                    itemClick.accept(identity, rowModel.getObject(), target);
                }
            }
        };
        add(link);

        Label icon = new Label("icon");
        link.add(icon);
        icon.add(AttributeModifier.replace("class", StringUtils.join(itemCss, " ")));
    }

    public ItemIconLink(String id, IModel<T> rowModel, List<String> itemCss,
                        WicketTriConsumer<String, T, AjaxRequestTarget> itemClick, String identity) {
        super(id);

        AjaxLink<T> link = new AjaxLink<T>("link", rowModel) {
            /**
             *
             */
            private static final long serialVersionUID = 807411944097534629L;

            @Override
            public void onClick(AjaxRequestTarget target) {
                target.appendJavaScript(Select2SingleChoice.REMOVE_POPUP_UP_SCRIPT);
                target.appendJavaScript(DateTextField.REMOVE_POPUP_UP_SCRIPT);
                if (itemClick != null) {
                    itemClick.accept(identity, rowModel.getObject(), target);
                }
            }
        };
        add(link);

        Label icon = new Label("icon");
        link.add(icon);

        icon.add(AttributeModifier.replace("class", StringUtils.join(itemCss, " ")));
    }

}
