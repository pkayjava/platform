package com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.filter;

import com.angkorteam.core.webui.provider.IFieldProvider;
import com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.converter.*;
import org.apache.wicket.Component;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.extensions.markup.html.repeater.data.grid.ICellPopulator;
import org.apache.wicket.extensions.markup.html.repeater.data.table.AbstractColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.export.IExportableColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.FilterForm;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.IFilteredColumn;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.util.convert.ConversionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Map;
import java.util.TimeZone;

public class TextFilteredColumn<T> extends AbstractColumn<Map<String, Object>, String>
        implements IFilteredColumn<Map<String, Object>, String>, IExportableColumn<Map<String, Object>, String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(TextFilteredColumn.class);

    /**
     *
     */
    private static final long serialVersionUID = -6585236681096888337L;

    protected final String key;

    protected IColumnConverter<T> converter;

    protected ColumnFunction function;

    public TextFilteredColumn(IModel<String> displayModel, String key, String column, IFieldProvider provider,
                              IColumnConverter<T> converter) {
        super(displayModel, key);
        this.key = key;
        this.converter = converter;
        provider.selectField(key, column, converter);
    }

    public TextFilteredColumn(IModel<String> displayModel, String key, String column, IFieldProvider provider,
                              Class<T> type) {
        super(displayModel, key);
        this.key = key;
        if (type == boolean.class || type == Boolean.class) {
            this.converter = (IColumnConverter<T>) new BooleanColumnConverter();
        } else if (type == byte.class || type == Byte.class) {
            this.converter = (IColumnConverter<T>) new ByteColumnConverter();
        } else if (type == short.class || type == Short.class) {
            this.converter = (IColumnConverter<T>) new ShortColumnConverter();
        } else if (type == int.class || type == Integer.class) {
            this.converter = (IColumnConverter<T>) new IntegerColumnConverter();
        } else if (type == long.class || type == Long.class) {
            this.converter = (IColumnConverter<T>) new LongColumnConverter();
        } else if (type == double.class || type == Double.class) {
            this.converter = (IColumnConverter<T>) new DoubleColumnConverter();
        } else if (type == Date.class) {
            this.converter = (IColumnConverter<T>) new DateColumnConverter();
        } else if (type == Time.class) {
            this.converter = (IColumnConverter<T>) new TimeColumnConverter();
        } else if (type == Timestamp.class) {
            this.converter = (IColumnConverter<T>) new TimestampColumnConverter();
        } else if (type == String.class) {
            this.converter = (IColumnConverter<T>) new StringColumnConverter();
        } else {
            throw new WicketRuntimeException("Not support " + type.getName());
        }
        provider.selectField(key, column, this.converter);
    }

    public TextFilteredColumn(IModel<String> displayModel, String key, String column, IFieldProvider provider,
                              TimeZone timeZone) {
        super(displayModel, key);
        this.key = key;
        this.converter = (IColumnConverter<T>) new TimestampColumnConverter(timeZone);
        provider.selectField(key, column, this.converter);
    }

    public TextFilteredColumn(IModel<String> displayModel, String key, String column, IFieldProvider provider,
                              String datetimePattern) {
        super(displayModel, key);
        this.key = key;
        this.converter = (IColumnConverter<T>) new DateTimeColumnConverter(datetimePattern);
        provider.selectField(key, column, this.converter);
    }

    @Override
    public void populateItem(Item<ICellPopulator<Map<String, Object>>> cellItem, String componentId,
                             IModel<Map<String, Object>> rowModel) {
        ItemPanel itemPanel = this.function == null ? null
                : this.function.function(this.key, getDisplayModel(), getDataModelValue(rowModel),
                rowModel.getObject());
        if (itemPanel == null) {
            cellItem.add(new Label(componentId, getDataModelValue(rowModel)));
        } else {
            CellPanel cell = new CellPanel(componentId);
            cellItem.add(cell);
            cell.add(itemPanel);
        }
    }

    @Override
    public IModel<?> getDataModel(IModel<Map<String, Object>> rowModel) {
        return Model.of(getDataModelValue(rowModel));
    }

    protected String getDataModelValue(IModel<Map<String, Object>> rowModel) {
        PropertyModel<T> propertyModel = new PropertyModel<>(rowModel, this.key);
        if (this.converter instanceof TimestampColumnConverter || this.converter instanceof DateColumnConverter
                || this.converter instanceof TimeColumnConverter || this.converter instanceof DateTimeColumnConverter) {
            if (propertyModel.getObject() instanceof String) {
                T value = this.converter.convertToObject(key, (String) propertyModel.getObject());
                try {
                    return this.converter.convertToString(this.key, value);
                } catch (ConversionException e) {
                    LOGGER.info("{} {}", propertyModel.getObject(), e.getMessage());
                    throw e;
                }
            } else {
                return this.converter.convertToString(this.key, propertyModel.getObject());
            }
        } else if (this.converter instanceof ByteColumnConverter || this.converter instanceof ShortColumnConverter
                || this.converter instanceof IntegerColumnConverter || this.converter instanceof LongColumnConverter
                || this.converter instanceof FloatColumnConverter || this.converter instanceof DoubleColumnConverter) {
            if (propertyModel.getObject() instanceof Number) {
                T value = this.converter.convertToObject(key, String.valueOf(propertyModel.getObject()));
                try {
                    return this.converter.convertToString(this.key, value);
                } catch (ConversionException e) {
                    LOGGER.info("{} {}", propertyModel.getObject(), e.getMessage());
                    throw e;
                }
            } else {
                return this.converter.convertToString(this.key, propertyModel.getObject());
            }
        } else {
            return this.converter.convertToString(this.key, propertyModel.getObject());
        }
    }

    @Override
    public Component getFilter(final String componentId, final FilterForm<?> form) {
        // NoFilter filter = new NoFilter(componentId);
        PropertyModel<Expression<T>> model = new PropertyModel<>(form.getDefaultModel(), getPropertyExpression());
        ExpressionConverter<T> converter = new ExpressionConverter<>(this.key, this.converter);
        return new TextFilter<>(componentId, model, form, converter);
    }

    @Override
    public String getSortProperty() {
        return this.key;
    }

    public String getPropertyExpression() {
        return this.key;
    }

}
