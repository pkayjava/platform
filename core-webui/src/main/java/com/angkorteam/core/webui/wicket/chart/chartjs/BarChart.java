package com.angkorteam.core.webui.wicket.chart.chartjs;

import com.angkorteam.core.webui.ReferenceUtilities;
import com.google.gson.Gson;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebComponent;
import org.apache.wicket.markup.parser.XmlTag.TagType;
import org.apache.wicket.model.IModel;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class BarChart extends WebComponent {

    /**
     *
     */
    private static final long serialVersionUID = -2031773587523085812L;

    private IModel<BarDataset> dataset;

    public BarChart(String id, IModel<BarDataset> model) {
        super(id);
        setOutputMarkupId(true);
        this.dataset = model;
    }

    @Override
    protected void onComponentTag(final ComponentTag tag) {
        checkComponentTag(tag, "canvas");
        super.onComponentTag(tag);
        if (tag.isOpenClose()) {
            tag.setType(TagType.OPEN);
        }
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);

        ReferenceUtilities.renderJavascript(response, "<!-- ChartJS 1.0.2 -->");
        ReferenceUtilities.renderJavascript(response,
                ReferenceUtilities.AdminLTE + "/bower_components/chart.js/Chart.min.js");

        Gson gson = new Gson();

        String markupId = getMarkupId();
        String chart = String.format(
                "new Chart(document.getElementById('%s').getContext('2d')).Bar(%s, { responsive : true })", markupId,
                gson.toJson(this.dataset.getObject()));
        response.render(OnDomReadyHeaderItem.forScript(chart));
    }

    public static class BarItem implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = -2390987653208130338L;

        private String fillColor;

        private String strokeColor;

        private String highlightFill;

        private String highlightStroke;

        private List<Integer> data = new LinkedList<>();

        public String getFillColor() {
            return fillColor;
        }

        public void setFillColor(String fillColor) {
            this.fillColor = fillColor;
        }

        public String getStrokeColor() {
            return strokeColor;
        }

        public void setStrokeColor(String strokeColor) {
            this.strokeColor = strokeColor;
        }

        public String getHighlightFill() {
            return highlightFill;
        }

        public void setHighlightFill(String highlightFill) {
            this.highlightFill = highlightFill;
        }

        public String getHighlightStroke() {
            return highlightStroke;
        }

        public void setHighlightStroke(String highlightStroke) {
            this.highlightStroke = highlightStroke;
        }

        public List<Integer> getData() {
            return data;
        }

    }

    public static class BarDataset implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = -6094082066773162902L;

        private List<String> labels = new LinkedList<>();

        private List<BarItem> datasets = new LinkedList<>();

        public List<String> getLabels() {
            return labels;
        }

        public List<BarItem> getDatasets() {
            return datasets;
        }

    }

}