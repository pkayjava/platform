package com.angkorteam.core.webui;

/**
 * Created by socheatkhauv on 6/12/17.
 */
public enum TextColor {

    Red("text-red"), Yellow("text-yellow"), Aqua("text-aqua"), Blue("text-blue"), Black("text-black"),
    LightBlue("text-light-blue"), Green("text-green"), Gray("text-gray"), Navy("text-navy"), Teal("text-teal"),
    Olive("text-olive"), Lime("text-lime"), Orange("text-orange"), Fuchsia("text-fuchsia"), Purple("text-purple"),
    Maroon("text-maroon");

    private final String literal;

    TextColor(String literal) {
        this.literal = literal;
    }

    public String getLiteral() {
        return literal;
    }
}
