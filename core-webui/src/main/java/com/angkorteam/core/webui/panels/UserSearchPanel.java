package com.angkorteam.core.webui.panels;

import com.angkorteam.core.webui.wicket.functional.WicketConsumer;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LambdaModel;

/**
 * Created by socheatkhauv on 6/14/17.
 */
public class UserSearchPanel extends Panel {

    /**
     *
     */
    private static final long serialVersionUID = 4028130386217346516L;

    private WebMarkupContainer wicketContainer;

    private IModel<Boolean> hasSearchFormModel;

    private Form<Void> form;

    private TextField<String> searchField;
    private String searchValue;

    private Button searchButton;

    private WicketConsumer<String> searchFunction;

    public UserSearchPanel(String id, WicketConsumer<String> searchFunction, IModel<Boolean> hasSearchFormModel) {
        super(id);
        this.hasSearchFormModel = hasSearchFormModel;
        this.searchFunction = searchFunction;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();
        this.wicketContainer = new WebMarkupContainer("wicketContainer");
        this.add(this.wicketContainer);

        if (this.hasSearchFormModel == null || this.hasSearchFormModel.getObject() == null
                || !this.hasSearchFormModel.getObject()) {
            this.wicketContainer.setVisible(false);
        }

        this.form = new Form<>("form");
        this.wicketContainer.add(this.form);

        this.searchField = new TextField<>("searchField", LambdaModel.of(this::getSearchValue, this::setSearchValue));
        this.form.add(this.searchField);

        this.searchButton = new Button("searchButton") {
            /**
             *
             */
            private static final long serialVersionUID = 5182961094783968475L;

            @Override
            public void onSubmit() {
                if (searchFunction != null) {
                    searchFunction.accept(searchValue == null ? "" : searchValue);
                }
            }
        };
        this.form.add(this.searchButton);
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

}
