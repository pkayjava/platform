package com.angkorteam.core.webui.provider;

import com.angkorteam.core.spring.jdbc.JdbcNamed;
import com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.converter.IColumnConverter;
import com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.filter.Expression;
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.extensions.markup.html.repeater.data.table.filter.IFilterStateLocator;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.util.MapModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public abstract class JdbcDataProvider extends SortableDataProvider<Map<String, Object>, String>
        implements IFilterStateLocator<Map<String, Expression<Object>>>, IFieldProvider {

    /**
     *
     */
    private static final long serialVersionUID = 2453015465083001162L;

    private static final Logger LOGGER = LoggerFactory.getLogger(JdbcDataProvider.class);

    private Map<String, Expression<Object>> filterState;

    private Map<String, String> negotiator;

    private String groupBy;

    private List<String> fields;

    protected String from;

    private final Map<String, String> join;

    private final Map<String, String> userWhere;

    private final Map<String, String> userHaving;

    private final List<String> aggregate;

    public JdbcDataProvider() {
        this("");
    }

    public JdbcDataProvider(String from) {
        this.from = from;
        this.join = new LinkedHashMap<>();
        this.aggregate = new ArrayList<>();
        this.negotiator = new LinkedHashMap<>();
        this.fields = new LinkedList<>();
        this.filterState = new LinkedHashMap<>();
        this.userWhere = new LinkedHashMap<>();
        this.userHaving = new LinkedHashMap<>();
    }

    protected String getFrom() {
        return this.from;
    }

    private List<String> buildWhere(Map<String, Object> params) {
        List<String> where = new ArrayList<>();
        if (this.filterState != null && !this.filterState.isEmpty()) {
            for (Map.Entry<String, Expression<Object>> entry : this.filterState.entrySet()) {
                if (!this.aggregate.contains(entry.getKey())) {
                    Expression<Object> expression = entry.getValue();
                    if (expression == null) {
                        continue;
                    }
                    IColumnConverter<Object> converter = expression.getConverter();
                    String condition = converter.buildJdbcQuery(entry.getKey(), this.negotiator.get(entry.getKey()),
                            expression.getOperator(), expression.getFirstOperand(), expression.getSecondOperand(),
                            params);
                    if (condition != null && !"".equals(condition)) {
                        where.add(condition);
                    }
                }
            }
        }
        List<String> userWhere = where();
        if (userWhere != null && !userWhere.isEmpty()) {
            where.addAll(userWhere);
        }
        return where;
    }

    public void applyWhere(String key, String filter) {
        this.userWhere.put(key, filter);
    }

    public String removeWhere(String key) {
        return this.userWhere.remove(key);
    }

    public void applyHaving(String key, String filter) {
        this.userHaving.put(key, filter);
    }

    public String removeHaving(String key) {
        return this.userHaving.remove(key);
    }

    public void applyJoin(String key, String join) {
        this.join.put(key, join);
    }

    public String removeJoin(String key) {
        return this.join.remove(key);
    }

    private List<String> buildHaving(Map<String, Object> params) {
        List<String> having = new ArrayList<>();
        if (this.filterState != null && !this.filterState.isEmpty()) {
            for (Map.Entry<String, Expression<Object>> entry : this.filterState.entrySet()) {
                if (this.aggregate.contains(entry.getKey())) {
                    Expression<Object> expression = entry.getValue();
                    if (expression == null) {
                        continue;
                    }
                    IColumnConverter<Object> converter = expression.getConverter();
                    String condition = converter.buildJdbcQuery(entry.getKey(), this.negotiator.get(entry.getKey()),
                            expression.getOperator(), expression.getFirstOperand(), expression.getSecondOperand(),
                            params);
                    if (condition != null && !"".equals(condition)) {
                        having.add(condition);
                    }
                }
            }
        }
        List<String> userHaving = having();
        if (userHaving != null && !userHaving.isEmpty()) {
            having.addAll(userHaving);
        }
        return having;
    }

    protected String buildOrderBy() {
        String orderBy = null;
        if (getSort() != null) {
            if (getSort().isAscending()) {
                orderBy = "`" + getSort().getProperty() + "` ASC";
            } else {
                orderBy = "`" + getSort().getProperty() + "` DESC";
            }
        }
        return orderBy;
    }

    public List<String> getFields() {
        List<String> fields = new ArrayList<>();
        for (Map.Entry<String, String> entry : this.negotiator.entrySet()) {
            String alias = entry.getKey();
            String value = entry.getValue();
            if (this.fields.contains(alias) && !fields.contains(value)) {
                fields.add(value + " " + "`" + alias + "`");
            }
        }
        return fields;
    }

    public void selectField(String key, String column) {
        this.fields.add(key);
        this.negotiator.put(key, column);
        if (isAggregate(column)) {
            this.aggregate.add(key);
        }
    }

    public void selectField(String key, String column, IColumnConverter<?> converter) {
        this.fields.add(key);
        this.negotiator.put(key, column);
        if (isAggregate(column)) {
            this.aggregate.add(key);
        }
    }

    private boolean isAggregate(String jdbcColumn) {
        String column = StringUtils.upperCase(jdbcColumn);
        if (StringUtils.startsWith(column, "AVG(") || StringUtils.startsWith(column, "AVG (")
                || StringUtils.startsWith(column, "BIT_AND(") || StringUtils.startsWith(column, "BIT_AND (")
                || StringUtils.startsWith(column, "BIT_OR(") || StringUtils.startsWith(column, "BIT_OR (")
                || StringUtils.startsWith(column, "BIT_XOR(") || StringUtils.startsWith(column, "BIT_XOR (")
                || StringUtils.startsWith(column, "COUNT(") || StringUtils.startsWith(column, "COUNT (")
                || StringUtils.startsWith(column, "GROUP_CONCAT(") || StringUtils.startsWith(column, "GROUP_CONCAT (")
                || StringUtils.startsWith(column, "MAX(") || StringUtils.startsWith(column, "MAX (")
                || StringUtils.startsWith(column, "MIN(") || StringUtils.startsWith(column, "MIN (")
                || StringUtils.startsWith(column, "STD(") || StringUtils.startsWith(column, "STD (")
                || StringUtils.startsWith(column, "STDDEV(") || StringUtils.startsWith(column, "STDDEV (")
                || StringUtils.startsWith(column, "STDDEV_POP(") || StringUtils.startsWith(column, "STDDEV_POP (")
                || StringUtils.startsWith(column, "STDDEV_SAMP(") || StringUtils.startsWith(column, "STDDEV_SAMP (")
                || StringUtils.startsWith(column, "SUM(") || StringUtils.startsWith(column, "SUM (")
                || StringUtils.startsWith(column, "VAR_POP(") || StringUtils.startsWith(column, "VAR_POP (")
                || StringUtils.startsWith(column, "VAR_SAMP(") || StringUtils.startsWith(column, "VAR_SAMP (")
                || StringUtils.startsWith(column, "VARIANCE(") || StringUtils.startsWith(column, "VARIANCE (")) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public final IModel<Map<String, Object>> model(Map<String, Object> object) {
        return new MapModel<>(object);
    }

    @Override
    public final Map<String, Expression<Object>> getFilterState() {
        return this.filterState;
    }

    @Override
    public final void setFilterState(Map<String, Expression<Object>> filterState) {
        this.filterState = filterState;
    }

    protected QueryBuilder buildQuery(Map<String, Object> params) {
        return buildQuery(params, null);
    }

    protected QueryBuilder buildQuery(Map<String, Object> params, String orderBy) {
        List<String> where = buildWhere(params);
        List<String> having = buildHaving(params);
        QueryBuilder builder = new QueryBuilder();
        builder.setFrom(getFrom());
        if (!this.join.isEmpty()) {
            for (String join : this.join.values()) {
                builder.addJoin(join);
            }
        }
        if (!getFields().isEmpty()) {
            for (String field : getFields()) {
                builder.addSelect(field);
            }
        }
        if (!where.isEmpty()) {
            for (String s : where) {
                builder.addWhere(s);
            }
        }
        if (!having.isEmpty()) {
            for (String s : having) {
                builder.addHaving(s);
            }
        }
        if (this.groupBy != null && !"".equals(this.groupBy)) {
            builder.addGroupBy(this.groupBy);
        }
        if (orderBy != null && !"".equals(orderBy)) {
            builder.addOrderBy(orderBy);
        }
        return builder;
    }

    protected List<String> where() {
        return new ArrayList<>(this.userWhere.values());
    }

    protected List<String> having() {
        return new ArrayList<>(this.userHaving.values());
    }

    @Override
    public Iterator<Map<String, Object>> iterator(long first, long count) {
        Map<String, Object> params = new HashMap<>();
        String orderBy = buildOrderBy();
        QueryBuilder select = buildQuery(params, orderBy);
        select.setLimit(first, count);
        String query = select.toSQL();
        JdbcNamed jdbcNamed = getJdbcNamed();
        List<Map<String, Object>> result = jdbcNamed.queryForList(query, params);
        return result.iterator();
    }

    @Override
    public long size() {
        Map<String, Object> params = new HashMap<>();
        QueryBuilder select = buildQuery(params);
        String query = select.toSQL();
        String countQuery = "SELECT COUNT(*) FROM (" + query + ") pp";
        JdbcNamed jdbcNamed = getJdbcNamed();
        return jdbcNamed.queryForObject(countQuery, params, long.class);
    }

    public void setGroupBy(String groupBy) {
        this.groupBy = groupBy;
    }

    public String getGroupBy() {
        return groupBy;
    }

    protected abstract JdbcNamed getJdbcNamed();

    @Override
    public void selectField(String column) {
        this.selectField(column, column);
    }

    @Override
    public void selectField(String column, IColumnConverter<?> converter) {
        this.selectField(column, column, converter);
    }

}