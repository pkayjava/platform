package com.angkorteam.core.webui.wicket.markup.html.form.validation;

import com.angkorteam.core.webui.wicket.functional.WicketConsumer;
import com.angkorteam.core.webui.wicket.markup.html.form.Form;
import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.markup.html.form.FormComponent;

public class LamdaFormValidator implements org.apache.wicket.markup.html.form.validation.IFormValidator {

    /**
     *
     */
    private static final long serialVersionUID = -4512493086950772591L;

    protected FormComponent<?>[] components;

    protected WicketConsumer<Form<?>> validator;

    public LamdaFormValidator(WicketConsumer<Form<?>> validator, FormComponent<?> component) {
        if (component == null) {
            throw new WicketRuntimeException("component is required");
        }
        this.components = new FormComponent<?>[]{component};
        this.validator = validator;
    }

    public LamdaFormValidator(WicketConsumer<Form<?>> validator, FormComponent<?>... components) {
        if (components == null || components.length == 0) {
            throw new WicketRuntimeException("components is required");
        }
        this.components = components;
        this.validator = validator;
    }

    @Override
    public FormComponent<?>[] getDependentFormComponents() {
        return this.components;
    }

    @Override
    public void validate(org.apache.wicket.markup.html.form.Form<?> form) {
        if (this.validator != null) {
            this.validator.accept((Form<?>) form);
        }
    }

}
