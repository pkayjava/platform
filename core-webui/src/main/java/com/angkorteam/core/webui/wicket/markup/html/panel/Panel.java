package com.angkorteam.core.webui.wicket.markup.html.panel;

import org.apache.wicket.model.IModel;

public abstract class Panel extends org.apache.wicket.markup.html.panel.Panel {

    /**
     *
     */
    private static final long serialVersionUID = 4083430395397049647L;

    public Panel(String id) {
        super(id);
    }

    public Panel(String id, IModel<?> model) {
        super(id, model);
    }

    @Override
    protected final void onInitialize() {
        initData();
        super.onInitialize();
        initComponent();
        configureMetaData();
    }

    protected abstract void initData();

    protected abstract void initComponent();

    protected abstract void configureMetaData();

}