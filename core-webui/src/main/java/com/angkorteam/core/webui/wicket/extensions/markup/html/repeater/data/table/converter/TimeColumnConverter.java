package com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.converter;

import com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.filter.Operator;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.apache.wicket.util.convert.ConversionException;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;

import java.sql.Time;
import java.text.ParseException;
import java.util.Map;

public class TimeColumnConverter implements IColumnConverter<Time> {

    /**
     *
     */
    private static final long serialVersionUID = -6833394376935100664L;

    protected String pattern;

    public TimeColumnConverter() {
        this.pattern = "HH:mm:ss";
    }

    @Override
    public Time convertToObject(String key, String value) throws ConversionException {
        if (value == null || "".equals(value)) {
            return null;
        }
        try {
            FastDateFormat df = FastDateFormat.getInstance(pattern, null, null);
            return new Time(df.parse(value).getTime());
        } catch (ParseException e) {
            throw new ConversionException(e);
        }
    }

    @Override
    public String convertToString(String key, Time value) {
        if (value == null) {
            return "";
        }
        return DateFormatUtils.format(value, this.pattern);
    }

    @Override
    public String buildJdbcQuery(String key, String column, Operator operator, Time firstOperand, Time secondOperand,
                                 Map<String, Object> params) {
        String condition = null;
        String firstParam = key + "_1st";
        String secondParam = key + "_2nd";
        if (operator == Operator.Equal || operator == Operator.Like) {
            condition = column + " = :" + firstParam;
            params.put(firstParam, firstOperand);
        } else if (operator == Operator.NotEqual || operator == Operator.NotLike) {
            condition = column + " != :" + firstParam;
            params.put(firstParam, firstOperand);
        } else if (operator == Operator.GreaterThan) {
            condition = column + " > :" + firstParam;
            params.put(firstParam, firstOperand);
        } else if (operator == Operator.GreaterThanOrEqual) {
            condition = column + " >= :" + firstParam;
            params.put(firstParam, firstOperand);
        } else if (operator == Operator.LessThan) {
            condition = column + " < :" + firstParam;
            params.put(firstParam, firstOperand);
        } else if (operator == Operator.LessThanOrEqual) {
            condition = column + " <= :" + firstParam;
            params.put(firstParam, firstOperand);
        } else if (operator == Operator.Between) {
            if (firstOperand.before(secondOperand)) {
                condition = column + " BETWEEN :" + firstParam + " AND :" + secondParam;
                params.put(firstParam, firstOperand);
                params.put(secondParam, secondOperand);
            } else {
                condition = column + " BETWEEN :" + secondParam + " AND :" + firstParam;
                params.put(firstParam, firstOperand);
                params.put(secondParam, secondOperand);
            }
        } else if (operator == Operator.NotBetween) {
            if (firstOperand.before(secondOperand)) {
                condition = column + " NOT BETWEEN :" + firstParam + " AND :" + secondParam;
                params.put(firstParam, firstOperand);
                params.put(secondParam, secondOperand);
            } else {
                condition = column + " NOT BETWEEN :" + secondParam + " AND :" + firstParam;
                params.put(firstParam, firstOperand);
                params.put(secondParam, secondOperand);
            }
        }
        return condition;
    }

    @Override
    public QueryBuilder buildElasticQuery(String key, String column, Operator operator, Time firstOperand,
                                          Time secondOperand) {
        if (operator == Operator.Equal || operator == Operator.Like) {
            return QueryBuilders.termQuery(key, convertToString(key, firstOperand));
        } else if (operator == Operator.NotEqual || operator == Operator.NotLike) {
            return QueryBuilders.rangeQuery(key).gt(convertToString(key, firstOperand))
                    .lt(convertToString(key, firstOperand)).format(this.pattern);
        } else if (operator == Operator.GreaterThan) {
            return QueryBuilders.rangeQuery(key).gt(convertToString(key, firstOperand)).format(this.pattern);
        } else if (operator == Operator.GreaterThanOrEqual) {
            return QueryBuilders.rangeQuery(key).gte(convertToString(key, firstOperand)).format(this.pattern);
        } else if (operator == Operator.LessThan) {
            return QueryBuilders.rangeQuery(key).lt(convertToString(key, firstOperand)).format(this.pattern);
        } else if (operator == Operator.LessThanOrEqual) {
            return QueryBuilders.rangeQuery(key).lte(convertToString(key, firstOperand)).format(this.pattern);
        } else if (operator == Operator.Between) {
            if (firstOperand.before(secondOperand)) {
                return QueryBuilders.rangeQuery(key).from(convertToString(key, firstOperand))
                        .to(convertToString(key, secondOperand)).format(this.pattern);
            } else {
                return QueryBuilders.rangeQuery(key).from(convertToString(key, secondOperand))
                        .to(convertToString(key, firstOperand)).format(this.pattern);
            }
        } else if (operator == Operator.NotBetween) {
            if (firstOperand.before(secondOperand)) {
                return QueryBuilders.rangeQuery(key).lt(convertToString(key, firstOperand))
                        .gt(convertToString(key, secondOperand)).format(this.pattern);
            } else {
                return QueryBuilders.rangeQuery(key).lt(convertToString(key, secondOperand))
                        .gt(convertToString(key, firstOperand)).format(this.pattern);
            }
        }
        return null;
    }

}
