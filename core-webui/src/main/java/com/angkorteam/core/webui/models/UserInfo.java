package com.angkorteam.core.webui.models;

import com.angkorteam.core.webui.Emoji;
import com.angkorteam.core.webui.TextColor;
import org.apache.wicket.Page;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.io.Serializable;

/**
 * Created by socheatkhauv on 6/13/17.
 */
public class UserInfo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -3272028874185671226L;

    private String image;

    private String title;

    private String description;

    private Class<? extends Page> page;

    private PageParameters parameters;

    private Emoji icon;

    private TextColor iconColor;

    public UserInfo setIcon(Emoji icon, TextColor color) {
        this.icon = icon;
        this.iconColor = color;
        return this;
    }

    public String getImage() {
        return image;
    }

    public UserInfo setImage(String image) {
        this.image = image;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public UserInfo setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public UserInfo setDescription(String description) {
        this.description = description;
        return this;
    }

    public Class<? extends Page> getPage() {
        return page;
    }

    public UserInfo setPage(Class<? extends Page> page) {
        this.page = page;
        return this;
    }

    public PageParameters getParameters() {
        return parameters;
    }

    public UserInfo setParameters(PageParameters parameters) {
        this.parameters = parameters;
        return this;
    }

    public Emoji getIcon() {
        return icon;
    }

    public TextColor getIconColor() {
        return iconColor;
    }
}
