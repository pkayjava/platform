package com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.converter;

import com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.filter.Operator;
import org.apache.wicket.util.convert.ConversionException;
import org.apache.wicket.util.io.IClusterable;
import org.elasticsearch.index.query.QueryBuilder;

import java.util.Map;

public interface IColumnConverter<T> extends IClusterable {

    T convertToObject(String key, String value) throws ConversionException;

    String convertToString(String key, T value);

    String buildJdbcQuery(String key, String column, Operator operator, T firstOperand, T secondOperand,
                          Map<String, Object> params);

    QueryBuilder buildElasticQuery(String key, String column, Operator operator, T firstOperand, T secondOperand);

}
