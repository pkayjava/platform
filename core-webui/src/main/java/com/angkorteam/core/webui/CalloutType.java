package com.angkorteam.core.webui;

/**
 * Created by socheatkhauv on 6/13/17.
 */
public enum CalloutType {

    Warning("callout-warning"),

    Danger("callout-danger"),

    Info("callout-info"),

    Success("callout-success");

    private String literal;

    CalloutType(String literal) {
        this.literal = literal;
    }

    public String getLiteral() {
        return literal;
    }

}
