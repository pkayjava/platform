package com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.filter;

import com.angkorteam.core.webui.wicket.extensions.markup.html.repeater.data.table.converter.IColumnConverter;
import org.apache.commons.lang3.StringUtils;
import org.apache.wicket.util.convert.ConversionException;

import java.io.Serializable;

public class Expression<C> implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -676351625506599512L;

    private IColumnConverter<C> converter;

    private C firstOperand;

    private Operator operator;

    private C secondOperand;

    private String filter;

    private String key;

    public Expression(String key, String filter, IColumnConverter<C> converter) throws ConversionException {
        this.converter = converter;
        this.filter = StringUtils.trimToEmpty(filter);

        if (StringUtils.startsWith(filter, "!= ")) {
            this.operator = Operator.NotEqual;
            this.firstOperand = this.converter.convertToObject(this.key,
                    StringUtils.trimToEmpty(StringUtils.substring(filter, 3)));
        } else if (StringUtils.startsWith(filter, ">= ")) {
            this.operator = Operator.GreaterThanOrEqual;
            this.firstOperand = this.converter.convertToObject(this.key,
                    StringUtils.trimToEmpty(StringUtils.substring(filter, 3)));
        } else if (StringUtils.startsWith(filter, "<= ")) {
            this.operator = Operator.LessThanOrEqual;
            this.firstOperand = this.converter.convertToObject(this.key,
                    StringUtils.trimToEmpty(StringUtils.substring(filter, 3)));
        } else if (StringUtils.startsWith(filter, "= ")) {
            this.operator = Operator.Equal;
            this.firstOperand = this.converter.convertToObject(this.key,
                    StringUtils.trimToEmpty(StringUtils.substring(filter, 2)));
        } else if (StringUtils.startsWithIgnoreCase(filter, "like")) {
            this.operator = Operator.Like;
            this.firstOperand = this.converter.convertToObject(this.key,
                    StringUtils.trimToEmpty(StringUtils.substring(filter, 4)));
        } else if (StringUtils.startsWithIgnoreCase(filter, "not like")) {
            this.operator = Operator.NotLike;
            this.firstOperand = this.converter.convertToObject(this.key,
                    StringUtils.trimToEmpty(StringUtils.substring(filter, 8)));
        } else if (StringUtils.startsWith(filter, "> ")) {
            this.operator = Operator.GreaterThan;
            this.firstOperand = this.converter.convertToObject(this.key,
                    StringUtils.trimToEmpty(StringUtils.substring(filter, 2)));
        } else if (StringUtils.startsWith(filter, "< ")) {
            this.operator = Operator.LessThan;
            this.firstOperand = this.converter.convertToObject(this.key,
                    StringUtils.trimToEmpty(StringUtils.substring(filter, 2)));
        } else if (StringUtils.containsIgnoreCase(filter, " to ")) {
            int mid = StringUtils.indexOfIgnoreCase(filter, " to ");
            this.operator = Operator.Between;
            String temp = StringUtils.trimToEmpty(StringUtils.substring(filter, 0, mid));
            if (StringUtils.startsWithIgnoreCase(temp, "not between ")) {
                this.operator = Operator.NotBetween;
                this.firstOperand = this.converter.convertToObject(this.key, temp.substring(12));
            } else {
                this.firstOperand = this.converter.convertToObject(this.key, temp);
            }
            this.secondOperand = this.converter.convertToObject(this.key,
                    StringUtils.trimToEmpty(StringUtils.substring(filter, mid + 4)));
        } else if (StringUtils.equals(filter, "=")) {
            this.operator = Operator.Equal;
            this.firstOperand = null;
        } else if (StringUtils.equals(filter, "!=")) {
            this.operator = Operator.NotEqual;
            this.firstOperand = null;
        } else {
            if (StringUtils.contains(filter, "%")) {
                this.operator = Operator.Like;
                this.firstOperand = this.converter.convertToObject(this.key, filter);
            } else {
                this.operator = Operator.Equal;
                this.firstOperand = this.converter.convertToObject(this.key, filter);
            }
        }
    }

    public C getFirstOperand() {
        return firstOperand;
    }

    public Operator getOperator() {
        return operator;
    }

    public C getSecondOperand() {
        return secondOperand;
    }

    public String getFilter() {
        return filter;
    }

    public IColumnConverter<C> getConverter() {
        return converter;
    }

    public String getKey() {
        return key;
    }

}