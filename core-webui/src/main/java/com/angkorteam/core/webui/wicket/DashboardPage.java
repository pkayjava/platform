package com.angkorteam.core.webui.wicket;

import com.angkorteam.core.webui.AdminLTEOption;
import com.angkorteam.core.webui.ReferenceUtilities;
import com.angkorteam.core.webui.models.*;
import com.angkorteam.core.webui.panels.*;
import com.angkorteam.core.webui.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.IModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by socheatkhauv on 6/16/17.
 */
public abstract class DashboardPage extends WebPage {

    /**
     *
     */
    private static final long serialVersionUID = -7990394497716007617L;

    private PageHeaderPanel pageHeaderPanel;

    private PageBreadcrumbPanel pageBreadcrumbPanel;

    private NavBarMenuPanel navBarMenuPanel;

    private UserInfoPanel userInfoPanel;

    private UserSearchPanel userSearchPanel;

    private SideMenuPanel sideMenuPanel;

    private PageLogoPanel pageLogoPanel;

    private PageFooterPanel pageFooterPanel;

    private WebMarkupContainer mainSidebar;

    private WebMarkupContainer toogleButton;

    protected FeedbackPanel feedbackPanel;

    private boolean leftSideBar;

    public DashboardPage() {
    }

    public DashboardPage(IModel<?> model) {
        super(model);
    }

    public DashboardPage(PageParameters parameters) {
        super(parameters);
    }

    @Override
    protected void onInitialize() {
        initData();

        super.onInitialize();

        this.feedbackPanel = new FeedbackPanel("feedbackPanel", this::report);
        this.add(this.feedbackPanel);
        this.feedbackPanel.setOutputMarkupId(true);

        IModel<PageHeader> pageHeader = buildPageHeader();
        this.pageHeaderPanel = new PageHeaderPanel("pageHeaderPanel", pageHeader);
        this.add(this.pageHeaderPanel);

        this.pageBreadcrumbPanel = new PageBreadcrumbPanel("pageBreadcrumbPanel", buildPageBreadcrumb());
        this.add(this.pageBreadcrumbPanel);

        this.navBarMenuPanel = new NavBarMenuPanel("navBarMenuPanel", buildNavBarMenu());
        this.add(this.navBarMenuPanel);

        this.mainSidebar = new WebMarkupContainer("mainSidebar");
        this.add(this.mainSidebar);

        IModel<UserInfo> userInfo = buildUserInfo();
        this.userInfoPanel = new UserInfoPanel("userInfoPanel", userInfo);
        this.mainSidebar.add(this.userInfoPanel);

        IModel<Boolean> searchForm = hasSearchForm();
        this.userSearchPanel = new UserSearchPanel("userSearchPanel", this::onSearchClick, searchForm);
        this.mainSidebar.add(this.userSearchPanel);

        IModel<List<SideMenu>> sideMenu = buildSideMenu();
        this.sideMenuPanel = new SideMenuPanel("sideMenuPanel", sideMenu);
        this.mainSidebar.add(this.sideMenuPanel);

        this.pageLogoPanel = new PageLogoPanel("pageLogoPanel", buildPageLogo());
        this.add(this.pageLogoPanel);
        this.pageFooterPanel = new PageFooterPanel("pageFooterPanel", buildPageFooter());
        this.add(this.pageFooterPanel);

        Label pageTitle = new Label("pageTitle",
                () -> pageHeader == null || pageHeader.getObject() == null || pageHeader.getObject().getTitle() == null
                        || (pageHeader.getObject().getTitle() == null || "".equals(pageHeader.getObject().getTitle()))
                        ? String.valueOf(LocalDateTime.now().getYear())
                        : pageHeader.getObject().getTitle());
        this.add(pageTitle);

        if ((userInfo == null || userInfo.getObject() == null)
                && (searchForm == null || searchForm.getObject() == null || !searchForm.getObject())
                && (sideMenu == null || sideMenu.getObject() == null) || sideMenu.getObject().isEmpty()) {
            this.leftSideBar = false;
        } else {
            this.leftSideBar = true;
        }
        this.mainSidebar.setVisible(this.leftSideBar);

        this.toogleButton = new WebMarkupContainer("toogleButton");
        this.add(this.toogleButton);
        this.toogleButton.setVisible(this.leftSideBar);

        initComponent();
        configureMetaData();
    }

    protected abstract void initData();

    protected abstract void initComponent();

    protected abstract void configureMetaData();

    private boolean report(FeedbackMessage feedbackMessage) {
        if (feedbackMessage.getReporter() instanceof org.apache.wicket.Page) {
            return true;
        }
        return false;
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);
        response.render(CssHeaderItem.forCSS(".dropdown-menu {  z-index: 100060 !important; }", "menu"));

        if (!this.leftSideBar) {
            response.render(
                    CssHeaderItem.forCSS(".content-wrapper {  margin-left:0px !important; }", "content-wrapper"));
            response.render(CssHeaderItem.forCSS(".main-footer {  margin-left:0px !important; }", "main-footer"));
        }
        AdminLTEOption option = new AdminLTEOption();
        option.Bootstrap = true;
        option.FontAwesome = true;
        option.Ionicons = true;
        option.AdminLTE = true;
        option.HTML5ShimAndRespondJS = true;
        option.GoogleFont = true;
        option.JQuery = true;
        ReferenceUtilities.render(option, response);
    }

    protected abstract IModel<PageLogo> buildPageLogo();

    protected abstract IModel<PageFooter> buildPageFooter();

    protected abstract IModel<PageHeader> buildPageHeader();

    protected abstract IModel<List<PageBreadcrumb>> buildPageBreadcrumb();

    protected abstract IModel<List<SideMenu>> buildSideMenu();

    protected abstract IModel<List<NavBarMenu>> buildNavBarMenu();

    protected abstract IModel<UserInfo> buildUserInfo();

    protected abstract IModel<Boolean> hasSearchForm();

    protected abstract void onSearchClick(String searchValue);

}
