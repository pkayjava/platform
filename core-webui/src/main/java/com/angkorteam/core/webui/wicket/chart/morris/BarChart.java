package com.angkorteam.core.webui.wicket.chart.morris;

import com.angkorteam.core.webui.ReferenceUtilities;
import com.google.gson.Gson;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.WebComponent;
import org.apache.wicket.markup.parser.XmlTag.TagType;
import org.apache.wicket.model.IModel;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class BarChart extends WebComponent {

    /**
     *
     */
    private static final long serialVersionUID = -5784824596082484788L;

    private IModel<BarDataset> dataset;

    public BarChart(String id, IModel<BarDataset> model) {
        super(id);
        setOutputMarkupId(true);
        this.dataset = model;
    }

    @Override
    protected void onComponentTag(final ComponentTag tag) {
        checkComponentTag(tag, "div");
        super.onComponentTag(tag);
        if (tag.isOpenClose()) {
            tag.setType(TagType.OPEN);
        }
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);

        ReferenceUtilities.renderJavascript(response, "<!-- Morris -->");
        ReferenceUtilities.renderJavascript(response,
                ReferenceUtilities.AdminLTE + "/bower_components/raphael/raphael.min.js");
        ReferenceUtilities.renderJavascript(response,
                ReferenceUtilities.AdminLTE + "/bower_components/morris.js/morris.min.js");

        String markupId = getMarkupId();
        this.dataset.getObject().setElement(markupId);

        Gson gson = new Gson();

        String chart = String.format("new Morris.Bar(%s)", gson.toJson(this.dataset.getObject()));
        response.render(OnDomReadyHeaderItem.forScript(chart));
    }

    public static class BarDataset implements Serializable {

        /**
         *
         */
        private static final long serialVersionUID = 6833934310839500435L;

        private String element;

        private String xkey;

        private List<String> ykeys = new LinkedList<>();

        private List<String> labels = new LinkedList<>();

        private List<String> barColors = new LinkedList<>();

        private String hideOver = "auto";

        private boolean resize = true;

        private List<Map<String, Object>> data = new LinkedList<>();

        public String getElement() {
            return element;
        }

        public void setElement(String element) {
            this.element = element;
        }

        public String getXkey() {
            return xkey;
        }

        public void setXkey(String xkey) {
            this.xkey = xkey;
        }

        public String getHideOver() {
            return hideOver;
        }

        public boolean isResize() {
            return resize;
        }

        public List<String> getYkeys() {
            return ykeys;
        }

        public List<String> getLabels() {
            return labels;
        }

        public List<String> getBarColors() {
            return barColors;
        }

        public List<Map<String, Object>> getData() {
            return data;
        }

    }

}