package com.angkorteam.core.webui.models;

import java.io.Serializable;

/**
 * Created by socheatkhauv on 6/11/17.
 */
public class PageHeader implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 4184459932089153421L;

    private String title;

    private String description;

    private PageHeader() {
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public static class Builder {

        private String title;

        private String description;

        public PageHeader build() {
            PageHeader item = new PageHeader();
            item.title = this.title;
            item.description = this.description;
            return item;
        }

        public Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

    }

}
