package com.angkorteam.core.webui.wicket.markup.html.form;

import com.angkorteam.core.webui.ReferenceUtilities;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.model.IModel;

public class JavascriptTextArea extends TextArea<String> {

    /**
     *
     */
    private static final long serialVersionUID = 7590641085407244298L;

    public JavascriptTextArea(String id) {
        super(id);
    }

    public JavascriptTextArea(String id, IModel<String> model) {
        super(id, model);
    }

    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);
        String markupId = this.getMarkupId(true);
        response.render(CssHeaderItem.forReference(
                ReferenceUtilities.loadResourceReference(ReferenceUtilities.CODEMIRROR + "lib/codemirror.css")));
        response.render(CssHeaderItem.forReference(
                ReferenceUtilities.loadResourceReference(ReferenceUtilities.CODEMIRROR + "addon/hint/show-hint.css")));
        response.render(CssHeaderItem.forReference(ReferenceUtilities
                .loadResourceReference(ReferenceUtilities.CODEMIRROR + "addon/display/fullscreen.css")));
        response.render(CssHeaderItem.forReference(
                ReferenceUtilities.loadResourceReference(ReferenceUtilities.CODEMIRROR + "theme/night.css")));
        response.render(JavaScriptHeaderItem.forReference(
                ReferenceUtilities.loadResourceReference(ReferenceUtilities.CODEMIRROR + "lib/codemirror.js")));
        response.render(JavaScriptHeaderItem.forReference(
                ReferenceUtilities.loadResourceReference(ReferenceUtilities.CODEMIRROR + "addon/hint/show-hint.js")));
        response.render(JavaScriptHeaderItem.forReference(ReferenceUtilities
                .loadResourceReference(ReferenceUtilities.CODEMIRROR + "addon/hint/javascript-hint.js")));
        response.render(JavaScriptHeaderItem.forReference(ReferenceUtilities
                .loadResourceReference(ReferenceUtilities.CODEMIRROR + "mode/javascript/javascript.js")));
        response.render(JavaScriptHeaderItem.forReference(ReferenceUtilities
                .loadResourceReference(ReferenceUtilities.CODEMIRROR + "addon/display/fullscreen.js")));
        response.render(OnDomReadyHeaderItem.forScript("CodeMirror.fromTextArea(document.getElementById('" + markupId
                + "'), { indentUnit: 4, lineNumbers: true, theme: 'night', extraKeys: {'Ctrl-Space': 'autocomplete', 'F11': function(cm) { cm.setOption('fullScreen', !cm.getOption('fullScreen')); }, 'Esc': function(cm) { if (cm.getOption('fullScreen')) cm.setOption('fullScreen', false); }}, mode: {name: 'javascript', globalVars: true} })"));
    }

    protected void onInitialize() {
        super.onInitialize();
        this.setOutputMarkupId(true);
    }
}
