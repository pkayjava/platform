package com.angkorteam.core.webui.wicket.markup.html.form.select2;

/**
 * Created by socheat on 5/25/16.
 */
public abstract class SingleChoiceProvider<T> extends IChoiceProvider<T> {

    /**
     *
     */
    private static final long serialVersionUID = -613218455319945156L;

    public abstract T toChoice(String id);

}
