package com.angkorteam.core.webui.models;

import com.angkorteam.core.webui.BadgeType;
import com.angkorteam.core.webui.Emoji;
import com.angkorteam.core.webui.NavBarMenuType;
import com.angkorteam.core.webui.TextColor;
import com.angkorteam.core.webui.panels.UserFooterPanel;
import com.angkorteam.core.webui.panels.UserHeaderPanel;
import org.apache.wicket.Page;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;

import java.io.Serializable;
import java.util.List;

/**
 * Created by socheatkhauv on 6/12/17.
 */
public class NavBarMenu implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6506467616658637943L;

    private Emoji icon;

    private TextColor iconColor;

    private String badge;

    private BadgeType badgeType;

    private String label;

    private String image;

    private Class<? extends Page> page;

    private PageParameters parameters;

    private List<NavBarMenuItem> children;

    private NavBarMenuType type;

    private String headerText;

    private Panel headerPanel;

    private String footerText;

    private Panel footerPanel;

    private Class<? extends Page> footerPage;

    private PageParameters footerParameters;

    private NavBarMenu() {
    }

    public Emoji getIcon() {
        return this.icon;
    }

    public String getBadge() {
        return this.badge;
    }

    public BadgeType getBadgeType() {
        return this.badgeType;
    }

    public TextColor getIconColor() {
        return this.iconColor;
    }

    public String getLabel() {
        return this.label;
    }

    public Class<? extends Page> getPage() {
        return page;
    }

    public PageParameters getParameters() {
        return parameters;
    }

    public List<NavBarMenuItem> getChildren() {
        return children;
    }

    public NavBarMenuType getType() {
        return type;
    }

    public String getImage() {
        return image;
    }

    public String getHeaderText() {
        return this.headerText;
    }

    public Panel getHeaderPanel() {
        return headerPanel;
    }

    public Panel getFooterPanel() {
        return footerPanel;
    }

    public String getFooterText() {
        return this.footerText;
    }

    public Class<? extends Page> getFooterPage() {
        return footerPage;
    }

    public PageParameters getFooterParameters() {
        return footerParameters;
    }

    public static class Builder {

        private Emoji icon;

        private TextColor iconColor;

        private String badge;

        private BadgeType badgeType;

        private String label;

        private String image;

        private Class<? extends Page> page;

        private PageParameters parameters;

        private List<NavBarMenuItem> children;

        private NavBarMenuType type;

        private String headerText;

        private Panel headerPanel;

        private String footerText;

        private Panel footerPanel;

        private Class<? extends Page> footerPage;

        private PageParameters footerParameters;

        public Builder() {
        }

        public NavBarMenu build() {
            NavBarMenu item = new NavBarMenu();
            item.icon = this.icon;
            item.iconColor = this.iconColor;
            item.badge = this.badge;
            item.badgeType = this.badgeType;
            item.label = this.label;
            item.image = this.image;
            item.page = this.page;
            item.parameters = this.parameters;
            item.children = this.children;
            item.type = this.type;
            item.headerText = this.headerText;
            item.headerPanel = this.headerPanel;
            item.footerText = this.footerText;
            item.footerPanel = this.footerPanel;
            item.footerPage = this.footerPage;
            item.footerParameters = this.footerParameters;
            return item;
        }

        public Builder withHeader(String label) {
            this.headerText = label;
            this.headerPanel = null;
            return this;
        }

        public Builder withHeader(UserHeaderPanel header) {
            this.headerText = null;
            this.headerPanel = header;
            return this;
        }

        public Builder withFooter(String label) {
            this.footerText = label;
            this.footerPage = null;
            this.footerParameters = null;
            this.footerPanel = null;
            return this;
        }

        public Builder withFooter(String label, Class<? extends Page> page) {
            return withFooter(label, page, new PageParameters());
        }

        public Builder withFooter(String label, Class<? extends Page> page, PageParameters parameters) {
            this.footerText = label;
            this.footerPage = page;
            this.footerParameters = parameters;
            this.footerPanel = null;
            return this;
        }

        public Builder withFooter(UserFooterPanel footer) {
            this.footerText = null;
            this.footerPage = null;
            this.footerParameters = null;
            this.footerPanel = footer;
            return this;
        }

        public Builder withIcon(Emoji icon, String label, Class<? extends Page> page) {
            return withIcon(icon, label, page, "");
        }

        public Builder withIcon(Emoji icon, String label, Class<? extends Page> page, String badge) {
            return withIcon(icon, label, page, badge, new PageParameters(), TextColor.Gray, BadgeType.Danger);
        }

        public Builder withIcon(Emoji icon, String label, Class<? extends Page> page, String badge,
                                PageParameters parameters, TextColor color, BadgeType badgeType) {
            this.type = NavBarMenuType.ItemIcon;
            this.page = page;
            this.parameters = parameters;
            this.icon = icon;
            this.iconColor = color;
            this.label = label;
            this.badge = badge;
            this.badgeType = badgeType;
            return this;
        }

        public Builder withIcon(Emoji icon, String label, List<NavBarMenuItem> items) {
            return withIcon(icon, label, items, "");
        }

        public Builder withIcon(Emoji icon, String label, List<NavBarMenuItem> items, String badge) {
            return withIcon(icon, label, items, badge, TextColor.Gray, BadgeType.Danger);
        }

        public Builder withIcon(Emoji icon, String label, List<NavBarMenuItem> items, String badge, TextColor color,
                                BadgeType badgeType) {
            this.type = NavBarMenuType.ItemIcon;
            this.icon = icon;
            this.label = label;
            this.badge = badge;
            this.iconColor = color;
            this.children = items;
            this.badgeType = badgeType;
            return this;
        }

        public Builder withImage(String image, String label, Class<? extends Page> page) {
            return withImage(image, label, page, "");
        }

        public Builder withImage(String image, String label, Class<? extends Page> page, String badge) {
            return withImage(image, label, page, badge, new PageParameters(), Model.of(BadgeType.Danger));
        }

        public Builder withImage(String image, String label, Class<? extends Page> page, String badge,
                                 PageParameters parameters, IModel<BadgeType> badgeType) {
            this.type = NavBarMenuType.ItemImage;
            this.page = page;
            this.parameters = parameters;
            this.image = image;
            this.label = label;
            this.badge = badge;
            return this;
        }

        public Builder withImage(String image, String label, List<NavBarMenuItem> items) {
            return withImage(image, label, items, "");
        }

        public Builder withImage(String image, String label, List<NavBarMenuItem> items, String badge) {
            return withImage(image, label, items, badge, BadgeType.Danger);
        }

        public Builder withImage(String image, String label, List<NavBarMenuItem> items, String badge,
                                 BadgeType badgeType) {
            this.type = NavBarMenuType.ItemImage;
            this.image = image;
            this.label = label;
            this.badge = badge;
            this.children = items;
            this.badgeType = badgeType;
            return this;
        }

    }

}
