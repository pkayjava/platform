package com.angkorteam.core.spring.jdbc;

import org.springframework.dao.DataAccessException;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LongResultSetExtractor implements org.springframework.jdbc.core.ResultSetExtractor<Long> {


    @Override
    public Long extractData(ResultSet rs) throws SQLException, DataAccessException {
        try {
            rs.next();
            return rs.getLong(1);
        } catch (SQLException e) {
            return null;
        } finally {
            rs.close();
        }
    }
}
