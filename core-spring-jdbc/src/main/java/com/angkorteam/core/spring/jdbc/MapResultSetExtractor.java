package com.angkorteam.core.spring.jdbc;

import org.springframework.dao.DataAccessException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

public class MapResultSetExtractor implements org.springframework.jdbc.core.ResultSetExtractor<Map<String, Object>> {


    @Override
    public Map<String, Object> extractData(ResultSet rs) throws SQLException, DataAccessException {
        try {
            rs.next();
            ColumnMapRowMapper mapRowMapper = new ColumnMapRowMapper(true);
            return mapRowMapper.mapRow(rs, 0);
        } catch (SQLException e) {
            return null;
        } finally {
            rs.close();
        }
    }
}
