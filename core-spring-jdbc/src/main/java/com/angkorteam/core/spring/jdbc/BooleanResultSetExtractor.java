package com.angkorteam.core.spring.jdbc;

import org.springframework.dao.DataAccessException;

import java.sql.ResultSet;
import java.sql.SQLException;

public class BooleanResultSetExtractor implements org.springframework.jdbc.core.ResultSetExtractor<Boolean> {


    @Override
    public Boolean extractData(ResultSet rs) throws SQLException, DataAccessException {
        try {
            rs.next();
            return rs.getBoolean(1);
        } catch (SQLException e) {
            return null;
        } finally {
            rs.close();
        }
    }
}
