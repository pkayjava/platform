package com.angkorteam.core.spring.jdbc;

import java.util.Locale;

public class LinkedCaseInsensitiveMap<V> extends org.springframework.util.LinkedCaseInsensitiveMap<V> {

    /**
     *
     */
    private static final long serialVersionUID = 2618945707828764858L;

    public LinkedCaseInsensitiveMap() {
        super();
    }

    public LinkedCaseInsensitiveMap(int initialCapacity, Locale locale) {
        super(initialCapacity, locale);
    }

    public LinkedCaseInsensitiveMap(int initialCapacity) {
        super(initialCapacity);
    }

    public LinkedCaseInsensitiveMap(Locale locale) {
        super(locale);
    }

    @Override
    public V get(Object key) {
        if (containsKey(key)) {
            return super.get(key);
        }
        throw new IllegalArgumentException(key + " is not found, please see your select statement again");
    }

}