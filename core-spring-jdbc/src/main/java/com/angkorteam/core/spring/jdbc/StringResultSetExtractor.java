package com.angkorteam.core.spring.jdbc;

import org.springframework.dao.DataAccessException;

import java.sql.ResultSet;
import java.sql.SQLException;

public class StringResultSetExtractor implements org.springframework.jdbc.core.ResultSetExtractor<String> {


    @Override
    public String extractData(ResultSet rs) throws SQLException, DataAccessException {
        try {
            rs.next();
            return rs.getString(1);
        } catch (SQLException e) {
            return null;
        } finally {
            rs.close();
        }
    }
}
