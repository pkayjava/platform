package com.angkorteam.core.spring.jdbc;

import org.springframework.dao.DataAccessException;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DateResultSetExtractor implements org.springframework.jdbc.core.ResultSetExtractor<Date> {


    @Override
    public Date extractData(ResultSet rs) throws SQLException, DataAccessException {
        try {
            rs.next();
            return rs.getDate(1);
        } catch (SQLException e) {
            return null;
        } finally {
            rs.close();
        }
    }
}
