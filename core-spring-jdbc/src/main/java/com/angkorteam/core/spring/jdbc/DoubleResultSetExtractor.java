package com.angkorteam.core.spring.jdbc;

import org.springframework.dao.DataAccessException;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DoubleResultSetExtractor implements org.springframework.jdbc.core.ResultSetExtractor<Double> {


    @Override
    public Double extractData(ResultSet rs) throws SQLException, DataAccessException {
        try {
            rs.next();
            return rs.getDouble(1);
        } catch (SQLException e) {
            return null;
        } finally {
            rs.close();
        }
    }

}
