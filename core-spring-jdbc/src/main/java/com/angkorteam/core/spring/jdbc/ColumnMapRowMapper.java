package com.angkorteam.core.spring.jdbc;

import org.springframework.jdbc.support.JdbcUtils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Map;

public class ColumnMapRowMapper extends org.springframework.jdbc.core.ColumnMapRowMapper {

    private final boolean ignoreTable;

    public ColumnMapRowMapper() {
        this(false);
    }

    public ColumnMapRowMapper(boolean ignoreTable) {
        this.ignoreTable = ignoreTable;
    }

    @Override
    public Map<String, Object> mapRow(ResultSet rs, int rowNum) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnCount = rsmd.getColumnCount();
        Map<String, Object> mapOfColValues = createColumnMap(columnCount);
        for (int i = 1; i <= columnCount; i++) {
            String key = getColumnKey(JdbcUtils.lookupColumnName(rsmd, i));
            String tableName = rsmd.getTableName(i);
            Object obj = getColumnValue(rs, i);
            if (obj != null) {
                if (obj instanceof Byte || obj instanceof Short || obj instanceof Integer) {
                    mapOfColValues.put(key, ((Number) obj).longValue());
                    if (!this.ignoreTable && tableName != null && !"".equals(tableName)) {
                        mapOfColValues.put(tableName + "." + key, ((Number) obj).longValue());
                    }
                } else if (obj instanceof Float) {
                    mapOfColValues.put(key, ((Number) obj).doubleValue());
                    if (!this.ignoreTable && tableName != null && !"".equals(tableName)) {
                        mapOfColValues.put(tableName + "." + key, ((Number) obj).doubleValue());
                    }
                } else if (obj instanceof BigInteger) {
                    mapOfColValues.put(key, ((Number) obj).longValue());
                    if (!this.ignoreTable && tableName != null && !"".equals(tableName)) {
                        mapOfColValues.put(tableName + "." + key, ((Number) obj).longValue());
                    }
                } else if (obj instanceof BigDecimal) {
                    mapOfColValues.put(key, ((Number) obj).doubleValue());
                    if (!this.ignoreTable && tableName != null && !"".equals(tableName)) {
                        mapOfColValues.put(tableName + "." + key, ((Number) obj).doubleValue());
                    }
                } else {
                    mapOfColValues.put(key, obj);
                    if (!this.ignoreTable && tableName != null && !"".equals(tableName)) {
                        mapOfColValues.put(tableName + "." + key, obj);
                    }
                }
            } else {
                mapOfColValues.put(key, null);
                if (!this.ignoreTable && tableName != null && !"".equals(tableName)) {
                    mapOfColValues.put(tableName + "." + key, null);
                }
            }
        }
        return mapOfColValues;
    }

    @Override
    protected Map<String, Object> createColumnMap(int columnCount) {
        return new LinkedCaseInsensitiveMap<>(columnCount);
    }

}
