package com.angkorteam.core.spring.jdbc;

import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by socheatkhauv on 30/1/17.
 */
public class JdbcNamed extends org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate {

    private static final ColumnMapRowMapper MAPPER = new ColumnMapRowMapper();

    private final ColumnMapRowMapper mapper;

    public JdbcNamed(DataSource dataSource) {
        this(dataSource, MAPPER);
    }

    public JdbcNamed(DataSource dataSource, ColumnMapRowMapper mapper) {
        super(dataSource);
        setQueryTimeout(10);
        this.mapper = mapper;
    }

    public void setQueryTimeout(int queryTimeout) {
        ((org.springframework.jdbc.core.JdbcTemplate) getJdbcOperations()).setQueryTimeout(queryTimeout);
    }

    public int getQueryTimeout() {
        return ((org.springframework.jdbc.core.JdbcTemplate) getJdbcOperations()).getQueryTimeout();
    }

    @Override
    public <T> T queryForObject(String sql, SqlParameterSource paramSource, Class<T> requiredType)
            throws DataAccessException {
        try {
            if (requiredType == Boolean.class || requiredType == boolean.class || requiredType == Byte.class
                    || requiredType == byte.class || requiredType == Short.class || requiredType == short.class
                    || requiredType == Integer.class || requiredType == int.class || requiredType == Long.class
                    || requiredType == long.class || requiredType == Float.class || requiredType == float.class
                    || requiredType == Double.class || requiredType == double.class || requiredType == Character.class
                    || requiredType == char.class || requiredType == String.class || requiredType == Date.class) {
                return super.queryForObject(sql, paramSource, requiredType);
            } else {
                return super.queryForObject(sql, paramSource, new BeanPropertyRowMapper<>(requiredType));
            }
        } catch (EmptyResultDataAccessException | IllegalStateException e) {
            return null;
        }
    }

    public <T> T queryForObject(String sql, Class<T> requiredType) throws DataAccessException {
        return queryForObject(sql, Collections.emptyMap(), requiredType);
    }

    @Override
    public <T> T queryForObject(String sql, Map<String, ?> paramMap, RowMapper<T> rowMapper)
            throws DataAccessException {
        try {
            return super.queryForObject(sql, paramMap, rowMapper);
        } catch (EmptyResultDataAccessException | IllegalStateException e) {
            return null;
        }
    }

    public <T> T queryForObject(String sql, RowMapper<T> rowMapper) throws DataAccessException {
        try {
            return super.queryForObject(sql, Collections.emptyMap(), rowMapper);
        } catch (EmptyResultDataAccessException | IllegalStateException e) {
            return null;
        }
    }

    @Override
    public <T> T queryForObject(String sql, Map<String, ?> paramMap, Class<T> requiredType) throws DataAccessException {
        try {
            if (requiredType == Boolean.class || requiredType == boolean.class || requiredType == Byte.class
                    || requiredType == byte.class || requiredType == Short.class || requiredType == short.class
                    || requiredType == Integer.class || requiredType == int.class || requiredType == Long.class
                    || requiredType == long.class || requiredType == Float.class || requiredType == float.class
                    || requiredType == Double.class || requiredType == double.class || requiredType == Character.class
                    || requiredType == char.class || requiredType == String.class || requiredType == Date.class) {
                return super.queryForObject(sql, paramMap, requiredType);
            } else {
                return super.queryForObject(sql, paramMap, new BeanPropertyRowMapper<>(requiredType));
            }
        } catch (EmptyResultDataAccessException | IllegalStateException e) {
            return null;
        }
    }

    @Override
    public Map<String, Object> queryForMap(String sql, SqlParameterSource paramSource) throws DataAccessException {
        try {
            return super.queryForObject(sql, paramSource, getColumnMapRowMapper());
        } catch (EmptyResultDataAccessException | IllegalStateException e) {
            return null;
        }
    }

    public Map<String, Object> queryForMap(String sql) throws DataAccessException {
        return queryForMap(sql, Collections.emptyMap());
    }

    @Override
    public Map<String, Object> queryForMap(String sql, Map<String, ?> paramMap) throws DataAccessException {
        try {
            return super.queryForObject(sql, paramMap, getColumnMapRowMapper());
        } catch (EmptyResultDataAccessException | IllegalStateException e) {
            return null;
        }
    }

    @Override
    public <T> List<T> queryForList(String sql, SqlParameterSource paramSource, Class<T> elementType)
            throws DataAccessException {
        if (elementType == Boolean.class || elementType == boolean.class || elementType == Byte.class
                || elementType == byte.class || elementType == Short.class || elementType == short.class
                || elementType == Integer.class || elementType == int.class || elementType == Long.class
                || elementType == long.class || elementType == Float.class || elementType == float.class
                || elementType == Double.class || elementType == double.class || elementType == Character.class
                || elementType == char.class || elementType == String.class || elementType == Date.class) {
            return super.queryForList(sql, paramSource, elementType);
        } else {
            return super.query(sql, paramSource, new BeanPropertyRowMapper<>(elementType));
        }
    }

    public <T> List<T> queryForList(String sql, Class<T> elementType) throws DataAccessException {
        return queryForList(sql, Collections.emptyMap(), elementType);
    }

    @Override
    public <T> List<T> queryForList(String sql, Map<String, ?> paramMap, Class<T> elementType)
            throws DataAccessException {
        if (elementType == Boolean.class || elementType == boolean.class || elementType == Byte.class
                || elementType == byte.class || elementType == Short.class || elementType == short.class
                || elementType == Integer.class || elementType == int.class || elementType == Long.class
                || elementType == long.class || elementType == Float.class || elementType == float.class
                || elementType == Double.class || elementType == double.class || elementType == Character.class
                || elementType == char.class || elementType == String.class || elementType == Date.class) {
            return super.queryForList(sql, paramMap, elementType);
        } else {
            return super.query(sql, paramMap, new BeanPropertyRowMapper<>(elementType));
        }
    }

    public List<Map<String, Object>> queryForList(String sql) throws DataAccessException {
        return queryForList(sql, Collections.emptyMap());
    }

    @Override
    public List<Map<String, Object>> queryForList(String sql, Map<String, ?> paramMap) throws DataAccessException {
        return super.query(sql, new MapSqlParameterSource(paramMap), getColumnMapRowMapper());

    }

    @Override
    public List<Map<String, Object>> queryForList(String sql, SqlParameterSource paramSource)
            throws DataAccessException {
        return super.query(sql, paramSource, getColumnMapRowMapper());
    }

    protected RowMapper<Map<String, Object>> getColumnMapRowMapper() {
        return this.mapper;
    }

}
