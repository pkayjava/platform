package com.angkorteam.core.spring.jdbc;

import org.springframework.dao.DataAccessException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;

public class TimeResultSetExtractor implements org.springframework.jdbc.core.ResultSetExtractor<Time> {


    @Override
    public Time extractData(ResultSet rs) throws SQLException, DataAccessException {
        try {
            rs.next();
            return rs.getTime(1);
        } catch (SQLException e) {
            return null;
        } finally {
            rs.close();
        }
    }
}
