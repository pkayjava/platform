package com.angkorteam.network.dns.server.packet;

import java.io.Serializable;
import java.nio.ByteBuffer;

public class DnsPacket implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1061218046420583193L;

    public final byte[] transactionId = new byte[2];

    public final byte[] flags = new byte[2];

    public boolean flagsResponse;

    public byte flagsOpcode;

    public boolean flagsTruncated;

    public boolean flagsRecursionDesired;

    public boolean flagsZ;

    public boolean flagsNonAuthenticated;

    public final byte[] questions = new byte[2];

    public final byte[] answerRrs = new byte[2];

    public final byte[] authorityRrs = new byte[2];

    public final byte[] additionalRrs = new byte[2];

    public String name;

    public byte[] type = new byte[2];

    public byte[] clazz = new byte[2];

    public int size() {
        int size = 0;

        int test1 = Short.parseShort("10000000", 2);

        int test2 = Short.parseShort("01000000", 2);

        System.out.println(Integer.toBinaryString(test1 | test2));

        size += transactionId.length;

        size += flags.length;

        size += questions.length;

        size += answerRrs.length;

        size += authorityRrs.length;

        if (this.name != null) {
            size += this.name.getBytes().length;
        }

        size += type.length;

        size += clazz.length;

        return size;
    }

    public static byte[] serialize(DnsPacket packet) {
        ByteBuffer buffer = ByteBuffer.allocate(packet.size());

        buffer.put(packet.transactionId);

        buffer.put(packet.flags);

        buffer.put(packet.questions);

        buffer.put(packet.answerRrs);

        buffer.put(packet.additionalRrs);

        if (packet.name != null) {
            buffer.put(packet.name.getBytes());
        }

        buffer.put(packet.type);

        buffer.put(packet.clazz);

        return buffer.array();
    }

    public static DnsPacket deserialize(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.wrap(bytes);

        DnsPacket packet = new DnsPacket();

        buffer.get(packet.transactionId);

        // 2 bytes
        short flags = buffer.getShort();


        buffer.get(packet.questions);

        buffer.get(packet.answerRrs);

        buffer.get(packet.authorityRrs);

        buffer.get(packet.additionalRrs);

        return packet;
    }

}
