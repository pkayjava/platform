//package com.angkorteam.network.dns.server;
//
//import java.util.Collections;
//import java.util.Map;
//import java.util.TreeMap;
//
//import org.pcap4j.core.NotOpenException;
//import org.pcap4j.core.PcapHandle;
//import org.pcap4j.core.PcapHandle.BlockingMode;
//import org.pcap4j.core.PcapNativeException;
//import org.pcap4j.packet.EthernetPacket;
//import org.pcap4j.packet.IpV4Packet;
//import org.pcap4j.packet.Packet;
//import org.pcap4j.packet.UdpPacket;
//import org.pcap4j.packet.UnknownPacket;
//import org.pcap4j.util.MacAddress;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.dao.EmptyResultDataAccessException;
//import org.springframework.jdbc.core.JdbcTemplate;
//
//import com.angkorteam.core.function.Bytes;
//import com.angkorteam.network.dns.server.packet.DhcpPacket;
//import com.angkorteam.network.dns.server.packet.Option;
//
//public class CaptureThread implements Runnable {
//
//    private static final Logger LOGGER = LoggerFactory.getLogger(CaptureThread.class);
//
//    private final String hardware;
//
//    private final PcapHandle handle;
//
//    public static final Map<String, String> DB = Collections.synchronizedMap(new TreeMap<>());
//
//    private final JdbcTemplate jdbcTemplate;
//
//    public CaptureThread(JdbcTemplate jdbcTemplate, String hardware, PcapHandle handle) {
//	this.handle = handle;
//	this.jdbcTemplate = jdbcTemplate;
//	this.hardware = hardware;
//    }
//
//    @Override
//    public void run() {
//	while (true) {
//	    try {
//		this.handle.setBlockingMode(BlockingMode.BLOCKING);
//		Packet packet = this.handle.getNextPacket();
//		if (packet != null) {
//		    if (packet instanceof EthernetPacket) {
//			parseEthernetPacket(handle, (EthernetPacket) packet);
//		    } else {
//			// LOGGER.info("packet class {}", packet.getClass().getName());
//		    }
//		}
//	    } catch (NotOpenException | PcapNativeException e) {
//		LOGGER.info("something wrong with next packet due to this reason {}", e.getMessage());
//	    }
//	}
//    }
//
//    protected void parseEthernetPacket(PcapHandle handle, EthernetPacket ethernetPacket) {
//	if (ethernetPacket.getPayload() instanceof IpV4Packet) {
//	    parseIpV4Packet(handle, ethernetPacket, (IpV4Packet) ethernetPacket.getPayload());
//	} else {
//	    if (ethernetPacket.getPayload() != null) {
//		// LOGGER.info("ethernet packet payload class {}",
//		// ethernetPacket.getPayload().getClass().getName());
//	    }
//	}
//    }
//
//    protected void parseIpV4Packet(PcapHandle handle, EthernetPacket ethernetPacket, IpV4Packet ipV4Packet) {
//	if (ipV4Packet.getPayload() instanceof UdpPacket) {
//	    parseUdpPacket(handle, ethernetPacket, ipV4Packet, (UdpPacket) ipV4Packet.getPayload());
//	} else {
//	    if (ipV4Packet.getPayload() != null) {
//		// LOGGER.info("ipv4 packet payload class {}",
//		// ipV4Packet.getPayload().getClass().getName());
//	    }
//	}
//    }
//
//    protected void parseUdpPacket(PcapHandle handle, EthernetPacket ethernetPacket, IpV4Packet ipV4Packet,
//	    UdpPacket udpPacket) {
//	if (udpPacket.getPayload() instanceof UnknownPacket) {
//	    parseUnknownPacket(handle, ethernetPacket, ipV4Packet, udpPacket, (UnknownPacket) udpPacket.getPayload());
//	} else {
//	    if (udpPacket.getPayload() != null) {
//		// LOGGER.info("udp packet payload class {}",
//		// udpPacket.getPayload().getClass().getName());
//	    }
//	}
//    }
//
//    protected void parseUnknownPacket(PcapHandle handle, EthernetPacket ethernetPacket, IpV4Packet ipV4Packet,
//	    UdpPacket udpPacket, UnknownPacket unknownPacket) {
//	if (udpPacket.getHeader().getDstPort().valueAsInt() != 67
//		&& udpPacket.getHeader().getDstPort().valueAsInt() != 68) {
//	    return;
//	}
//	DhcpPacket dhcpPacket = DhcpPacket.deserialize(unknownPacket.getRawData());
//
//	String server_nic_id = null;
//	try {
//	    server_nic_id = this.jdbcTemplate.queryForObject(
//		    "SELECT server_nic_id FROM tbl_server_nic WHERE name = ? AND primary_hardware = true AND primary_host = true AND enabled = true",
//		    String.class, this.hardware);
//	} catch (EmptyResultDataAccessException e) {
//	    return;
//	}
//	String serverHardwareAddress = this.jdbcTemplate.queryForObject(
//		"SELECT hardware_address FROM tbl_server_nic WHERE server_nic_id = ?", String.class, server_nic_id);
//	String clientHardwareAddress = MacAddress.getByAddress(Bytes.extract(dhcpPacket.chaddr, 0, dhcpPacket.hlen))
//		.toString();
//
//	String xid = Integer.toHexString(Bytes.toInteger(dhcpPacket.xid));
//	if (dhcpPacket.op == DhcpPacket.OP_REQUEST) {
//	    byte[] values = dhcpPacket.options.get(Option.P_053.getLiteral());
//	    if (values != null && values.length > 0) {
//		if (values[0] == DhcpPacket.DHCP_DISCOVER || values[0] == DhcpPacket.DHCP_REQUEST) {
//		    String type = "?";
//		    if (values[0] == DhcpPacket.DHCP_DISCOVER) {
//			type = "D";
//		    } else if (values[0] == DhcpPacket.DHCP_REQUEST) {
//			type = "R";
//		    }
//		    if (DB.containsKey(xid)) {
//			if (server_nic_id.equalsIgnoreCase(DB.get(xid))) {
//			    LOGGER.info("[{}] [Captured {}] SMAC [{}] CMAC [{}]", xid, type, serverHardwareAddress,
//				    clientHardwareAddress);
//			} else {
//			    String temp = DB.remove(xid);
//			    LOGGER.info("[{}] [Duplicated] SMAC [{}] CMAC [{}] XMAC [{}]", xid, serverHardwareAddress,
//				    clientHardwareAddress, temp);
//			}
//		    } else {
//			DB.put(xid, server_nic_id);
//			LOGGER.info("[{}] [Captured {}] SMAC [{}] CMAC [{}]", xid, type, serverHardwareAddress,
//				clientHardwareAddress);
//		    }
//		}
//	    }
//	} else if (dhcpPacket.op == DhcpPacket.OP_REPLY) {
//	    byte[] values = dhcpPacket.options.get(Option.P_053.getLiteral());
//	    if (values != null && values.length > 0) {
//		if (values[0] == DhcpPacket.DHCP_ACK) {
//		    DB.remove(xid);
//		    LOGGER.info("[{}] [Accepted  ] SMAC [{}] CMAC [{}]", xid, serverHardwareAddress,
//			    clientHardwareAddress);
//		}
//	    }
//	}
//    }
//
//}
