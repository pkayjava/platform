package com.angkorteam.network.dns.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class BootApplication {

    public static void main(String[] args) throws Exception {

        ApplicationContext context = SpringApplication.run(BootApplication.class, args);

    }

    public void makeOffer() {

        // from dhcp server

        // source 172.16.1.1
        // destination 172.16.1.100
        // message type = 2
        // hardware type = 1 (ethernet)
        // hardware address length = 6
        // hops = 0

        // transaction id : same

        // seconds elapsed : 0
        // bootp flags: 0

        // client ip address : 0.0.0.0
        // your (client) ip address : 172.16.1.100
        // next server ip address : 0.0.0.0
        // relay agent ip address : 0.0.0.0
        // client mac address : c4:8e:8f:fb:f3:01
        // client hardware address padding : 0
        // server host name not given
        // boot file name not given
        // magic cookie: DHCP

        // option: 53 - DHCP message type -> offer
        // option: 54 - DHCP Server Identifier -> 172.16.1.1
        // option: 51 - IP address lease time -> 86400s (1day)
        // option: 1 - Subnet Mask -> 255.255.255.0
        // option: 3 - router -> 172.16.1.1
        // option: 6 - Domain Name Server -> 172.16.1.1

    }

    public void request() {
        // from dhcp client

        // source 0.0.0.0
        // destination 255.255.255.255
        // message type = 1
        // hardware type = 1 (ethernet)
        // hardware address length = 6
        // hops = 0

        // transaction id : same

        // seconds elapsed : 0
        // bootp flags: 0

        // client ip address : 0.0.0.0
        // your (client) ip address : 0.0.0.0
        // next server ip address : 0.0.0.0
        // relay agent ip address : 0.0.0.0
        // client mac address : c4:8e:8f:fb:f3:01
        // client hardware address padding : 0
        // server host name not given
        // boot file name not given
        // magic cookie: DHCP

        // option: 53 - DHCP message type -> request
        // option: 54 - DHCP Server Identifier -> 172.16.1.1
        // option: 50 - DHCP Server Identifier -> 172.16.1.100
        // option: 12 - hostname -> skhauv-XPS-8900
        // option: 55 - parameter request list
        // 1 - subnet mask
        // 28 - broadcast address
        // 2 - time offset
        // 3 - router
        // 15 - domain name
        // 6 - domain name server
        // 119 - domain search
        // 12 - host name
        // 44 - netbios over tcp/ip name server
        // 47 - netbios over tcp/ip scope
        // 26 - interface mtu
        // 121 - classless static route
        // 42 - network time protocol server

    }

    public void dhcpAck() {
        // from dhcp server

        // source 172.16.1.1
        // destination 172.16.1.100
        // message type = 2
        // hardware type = 1 (ethernet)
        // hardware address length = 6
        // hops = 0

        // transaction id : same

        // seconds elapsed : 0
        // bootp flags: 0

        // client ip address : 0.0.0.0
        // your (client) ip address : 172.16.1.100
        // next server ip address : 0.0.0.0
        // relay agent ip address : 0.0.0.0
        // client mac address : c4:8e:8f:fb:f3:01
        // client hardware address padding : 0
        // server host name not given
        // boot file name not given
        // magic cookie: DHCP

        // option: 53 - DHCP message type -> ack
        // option: 54 - DHCP Server Identifier -> 172.16.1.1
        // option: 51 - IP address lease time -> 86400s (1day)
        // option: 1 - Subnet Mask -> 255.255.255.0
        // option: 3 - router -> 172.16.1.1
        // option: 6 - Domain Name Server -> 172.16.1.1
    }

}
