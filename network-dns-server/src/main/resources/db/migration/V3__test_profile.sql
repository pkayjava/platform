delete from tbl_profile;
delete from tbl_profile_option;

INSERT INTO tbl_profile(profile_id, name, enabled) VALUES(UUID(), 'test', true);

INSERT INTO tbl_profile_option(profile_option_id, profile_id, option_id, option_value) VALUES(UUID(), (select profile_id from tbl_profile where name = 'test'), (select option_id from tbl_option where code = 51), '86400');
INSERT INTO tbl_profile_option(profile_option_id, profile_id, option_id, option_value) VALUES(UUID(), (select profile_id from tbl_profile where name = 'test'), (select option_id from tbl_option where code = 3), '192.168.1.1');
INSERT INTO tbl_profile_option(profile_option_id, profile_id, option_id, option_value) VALUES(UUID(), (select profile_id from tbl_profile where name = 'test'), (select option_id from tbl_option where code = 6), '192.168.1.1');
INSERT INTO tbl_profile_option(profile_option_id, profile_id, option_id, option_value) VALUES(UUID(), (select profile_id from tbl_profile where name = 'test'), (select option_id from tbl_option where code = 15), 'home.com.kh');

update tbl_address_pool set profile_id = (select profile_id from tbl_profile where name = 'test');

update tbl_server_nic set enabled = false where name = 'enp0s31f6';
