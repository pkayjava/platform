CREATE TABLE tbl_server_nic (
	server_nic_id    VARCHAR(100) NOT NULL,
	name             VARCHAR(100) NOT NULL,
	hardware_address VARCHAR(20) NOT NULL,
	host_address     VARCHAR(20) NOT NULL,
	netmask_address  VARCHAR(20) NOT NULL,
    primary_hardware BIT(1) NOT NULL,
    primary_host     BIT(1) NOT NULL,
	enabled          BIT(1) NOT NULL,
	PRIMARY KEY (server_nic_id)
);

CREATE UNIQUE INDEX tbl_server_nic_001 ON tbl_server_nic(hardware_address);
CREATE INDEX tbl_server_nic_002 ON tbl_server_nic(host_address);
CREATE INDEX tbl_server_nic_003 ON tbl_server_nic(netmask_address);
CREATE INDEX tbl_server_nic_004 ON tbl_server_nic(enabled);
CREATE INDEX tbl_server_nic_005 ON tbl_server_nic(name);
CREATE INDEX tbl_server_nic_006 ON tbl_server_nic(primary_hardware);
CREATE INDEX tbl_server_nic_007 ON tbl_server_nic(primary_host);

CREATE TABLE tbl_option (
	option_id   VARCHAR(100) NOT NULL,
	code INT(4) NOT NULL,
	type VARCHAR(10) NOT NULL,
	description VARCHAR(255) NOT NULL,
	PRIMARY KEY (option_id)
);

CREATE UNIQUE INDEX tbl_option_001 ON tbl_option(code);
CREATE INDEX tbl_option_002 ON tbl_option(type);

CREATE TABLE tbl_profile (
	profile_id VARCHAR(100) NOT NULL,
	name       VARCHAR(100) NOT NULL,
	enabled    BIT(1) NOT NULL,
	PRIMARY KEY (profile_id)
);

CREATE UNIQUE INDEX tbl_profile_001 ON tbl_profile(name);
CREATE INDEX tbl_profile_002 ON tbl_profile(enabled);

CREATE TABLE tbl_profile_option (
	profile_option_id VARCHAR(100) NOT NULL,
	profile_id        VARCHAR(100) NOT NULL,
	option_id         VARCHAR(100) NOT NULL,
	option_value      VARCHAR(255) NULL,
	PRIMARY KEY (profile_option_id)
);

CREATE UNIQUE INDEX tbl_profile_option_001 ON tbl_profile_option(profile_id, option_id);
CREATE INDEX tbl_profile_option_002 ON tbl_profile_option(option_value);

CREATE TABLE tbl_address_pool (
	address_pool_id         VARCHAR(100) NOT NULL,
	transaction_id          VARCHAR(8) NOT NULL,
	server_nic_id           VARCHAR(100) NOT NULL,
	profile_id              VARCHAR(100) NULL,
	client_hardware_address VARCHAR(20) NOT NULL,
	client_host_address     VARCHAR(20) NOT NULL,
	occupied BIT(1)         NOT NULL,
	PRIMARY KEY (address_pool_id)
);

CREATE INDEX tbl_address_pool_001 ON tbl_address_pool(transaction_id);
CREATE INDEX tbl_address_pool_002 ON tbl_address_pool(server_nic_id);
CREATE INDEX tbl_address_pool_003 ON tbl_address_pool(profile_id);
CREATE INDEX tbl_address_pool_004 ON tbl_address_pool(client_hardware_address);
CREATE INDEX tbl_address_pool_005 ON tbl_address_pool(client_host_address);
CREATE INDEX tbl_address_pool_006 ON tbl_address_pool(occupied);
