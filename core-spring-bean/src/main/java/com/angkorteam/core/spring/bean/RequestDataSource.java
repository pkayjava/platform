package com.angkorteam.core.spring.bean;

import javax.sql.DataSource;

public class RequestDataSource {

    private final DataSource dataSource;

    public RequestDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public DataSource getDataSource() {
        return this.dataSource;
    }

}
