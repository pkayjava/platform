package com.angkorteam.core.spring.bean.factory;

import com.angkorteam.core.spring.bean.RequestDataSource;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.datasource.SmartDataSource;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

public class RequestDataSourceFactoryBean implements FactoryBean<RequestDataSource>, InitializingBean, DisposableBean {

    private DataSource dataSource;

    private RequestDataSource bean;

    @Override
    public RequestDataSource getObject() throws Exception {
        return this.bean;
    }

    @Override
    public Class<?> getObjectType() {
        return RequestDataSource.class;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (this.dataSource == null) {
            throw new Exception("dataSource is required");
        }
        HttpServletRequestDataSource dataSource = new HttpServletRequestDataSource(this.dataSource);
        this.bean = new RequestDataSource(dataSource);
    }

    @Override
    public void destroy() throws Exception {
        try (Connection connection = this.bean.getDataSource().getConnection()) {
            if (connection != null) {
                try {
                    connection.commit();
                } catch (SQLException e) {
                    connection.rollback();
                }
            }
        }
    }

    protected static class HttpServletRequestDataSource implements SmartDataSource {

        private final DataSource dataSource;

        private final Connection connection;

        public HttpServletRequestDataSource(DataSource dataSource) {
            this.dataSource = dataSource;
            try {
                this.connection = dataSource.getConnection();
                this.connection.setAutoCommit(false);
                this.connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public Connection getConnection() throws SQLException {
            return this.connection;
        }

        @Override
        public Connection getConnection(String username, String password) throws SQLException {
            return this.connection;
        }

        @Override
        public <T> T unwrap(Class<T> iface) throws SQLException {
            return this.dataSource.unwrap(iface);
        }

        @Override
        public boolean isWrapperFor(Class<?> iface) throws SQLException {
            return this.dataSource.isWrapperFor(iface);
        }

        @Override
        public PrintWriter getLogWriter() throws SQLException {
            return this.dataSource.getLogWriter();
        }

        @Override
        public void setLogWriter(PrintWriter out) throws SQLException {
            this.dataSource.setLogWriter(out);
        }

        @Override
        public void setLoginTimeout(int seconds) throws SQLException {
            this.dataSource.setLoginTimeout(seconds);
        }

        @Override
        public int getLoginTimeout() throws SQLException {
            return this.dataSource.getLoginTimeout();
        }

        @Override
        public Logger getParentLogger() throws SQLFeatureNotSupportedException {
            return this.dataSource.getParentLogger();
        }

        @Override
        public boolean shouldClose(Connection con) {
            return false;
        }

    }

}
