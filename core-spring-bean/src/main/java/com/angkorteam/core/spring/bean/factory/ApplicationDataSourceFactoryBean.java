package com.angkorteam.core.spring.bean.factory;

import com.angkorteam.core.spring.bean.ApplicationDataSource;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

import javax.sql.DataSource;

public class ApplicationDataSourceFactoryBean implements FactoryBean<ApplicationDataSource>, InitializingBean {

    private DataSource dataSource;

    private ApplicationDataSource bean;

    @Override
    public void afterPropertiesSet() throws Exception {
        this.bean = new ApplicationDataSource(this.dataSource);
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public ApplicationDataSource getObject() throws Exception {
        return this.bean;
    }

    @Override
    public Class<?> getObjectType() {
        return ApplicationDataSource.class;
    }

}
