package com.angkorteam.core.spring.bean;

import javax.sql.DataSource;

public class ApplicationDataSource {

    private final DataSource dataSource;

    public ApplicationDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public DataSource getDataSource() {
        return this.dataSource;
    }

}
