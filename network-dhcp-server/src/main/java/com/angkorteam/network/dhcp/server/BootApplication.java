package com.angkorteam.network.dhcp.server;

import com.angkorteam.network.dhcp.server.boot.AppProperties;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioDatagramChannel;
import org.pcap4j.core.*;
import org.pcap4j.core.PcapNetworkInterface.PromiscuousMode;
import org.pcap4j.util.LinkLayerAddress;
import org.pcap4j.util.MacAddress;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SpringBootApplication
public class BootApplication {

    public static void main(String[] args) throws Exception {

        List<Thread> threads = new ArrayList<>();

        List<PcapNetworkInterface> pacs = Pcaps.findAllDevs().stream()
                .filter(e -> !e.isLoopBack() && e.isRunning() && e.isUp() && e.isLocal()
                        && e.getLinkLayerAddresses() != null && !e.getLinkLayerAddresses().isEmpty())
                .collect(Collectors.toList());
        List<NetworkInterface> nics = new ArrayList<>();

        List<String> hardwares = new ArrayList<>();
        for (PcapNetworkInterface nic : pacs) {
            hardwares.add(nic.getName());
            List<String> hardwareAddresses = new ArrayList<>();
            for (LinkLayerAddress address : nic.getLinkLayerAddresses()) {
                hardwareAddresses.add(MacAddress.getByAddress(address.getAddress()).toString());
            }
            for (String hardwareAddress : hardwareAddresses) {
                String hostAddress = null;
                String netmaskAddress = null;
                String broadcastAddress = null;
                for (PcapAddress address : nic.getAddresses()) {
                    if (address instanceof PcapIpV4Address) {
                        hostAddress = address.getAddress().getHostAddress();
                        netmaskAddress = address.getNetmask().getHostAddress();
                        broadcastAddress = address.getBroadcastAddress().getHostAddress();
                        nics.add(new NetworkInterface(nic.getName(), hostAddress, netmaskAddress, broadcastAddress,
                                hardwareAddress));
                    }
                }
            }
        }

        ApplicationContext context = SpringApplication.run(BootApplication.class, args);

        JdbcTemplate jdbcTemplate = context.getBean(JdbcTemplate.class);

        List<String> ids = jdbcTemplate.queryForList("SELECT server_nic_id FROM tbl_server_nic WHERE enabled = true",
                String.class);
        for (NetworkInterface nic : nics) {
            Map<String, Object> rec = null;
            try {
                rec = jdbcTemplate.queryForMap(
                        "SELECT server_nic_id, netmask_address, enabled FROM tbl_server_nic WHERE name = ? AND hardware_address = ? AND host_address = ?",
                        nic.getName(), nic.getHardwareAddress(), nic.getHostAddress());
            } catch (EmptyResultDataAccessException e) {
            }
            if (rec != null) {
                String server_nic_id = (String) rec.get("server_nic_id");
                String netmask_address = (String) rec.get("netmask_address");
                ids.remove(server_nic_id);
                if (!nic.getNetmaskAddress().equalsIgnoreCase(netmask_address)) {
                    jdbcTemplate.update("UPDATE tbl_server_nic SET netmask_address = ? WHERE server_nic_id = ?",
                            nic.getNetmaskAddress(), server_nic_id);
                    jdbcTemplate.update("DELETE FROM tbl_address_pool WHERE server_nic_id = ?", server_nic_id);
                }
                if (Boolean.TRUE.equals(rec.get("enabled"))) {
                    ids.remove(server_nic_id);
                }
            } else {
                String server_nic_id = jdbcTemplate.queryForObject("select UUID()", String.class);
                String name = nic.getName();
                String hardware_address = nic.getHardwareAddress();
                String host_address = nic.getHostAddress();
                String netmask_address = nic.getNetmaskAddress();
                boolean primary_hardware = false;
                boolean primary_host = false;
                boolean enabled = true;
                jdbcTemplate.update(
                        "INSERT INTO tbl_server_nic(server_nic_id, name, hardware_address, host_address, netmask_address, primary_hardware, primary_host, enabled) VALUES(?, ?, ?, ?, ?, ?, ?, ?)",
                        server_nic_id, name, hardware_address, host_address, netmask_address, primary_hardware,
                        primary_host, enabled);
                ids.remove(server_nic_id);
            }
        }
        for (String id : ids) {
            jdbcTemplate.update("UPDATE tbl_server_nic SET enable = false WHERE server_nic_id = ?", id);
        }
        List<String> names = jdbcTemplate.queryForList("SELECT MAX(name) FROM tbl_server_nic GROUP BY name",
                String.class);
        for (String name : names) {
            boolean has = jdbcTemplate.queryForObject(
                    "SELECT COUNT(*) FROM tbl_server_nic WHERE name = ? AND primary_hardware = true AND enabled = true",
                    Boolean.class, name);
            if (!has) {
                jdbcTemplate.update(
                        "UPDATE tbl_server_nic SET primary_hardware = true WHERE name = ? AND enabled = true LIMIT 1",
                        name);
            }
            List<String> hardware_addresses = jdbcTemplate.queryForList(
                    "SELECT MAX(hardware_address) FROM tbl_server_nic WHERE name = ? AND enabled = true GROUP BY hardware_address",
                    String.class, name);
            for (String hardware_address : hardware_addresses) {
                has = jdbcTemplate.queryForObject(
                        "SELECT COUNT(*) FROM tbl_server_nic WHERE name = ? AND hardware_address = ? AND enabled = true AND primary_host = true",
                        Boolean.class, name, hardware_address);
                if (!has) {
                    jdbcTemplate.update(
                            "UPDATE tbl_server_nic SET primary_host = true WHERE name = ? AND hardware_address = ? AND enabled = true LIMIT 1",
                            name, hardware_address);
                }
            }
        }

        for (NetworkInterface nic : nics) {
            System.out.println(nic.getName() + " - " + nic.getHardwareAddress());
            System.out.println("  HostAddress : " + nic.getHostAddress());
            System.out.println("  NetmaskAddress : " + nic.getNetmaskAddress());
            System.out.println("  BroadcastAddress : " + nic.getBroadcastAddress());
        }

        Map<String, PcapHandle> handles = new HashMap<>();
        for (String hardware : hardwares) {
            PcapNetworkInterface pcap = Pcaps.getDevByName(hardware);
            PcapHandle handle = pcap.openLive(65536, PromiscuousMode.PROMISCUOUS, 0);
            handles.put(hardware, handle);
        }

        AppProperties properties = context.getBean(AppProperties.class);

        for (String hardware : hardwares) {
            Thread thread = new Thread(new CaptureThread(jdbcTemplate, hardware, handles.get(hardware)));
            threads.add(thread);
            thread.start();
        }

        EventLoopGroup group = new NioEventLoopGroup();
        try {

            Bootstrap offerBootstrap = new Bootstrap();
            offerBootstrap.group(group).channel(NioDatagramChannel.class).handler(new EmptyHandler())
                    .option(ChannelOption.SO_BROADCAST, true);

            Bootstrap discoverBootstrap = new Bootstrap();
            discoverBootstrap.group(group).channel(NioDatagramChannel.class).option(ChannelOption.SO_BROADCAST, true)
                    .handler(new DhcpPacketHandler(properties, jdbcTemplate, handles));
            discoverBootstrap.bind("255.255.255.255", properties.getPort()).sync().channel().closeFuture().await();
            for (Thread thread : threads) {
                thread.interrupt();
            }
        } finally {
            group.shutdownGracefully();
        }
    }

    public void makeOffer() {

        // from dhcp server

        // source 172.16.1.1
        // destination 172.16.1.100
        // message type = 2
        // hardware type = 1 (ethernet)
        // hardware address length = 6
        // hops = 0

        // transaction id : same

        // seconds elapsed : 0
        // bootp flags: 0

        // client ip address : 0.0.0.0
        // your (client) ip address : 172.16.1.100
        // next server ip address : 0.0.0.0
        // relay agent ip address : 0.0.0.0
        // client mac address : c4:8e:8f:fb:f3:01
        // client hardware address padding : 0
        // server host name not given
        // boot file name not given
        // magic cookie: DHCP

        // option: 53 - DHCP message type -> offer
        // option: 54 - DHCP Server Identifier -> 172.16.1.1
        // option: 51 - IP address lease time -> 86400s (1day)
        // option: 1 - Subnet Mask -> 255.255.255.0
        // option: 3 - router -> 172.16.1.1
        // option: 6 - Domain Name Server -> 172.16.1.1

    }

    public void request() {
        // from dhcp client

        // source 0.0.0.0
        // destination 255.255.255.255
        // message type = 1
        // hardware type = 1 (ethernet)
        // hardware address length = 6
        // hops = 0

        // transaction id : same

        // seconds elapsed : 0
        // bootp flags: 0

        // client ip address : 0.0.0.0
        // your (client) ip address : 0.0.0.0
        // next server ip address : 0.0.0.0
        // relay agent ip address : 0.0.0.0
        // client mac address : c4:8e:8f:fb:f3:01
        // client hardware address padding : 0
        // server host name not given
        // boot file name not given
        // magic cookie: DHCP

        // option: 53 - DHCP message type -> request
        // option: 54 - DHCP Server Identifier -> 172.16.1.1
        // option: 50 - DHCP Server Identifier -> 172.16.1.100
        // option: 12 - hostname -> skhauv-XPS-8900
        // option: 55 - parameter request list
        // 1 - subnet mask
        // 28 - broadcast address
        // 2 - time offset
        // 3 - router
        // 15 - domain name
        // 6 - domain name server
        // 119 - domain search
        // 12 - host name
        // 44 - netbios over tcp/ip name server
        // 47 - netbios over tcp/ip scope
        // 26 - interface mtu
        // 121 - classless static route
        // 42 - network time protocol server

    }

    public void dhcpAck() {
        // from dhcp server

        // source 172.16.1.1
        // destination 172.16.1.100
        // message type = 2
        // hardware type = 1 (ethernet)
        // hardware address length = 6
        // hops = 0

        // transaction id : same

        // seconds elapsed : 0
        // bootp flags: 0

        // client ip address : 0.0.0.0
        // your (client) ip address : 172.16.1.100
        // next server ip address : 0.0.0.0
        // relay agent ip address : 0.0.0.0
        // client mac address : c4:8e:8f:fb:f3:01
        // client hardware address padding : 0
        // server host name not given
        // boot file name not given
        // magic cookie: DHCP

        // option: 53 - DHCP message type -> ack
        // option: 54 - DHCP Server Identifier -> 172.16.1.1
        // option: 51 - IP address lease time -> 86400s (1day)
        // option: 1 - Subnet Mask -> 255.255.255.0
        // option: 3 - router -> 172.16.1.1
        // option: 6 - Domain Name Server -> 172.16.1.1
    }

}
