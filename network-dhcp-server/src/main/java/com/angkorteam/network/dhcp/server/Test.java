package com.angkorteam.network.dhcp.server;

import com.angkorteam.network.dhcp.server.packet.Option;

public class Test {

    public static void main(String[] args) throws Exception {

        for (Option option : Option.values()) {
            System.out.println("INSERT INTO tbl_option(option_id, code, type, description) VALUES(UUID(), "
                    + Integer.valueOf(option.name().substring(2)) + ", 'TEXT', '" + option.getDescription() + "');");
        }

    }

}
