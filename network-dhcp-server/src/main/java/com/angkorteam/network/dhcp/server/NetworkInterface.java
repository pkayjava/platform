package com.angkorteam.network.dhcp.server;

import java.io.Serializable;

public class NetworkInterface implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 5337076286850720814L;

    private final String name;

    private final String hardwareAddress;

    private final String hostAddress;

    private final String netmaskAddress;

    private final String broadcastAddress;

    public NetworkInterface(String name, String hostAddress, String netmaskAddress, String broadcastAddress,
                            String hardwareAddress) {
        this.name = name;
        this.hardwareAddress = hardwareAddress;
        this.hostAddress = hostAddress;
        this.netmaskAddress = netmaskAddress;
        this.broadcastAddress = broadcastAddress;
    }

    public String getName() {
        return name;
    }

    public String getHardwareAddress() {
        return hardwareAddress;
    }

    public String getHostAddress() {
        return hostAddress;
    }

    public String getNetmaskAddress() {
        return netmaskAddress;
    }

    public String getBroadcastAddress() {
        return broadcastAddress;
    }

}
