package com.angkorteam.network.dhcp.server.packet;

public class OptionData {

    private final Option key;

    private final byte[] data;

    public OptionData(Option key, byte[] data) {
        super();
        this.key = key;
        this.data = data;
    }

    public Option getKey() {
        return key;
    }

    public byte[] getData() {
        return data;
    }

}
