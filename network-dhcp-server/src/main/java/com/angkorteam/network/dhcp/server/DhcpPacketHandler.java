package com.angkorteam.network.dhcp.server;

import com.angkorteam.core.function.Bytes;
import com.angkorteam.network.dhcp.server.boot.AppProperties;
import com.angkorteam.network.dhcp.server.packet.DhcpPacket;
import com.angkorteam.network.dhcp.server.packet.Option;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.util.SubnetUtils;
import org.pcap4j.core.PcapHandle;
import org.pcap4j.packet.*;
import org.pcap4j.packet.namednumber.EtherType;
import org.pcap4j.packet.namednumber.IpNumber;
import org.pcap4j.packet.namednumber.IpVersion;
import org.pcap4j.packet.namednumber.UdpPort;
import org.pcap4j.util.MacAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

public class DhcpPacketHandler extends SimpleChannelInboundHandler<DatagramPacket> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DhcpPacketHandler.class);

    private final Map<String, PcapHandle> handles;

    private final AppProperties properties;

    private final JdbcTemplate jdbcTemplate;

    public DhcpPacketHandler(AppProperties properties, JdbcTemplate jdbcTemplate, Map<String, PcapHandle> handles) {
        this.properties = properties;
        this.handles = handles;
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void channelRead0(ChannelHandlerContext ctx, DatagramPacket datagram) throws Exception {
        InetSocketAddress sender = datagram.sender();
        InetSocketAddress recipient = datagram.recipient();
        ByteBuf byteBuffer = datagram.content();

        byte[] bytes = new byte[byteBuffer.readableBytes()];
        byteBuffer.duplicate().readBytes(bytes);

        DhcpPacket packet = DhcpPacket.deserialize(bytes);

        String xid = Integer.toHexString(Bytes.toInteger(packet.xid));
        String clientHardwareAddress = MacAddress.getByAddress(Bytes.extract(packet.chaddr, 0, packet.hlen)).toString();

        if (packet.op == DhcpPacket.OP_REQUEST) {
            byte[] values = packet.options.get(Option.P_053.getLiteral());
            if (values != null && values.length > 0) {
                if (values[0] == DhcpPacket.DHCP_DISCOVER) {
                    String server_nic_id = lookupServerId(xid);

                    if (server_nic_id != null) {

                        Map<String, Object> serverNic = this.jdbcTemplate.queryForMap(
                                "SELECT name, netmask_address, host_address, hardware_address FROM tbl_server_nic WHERE server_nic_id = ?",
                                server_nic_id);

                        String serverHardwareAddress = (String) serverNic.get("hardware_address");

                        String clientHostAddress = null;
                        String profileId = null;
                        String address_pool_id = null;
                        Map<String, Object> clientObject = null;
                        try {
                            clientObject = jdbcTemplate.queryForMap(
                                    "SELECT address_pool_id, client_host_address, profile_id FROM tbl_address_pool WHERE server_nic_id = ? AND client_hardware_address = ?",
                                    server_nic_id, clientHardwareAddress);
                        } catch (EmptyResultDataAccessException e) {
                        }

                        if (clientObject != null) {
                            clientHostAddress = (String) clientObject.get("client_host_address");
                            profileId = (String) clientObject.get("profile_id");
                            address_pool_id = (String) clientObject.get("address_pool_id");
                        }

                        SubnetUtils network = new SubnetUtils((String) serverNic.get("host_address"),
                                (String) serverNic.get("netmask_address"));
                        if (clientHostAddress != null) {
                            if (!network.getInfo().isInRange(clientHostAddress)
                                    || clientHostAddress.equals(serverNic.get("host_address"))) {
                                jdbcTemplate.update("DELETE FROM tbl_address_pool WHERE address_pool_id = ?",
                                        address_pool_id);
                                clientHostAddress = null;
                                address_pool_id = null;
                            }
                        }
                        if (clientHostAddress == null) {
                            List<String> pools = jdbcTemplate.queryForList(
                                    "SELECT client_host_address FROM tbl_address_pool WHERE server_nic_id = ? AND occupied = true",
                                    String.class, server_nic_id);
                            String[] addresses = network.getInfo().getAllAddresses();
                            for (String address : addresses) {
                                if (address.equals(serverNic.get("host_address")) || pools.contains(address)) {
                                    continue;
                                } else {
                                    clientHostAddress = address;
                                    break;
                                }
                            }
                        }

                        if (clientHostAddress == null) {
                            LOGGER.info("[{}] [Try Again ] SMAC [{}] CMAC [{}]", xid, serverHardwareAddress,
                                    clientHardwareAddress);
                            return;
                        }

                        String clientNetmaskAddress = (String) serverNic.get("netmask_address");
                        LOGGER.info("[{}] [Offered   ] SMAC [{}] CMAC [{}] [{}/{}]", xid,
                                (serverHardwareAddress == null ? StringUtils.repeat(" ", 17) : serverHardwareAddress),
                                clientHardwareAddress, clientHostAddress, clientNetmaskAddress);

                        EthernetPacket offerPacket = offerPacket(serverNic, clientHostAddress, clientNetmaskAddress,
                                recipient.getPort(), sender.getPort(), profileId, packet);
                        if (address_pool_id == null) {
                            this.jdbcTemplate.update(
                                    "INSERT INTO tbl_address_pool(address_pool_id, transaction_id, server_nic_id, client_hardware_address, client_host_address, profile_id, occupied) VALUES(UUID(), ?, ?, ?, ?, ?, true)",
                                    xid, server_nic_id, clientHardwareAddress, clientHostAddress, profileId);
                        } else {
                            this.jdbcTemplate.update(
                                    "UPDATE tbl_address_pool SET transaction_id = ?, client_host_address = ? WHERE address_pool_id = ?",
                                    xid, clientHostAddress, address_pool_id);
                        }
                        PcapHandle handle = this.handles.get((String) serverNic.get("name"));
                        handle.sendPacket(offerPacket);
                    }
                } else if (values[0] == DhcpPacket.DHCP_REQUEST) {
                    String server_nic_id = lookupServerId(xid);
                    if (server_nic_id != null) {
                        Map<String, Object> serverNic = this.jdbcTemplate.queryForMap(
                                "SELECT name, netmask_address, host_address, hardware_address FROM tbl_server_nic WHERE server_nic_id = ?",
                                server_nic_id);

                        String serverHardwareAddress = (String) serverNic.get("hardware_address");

                        String clientHostAddress = null;
                        try {
                            clientHostAddress = Inet4Address.getByAddress(packet.options.get(Option.P_050.getLiteral()))
                                    .getHostAddress();
                        } catch (UnknownHostException e) {
                        }

                        String profile_id = null;
                        Map<String, Object> addressPool = null;
                        try {
                            addressPool = this.jdbcTemplate.queryForMap(
                                    "SELECT address_pool_id, client_host_address, profile_id FROM tbl_address_pool WHERE server_nic_id = ? AND client_hardware_address = ?",
                                    server_nic_id, clientHardwareAddress);
                        } catch (EmptyResultDataAccessException e) {
                        }

                        if (addressPool != null && clientHostAddress.equals(addressPool.get("client_host_address"))) {
                            profile_id = (String) addressPool.get("profile_id");
                            String clientNetmaskAddress = (String) serverNic.get("netmask_address");
                            LOGGER.info("[{}] [Offered   ] SMAC [{}] CMAC [{}] [{}/{}]", xid, serverHardwareAddress,
                                    clientHardwareAddress, clientHostAddress, clientNetmaskAddress);
                            EthernetPacket ackPacket = ackPacket(serverNic, clientHostAddress, clientNetmaskAddress,
                                    recipient.getPort(), sender.getPort(), profile_id, packet);
                            PcapHandle handle = this.handles.get((String) serverNic.get("name"));
                            handle.sendPacket(ackPacket);
                        } else {
                            EthernetPacket nakPacket = nakPacket(serverNic, recipient.getPort(), sender.getPort(),
                                    packet);
                            String clientNetmaskAddress = (String) serverNic.get("netmask_address");
                            LOGGER.info("[{}] [Denied    ] SMAC [{}] CMAC [{}] [{}/{}]", xid, serverHardwareAddress,
                                    clientHardwareAddress, clientHostAddress, clientNetmaskAddress);
                            PcapHandle handle = this.handles.get((String) serverNic.get("name"));
                            handle.sendPacket(nakPacket);
                        }
                    }
                }
            }
        }
    }

    public String lookupServerId(String xid) {
        long timeout = System.currentTimeMillis() + 15000;
        while (timeout > System.currentTimeMillis()) {
            if (CaptureThread.DB.containsKey(xid)) {
                return CaptureThread.DB.get(xid);
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                return null;
            }
        }
        return null;
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
    }

    public EthernetPacket nakPacket(Map<String, Object> serverNic, int serverPort, int clientPort,
                                    DhcpPacket requestPacket) {

        Inet4Address clientHostAddress = null;
        try {
            clientHostAddress = (Inet4Address) Inet4Address.getByName("255.255.255.255");
        } catch (UnknownHostException e) {
        }
        Inet4Address serverHostAddress = null;
        try {
            serverHostAddress = (Inet4Address) Inet4Address.getByName((String) serverNic.get("host_address"));
        } catch (UnknownHostException e) {
        }

        DhcpPacket packet = new DhcpPacket();

        packet.op = DhcpPacket.OP_REPLY;
        packet.htype = requestPacket.htype;
        packet.hlen = requestPacket.hlen;
        packet.hops = 0;

        Bytes.transfer(requestPacket.secs, packet.secs);
        Bytes.transfer(requestPacket.flags, packet.flags);

        Bytes.transfer(requestPacket.xid, packet.xid);

        Bytes.transfer(requestPacket.chaddr, packet.chaddr);

        packet.options.put(Option.P_053.getLiteral(), new byte[]{DhcpPacket.DHCP_NAK});

        packet.options.put(Option.P_054.getLiteral(), serverHostAddress.getAddress());

        packet.options.put(Option.P_255.getLiteral(), null);
        packet.options.put(Option.P_000.getLiteral(), null);

        UnknownPacket.Builder dhcpBuilder = new UnknownPacket.Builder();
        dhcpBuilder.rawData(packet.serialize());

        UdpPacket.Builder udpPacketBuilder = new UdpPacket.Builder();
        udpPacketBuilder.payloadBuilder(dhcpBuilder);
        udpPacketBuilder.srcPort(UdpPort.getInstance((short) serverPort));
        udpPacketBuilder.dstPort(UdpPort.getInstance((short) clientPort));
        udpPacketBuilder.srcAddr(serverHostAddress);
        udpPacketBuilder.dstAddr(clientHostAddress);
        udpPacketBuilder.correctChecksumAtBuild(true);
        udpPacketBuilder.correctLengthAtBuild(true);

        IpV4Packet.Builder ipV4PacketBuilder = new IpV4Packet.Builder();
        ipV4PacketBuilder.payloadBuilder(udpPacketBuilder);
        ipV4PacketBuilder.srcAddr(serverHostAddress);
        ipV4PacketBuilder.dstAddr(clientHostAddress);
        ipV4PacketBuilder.protocol(IpNumber.UDP);
        ipV4PacketBuilder.correctChecksumAtBuild(true);
        ipV4PacketBuilder.correctLengthAtBuild(true);
        ipV4PacketBuilder.version(IpVersion.IPV4);
        ipV4PacketBuilder.tos(IpV4Rfc1349Tos.newInstance((byte) 0x75));
        ipV4PacketBuilder.ihl((byte) 9);
        ipV4PacketBuilder.ttl((byte) 100);
        ipV4PacketBuilder.paddingAtBuild(true);

        EthernetPacket.Builder ethernetPacketBuilder = new EthernetPacket.Builder();
        ethernetPacketBuilder.payloadBuilder(ipV4PacketBuilder);
        ethernetPacketBuilder.type(EtherType.IPV4);
        ethernetPacketBuilder.paddingAtBuild(true);
        ethernetPacketBuilder.srcAddr(MacAddress.getByName((String) serverNic.get("hardware_address"), ":"));
        ethernetPacketBuilder.dstAddr(MacAddress.getByName("ff:ff:ff:ff:ff:ff", ":"));

        return ethernetPacketBuilder.build();
    }

    public EthernetPacket ackPacket(Map<String, Object> serverNic, String hostAddress, String netmaskAddress,
                                    int serverPort, int clientPort, String profileId, DhcpPacket requestPacket) {

        Inet4Address clientHostAddress = null;
        try {
            clientHostAddress = (Inet4Address) Inet4Address.getByName(hostAddress);
        } catch (UnknownHostException e) {
        }
        Inet4Address serverHostAddress = null;
        try {
            serverHostAddress = (Inet4Address) Inet4Address.getByName((String) serverNic.get("host_address"));
        } catch (UnknownHostException e) {
        }
        byte[] clientNetmaskAddress = null;
        try {
            clientNetmaskAddress = InetAddress.getByName(netmaskAddress).getAddress();
        } catch (UnknownHostException e) {
        }

        DhcpPacket packet = new DhcpPacket();

        packet.op = DhcpPacket.OP_REPLY;
        packet.htype = requestPacket.htype;
        packet.hlen = requestPacket.hlen;
        packet.hops = 0;

        Bytes.transfer(requestPacket.secs, packet.secs);
        Bytes.transfer(requestPacket.flags, packet.flags);

        Bytes.transfer(requestPacket.xid, packet.xid);

        Bytes.transfer(requestPacket.chaddr, packet.chaddr);

        Bytes.transfer(clientHostAddress.getAddress(), packet.yiaddr);

        packet.options.put(Option.P_053.getLiteral(), new byte[]{DhcpPacket.DHCP_ACK});

        packet.options.put(Option.P_054.getLiteral(), serverHostAddress.getAddress());

        packet.options.put(Option.P_001.getLiteral(), clientNetmaskAddress);

        if (profileId != null) {
            List<Map<String, Object>> options = jdbcTemplate.queryForList(
                    "SELECT tbl_option.code as code, tbl_option.type as type, tbl_profile_option.option_value as value FROM tbl_profile INNER JOIN tbl_profile_option ON tbl_profile.profile_id = tbl_profile_option.profile_id INNER JOIN tbl_option ON tbl_option.option_id = tbl_profile_option.option_id WHERE tbl_profile.enabled = true AND tbl_profile.profile_id = ?",
                    profileId);
            for (Map<String, Object> option : options) {
                String type = (String) option.get("type");
                int code = (int) option.get("code");
                String value = (String) option.get("value");

                if ("HEX".equals(type)) {
                    // Hex
                    try {
                        packet.options.put((byte) code, Hex.decodeHex(value));
                    } catch (NumberFormatException | DecoderException e) {
                    }
                } else if ("TEXT".equals(type)) {
                    // TEXT
                    packet.options.put((byte) code, value == null || "".equals(value) ? null : value.getBytes());
                } else if ("IP".equals(type)) {
                    // IP - 255.255.255.255
                    try {
                        packet.options.put((byte) code,
                                value == null || "".equals(value) ? null : Inet4Address.getByName(value).getAddress());
                    } catch (NumberFormatException | UnknownHostException e) {
                    }
                } else if ("MAC".equals(type)) {
                    // Mac - ff:ff:ff:ff:ff:ff
                    packet.options.put((byte) code,
                            value == null || "".equals(value) ? null : MacAddress.getByName(value, ":").getAddress());
                } else if ("D1".equals(type)) {
                    // Decimal 1byte = byte
                    packet.options.put((byte) code, value == null || "".equals(value) ? null
                            : new byte[]{(byte) Integer.valueOf(value).intValue()});
                } else if ("D2".equals(type)) {
                    // Decimal 2byte = short
                    packet.options.put((byte) code, value == null || "".equals(value) ? null
                            : Bytes.toBytes((short) Integer.valueOf(value).intValue()));
                } else if ("D4".equals(type)) {
                    // Decimal 4byte - integer
                    packet.options.put((byte) code, value == null || "".equals(value) ? null
                            : Bytes.toBytes((int) Integer.valueOf(value).intValue()));
                }
            }
        }

        packet.options.put(Option.P_255.getLiteral(), null);
        packet.options.put(Option.P_000.getLiteral(), null);

        UnknownPacket.Builder dhcpBuilder = new UnknownPacket.Builder();
        dhcpBuilder.rawData(packet.serialize());

        UdpPacket.Builder udpPacketBuilder = new UdpPacket.Builder();
        udpPacketBuilder.payloadBuilder(dhcpBuilder);
        udpPacketBuilder.srcPort(UdpPort.getInstance((short) serverPort));
        udpPacketBuilder.dstPort(UdpPort.getInstance((short) clientPort));
        udpPacketBuilder.srcAddr(serverHostAddress);
        udpPacketBuilder.dstAddr(clientHostAddress);
        udpPacketBuilder.correctChecksumAtBuild(true);
        udpPacketBuilder.correctLengthAtBuild(true);

        IpV4Packet.Builder ipV4PacketBuilder = new IpV4Packet.Builder();
        ipV4PacketBuilder.payloadBuilder(udpPacketBuilder);
        ipV4PacketBuilder.srcAddr(serverHostAddress);
        ipV4PacketBuilder.dstAddr(clientHostAddress);
        ipV4PacketBuilder.protocol(IpNumber.UDP);
        ipV4PacketBuilder.correctChecksumAtBuild(true);
        ipV4PacketBuilder.correctLengthAtBuild(true);
        ipV4PacketBuilder.version(IpVersion.IPV4);
        ipV4PacketBuilder.tos(IpV4Rfc1349Tos.newInstance((byte) 0x75));
        ipV4PacketBuilder.ihl((byte) 9);
        ipV4PacketBuilder.ttl((byte) 100);
        ipV4PacketBuilder.paddingAtBuild(true);

        EthernetPacket.Builder ethernetPacketBuilder = new EthernetPacket.Builder();
        ethernetPacketBuilder.payloadBuilder(ipV4PacketBuilder);
        ethernetPacketBuilder.type(EtherType.IPV4);
        ethernetPacketBuilder.paddingAtBuild(true);
        ethernetPacketBuilder.srcAddr(MacAddress.getByName((String) serverNic.get("hardware_address"), ":"));
        ethernetPacketBuilder.dstAddr(MacAddress.getByAddress(Bytes.extract(requestPacket.chaddr, 0, 6)));

        return ethernetPacketBuilder.build();
    }

    public EthernetPacket offerPacket(Map<String, Object> serverNic, String hostAddress, String netmaskAddress,
                                      int serverPort, int clientPort, String profileId, DhcpPacket discoverPacket) {
        Inet4Address clientHostAddress = null;
        try {
            clientHostAddress = (Inet4Address) Inet4Address.getByName(hostAddress);
        } catch (UnknownHostException e) {
        }
        Inet4Address serverHostAddress = null;
        try {
            serverHostAddress = (Inet4Address) Inet4Address.getByName((String) serverNic.get("host_address"));
        } catch (UnknownHostException e) {
        }
        byte[] clientNetmaskAddress = null;
        try {
            clientNetmaskAddress = InetAddress.getByName(netmaskAddress).getAddress();
        } catch (UnknownHostException e) {
        }

        DhcpPacket packet = new DhcpPacket();

        packet.op = DhcpPacket.OP_REPLY;
        packet.htype = discoverPacket.htype;
        packet.hlen = discoverPacket.hlen;
        packet.hops = 0;

        Bytes.transfer(discoverPacket.secs, packet.secs);
        Bytes.transfer(discoverPacket.flags, packet.flags);

        Bytes.transfer(discoverPacket.xid, packet.xid);

        Bytes.transfer(discoverPacket.chaddr, packet.chaddr);

        Bytes.transfer(clientHostAddress.getAddress(), packet.yiaddr);

        packet.options.put(Option.P_053.getLiteral(), new byte[]{DhcpPacket.DHCP_OFFER});

        packet.options.put(Option.P_054.getLiteral(), serverHostAddress.getAddress());

        packet.options.put(Option.P_001.getLiteral(), clientNetmaskAddress);

        if (profileId != null) {
            List<Map<String, Object>> options = jdbcTemplate.queryForList(
                    "SELECT tbl_option.code as code, tbl_option.type as type, tbl_profile_option.option_value as value FROM tbl_profile INNER JOIN tbl_profile_option ON tbl_profile.profile_id = tbl_profile_option.profile_id INNER JOIN tbl_option ON tbl_option.option_id = tbl_profile_option.option_id WHERE tbl_profile.enabled = true AND tbl_profile.profile_id = ?",
                    profileId);
            for (Map<String, Object> option : options) {
                String type = (String) option.get("type");
                int code = (int) option.get("code");
                String value = (String) option.get("value");

                if ("HEX".equals(type)) {
                    // Hex
                    try {
                        packet.options.put((byte) code, Hex.decodeHex(value));
                    } catch (NumberFormatException | DecoderException e) {
                    }
                } else if ("TEXT".equals(type)) {
                    // TEXT
                    packet.options.put((byte) code, value == null || "".equals(value) ? null : value.getBytes());
                } else if ("IP".equals(type)) {
                    // IP - 255.255.255.255
                    try {
                        packet.options.put((byte) code,
                                value == null || "".equals(value) ? null : Inet4Address.getByName(value).getAddress());
                    } catch (NumberFormatException | UnknownHostException e) {
                    }
                } else if ("MAC".equals(type)) {
                    // Mac - ff:ff:ff:ff:ff:ff
                    packet.options.put((byte) code,
                            value == null || "".equals(value) ? null : MacAddress.getByName(value, ":").getAddress());
                } else if ("D1".equals(type)) {
                    // Decimal 1byte = byte
                    packet.options.put((byte) code, value == null || "".equals(value) ? null
                            : new byte[]{(byte) Integer.valueOf(value).intValue()});
                } else if ("D2".equals(type)) {
                    // Decimal 2byte = short
                    packet.options.put((byte) code, value == null || "".equals(value) ? null
                            : Bytes.toBytes((short) Integer.valueOf(value).intValue()));
                } else if ("D4".equals(type)) {
                    // Decimal 4byte - integer
                    packet.options.put((byte) code, value == null || "".equals(value) ? null
                            : Bytes.toBytes((int) Integer.valueOf(value).intValue()));
                }
            }
        }

        packet.options.put(Option.P_255.getLiteral(), null);
        packet.options.put(Option.P_000.getLiteral(), null);

        UnknownPacket.Builder dhcpBuilder = new UnknownPacket.Builder();
        dhcpBuilder.rawData(packet.serialize());

        UdpPacket.Builder udpPacketBuilder = new UdpPacket.Builder();
        udpPacketBuilder.payloadBuilder(dhcpBuilder);
        udpPacketBuilder.srcPort(UdpPort.getInstance((short) serverPort));
        udpPacketBuilder.dstPort(UdpPort.getInstance((short) clientPort));
        udpPacketBuilder.srcAddr(serverHostAddress);
        udpPacketBuilder.dstAddr(clientHostAddress);
        udpPacketBuilder.correctChecksumAtBuild(true);
        udpPacketBuilder.correctLengthAtBuild(true);

        IpV4Packet.Builder ipV4PacketBuilder = new IpV4Packet.Builder();
        ipV4PacketBuilder.payloadBuilder(udpPacketBuilder);
        ipV4PacketBuilder.srcAddr(serverHostAddress);
        ipV4PacketBuilder.dstAddr(clientHostAddress);
        ipV4PacketBuilder.protocol(IpNumber.UDP);
        ipV4PacketBuilder.correctChecksumAtBuild(true);
        ipV4PacketBuilder.correctLengthAtBuild(true);
        ipV4PacketBuilder.version(IpVersion.IPV4);
        ipV4PacketBuilder.tos(IpV4Rfc1349Tos.newInstance((byte) 0x75));
        ipV4PacketBuilder.ihl((byte) 9);
        ipV4PacketBuilder.ttl((byte) 100);
        ipV4PacketBuilder.paddingAtBuild(true);

        EthernetPacket.Builder ethernetPacketBuilder = new EthernetPacket.Builder();
        ethernetPacketBuilder.payloadBuilder(ipV4PacketBuilder);
        ethernetPacketBuilder.type(EtherType.IPV4);
        ethernetPacketBuilder.paddingAtBuild(true);
        ethernetPacketBuilder.srcAddr(MacAddress.getByName((String) serverNic.get("hardware_address"), ":"));
        ethernetPacketBuilder.dstAddr(MacAddress.getByAddress(Bytes.extract(discoverPacket.chaddr, 0, 6)));

        return ethernetPacketBuilder.build();
    }
}