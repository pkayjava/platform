package com.angkorteam.network.dhcp.server.boot;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "app", ignoreUnknownFields = true)
public class AppProperties {

    // server port
    private int port = 67;

    // 1 day = 86400 seconds
    private int leaseTime = 86400;

    // router address
    private String gatewayAddress;

    // router address
    private String[] dnsServer;

    private String domainName;

    private String ntpServer;

    private int timeoffset;

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getLeaseTime() {
        return leaseTime;
    }

    public void setLeaseTime(int leaseTime) {
        this.leaseTime = leaseTime;
    }

    public String getGatewayAddress() {
        return gatewayAddress;
    }

    public void setGatewayAddress(String gatewayAddress) {
        this.gatewayAddress = gatewayAddress;
    }

    public String[] getDnsServer() {
        return dnsServer;
    }

    public void setDnsServer(String[] dnsServer) {
        this.dnsServer = dnsServer;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getNtpServer() {
        return ntpServer;
    }

    public void setNtpServer(String ntpServer) {
        this.ntpServer = ntpServer;
    }

    public int getTimeoffset() {
        return timeoffset;
    }

    public void setTimeoffset(int timeoffset) {
        this.timeoffset = timeoffset;
    }

}
