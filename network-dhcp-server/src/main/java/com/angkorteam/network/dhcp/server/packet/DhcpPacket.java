package com.angkorteam.network.dhcp.server.packet;

import com.angkorteam.core.function.Bytes;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class DhcpPacket implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4914162751709623710L;

    public static final byte OP_REQUEST = 1;
    public static final byte OP_REPLY = 2;

    public static final byte HTYPE_ETHER = 1;
    public static final byte HTYPE_IEEE802 = 6;
    public static final byte HTYPE_FDDI = 8;
    public static final byte HTYPE_IEEE1394 = 24; // rfc 2855

    public static final byte DHCP_DISCOVER = 1;
    public static final byte DHCP_REQUEST = 3;
    public static final byte DHCP_RELEASE = 7;
    public static final byte DHCP_DECLINE = 4;

    public static final byte DHCP_OFFER = 2;
    public static final byte DHCP_ACK = 5;
    public static final byte DHCP_NAK = 6;

    public static final byte DHCP_INFORM = 8;
    public static final byte DHCP_FORCE_RENEW = 9;
    public static final byte DHCP_LEASE_QUERY = 10; // RFC 4388
    public static final byte DHCP_LEASE_UN_ASSIGNED = 11; // RFC 4388
    public static final byte DHCP_LEASE_UNKNOWN = 12; // RFC 4388
    public static final byte DHCP_LEASE_ACTIVE = 13; // RFC 4388

    public static final byte MAGIC_COOKIE_LENGTH = 4;
    public static final byte[] MAGIC_COOKIE = new byte[MAGIC_COOKIE_LENGTH];

    public byte op;
    public byte htype;
    public byte hlen;
    public byte hops;

    public static final byte XID_LENGTH = 4;
    public final byte[] xid = new byte[XID_LENGTH];

    public static final byte SECS_LENGTH = 2;
    public final byte[] secs = new byte[SECS_LENGTH];

    public static final byte FLAGS_LENGTH = 2;
    public final byte[] flags = new byte[FLAGS_LENGTH];

    public static final byte CIADDR_LENGTH = 4;
    public final byte[] ciaddr = new byte[CIADDR_LENGTH];

    public static final byte YIADDR_LENGTH = 4;
    public final byte[] yiaddr = new byte[YIADDR_LENGTH];

    public static final byte SIADDR_LENGTH = 4;
    public final byte[] siaddr = new byte[SIADDR_LENGTH];

    public static final byte GIADDR_LENGTH = 4;
    public final byte[] giaddr = new byte[GIADDR_LENGTH];

    public static final byte CHADDR_LENGTH = 16;
    public final byte[] chaddr = new byte[CHADDR_LENGTH];

    public static final byte SNAME_LENGTH = 64;
    public final byte[] sname = new byte[SNAME_LENGTH];

    public static final short FILE_LENGTH = 128;
    public final byte[] file = new byte[FILE_LENGTH];

    public final byte[] magicCookie = new byte[MAGIC_COOKIE_LENGTH];

    public final Map<Byte, byte[]> options = new LinkedHashMap<Byte, byte[]>();

    static {
        Bytes.transfer(Bytes.toBytes((int) 0x63825363), MAGIC_COOKIE);
    }

    {
        Bytes.transfer(MAGIC_COOKIE, magicCookie);
    }

    public byte[] serialize() {
        return DhcpPacket.serialize(this);
    }

    public int size() {
        int size = 0;

        // op;
        size++;

        // htype;
        size++;

        // hlen;
        size++;

        // hops;
        size++;

        // xid
        size += xid.length;

        // secs
        size += secs.length;

        // flags
        size += flags.length;

        // ciaddr
        size += ciaddr.length;

        // yiaddr
        size += yiaddr.length;

        // siaddr
        size += siaddr.length;

        // giaddr
        size += giaddr.length;

        // chaddr
        size += chaddr.length;

        // sname
        size += sname.length;

        // file
        size += file.length;

        // magicCookie
        size += magicCookie.length;

        for (Entry<Byte, byte[]> option : options.entrySet()) {
            size++;
            if (option.getValue() != null) {
                size++;
                size += option.getValue().length;
            }
        }

        return size;
    }

    /**
     * https://www.ietf.org/rfc/rfc2131.txt
     *
     * @param packet
     * @return
     */
    public static byte[] serialize(DhcpPacket packet) {
        ByteBuffer buffer = ByteBuffer.allocate(packet.size());

        buffer.put(packet.op);
        buffer.put(packet.htype);
        buffer.put(packet.hlen);
        buffer.put(packet.hops);

        buffer.put(packet.xid);

        buffer.put(packet.secs);
        buffer.put(packet.flags);

        buffer.put(packet.ciaddr);

        buffer.put(packet.yiaddr);

        buffer.put(packet.siaddr);

        buffer.put(packet.giaddr);

        buffer.put(packet.chaddr);

        buffer.put(packet.sname);

        buffer.put(packet.file);

        buffer.put(packet.magicCookie);

        for (Entry<Byte, byte[]> option : packet.options.entrySet()) {
            buffer.put(option.getKey());
            if (option.getValue() != null) {
                buffer.put((byte) option.getValue().length);
                buffer.put(option.getValue());
            }
        }

        return buffer.array();
    }

    public static DhcpPacket deserialize(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.wrap(bytes);

        DhcpPacket packet = new DhcpPacket();

        packet.op = buffer.get();
        packet.htype = buffer.get();
        packet.hlen = buffer.get();
        packet.hops = buffer.get();

        buffer.get(packet.xid);

        buffer.get(packet.secs);
        buffer.get(packet.flags);

        buffer.get(packet.ciaddr);

        buffer.get(packet.yiaddr);

        buffer.get(packet.siaddr);

        buffer.get(packet.giaddr);

        buffer.get(packet.chaddr);

        buffer.get(packet.sname);

        buffer.get(packet.file);

        buffer.get(packet.magicCookie);

        while (true) {
            byte option = buffer.get();
            if (option == Option.P_255.getLiteral()) {
                break;
            }
            byte length = buffer.get();
            byte[] data = new byte[length];
            buffer.get(data);
            packet.options.put(option, data);
        }
        packet.options.put(Option.P_255.getLiteral(), null);

        return packet;
    }

}
