package com.angkorteam.network.dhcp.server;

import com.angkorteam.core.function.Bytes;
import com.angkorteam.network.dhcp.server.packet.DhcpPacket;
import com.angkorteam.network.dhcp.server.packet.Option;
import com.angkorteam.network.dhcp.server.packet.OptionData;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.pcap4j.core.*;
import org.pcap4j.core.PcapNetworkInterface.PromiscuousMode;
import org.pcap4j.packet.*;
import org.pcap4j.packet.namednumber.EtherType;
import org.pcap4j.packet.namednumber.IpNumber;
import org.pcap4j.packet.namednumber.IpVersion;
import org.pcap4j.packet.namednumber.UdpPort;
import org.pcap4j.util.MacAddress;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DhcpClient {

    private static final String SNAPLEN_KEY = DhcpClient.class.getName() + ".snaplen";
    private static final int SNAPLEN = Integer.getInteger(SNAPLEN_KEY, 65536); // [bytes]

    public static void main(String[] args) throws Exception {

        PcapNetworkInterface nif = Pcaps.getDevByName("enp0s31f6");

        PcapHandle handle = nif.openLive(SNAPLEN, PromiscuousMode.PROMISCUOUS, 0);
        ExecutorService pool = Executors.newSingleThreadExecutor();

        PacketListener listener = new PacketListener() {
            @Override
            public void gotPacket(Packet packet) {
                if (packet instanceof EthernetPacket) {
                    parseEthernetPacket(handle, (EthernetPacket) packet);
                } else {
                    System.out.println(packet.getClass().getName());
                }
            }
        };

        Task t = new Task(handle, listener);
        pool.execute(t);

        try {
            {
                EthernetPacket packet = discoverPacket(RandomUtils.nextInt(), "64:00:6a:60:e2:5d",
                        new OptionData(Option.P_057, Bytes.toBytes((short) 1500)));
                handle.sendPacket(packet);

            }

        } finally {
            if (handle != null && handle.isOpen()) {
                handle.close();
            }
            if (pool != null && !pool.isShutdown()) {
                pool.shutdown();
            }

        }

    }

    private static class Task implements Runnable {

        private PcapHandle handle;
        private PacketListener listener;

        public Task(PcapHandle handle, PacketListener listener) {
            this.handle = handle;
            this.listener = listener;
        }

        @Override
        public void run() {
            try {
                handle.loop(5, listener);
            } catch (PcapNativeException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (NotOpenException e) {
                e.printStackTrace();
            }
        }

    }

    public static EthernetPacket dicoverPacket(int xid, String macAddress) {
        return discoverPacket(xid, macAddress, new OptionData[]{});
    }

    public static EthernetPacket discoverPacket(int xid, String macAddress, OptionData... data) {
        DhcpPacket dhcpPacket = new DhcpPacket();

        dhcpPacket.op = DhcpPacket.OP_REQUEST;
        dhcpPacket.htype = DhcpPacket.HTYPE_ETHER;
        dhcpPacket.hlen = 6;
        dhcpPacket.hops = 0;

        Bytes.transfer(Bytes.toBytes(xid), dhcpPacket.xid);

        String chaddr = StringUtils.replace(macAddress, ":", "");

        try {
            Bytes.transfer(Hex.decodeHex(chaddr + StringUtils.repeat("00", 10)), dhcpPacket.chaddr);
        } catch (DecoderException e) {
        }

        if (data != null && data.length > 0) {
            for (OptionData i : data) {
                if (i.getKey() == Option.P_255 || i.getKey() == Option.P_053 || i.getKey() == Option.P_061) {
                    continue;
                } else {
                    dhcpPacket.options.put(i.getKey().getLiteral(), i.getData());
                }
            }
        }

        dhcpPacket.options.put(Option.P_053.getLiteral(), new byte[]{DhcpPacket.DHCP_DISCOVER});
        try {
            dhcpPacket.options.put(Option.P_061.getLiteral(),
                    Bytes.join(DhcpPacket.HTYPE_ETHER, Hex.decodeHex(chaddr)));
        } catch (DecoderException e) {
            throw new IllegalArgumentException(e);
        }
        dhcpPacket.options.put(Option.P_255.getLiteral(), null);
        dhcpPacket.options.put(Option.P_000.getLiteral(), null);

        Inet4Address srcAddr = null;
        try {
            srcAddr = (Inet4Address) Inet4Address.getByName("0.0.0.0");
        } catch (UnknownHostException e) {
            throw new IllegalArgumentException(e);
        }

        Inet4Address dstAddr = null;
        try {
            dstAddr = (Inet4Address) Inet4Address.getByName("255.255.255.255");
        } catch (UnknownHostException e) {
            throw new IllegalArgumentException(e);
        }

        UnknownPacket.Builder dhcpBuilder = new UnknownPacket.Builder();
        dhcpBuilder.rawData(dhcpPacket.serialize());

        UdpPacket.Builder udpPacketBuilder = new UdpPacket.Builder();
        udpPacketBuilder.payloadBuilder(dhcpBuilder);
        udpPacketBuilder.srcPort(UdpPort.BOOTPC);
        udpPacketBuilder.dstPort(UdpPort.BOOTPS);
        udpPacketBuilder.srcAddr(srcAddr);
        udpPacketBuilder.dstAddr(dstAddr);
        udpPacketBuilder.correctChecksumAtBuild(true);
        udpPacketBuilder.correctLengthAtBuild(true);

        IpV4Packet.Builder ipV4PacketBuilder = new IpV4Packet.Builder();
        ipV4PacketBuilder.payloadBuilder(udpPacketBuilder);
        ipV4PacketBuilder.srcAddr(srcAddr);
        ipV4PacketBuilder.dstAddr(dstAddr);
        ipV4PacketBuilder.protocol(IpNumber.UDP);
        ipV4PacketBuilder.correctChecksumAtBuild(true);
        ipV4PacketBuilder.correctLengthAtBuild(true);
        ipV4PacketBuilder.version(IpVersion.IPV4);
        ipV4PacketBuilder.tos(IpV4Rfc1349Tos.newInstance((byte) 0x75));
        ipV4PacketBuilder.ihl((byte) 9);
        ipV4PacketBuilder.ttl((byte) 100);
        ipV4PacketBuilder.paddingAtBuild(true);

        EthernetPacket.Builder ethernetPacketBuilder = new EthernetPacket.Builder();
        ethernetPacketBuilder.payloadBuilder(ipV4PacketBuilder);
        ethernetPacketBuilder.type(EtherType.IPV4);
        ethernetPacketBuilder.paddingAtBuild(true);
        ethernetPacketBuilder.srcAddr(MacAddress.getByName(macAddress, ":"));
        ethernetPacketBuilder.dstAddr(MacAddress.getByName("ff:ff:ff:ff:ff:ff", ":"));

        return ethernetPacketBuilder.build();
    }

    public static EthernetPacket requestPacket(String hostName, DhcpPacket offerPacket) {
        DhcpPacket requestPacket = new DhcpPacket();

        requestPacket.op = DhcpPacket.OP_REQUEST;
        requestPacket.htype = offerPacket.htype;
        requestPacket.hlen = offerPacket.hlen;
        requestPacket.hops = 0;

        Bytes.transfer(offerPacket.xid, requestPacket.xid);

        Bytes.transfer(offerPacket.chaddr, requestPacket.chaddr);

        requestPacket.options.put(Option.P_054.getLiteral(), offerPacket.options.get(Option.P_054.getLiteral()));

        try {
            requestPacket.options.put(Option.P_050.getLiteral(), InetAddress.getByName("172.16.1.3").getAddress());
        } catch (UnknownHostException e1) {
        }

        requestPacket.options.put(Option.P_012.getLiteral(), hostName.getBytes());

        requestPacket.options.put(Option.P_053.getLiteral(), new byte[]{DhcpPacket.DHCP_REQUEST});

        requestPacket.options.put(Option.P_055.getLiteral(),
                new byte[]{Option.P_001.getLiteral(), Option.P_002.getLiteral(), Option.P_003.getLiteral(),
                        Option.P_006.getLiteral(), Option.P_012.getLiteral(), Option.P_015.getLiteral(),
                        Option.P_026.getLiteral(), Option.P_028.getLiteral(), Option.P_042.getLiteral(),
                        Option.P_044.getLiteral(), Option.P_047.getLiteral(), Option.P_119.getLiteral(),
                        Option.P_121.getLiteral()});

        requestPacket.options.put(Option.P_255.getLiteral(), null);
        requestPacket.options.put(Option.P_000.getLiteral(), null);

        Inet4Address srcAddr = null;
        try {
            srcAddr = (Inet4Address) Inet4Address.getByName("0.0.0.0");
        } catch (UnknownHostException e) {
            throw new IllegalArgumentException(e);
        }

        Inet4Address dstAddr = null;
        try {
            dstAddr = (Inet4Address) Inet4Address.getByName("255.255.255.255");
        } catch (UnknownHostException e) {
            throw new IllegalArgumentException(e);
        }

        UnknownPacket.Builder dhcpBuilder = new UnknownPacket.Builder();
        dhcpBuilder.rawData(requestPacket.serialize());

        UdpPacket.Builder udpPacketBuilder = new UdpPacket.Builder();
        udpPacketBuilder.payloadBuilder(dhcpBuilder);
        udpPacketBuilder.srcPort(UdpPort.BOOTPC);
        udpPacketBuilder.dstPort(UdpPort.BOOTPS);
        udpPacketBuilder.srcAddr(srcAddr);
        udpPacketBuilder.dstAddr(dstAddr);
        udpPacketBuilder.correctChecksumAtBuild(true);
        udpPacketBuilder.correctLengthAtBuild(true);

        IpV4Packet.Builder ipV4PacketBuilder = new IpV4Packet.Builder();
        ipV4PacketBuilder.payloadBuilder(udpPacketBuilder);
        ipV4PacketBuilder.srcAddr(srcAddr);
        ipV4PacketBuilder.dstAddr(dstAddr);
        ipV4PacketBuilder.protocol(IpNumber.UDP);
        ipV4PacketBuilder.correctChecksumAtBuild(true);
        ipV4PacketBuilder.correctLengthAtBuild(true);
        ipV4PacketBuilder.version(IpVersion.IPV4);
        ipV4PacketBuilder.tos(IpV4Rfc1349Tos.newInstance((byte) 0x75));
        ipV4PacketBuilder.ihl((byte) 9);
        ipV4PacketBuilder.ttl((byte) 100);
        ipV4PacketBuilder.paddingAtBuild(true);

        EthernetPacket.Builder ethernetPacketBuilder = new EthernetPacket.Builder();
        ethernetPacketBuilder.payloadBuilder(ipV4PacketBuilder);
        ethernetPacketBuilder.type(EtherType.IPV4);
        ethernetPacketBuilder.paddingAtBuild(true);
        ethernetPacketBuilder.srcAddr(MacAddress.getByAddress(Bytes.extract(offerPacket.chaddr, 0, 6)));
        ethernetPacketBuilder.dstAddr(MacAddress.getByName("ff:ff:ff:ff:ff:ff", ":"));

        return ethernetPacketBuilder.build();
    }

    protected static void parseEthernetPacket(PcapHandle handle, EthernetPacket ethernetPacket) {
        if (ethernetPacket.getPayload() instanceof IpV4Packet) {
            parseIpV4Packet(handle, ethernetPacket, (IpV4Packet) ethernetPacket.getPayload());
        } else {
            if (ethernetPacket.getPayload() != null) {
                System.out.println(ethernetPacket.getPayload().getClass().getName());
            }
        }
    }

    protected static void parseIpV4Packet(PcapHandle handle, EthernetPacket ethernetPacket, IpV4Packet ipV4Packet) {
        if (ipV4Packet.getPayload() instanceof UdpPacket) {
            parseUdpPacket(handle, ethernetPacket, ipV4Packet, (UdpPacket) ipV4Packet.getPayload());
        } else {
            if (ipV4Packet.getPayload() != null) {
                System.out.println(ipV4Packet.getPayload().getClass().getName());
            }
        }
    }

    protected static void parseUdpPacket(PcapHandle handle, EthernetPacket ethernetPacket, IpV4Packet ipV4Packet,
                                         UdpPacket udpPacket) {
        if (udpPacket.getPayload() instanceof UnknownPacket) {
            parseUnknownPacket(handle, ethernetPacket, ipV4Packet, udpPacket, (UnknownPacket) udpPacket.getPayload());
        } else {
            if (udpPacket.getPayload() != null) {
                System.out.println(udpPacket.getPayload().getClass().getName());
            }
        }
    }

    protected static void parseUnknownPacket(PcapHandle handle, EthernetPacket ethernetPacket, IpV4Packet ipV4Packet,
                                             UdpPacket udpPacket, UnknownPacket unknownPacket) {
        if (udpPacket.getHeader().getDstPort().valueAsInt() != 67
                && udpPacket.getHeader().getDstPort().valueAsInt() != 68) {
            return;
        }
        DhcpPacket offerPacket = DhcpPacket.deserialize(unknownPacket.getRawData());

        if (offerPacket.op == DhcpPacket.OP_REPLY) {
            byte[] values = offerPacket.options.get(Option.P_053.getLiteral());
            if (values != null && values.length > 0) {
                if (values[0] == DhcpPacket.DHCP_OFFER) {
                    EthernetPacket packet = requestPacket("skhauv-XPS-8900", offerPacket);
                    try {
                        handle.sendPacket(packet);
                    } catch (PcapNativeException | NotOpenException e) {
                    }
                }
            }

        }
    }

}
