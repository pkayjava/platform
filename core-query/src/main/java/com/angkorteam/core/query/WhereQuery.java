package com.angkorteam.core.query;

import com.angkorteam.core.function.CharacterExtension;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by socheatkhauv on 29/1/17.
 */
public abstract class WhereQuery extends ParamQuery {

    /**
     *
     */
    private static final long serialVersionUID = 3458379584516491058L;

    protected final List<String> where = new LinkedList<>();

    protected WhereQuery addWhere(String criteria) {
        if (criteria.contains(":")) {
            throw new IllegalArgumentException(": is invalid");
        }
        this.where.add(criteria);
        this.dirty = true;
        return this;
    }

    protected WhereQuery addWhere(String criteria, Object value) {
        addInternalCriteria(this.where, this.param, criteria, value);
        return this;
    }

    protected WhereQuery addWhere(String criteria, String paramName, Object paramValue) {
        if (!criteria.contains(":" + paramName)) {
            throw new IllegalArgumentException(paramName + " is missing");
        }
        this.where.add(criteria);
        this.param.put(paramName, paramValue);
        this.dirty = true;
        return this;
    }

    protected <T> WhereQuery addWhere(String criteria, Class<T> typeClass, List<T> values) {
        addInternalCriteria(this.where, this.param, criteria, typeClass, values);
        return this;
    }

    protected <T> WhereQuery addWhere(String criteria, Class<T> typeClass, T value1, T value2) {
        addInternalCriteria(this.where, this.param, criteria, typeClass, value1, value2);
        return this;
    }

    protected <T> void addInternalCriteria(List<String> list, Map<String, Object> listParam, String criteria,
                                           Class<T> typeClass, T value1, T value2) {
        if (typeClass == Boolean.class || typeClass == boolean.class || typeClass == Byte.class
                || typeClass == byte.class || typeClass == Short.class || typeClass == short.class
                || typeClass == Integer.class || typeClass == int.class || typeClass == Long.class
                || typeClass == long.class || typeClass == Float.class || typeClass == float.class
                || typeClass == Double.class || typeClass == double.class || typeClass == Date.class
                || typeClass == Character.class || typeClass == char.class || typeClass == String.class) {
        } else {
            throw new IllegalArgumentException(typeClass + " is not support");
        }
        boolean beginColon = false;
        String paramName = "";
        int paramIndex = 0;
        boolean value1Param = false;
        boolean value2Param = false;
        for (Character ch : criteria.toCharArray()) {
            if (ch == ':') {
                beginColon = true;
                paramIndex++;
                continue;
            }
            if (beginColon) {
                if (Character.isDigit(ch) || CharacterExtension.isAlphabet(ch) || ch == '_') {
                    paramName += ch;
                } else {
                    if (!StringUtils.isBlank(paramName)) {
                        if (paramIndex == 1) {
                            listParam.put(paramName, value1);
                            value1Param = true;
                        } else if (paramIndex == 2) {
                            listParam.put(paramName, value2);
                            value2Param = true;
                        }
                        paramName = "";
                    }
                }
            }
        }
        if (paramIndex == 1) {
            listParam.put(paramName, value1);
            value1Param = true;
        } else if (paramIndex == 2) {
            listParam.put(paramName, value2);
            value2Param = true;
        }
        if (!value1Param && !value2Param) {
            throw new IllegalArgumentException("parameter for value1/value2 must be defined");
        }
        if (!value1Param) {
            throw new IllegalArgumentException("parameter for value1 must be defined");
        }
        if (!value2Param) {
            throw new IllegalArgumentException("parameter for value2 must be defined");
        }
        list.add(criteria);
        this.dirty = true;
    }

    protected void addInternalCriteria(List<String> list, Map<String, Object> listParam, String criteria,
                                       Object value) {
        if (value != null) {
            if (value instanceof Boolean || value instanceof Byte || value instanceof Short || value instanceof Integer
                    || value instanceof Long || value instanceof Float || value instanceof Double
                    || value instanceof Date || value instanceof Character || value instanceof String) {
            } else {
                throw new IllegalArgumentException(value + " is not support");
            }
        }
        boolean beginColon = false;
        String paramName = "";
        boolean valueParam = false;
        for (Character ch : criteria.toCharArray()) {
            if (ch == ':') {
                beginColon = true;
                continue;
            }
            if (beginColon) {
                if (Character.isDigit(ch) || CharacterExtension.isAlphabet(ch) || ch == '_') {
                    paramName += ch;
                } else {
                    if (!StringUtils.isBlank(paramName)) {
                        listParam.put(paramName, value);
                        paramName = "";
                        valueParam = true;
                    }
                    break;
                }
            }
        }
        if (!StringUtils.isBlank(paramName)) {
            listParam.put(paramName, value);
            valueParam = true;
        }
        if (!valueParam) {
            throw new IllegalArgumentException("parameter for value must be defined");
        }
        list.add(criteria);
        this.dirty = true;
    }

    protected <T> void addInternalCriteria(List<String> list, Map<String, Object> listParam, String criteria,
                                           Class<T> typeClass, List<T> values) {
        if (typeClass == Boolean.class || typeClass == boolean.class || typeClass == Byte.class
                || typeClass == byte.class || typeClass == Short.class || typeClass == short.class
                || typeClass == Integer.class || typeClass == int.class || typeClass == Long.class
                || typeClass == long.class || typeClass == Float.class || typeClass == float.class
                || typeClass == Double.class || typeClass == double.class || typeClass == Date.class
                || typeClass == Character.class || typeClass == char.class || typeClass == String.class) {
        } else {
            throw new IllegalArgumentException(typeClass + " is not support");
        }

        boolean beginColon = false;
        String paramName = "";
        boolean valueParam = false;
        for (Character ch : criteria.toCharArray()) {
            if (ch == ':') {
                beginColon = true;
                continue;
            }
            if (beginColon) {
                if (Character.isDigit(ch) || CharacterExtension.isAlphabet(ch) || ch == '_') {
                    paramName += ch;
                } else {
                    if (!StringUtils.isBlank(paramName)) {
                        listParam.put(paramName, values);
                        paramName = "";
                        valueParam = true;
                    }
                    break;
                }
            }
        }
        if (!StringUtils.isBlank(paramName)) {
            listParam.put(paramName, values);
            valueParam = true;
        }
        if (!valueParam) {
            throw new IllegalArgumentException("parameter for values must be defined");
        }
        list.add(criteria);
        this.dirty = true;
    }

}
