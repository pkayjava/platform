package com.angkorteam.core.query;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * Created by socheatkhauv on 29/1/17.
 */
public class DeleteQuery extends WhereQuery {

    /**
     *
     */
    private static final long serialVersionUID = -7027321930702898882L;

    protected final String tableName;

    public DeleteQuery(String tableName) {
        this.tableName = tableName;
    }

    public String toSQL() {
        if (!this.dirty) {
            return cached;
        }
        StringBuilder builder = new StringBuilder();
        builder.append("DELETE FROM ").append(this.tableName);
        if (!this.where.isEmpty()) {
            builder.append(" WHERE ").append(StringUtils.join(this.where, " AND "));
        }
        this.cached = builder.toString();
        this.dirty = false;
        return this.cached;
    }

    @Override
    public DeleteQuery addWhere(String criteria, Object value) {
        return (DeleteQuery) super.addWhere(criteria, value);
    }

    @Override
    public DeleteQuery addWhere(String criteria, String paramName, Object paramValue) {
        return (DeleteQuery) super.addWhere(criteria, paramName, paramValue);
    }

    @Override
    public <T> DeleteQuery addWhere(String criteria, Class<T> typeClass, List<T> values) {
        return (DeleteQuery) super.addWhere(criteria, typeClass, values);
    }

    @Override
    public <T> DeleteQuery addWhere(String criteria, Class<T> typeClass, T value1, T value2) {
        return (DeleteQuery) super.addWhere(criteria, typeClass, value1, value2);
    }

}
