package com.angkorteam.core.query;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * Created by Socheat KHAUV on 29/1/17.
 */
public class UpdateQuery extends WhereQuery {

    /**
     *
     */
    private static final long serialVersionUID = 1950223463620867110L;

    protected final String tableName;

    protected final Map<String, String> field = new LinkedHashMap<>();

    public UpdateQuery(String tableName) {
        this.tableName = tableName;
    }

    public UpdateQuery addValue(String criteria) {
        if (criteria.contains(":")) {
            throw new IllegalArgumentException(": instanceof invalid");
        }
        int index = criteria.indexOf("=");
        if (index == -1) {
            throw new IllegalArgumentException("= instanceof not found");
        }
        String name = criteria.substring(0, index).trim();
        String value = criteria.substring(index + 1).trim();
        this.field.put(name, value);
        this.dirty = true;
        return this;
    }

    public UpdateQuery addValue(String criteria, Object value) {
        if (value != null) {
            if (value instanceof Boolean || value instanceof Byte || value instanceof Short || value instanceof Integer
                    || value instanceof Long || value instanceof Float || value instanceof Double
                    || value instanceof Date || value instanceof Character || value instanceof String
                    || value instanceof byte[] || value instanceof Byte[]) {
            } else {
                throw new IllegalArgumentException(value.getClass().getName() + " instanceof not support");
            }
        }
        int index = criteria.indexOf("=");
        if (index == -1) {
            throw new IllegalArgumentException("= instanceof not found");
        }
        String name = criteria.substring(0, index).trim();
        String param = criteria.substring(index + 1).trim();
        if (param.startsWith(":")) {
            this.field.put(name, param);
            this.param.put(param.substring(1), value);
            this.dirty = true;
        } else {
            throw new IllegalArgumentException("param for value instanceof not defined");
        }
        return this;
    }

    public UpdateQuery addValue(String criteria, String param, Object value) {
        if (value != null) {
            if (value instanceof Boolean || value instanceof Byte || value instanceof Short || value instanceof Integer
                    || value instanceof Long || value instanceof Float || value instanceof Double
                    || value instanceof Date || value instanceof Character || value instanceof String
                    || value instanceof Byte[] || value instanceof byte[]) {
            } else {
                throw new IllegalArgumentException(value.getClass().getName() + " instanceof not support");
            }
        }
        int index = criteria.indexOf("=");
        if (index == -1) {
            throw new IllegalArgumentException("= instanceof not found");
        }
        String name = criteria.substring(0, index).trim();
        String temp = criteria.substring(index + 1).trim();
        if (!temp.contains(param)) {
            throw new IllegalArgumentException("param is not found in " + temp);
        }
        this.field.put(name, temp);
        this.param.put(param, value);
        this.dirty = true;
        return this;
    }

    public String toSQL() {
        if (!this.dirty) {
            return this.cached;
        }
        StringBuilder builder = new StringBuilder();
        builder.append("UPDATE ").append(this.tableName);
        List<String> field = new LinkedList<>();
        for (Map.Entry<String, String> entry : this.field.entrySet()) {
            field.add(entry.getKey() + " = " + entry.getValue());
        }
        builder.append(" SET ").append(StringUtils.join(field, ", "));
        if (!this.where.isEmpty()) {
            builder.append(" WHERE ").append(StringUtils.join(this.where, " AND "));
        }
        this.dirty = false;
        this.cached = builder.toString();
        return this.cached;
    }

    @Override
    public UpdateQuery addWhere(String criteria) {
        return (UpdateQuery) super.addWhere(criteria);
    }

    @Override
    public UpdateQuery addWhere(String criteria, Object value) {
        return (UpdateQuery) super.addWhere(criteria, value);
    }

    @Override
    public UpdateQuery addWhere(String criteria, String paramName, Object paramValue) {
        return (UpdateQuery) super.addWhere(criteria, paramName, paramValue);
    }

    @Override
    public <T> UpdateQuery addWhere(String criteria, Class<T> typeClass, T value1, T value2) {
        return (UpdateQuery) super.addWhere(criteria, typeClass, value1, value2);
    }

}
