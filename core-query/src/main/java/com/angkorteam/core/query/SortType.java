package com.angkorteam.core.query;

/**
 * Created by socheatkhauv on 30/1/17.
 */
public enum SortType {
    Asc, Desc
}
