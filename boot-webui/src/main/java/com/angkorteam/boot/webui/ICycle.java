package com.angkorteam.boot.webui;

import com.angkorteam.core.webui.WebUiProperties;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.protocol.http.WebSession;
import org.apache.wicket.util.io.IClusterable;

public interface ICycle extends IClusterable {

    default void onInitializedApplication(WebUiProperties webUiProperties, WebApplication application) {
    }

    default void onDestroyedApplication(WebUiProperties webUiProperties, WebApplication application) {
    }

    default void onInitializedSession(WebUiProperties webUiProperties, WebSession session) {
    }

    default void onDestroyedSession(WebUiProperties webUiProperties, String sessionId) {
    }

    Class<? extends WebPage> getHomePageClass(WebUiProperties webUiProperties, WebSession session);

}
