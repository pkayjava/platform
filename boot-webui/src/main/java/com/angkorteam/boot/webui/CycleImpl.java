package com.angkorteam.boot.webui;

import com.angkorteam.core.webui.WebUiProperties;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebSession;

public class CycleImpl implements ICycle {

    /**
     *
     */
    private static final long serialVersionUID = 4050820611187965968L;

    @Override
    public Class<? extends WebPage> getHomePageClass(WebUiProperties webUiProperties, WebSession session) {
        return HomePage.class;
    }

}
