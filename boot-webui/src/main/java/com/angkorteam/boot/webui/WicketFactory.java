package com.angkorteam.boot.webui;

import com.angkorteam.core.webui.ReferenceUtilities;
import com.angkorteam.core.webui.ResourceScope;
import com.angkorteam.core.webui.WebUiProperties;
import org.apache.wicket.*;
import org.apache.wicket.markup.html.IPackageResourceGuard;
import org.apache.wicket.markup.html.SecurePackageResourceGuard;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.IWebApplicationFactory;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.protocol.http.WebSession;
import org.apache.wicket.protocol.http.WicketFilter;
import org.apache.wicket.request.resource.PackageResourceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class WicketFactory extends org.apache.wicket.protocol.http.WebApplication
        implements IWebApplicationFactory, ISessionListener, IApplicationListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(WicketFactory.class);

    private transient static ApplicationContext applicationContext;

    private ICycle cycle;

    private WebUiProperties webUiProperties;

    public WicketFactory() {
    }

    @Override
    protected void init() {
        super.init();
        getApplicationListeners().add(this);
        getSessionListeners().add(this);
        IPackageResourceGuard packageResourceGuard = this.getResourceSettings().getPackageResourceGuard();
        if (packageResourceGuard instanceof SecurePackageResourceGuard) {
            SecurePackageResourceGuard guard = (SecurePackageResourceGuard) packageResourceGuard;
            guard.addPattern("+*.ogg");
            guard.addPattern("+*.mp3");
        }
        getJavaScriptLibrarySettings()
                .setJQueryReference(new PackageResourceReference(ResourceScope.class, ReferenceUtilities.J_QUERY_JS));

        getMarkupSettings().setStripWicketTags(true);
    }

    @Override
    public Class<? extends WebPage> getHomePage() {
        return getCycle().getHomePageClass(getWebUiProperties(), WebSession.get());
    }

    protected ICycle getCycle() {
        return this.cycle;
    }

    @Override
    public RuntimeConfigurationType getConfigurationType() {
        return getWebUiProperties().getConfigurationType();
    }

    protected WebUiProperties getWebUiProperties() {
        return this.webUiProperties;
    }

    @Override
    public org.apache.wicket.protocol.http.WebApplication createApplication(WicketFilter filter) {
        applicationContext = WebApplicationContextUtils
                .getRequiredWebApplicationContext(filter.getFilterConfig().getServletContext());
        WebUiProperties webUiProperties = getApplicationContext().getBean(WebUiProperties.class);
        this.webUiProperties = webUiProperties;
        if (webUiProperties.getCycleClass() == null) {
            this.cycle = new CycleImpl();
        } else {
            if (ICycle.class.isAssignableFrom(webUiProperties.getCycleClass())) {
                try {
                    this.cycle = (ICycle) webUiProperties.getCycleClass().newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    throw new WicketRuntimeException(e);
                }
            } else {
                throw new WicketRuntimeException("webui.cycleClass is not instance of " + ICycle.class.getName());
            }
        }
        return this;
    }

    public static ApplicationContext getApplicationContext() {
        if (applicationContext == null) {
            throw new WicketRuntimeException("NPE");
        }
        return applicationContext;
    }

    @Override
    public void onCreated(Session session) {
        getCycle().onInitializedSession(getWebUiProperties(), (WebSession) session);
    }

    @Override
    public void onUnbound(String sessionId) {
        getCycle().onDestroyedSession(getWebUiProperties(), sessionId);
    }

    @Override
    public void onAfterInitialized(Application application) {
        getCycle().onInitializedApplication(getWebUiProperties(), (WebApplication) application);
    }

    @Override
    public void onBeforeDestroyed(Application application) {
        getCycle().onDestroyedApplication(getWebUiProperties(), (WebApplication) application);
    }

    @Override
    public void destroy(WicketFilter filter) {
    }
}
