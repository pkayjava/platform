package com.angkorteam.core.crypto;

//package com.angkorteam.framework.security;
//
//import java.io.IOException;
//import java.io.OutputStreamWriter;
//import java.math.BigInteger;
//import java.security.KeyPair;
//import java.security.KeyPairGenerator;
//import java.security.NoSuchAlgorithmException;
//import java.security.PrivateKey;
//import java.security.PublicKey;
//import java.security.Security;
//import java.util.Date;
//
//import javax.security.auth.x500.X500Principal;
//import javax.security.cert.Certificate;
//
//import org.bouncycastle.asn1.x500.X500Name;
//import org.bouncycastle.cert.X509CertificateHolder;
//import org.bouncycastle.cert.X509v1CertificateBuilder;
//import org.bouncycastle.cert.jcajce.JcaX509v1CertificateBuilder;
//import org.bouncycastle.jce.provider.BouncyCastleProvider;
//import org.bouncycastle.openssl.jcajce.JcaMiscPEMGenerator;
//import org.bouncycastle.operator.ContentSigner;
//import org.bouncycastle.operator.OperatorCreationException;
//import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
//import org.bouncycastle.pkcs.PKCS10CertificationRequest;
//import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
//import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder;
//import org.bouncycastle.util.io.pem.PemWriter;
//
//public class Testing {
//
//    static {
//        Security.addProvider(new BouncyCastleProvider());
//    }
//
//    public static void main(String[] args) throws NoSuchAlgorithmException, OperatorCreationException, IOException {
//
//    }
//
//    static char[] passwd = { 'h', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd' };
//
//    /**
//     * we generate the CA's certificate
//     */
//    public static Certificate createMasterCert(PublicKey pubKey, PrivateKey privKey) throws Exception {
//        //
//        // signers name
//        //
//        String issuer = "C=AU, O=The Legion of the Bouncy Castle, OU=Bouncy Primary Certificate";
//
//        //
//        // subjects name - the same as we are self signed.
//        //
//        String subject = "C=AU, O=The Legion of the Bouncy Castle, OU=Bouncy Primary Certificate";
//
//        //
//        // create the certificate - version 1
//        //
//        X509v1CertificateBuilder v1Bldr = new JcaX509v1CertificateBuilder(new X500Name(issuer), BigInteger.valueOf(1), new Date(System.currentTimeMillis() - 1000L * 60 * 60 * 24 * 30), new Date(System.currentTimeMillis() + (1000L * 60 * 60 * 24 * 30)), new X500Name(subject), pubKey);
//
//        X509CertificateHolder certHldr = v1Bldr.build(new JcaContentSignerBuilder("SHA1WithRSA").setProvider("BC").build(privKey));
//
//        X509Certificate cert = new JcaX509CertificateConverter().setProvider("BC").getCertificate(certHldr);
//
//        cert.checkValidity(new Date());
//
//        cert.verify(pubKey);
//
//        PKCS12BagAttributeCarrier bagAttr = (PKCS12BagAttributeCarrier) cert;
//
//        //
//        // this is actually optional - but if you want to have control
//        // over setting the friendly name this is the way to do it...
//        //
//        bagAttr.setBagAttribute(PKCSObjectIdentifiers.pkcs_9_at_friendlyName, new DERBMPString("Bouncy Primary Certificate"));
//
//        return cert;
//    }
//
//    public static void CSR() throws NoSuchAlgorithmException, OperatorCreationException, IOException {
//        KeyPairGenerator gen = KeyPairGenerator.getInstance("RSA");
//        gen.initialize(2048);
//        KeyPair pair = gen.generateKeyPair();
//
//        PrivateKey privateKey = pair.getPrivate();
//        PublicKey publicKey = pair.getPublic();
//
//        // http://www.bouncycastle.org/wiki/display/JA1/BC+Version+2+APIs
//        ContentSigner signGen = new JcaContentSignerBuilder("SHA1withRSA").build(privateKey);
//
//        X500Principal subject = new X500Principal("C=NO, ST=Trondheim, L=Trondheim, O=Senthadev, OU=Innovation, CN=www.senthadev.com, EMAILADDRESS=senthadev@gmail.com");
//        PKCS10CertificationRequestBuilder builder = new JcaPKCS10CertificationRequestBuilder(subject, publicKey);
//        PKCS10CertificationRequest request = builder.build(signGen);
//
//        OutputStreamWriter output = new OutputStreamWriter(System.out);
//        PemWriter pem = new PemWriter(output);
//
//        JcaMiscPEMGenerator generator = new JcaMiscPEMGenerator(request);
//        pem.writeObject(generator);
//        pem.close();
//    }
//
//}
