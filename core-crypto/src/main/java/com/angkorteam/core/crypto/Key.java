package com.angkorteam.core.crypto;

public interface Key {

    public String getEncoded();

}
