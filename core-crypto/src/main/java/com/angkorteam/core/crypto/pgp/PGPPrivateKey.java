package com.angkorteam.core.crypto.pgp;

import com.angkorteam.core.crypto.PrivateKey;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.BCPGOutputStream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.*;
import org.bouncycastle.openpgp.jcajce.JcaPGPObjectFactory;
import org.bouncycastle.openpgp.operator.PublicKeyDataDecryptorFactory;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPContentSignerBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcePublicKeyDataDecryptorFactoryBuilder;
import org.bouncycastle.util.io.Streams;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Security;
import java.util.Iterator;

public class PGPPrivateKey implements PrivateKey {

    static {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    protected org.bouncycastle.openpgp.PGPPrivateKey privateKey;

    protected PGPSecretKey secretKey;

    protected PGPPrivateKey(org.bouncycastle.openpgp.PGPPrivateKey privateKey, PGPSecretKey secretKey) {
        this.privateKey = privateKey;
        this.secretKey = secretKey;
    }

    @Override
    public String getEncoded() {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            ArmoredOutputStream stream = new ArmoredOutputStream(out);
            this.secretKey.encode(stream);
            stream.close();
            return new String(out.toByteArray());
        } catch (IOException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public String sign(String message) {
        try {
            ByteArrayOutputStream signature = new ByteArrayOutputStream();
            try (ArmoredOutputStream armored = new ArmoredOutputStream(signature)) {
                JcaPGPContentSignerBuilder builder = new JcaPGPContentSignerBuilder(
                        this.secretKey.getPublicKey().getAlgorithm(), PGPUtil.SHA256)
                        .setProvider(BouncyCastleProvider.PROVIDER_NAME);
                PGPSignatureGenerator sGen = new PGPSignatureGenerator(builder);
                sGen.init(PGPSignature.BINARY_DOCUMENT, this.privateKey);
                BCPGOutputStream bOut = new BCPGOutputStream(armored);
                sGen.update(message.getBytes());
                sGen.generate().encode(bOut);
            }
            return new String(signature.toByteArray());
        } catch (IOException | PGPException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public String decrypt(String message) {
        byte[] decrpytedData = message.getBytes();
        try {
            ByteArrayOutputStream clearData = new ByteArrayOutputStream();
            try (InputStream inputStream = new ByteArrayInputStream(decrpytedData)) {
                try (InputStream decoderStream = PGPUtil.getDecoderStream(inputStream)) {
                    PGPPublicKeyEncryptedData publicKeyEncryptedData;
                    {
                        JcaPGPObjectFactory objectFactory = new JcaPGPObjectFactory(decoderStream);
                        Object object = objectFactory.nextObject();
                        PGPEncryptedDataList encryptedDataList;
                        if (object instanceof PGPEncryptedDataList) {
                            encryptedDataList = (PGPEncryptedDataList) object;
                        } else {
                            encryptedDataList = (PGPEncryptedDataList) objectFactory.nextObject();
                        }
                        Iterator it = encryptedDataList.getEncryptedDataObjects();
                        publicKeyEncryptedData = (PGPPublicKeyEncryptedData) it.next();
                    }

                    JcePublicKeyDataDecryptorFactoryBuilder builder = new JcePublicKeyDataDecryptorFactoryBuilder();
                    builder.setProvider(BouncyCastleProvider.PROVIDER_NAME);
                    PublicKeyDataDecryptorFactory decryptionFactory = builder.build(this.privateKey);
                    InputStream clear = publicKeyEncryptedData.getDataStream(decryptionFactory);

                    Object object;
                    {
                        JcaPGPObjectFactory objectFactory = new JcaPGPObjectFactory(clear);
                        object = objectFactory.nextObject();
                    }

                    if (object instanceof PGPCompressedData) {
                        PGPCompressedData cData = (PGPCompressedData) object;
                        JcaPGPObjectFactory objectFactory = new JcaPGPObjectFactory(cData.getDataStream());
                        object = objectFactory.nextObject();
                    }
                    if (object instanceof PGPLiteralData) {
                        PGPLiteralData literalData = (PGPLiteralData) object;
                        try (InputStream unc = literalData.getInputStream()) {
                            Streams.pipeAll(unc, clearData);
                        }
                    } else if (object instanceof PGPOnePassSignatureList) {
                        throw new PGPException("encrypted message contains a signed message - not literal data.");
                    } else {
                        throw new PGPException("message is not a simple encrypted file - type unknown.");
                    }
                    if (publicKeyEncryptedData.isIntegrityProtected()) {
                        if (!publicKeyEncryptedData.verify()) {
                            System.err.println("message failed integrity check");
                        }
                    } else {
                        System.err.println("no message integrity check");
                    }
                }
            }
            return new String(clearData.toByteArray());
        } catch (IOException | PGPException e) {
            throw new UnsupportedOperationException(e);
        }
    }

}
