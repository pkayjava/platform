package com.angkorteam.core.crypto;

public interface PublicKey extends Key {

    public boolean verify(String message, String signature);

    public String encrypt(String message);

}
