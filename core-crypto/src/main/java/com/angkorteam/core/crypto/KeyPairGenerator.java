package com.angkorteam.core.crypto;

import java.security.Provider;

public interface KeyPairGenerator {

    public void initialize(int keysize);

    public String getAlgorithm();

    public Provider getProvider();

    public KeyPair generateKeyPair();
}
