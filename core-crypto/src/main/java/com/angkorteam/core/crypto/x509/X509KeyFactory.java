package com.angkorteam.core.crypto.x509;

import com.angkorteam.core.crypto.KeyFactory;
import com.angkorteam.core.crypto.PrivateKey;
import com.angkorteam.core.crypto.PublicKey;
import org.apache.commons.io.FileUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class X509KeyFactory implements KeyFactory {

    private static final String ALGORITHM = "RSA";

    static {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    private java.security.KeyFactory keyFactory;

    private X509KeyFactory(java.security.KeyFactory keyFactory) {
        this.keyFactory = keyFactory;
    }

    @Override
    public PrivateKey generatePrivate(String privateKey) {
        try {
            PKCS8EncodedKeySpec spek = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKey));
            java.security.PrivateKey key = this.keyFactory.generatePrivate(spek);
            return new X509PrivateKey(key);
        } catch (InvalidKeySpecException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public PrivateKey generatePrivate(File privateKey) {
        try {
            PKCS8EncodedKeySpec spek = new PKCS8EncodedKeySpec(FileUtils.readFileToByteArray(privateKey));
            java.security.PrivateKey key = this.keyFactory.generatePrivate(spek);
            return new X509PrivateKey(key);
        } catch (IOException | InvalidKeySpecException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public PublicKey generatePublic(String publicKey) {
        try {
            X509EncodedKeySpec spec = new X509EncodedKeySpec(Base64.getDecoder().decode(publicKey));
            java.security.PublicKey key = this.keyFactory.generatePublic(spec);
            return new X509PublicKey(key);
        } catch (InvalidKeySpecException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public PublicKey generatePublic(File publicKey) {
        try {
            X509EncodedKeySpec spec = new X509EncodedKeySpec(FileUtils.readFileToByteArray(publicKey));
            java.security.PublicKey key = this.keyFactory.generatePublic(spec);
            return new X509PublicKey(key);
        } catch (IOException | InvalidKeySpecException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public static KeyFactory getInstance() {
        try {
            return new X509KeyFactory(
                    java.security.KeyFactory.getInstance(ALGORITHM, BouncyCastleProvider.PROVIDER_NAME));
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new UnsupportedOperationException(e);
        }
    }

}
