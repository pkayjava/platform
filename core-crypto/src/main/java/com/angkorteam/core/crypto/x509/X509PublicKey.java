package com.angkorteam.core.crypto.x509;

import com.angkorteam.core.crypto.PublicKey;

import java.security.InvalidKeyException;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Base64;

public class X509PublicKey implements PublicKey {

    protected java.security.PublicKey publicKey;

    protected X509PublicKey(java.security.PublicKey publicKey) {
        this.publicKey = publicKey;
    }

    @Override
    public String getEncoded() {
        return Base64.getEncoder().encodeToString(this.publicKey.getEncoded());
    }

    @Override
    public boolean verify(String message, String signature) {
        try {
            Signature s = X509Signature.getInstance();
            s.initVerify(this.publicKey);
            s.update(message.getBytes());
            return s.verify(Base64.getDecoder().decode(signature));
        } catch (SignatureException | InvalidKeyException e) {
            return false;
        }
    }

    @Override
    public String encrypt(String message) {
        throw new UnsupportedOperationException("encrypt is not supported");
    }

}
