package com.angkorteam.core.crypto;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.BCPGOutputStream;
import org.bouncycastle.bcpg.CompressionAlgorithmTags;
import org.bouncycastle.bcpg.HashAlgorithmTags;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.*;
import org.bouncycastle.openpgp.jcajce.JcaPGPObjectFactory;
import org.bouncycastle.openpgp.operator.PBESecretKeyDecryptor;
import org.bouncycastle.openpgp.operator.PGPDigestCalculator;
import org.bouncycastle.openpgp.operator.PublicKeyDataDecryptorFactory;
import org.bouncycastle.openpgp.operator.jcajce.*;
import org.bouncycastle.util.io.Streams;

import java.io.*;
import java.security.*;
import java.util.Date;
import java.util.Iterator;

public class OpenPGP {

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    public static void issueKey(File publicKey, File secretKey, String secretPassPhrase, String identity)
            throws IOException, InvalidKeyException, NoSuchProviderException, SignatureException, PGPException,
            NoSuchAlgorithmException {
        OutputStream secretOut = FileUtils.openOutputStream(secretKey);
        secretOut = new ArmoredOutputStream(secretOut);
        java.security.KeyPairGenerator kpg = java.security.KeyPairGenerator.getInstance("RSA",
                BouncyCastleProvider.PROVIDER_NAME);
        kpg.initialize(1024);
        java.security.KeyPair pair = kpg.generateKeyPair();
        PGPDigestCalculator sha1Calc = new JcaPGPDigestCalculatorProviderBuilder().build().get(HashAlgorithmTags.SHA1);
        PGPKeyPair keyPair = new JcaPGPKeyPair(PGPPublicKey.RSA_GENERAL, pair, new Date());
        PGPSecretKey skey = new PGPSecretKey(PGPSignature.DEFAULT_CERTIFICATION, keyPair, identity, sha1Calc, null,
                null, new JcaPGPContentSignerBuilder(keyPair.getPublicKey().getAlgorithm(), HashAlgorithmTags.SHA1),
                new JcePBESecretKeyEncryptorBuilder(PGPEncryptedData.CAST5, sha1Calc)
                        .setProvider(BouncyCastleProvider.PROVIDER_NAME).build(secretPassPhrase.toCharArray()));
        skey.encode(secretOut);
        secretOut.close();
        OutputStream publicOut = FileUtils.openOutputStream(publicKey);
        publicOut = new ArmoredOutputStream(publicOut);
        PGPPublicKey key = skey.getPublicKey();
        key.encode(publicOut);
        publicOut.close();
    }

    public static File sign(byte[] secretKey, String secretPassPhase, File document)
            throws GeneralSecurityException, IOException, PGPException {
        return sign(loadSecretKey(secretKey), secretPassPhase, document);
    }

    public static File sign(File secretKey, String secretPassPhase, File document)
            throws GeneralSecurityException, IOException, PGPException {
        return sign(FileUtils.readFileToByteArray(secretKey), secretPassPhase, document);
    }

    public static File sign(PGPSecretKey secretKey, String secretPassPhase, File document)
            throws IOException, PGPException {
        File signatureFile = new File(FileUtils.getTempDirectory(), document.getName() + ".sig");
        FileUtils.deleteQuietly(signatureFile);
        PGPPrivateKey privateKey = loadPrivateKey(secretKey, secretPassPhase);
        try (OutputStream signatureOutputStream = FileUtils.openOutputStream(signatureFile)) {
            try (ArmoredOutputStream armored = new ArmoredOutputStream(signatureOutputStream)) {
                JcaPGPContentSignerBuilder builder = new JcaPGPContentSignerBuilder(
                        secretKey.getPublicKey().getAlgorithm(), PGPUtil.SHA256)
                        .setProvider(BouncyCastleProvider.PROVIDER_NAME);
                PGPSignatureGenerator sGen = new PGPSignatureGenerator(builder);
                sGen.init(PGPSignature.BINARY_DOCUMENT, privateKey);
                BCPGOutputStream bOut = new BCPGOutputStream(armored);
                try (InputStream fIn = FileUtils.openInputStream(document)) {
                    int ch;
                    while ((ch = fIn.read()) >= 0) {
                        sGen.update((byte) ch);
                    }
                }
                sGen.generate().encode(bOut);
            }
        }
        return signatureFile;
    }

    public static boolean verify(byte[] publicKey, PGPSignature signatureKey, File document)
            throws GeneralSecurityException, IOException, PGPException {
        return verify(loadPublicKey(publicKey), signatureKey, document);
    }

    public static boolean verify(File publicKey, PGPSignature signatureKey, File document)
            throws GeneralSecurityException, IOException, PGPException {
        return verify(loadPublicKey(publicKey), signatureKey, document);
    }

    public static boolean verify(byte[] publicKey, File signatureKey, File document)
            throws GeneralSecurityException, IOException, PGPException {
        return verify(loadPublicKey(publicKey), signatureKey, document);
    }

    public static boolean verify(PGPPublicKey publicKey, File signatureKey, File document)
            throws GeneralSecurityException, IOException, PGPException {
        return verify(publicKey, loadSignatureKey(signatureKey), document);
    }

    public static boolean verify(File publicKey, File signatureKey, File document)
            throws GeneralSecurityException, IOException, PGPException {
        return verify(FileUtils.readFileToByteArray(publicKey), loadSignatureKey(signatureKey), document);
    }

    public static boolean verify(PGPPublicKey publicKey, PGPSignature signature, File document)
            throws GeneralSecurityException, IOException, PGPException {
        JcaPGPContentVerifierBuilderProvider builder = new JcaPGPContentVerifierBuilderProvider();
        builder.setProvider(BouncyCastleProvider.PROVIDER_NAME);
        boolean verified;
        try (InputStream dIn = FileUtils.openInputStream(document)) {
            signature.init(builder, publicKey);
            int ch;
            while ((ch = dIn.read()) >= 0) {
                signature.update((byte) ch);
            }
            verified = signature.verify();
        }
        return verified;
    }

    public static File encrypt(byte[] publicKey, File document)
            throws IOException, NoSuchProviderException, PGPException {
        return encrypt(loadPublicKey(publicKey), document);
    }

    public static File encrypt(File publicKey, File document)
            throws IOException, NoSuchProviderException, PGPException {
        return encrypt(FileUtils.readFileToByteArray(publicKey), document);
    }

    public static File encrypt(PGPPublicKey publicKey, File document)
            throws IOException, NoSuchProviderException, PGPException {
        File encryptedDocument = new File(FileUtils.getTempDirectory(), document.getName() + ".asc");
        FileUtils.deleteQuietly(encryptedDocument);
        SecureRandom secureRandom = new SecureRandom();
        JcePGPDataEncryptorBuilder builder = new JcePGPDataEncryptorBuilder(PGPEncryptedData.CAST5);
        builder.setWithIntegrityPacket(true);
        builder.setSecureRandom(secureRandom);
        builder.setProvider(BouncyCastleProvider.PROVIDER_NAME);
        PGPEncryptedDataGenerator generator = new PGPEncryptedDataGenerator(builder);
        JcePublicKeyKeyEncryptionMethodGenerator methodGenerator = new JcePublicKeyKeyEncryptionMethodGenerator(
                publicKey);
        methodGenerator.setProvider(BouncyCastleProvider.PROVIDER_NAME);
        generator.addMethod(methodGenerator);

        ByteArrayOutputStream bOut = new ByteArrayOutputStream();
        PGPCompressedDataGenerator comData = new PGPCompressedDataGenerator(CompressionAlgorithmTags.ZIP);
        PGPUtil.writeFileToLiteralData(comData.open(bOut), PGPLiteralData.BINARY, document);
        comData.close();
        byte[] compressFile = bOut.toByteArray();

        try (FileOutputStream encrypted = FileUtils.openOutputStream(encryptedDocument)) {
            try (OutputStream out = new ArmoredOutputStream(encrypted)) {
                try (OutputStream cOut = generator.open(out, compressFile.length)) {
                    cOut.write(compressFile);
                }
            }
            return encryptedDocument;
        }
    }

    public static File decrypt(byte[] secretKey, String secretPassPhase, File document)
            throws IOException, NoSuchProviderException, PGPException {
        return decrypt(loadSecretKey(secretKey), secretPassPhase, document);
    }

    public static File decrypt(File secretKey, String secretPassPhase, File document)
            throws IOException, NoSuchProviderException, PGPException {
        return decrypt(FileUtils.readFileToByteArray(secretKey), secretPassPhase, document);
    }

    public static File decrypt(PGPSecretKey secretKey, String secretPassPhase, File document)
            throws IOException, NoSuchProviderException, PGPException {
        String baseName = FilenameUtils.getBaseName(document.getName());
        File tempDirectory = FileUtils.getTempDirectory();
        File decryptedDocument = new File(tempDirectory, baseName);
        FileUtils.deleteQuietly(decryptedDocument);
        PGPPrivateKey privateKey = loadPrivateKey(secretKey, secretPassPhase);
        if (privateKey == null) {
            throw new IllegalArgumentException("secret key for message not found.");
        }
        try (InputStream inputStream = FileUtils.openInputStream(document)) {
            try (InputStream decoderStream = PGPUtil.getDecoderStream(inputStream)) {
                PGPPublicKeyEncryptedData publicKeyEncryptedData;
                {
                    JcaPGPObjectFactory objectFactory = new JcaPGPObjectFactory(decoderStream);
                    Object object = objectFactory.nextObject();
                    PGPEncryptedDataList encryptedDataList;
                    if (object instanceof PGPEncryptedDataList) {
                        encryptedDataList = (PGPEncryptedDataList) object;
                    } else {
                        encryptedDataList = (PGPEncryptedDataList) objectFactory.nextObject();
                    }
                    Iterator it = encryptedDataList.getEncryptedDataObjects();
                    publicKeyEncryptedData = (PGPPublicKeyEncryptedData) it.next();
                }

                JcePublicKeyDataDecryptorFactoryBuilder builder = new JcePublicKeyDataDecryptorFactoryBuilder();
                builder.setProvider(BouncyCastleProvider.PROVIDER_NAME);
                PublicKeyDataDecryptorFactory decryptionFactory = builder.build(privateKey);
                InputStream clear = publicKeyEncryptedData.getDataStream(decryptionFactory);

                Object message;
                {
                    JcaPGPObjectFactory objectFactory = new JcaPGPObjectFactory(clear);
                    message = objectFactory.nextObject();
                }

                if (message instanceof PGPCompressedData) {
                    PGPCompressedData cData = (PGPCompressedData) message;
                    JcaPGPObjectFactory objectFactory = new JcaPGPObjectFactory(cData.getDataStream());
                    message = objectFactory.nextObject();
                }
                if (message instanceof PGPLiteralData) {
                    PGPLiteralData literalData = (PGPLiteralData) message;
                    try (InputStream unc = literalData.getInputStream()) {
                        try (OutputStream fOut = new BufferedOutputStream(new FileOutputStream(decryptedDocument))) {
                            Streams.pipeAll(unc, fOut);
                        }
                    }
                } else if (message instanceof PGPOnePassSignatureList) {
                    throw new PGPException("encrypted message contains a signed message - not literal data.");
                } else {
                    throw new PGPException("message is not a simple encrypted file - type unknown.");
                }
                if (publicKeyEncryptedData.isIntegrityProtected()) {
                    if (!publicKeyEncryptedData.verify()) {
                        System.err.println("message failed integrity check");
                    } else {
                        System.err.println("message integrity check passed");
                    }
                } else {
                    System.err.println("no message integrity check");
                }
            }
        }
        return decryptedDocument;
    }

    public static PGPPublicKey loadPublicKey(File publicKey) throws IOException, PGPException {
        byte[] publicKeyByte = FileUtils.readFileToByteArray(publicKey);
        return loadPublicKey(publicKeyByte);
    }

    public static PGPPublicKey loadPublicKey(byte[] publicKey) throws IOException, PGPException {
        try (InputStream publicKeyInputStream = new ByteArrayInputStream(publicKey)) {
            try (InputStream decoderStream = PGPUtil.getDecoderStream(publicKeyInputStream)) {
                JcaKeyFingerprintCalculator calculator = new JcaKeyFingerprintCalculator();
                PGPPublicKeyRingCollection publicKeyRingCollection = new PGPPublicKeyRingCollection(decoderStream,
                        calculator);
                Iterator<PGPPublicKeyRing> keyRings = publicKeyRingCollection.getKeyRings();
                while (keyRings.hasNext()) {
                    PGPPublicKeyRing publicKeyRing = keyRings.next();
                    Iterator<PGPPublicKey> publicKeys = publicKeyRing.getPublicKeys();
                    while (publicKeys.hasNext()) {
                        PGPPublicKey key = publicKeys.next();
                        if (key.isEncryptionKey()) {
                            return key;
                        }
                    }
                }
            }
        }
        throw new IllegalArgumentException("Can't find encryption key in key ring.");
    }

    public static PGPPrivateKey loadPrivateKey(File secretKey, String secretPassPhase)
            throws PGPException, IOException {
        return loadPrivateKey(loadSecretKey(secretKey), secretPassPhase);
    }

    public static PGPPrivateKey loadPrivateKey(PGPSecretKey secretKey, String secretPassPhase) throws PGPException {
        JcePBESecretKeyDecryptorBuilder builder = new JcePBESecretKeyDecryptorBuilder();
        builder.setProvider(BouncyCastleProvider.PROVIDER_NAME);
        PBESecretKeyDecryptor decryption = builder.build(secretPassPhase.toCharArray());
        return secretKey.extractPrivateKey(decryption);
    }

    public static PGPSecretKey loadSecretKey(File secretKey) throws IOException, PGPException {
        byte[] secretKeyByte = FileUtils.readFileToByteArray(secretKey);
        return loadSecretKey(secretKeyByte);
    }

    public static PGPSecretKey loadSecretKey(byte[] secretKey) throws IOException, PGPException {
        try (InputStream secretKeyInputStream = new ByteArrayInputStream(secretKey)) {
            try (InputStream decoderStream = PGPUtil.getDecoderStream(secretKeyInputStream)) {
                JcaKeyFingerprintCalculator calculator = new JcaKeyFingerprintCalculator();
                PGPSecretKeyRingCollection secretKeyRingCollection = new PGPSecretKeyRingCollection(decoderStream,
                        calculator);
                Iterator<PGPSecretKeyRing> keyRings = secretKeyRingCollection.getKeyRings();
                while (keyRings.hasNext()) {
                    PGPSecretKeyRing secretKeyRing = keyRings.next();
                    Iterator<PGPSecretKey> secretKeys = secretKeyRing.getSecretKeys();
                    while (secretKeys.hasNext()) {
                        PGPSecretKey key = secretKeys.next();
                        if (key.isSigningKey()) {
                            return key;
                        }
                    }
                }
            }
        }
        throw new IllegalArgumentException("Can't find signing key in key ring.");
    }

    public static PGPSignature loadSignatureKey(File signatureKey) throws IOException, PGPException {
        byte[] signatureKeyByte = FileUtils.readFileToByteArray(signatureKey);
        return loadSignatureKey(signatureKeyByte);
    }

    public static PGPSignature loadSignatureKey(byte[] signatureKey) throws IOException, PGPException {
        try (InputStream decoderStream = PGPUtil.getDecoderStream(new ByteArrayInputStream(signatureKey))) {
            JcaPGPObjectFactory objectFactory = new JcaPGPObjectFactory(decoderStream);
            Object object = objectFactory.nextObject();
            PGPSignatureList signatures;
            if (object instanceof PGPCompressedData) {
                PGPCompressedData c1 = (PGPCompressedData) object;
                objectFactory = new JcaPGPObjectFactory(c1.getDataStream());
                signatures = (PGPSignatureList) objectFactory.nextObject();
            } else {
                signatures = (PGPSignatureList) object;
            }
            return signatures.get(0);
        }
    }

}
