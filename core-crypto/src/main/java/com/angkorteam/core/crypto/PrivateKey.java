package com.angkorteam.core.crypto;

public interface PrivateKey extends Key {

    public String sign(String message);

    public String decrypt(String message);

}
