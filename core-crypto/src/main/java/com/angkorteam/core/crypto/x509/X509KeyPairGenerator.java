package com.angkorteam.core.crypto.x509;

import com.angkorteam.core.crypto.KeyPair;
import com.angkorteam.core.crypto.KeyPairGenerator;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Security;

public class X509KeyPairGenerator implements KeyPairGenerator {

    private static final String ALGORITHM = "RSA";

    static {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    private java.security.KeyPairGenerator generator;

    private X509KeyPairGenerator(java.security.KeyPairGenerator generator) {
        this.generator = generator;
    }

    @Override
    public void initialize(int keysize) {
        this.generator.initialize(keysize);
    }

    @Override
    public String getAlgorithm() {
        return this.generator.getAlgorithm();
    }

    @Override
    public Provider getProvider() {
        return this.generator.getProvider();
    }

    @Override
    public KeyPair generateKeyPair() {
        java.security.KeyPair pair = this.generator.generateKeyPair();
        X509PublicKey publicKey = new X509PublicKey(pair.getPublic());
        X509PrivateKey privateKey = new X509PrivateKey(pair.getPrivate());
        return new KeyPair(publicKey, privateKey);
    }

    public static KeyPairGenerator getInstance() {
        try {
            return new X509KeyPairGenerator(
                    java.security.KeyPairGenerator.getInstance(ALGORITHM, BouncyCastleProvider.PROVIDER_NAME));
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new UnsupportedOperationException(e);
        }
    }

}
