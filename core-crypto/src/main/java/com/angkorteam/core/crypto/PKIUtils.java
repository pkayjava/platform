package com.angkorteam.core.crypto;

import org.bouncycastle.asn1.DERBMPString;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.interfaces.PKCS12BagAttributeCarrier;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.jcajce.JcaMiscPEMGenerator;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.util.io.pem.PemWriter;

import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class PKIUtils {

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    public static Certificate createMasterCert(KeyPair key, BigInteger serial, Date notBefore, Date notAfter,
                                               String country, String locality, String organization, String organizationUnit, String state,
                                               String commonName, String emailAddress) throws Exception {

        X500NameBuilder issuerBuilder = new X500NameBuilder();
        X500NameBuilder subjectBuilder = new X500NameBuilder();
        if (country != null && !"".equals(country)) {
            issuerBuilder.addRDN(BCStyle.C, country);
            subjectBuilder.addRDN(BCStyle.C, country);
        }
        if (organization != null && !"".equals(organization)) {
            issuerBuilder.addRDN(BCStyle.O, organization);
            subjectBuilder.addRDN(BCStyle.O, organization);
        }
        if (organizationUnit != null && !"".equals(organizationUnit)) {
            issuerBuilder.addRDN(BCStyle.OU, organizationUnit);
            subjectBuilder.addRDN(BCStyle.OU, organizationUnit);
        }
        if (locality != null && !"".equals(locality)) {
            issuerBuilder.addRDN(BCStyle.L, locality);
            subjectBuilder.addRDN(BCStyle.L, locality);
        }
        if (state != null && !"".equals(state)) {
            issuerBuilder.addRDN(BCStyle.ST, state);
            subjectBuilder.addRDN(BCStyle.ST, state);
        }
        if (commonName != null && !"".equals(commonName)) {
            issuerBuilder.addRDN(BCStyle.CN, commonName);
            subjectBuilder.addRDN(BCStyle.CN, commonName);
        }
        if (emailAddress != null && !"".equals(emailAddress)) {
            issuerBuilder.addRDN(BCStyle.EmailAddress, emailAddress);
            subjectBuilder.addRDN(BCStyle.EmailAddress, emailAddress);
        }
        // signers name
        X500Name issuer = issuerBuilder.build();

        // subjects name - the same as we are self signed.
        X500Name subject = subjectBuilder.build();

        PublicKey publicKey = key.getPublic();

        PrivateKey privateKey = key.getPrivate();

        // create the certificate - version 3
        X509v3CertificateBuilder certificateBuilder = new JcaX509v3CertificateBuilder(issuer, serial, notBefore,
                notAfter, subject, publicKey);

        JcaContentSignerBuilder signerBuilder = new JcaContentSignerBuilder("SHA1WithRSA");
        signerBuilder.setProvider(BouncyCastleProvider.PROVIDER_NAME);
        ContentSigner signer = signerBuilder.build(privateKey);

        X509CertificateHolder certificateHolder = certificateBuilder.build(signer);

        JcaX509CertificateConverter certificateConverter = new JcaX509CertificateConverter();
        certificateConverter.setProvider(BouncyCastleProvider.PROVIDER_NAME);

        X509Certificate certificate = certificateConverter.getCertificate(certificateHolder);

        certificate.checkValidity(new Date());

        certificate.verify(key.getPublic());

        PKCS12BagAttributeCarrier attributeCarrier = (PKCS12BagAttributeCarrier) certificate;

        // this is actually optional - but if you want to have control
        // over setting the friendly name this is the way to do it...
        if (commonName != null && !"".equals(commonName)) {
            attributeCarrier.setBagAttribute(PKCSObjectIdentifiers.pkcs_9_at_friendlyName,
                    new DERBMPString(commonName));
        }

        return certificate;
    }

    public static void main(String[] arbc) throws Exception {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(2048);
        KeyPair key = generator.generateKeyPair();

        Certificate s = createMasterCert(key, BigInteger.valueOf(1),
                Date.from(LocalDate.now().minusYears(1).atStartOfDay(ZoneId.systemDefault()).toInstant()),
                Date.from(LocalDate.now().plusYears(1).atStartOfDay(ZoneId.systemDefault()).toInstant()), "KH",
                "Phnom Penh", "Angkor Team", "IT Technology", "Phnom Penh", "Angkor Team Root CA",
                "pkayjava@gmail.com");

        {
            OutputStreamWriter output = new OutputStreamWriter(new FileOutputStream("test.crt"));
            PemWriter pem = new PemWriter(output);

            JcaMiscPEMGenerator generators = new JcaMiscPEMGenerator(s);
            pem.writeObject(generators);
            pem.close();
        }
        {
            OutputStreamWriter output = new OutputStreamWriter(new FileOutputStream("key.crt"));
            PemWriter pem = new PemWriter(output);

            JcaMiscPEMGenerator generators = new JcaMiscPEMGenerator(key.getPrivate());
            pem.writeObject(generators);
            pem.close();
        }
        {
            OutputStreamWriter output = new OutputStreamWriter(new FileOutputStream("ssss.crt"));
            PemWriter pem = new PemWriter(output);

            JcaMiscPEMGenerator generators = new JcaMiscPEMGenerator(key.getPublic());
            pem.writeObject(generators);
            pem.close();
        }
    }
}
