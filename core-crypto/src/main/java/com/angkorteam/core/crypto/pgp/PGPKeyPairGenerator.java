package com.angkorteam.core.crypto.pgp;

import com.angkorteam.core.crypto.KeyPair;
import com.angkorteam.core.crypto.KeyPairGenerator;
import org.bouncycastle.bcpg.HashAlgorithmTags;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.*;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.operator.PGPDigestCalculator;
import org.bouncycastle.openpgp.operator.jcajce.*;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Security;
import java.util.Date;

public class PGPKeyPairGenerator implements KeyPairGenerator {

    private static final String ALGORITHM = "RSA";

    static {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }
    }

    private java.security.KeyPairGenerator generator;

    private final String id;

    private final char[] password;

    private PGPKeyPairGenerator(String id, String password, java.security.KeyPairGenerator generator) {
        this.generator = generator;
        this.id = id;
        this.password = password.toCharArray();
    }

    @Override
    public void initialize(int keysize) {
        this.generator.initialize(keysize);
    }

    @Override
    public String getAlgorithm() {
        return this.generator.getAlgorithm();
    }

    @Override
    public Provider getProvider() {
        return this.generator.getProvider();
    }

    @Override
    public KeyPair generateKeyPair() {
        try {
            PGPDigestCalculator calculator = new JcaPGPDigestCalculatorProviderBuilder().build()
                    .get(HashAlgorithmTags.SHA1);
            PGPKeyPair keyPair = new JcaPGPKeyPair(PGPPublicKey.RSA_GENERAL, this.generator.generateKeyPair(),
                    new Date());
            PGPSecretKey secretKey = new PGPSecretKey(PGPSignature.DEFAULT_CERTIFICATION, keyPair, this.id, calculator,
                    null, null,
                    new JcaPGPContentSignerBuilder(keyPair.getPublicKey().getAlgorithm(), HashAlgorithmTags.SHA1),
                    new JcePBESecretKeyEncryptorBuilder(PGPEncryptedData.CAST5, calculator)
                            .setProvider(BouncyCastleProvider.PROVIDER_NAME).build(this.password));
            com.angkorteam.core.crypto.pgp.PGPPublicKey publicKey = new com.angkorteam.core.crypto.pgp.PGPPublicKey(
                    secretKey.getPublicKey());
            com.angkorteam.core.crypto.pgp.PGPPrivateKey privateKey = new com.angkorteam.core.crypto.pgp.PGPPrivateKey(
                    secretKey.extractPrivateKey(new JcePBESecretKeyDecryptorBuilder()
                            .setProvider(BouncyCastleProvider.PROVIDER_NAME).build(this.password)),
                    secretKey);
            return new KeyPair(publicKey, privateKey);
        } catch (PGPException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public static KeyPairGenerator getInstance(String id, String password) {
        try {
            return new PGPKeyPairGenerator(id, password,
                    java.security.KeyPairGenerator.getInstance(ALGORITHM, BouncyCastleProvider.PROVIDER_NAME));
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new UnsupportedOperationException(e);
        }
    }
}