package com.angkorteam.cicd.ci.master.controller;

import com.angkorteam.cicd.ci.master.boot.AppProperties;
import com.angkorteam.cicd.ci.master.jenkin.JenkinClient;
import com.angkorteam.cicd.ci.master.json.Change;
import com.angkorteam.cicd.ci.master.json.CrumbIssuer;
import com.angkorteam.cicd.ci.master.json.Payload;
import com.angkorteam.cicd.ci.master.xml.*;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import com.google.gson.Gson;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@RestController
public class HookController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HookController.class);

    private static final String SIGNATURE = "X-Hub-Signature";
    private static final String REQUEST_ID = "X-Request-Id";

    private final AppProperties appProperties;

    private final HashFunction pipelineHash;

    private final Gson gson;

    private final Unmarshaller unmarshaller;

    private final Map<String, List<String>> job;

    private final JenkinClient jenkinClient;

    public HookController(JenkinClient jenkinClient, Unmarshaller unmarshaller, Gson gson, AppProperties appProperties)
            throws JAXBException {
        this.appProperties = appProperties;
        this.gson = gson;
        this.jenkinClient = jenkinClient;
        this.pipelineHash = Hashing.hmacSha256(appProperties.getSecret().getBytes());
        this.unmarshaller = unmarshaller;
        this.job = new ConcurrentHashMap<String, List<String>>();
    }

    protected JenkinClient getJenkinClient() {
        return this.jenkinClient;
    }

    protected HashFunction getPipelineHash() {
        return this.pipelineHash;
    }

    protected Gson getGson() {
        return this.gson;
    }

    protected Unmarshaller getUnmarshaller() {
        return this.unmarshaller;
    }

    protected Map<String, List<String>> getJob() {
        return this.job;
    }

    protected AppProperties getAppProperties() {
        return this.appProperties;
    }

    @RequestMapping(path = "/pipeline")
    public void pipeline(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String signature = request.getHeader(SIGNATURE);
        String requestId = request.getHeader(REQUEST_ID);

        if (requestId != null && !"".equals(requestId)) {
            LOGGER.info("Repostiory Pipeline Request ID [{}]", requestId);
        }

        if (signature == null || "".equals(signature)) {
            Map<String, Object> body = new HashMap<>();
            body.put("X-Request-Id", requestId);
            response.getWriter().write(getGson().toJson(body));
            return;
        } else {
            String sha = "sha256=";
            if (signature.startsWith(sha)) {
                signature = signature.substring(sha.length());
            }
            byte[] bytes = IOUtils.toByteArray(request.getInputStream());
            String hash = getPipelineHash().hashBytes(bytes).toString();
            if (!hash.equals(signature)) {
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            }

            File workspaceFile = new File(getAppProperties().getWorkspace());
            File scriptFile = new File(FileUtils.getTempDirectory(), System.currentTimeMillis() + ".sh");
            try {
                List<String> lines = new ArrayList<>();
                lines.add("#!/bin/sh");
                if (workspaceFile.exists()) {
                    lines.add("cd " + workspaceFile.getAbsolutePath());
                    lines.add("git pull");
                } else {
                    lines.add("git clone " + getAppProperties().getPipeline() + " " + workspaceFile.getAbsolutePath());
                }

                FileUtils.writeLines(scriptFile, lines);
                DefaultExecutor executor = new DefaultExecutor();
                executor.setWorkingDirectory(FileUtils.getTempDirectory());
                CommandLine command = CommandLine.parse(getAppProperties().getSh());
                command.addArgument(scriptFile.getAbsolutePath(), false);
                executor.execute(command);
            } finally {
                FileUtils.deleteQuietly(scriptFile);
            }
            Map<String, Object> body = new HashMap<>();
            body.put("X-Request-Id", requestId);
            response.getWriter().write(getGson().toJson(body));
        }
    }

    @RequestMapping(path = "/notification")
    public void notification(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String pipeline = req.getParameter("pipeline");
        if (pipeline == null || "".equals(pipeline)) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        String buildId = req.getParameter("buildId");
        if (buildId == null || "".equals(buildId)) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        if (buildId != null && !"".equals(buildId)) {
            LOGGER.info("Build [{}] is completed", buildId);
        }

        if (!getJob().containsKey(pipeline)) {
            getJob().put(pipeline, Collections.synchronizedList(new ArrayList<>(2)));
        }

        List<String> builds = getJob().get(pipeline);
        builds.remove(buildId);
    }

    @RequestMapping(path = "/webhook")
    public void webhook(HttpServletRequest req, HttpServletResponse resp) throws IOException, JAXBException {
        String pipelineName = req.getParameter("pipeline");
        if (pipelineName == null || "".equals(pipelineName)) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        File pipelineFile = new File(getAppProperties().getWorkspace(), pipelineName + ".xml");
        if (!pipelineFile.exists()) {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        Pipeline pipeline = (Pipeline) getUnmarshaller().unmarshal(pipelineFile);

        HashFunction hash = Hashing.hmacSha256(pipeline.getSecret().getBytes());

        String signature = req.getHeader(SIGNATURE);
        String requestId = req.getHeader(REQUEST_ID);

        if (signature == null || "".equals(signature)) {
            Map<String, Object> body = new HashMap<>();
            body.put("X-Request-Id", requestId);
            resp.getWriter().write(getGson().toJson(body));
            return;
        }

        if (signature.startsWith("sha256=")) {
            signature = signature.substring("sha256=".length());
        }
        byte[] bytes = IOUtils.toByteArray(req.getInputStream());
        String secret = hash.hashBytes(bytes).toString();
        if (!secret.equals(signature)) {
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }

        Payload payload = getGson().fromJson(IOUtils.toString(new ByteArrayInputStream(bytes), "UTF-8"), Payload.class);

        List<String> branches = new ArrayList<>();
        List<String> tags = new ArrayList<>();
        for (Change change : payload.getChanges()) {
            if ("BRANCH".equals(change.getRef().getType())) {
                if (!branches.contains(change.getRef().getDisplayId())) {
                    branches.add(change.getRef().getDisplayId());
                }
            } else if ("TAG".equals(change.getRef().getType())) {
                tags.add(change.getRef().getDisplayId());
            } else {
                LOGGER.info("{}", IOUtils.toString(bytes, "UTF-8"));
            }
        }

        CrumbIssuer crumbIssuer = getJenkinClient().findCrumbIssuer(pipeline.getJenkin());

        List<String> jobs = getJenkinClient().findJobs(pipeline.getJenkin());

        String pipelineReleaseName = pipelineName + "-release";

        buildReleaseJob(crumbIssuer, jobs, getJenkinClient(), pipelineReleaseName, pipeline);

        String pipelinePublishName = pipelineName + "-publish";

        buildPublishJob(crumbIssuer, jobs, getJenkinClient(), pipelinePublishName, pipeline);

        List<Map<String, Object>> infos = new ArrayList<>();
        for (String branch : branches) {
            String pipelineBranchName = pipelineName + "-" + branch;
            if (!getJob().containsKey(pipelineBranchName)) {
                getJob().put(pipelineBranchName, Collections.synchronizedList(new ArrayList<>(2)));
            }

            List<String> builds = getJob().get(pipelineBranchName);

            String buildId = UUID.randomUUID().toString();
            if (builds.size() > 2) {
                return;
            } else {
                builds.add(buildId);
            }

            if (requestId != null && !"".equals(requestId)) {
                LOGGER.info("Build [{}] is scheduled. Pipeline Name [{}] WebHook Id [{}]", buildId, pipelineName,
                        requestId);
            }

            buildBranchJob(crumbIssuer, jobs, getJenkinClient(), pipelineBranchName, pipeline);

            Map<String, Object> info = triggerBranchJob(getJenkinClient(), req, crumbIssuer, buildId,
                    pipelineBranchName, pipeline, branch);
            infos.add(info);

        }

        for (String tag : tags) {
            triggerPublishJob(getJenkinClient(), crumbIssuer, pipelinePublishName, pipeline, tag);
        }

        Map<String, Object> body = new HashMap<String, Object>();
        body.put("X-Request-Id", requestId);
        body.put("X-Build", infos);
        resp.getWriter().write(getGson().toJson(body));

    }

    protected Map<String, Object> triggerBranchJob(JenkinClient client, HttpServletRequest req, CrumbIssuer crumbIssuer,
                                                   String buildId, String jobName, Pipeline pipeline, String branchName)
            throws ClientProtocolException, IOException {

        String notificationUrl = getHttpAddress(req) + "/api/notification?buildId=" + buildId + "&pipeline=" + jobName;

        Map<String, String> params = new HashMap<>();
        params.put("branch", branchName);
        params.put("notification", notificationUrl);

        String location = client.triggerJob(pipeline.getJenkin(), crumbIssuer, jobName, pipeline.getAuthToken(),
                params);

        Map<String, Object> info = new HashMap<>();
        info.put("X-Branch-Id", branchName);
        info.put("X-Build-Id", buildId);
        info.put("X-Notification-Url", notificationUrl);
        if (location != null && !"".equals(location)) {
            info.put("X-Schedule-Id", location);
        }
        return info;
    }

    protected void triggerPublishJob(JenkinClient client, CrumbIssuer crumbIssuer, String jobName, Pipeline pipeline,
                                     String tagName) throws ClientProtocolException, IOException {
        Map<String, String> params = new HashMap<>();
        params.put("tag", tagName);
        client.triggerJob(pipeline.getJenkin(), crumbIssuer, jobName, pipeline.getAuthToken(), params);
    }

    protected CrumbIssuer lookupCrumbIssuer(String authorization, Pipeline pipeline, CloseableHttpClient client)
            throws ClientProtocolException, IOException {
        String uri = pipeline.getJenkin().getAddress() + "/crumbIssuer/api/json";
        HttpGet request = new HttpGet(uri);

        request.addHeader("Authorization", authorization);

        CrumbIssuer crumbIssuer = null;
        try (CloseableHttpResponse response = client.execute(request)) {
            crumbIssuer = getGson().fromJson(EntityUtils.toString(response.getEntity()), CrumbIssuer.class);
        }
        return crumbIssuer;
    }

    protected void buildPublishJob(CrumbIssuer crumbIssuer, List<String> jobs, JenkinClient client, String jobName,
                                   Pipeline pipeline) throws ClientProtocolException, IOException, JAXBException {

        boolean found = jobs.contains(jobName);

        Project project = buildProject(pipeline.getAuthToken(), pipeline.getDescription(),
                pipeline.getPublish().getCommand(), pipeline.getPublish().getArgs());

        if (found) {
            client.updateJob(pipeline.getJenkin(), crumbIssuer, jobName, project);
        } else {
            client.createJob(pipeline.getJenkin(), crumbIssuer, jobName, project);
        }
    }

    protected void buildReleaseJob(CrumbIssuer crumbIssuer, List<String> jobs, JenkinClient client, String jobName,
                                   Pipeline pipeline) throws ClientProtocolException, IOException, JAXBException {

        boolean found = jobs.contains(jobName);

        Project project = buildProject(pipeline.getAuthToken(), pipeline.getDescription(),
                pipeline.getRelease().getCommand(), pipeline.getRelease().getArgs());

        if (found) {
            client.updateJob(pipeline.getJenkin(), crumbIssuer, jobName, project);
        } else {
            client.createJob(pipeline.getJenkin(), crumbIssuer, jobName, project);
        }
    }

    protected void buildBranchJob(CrumbIssuer crumbIssuer, List<String> jobs, JenkinClient client, String jobName,
                                  Pipeline pipeline) throws ClientProtocolException, IOException, JAXBException {

        boolean found = jobs.contains(jobName);

        List<Param> params = new ArrayList<>();
        params.add(new Param("notification", "notification of completion", ""));
        params.add(new Param("branch", "branch bane to build", ""));
        Project project = buildProject(pipeline.getAuthToken(), pipeline.getDescription(),
                pipeline.getBuild().getCommand(), params);

        if (found) {
            client.updateJob(pipeline.getJenkin(), crumbIssuer, jobName, project);
        } else {
            client.createJob(pipeline.getJenkin(), crumbIssuer, jobName, project);
        }
    }

    protected Project buildProject(String authToken, String description, List<String> command) {
        return buildProject(authToken, description, command, Collections.emptyList());
    }

    protected Project buildProject(String authToken, String description, List<String> command, Args args) {
        return buildProject(authToken, description, command, args == null ? Collections.emptyList() : args.getParam());
    }

    protected Project buildProject(String authToken, String description, List<String> command, List<Param> params) {
        Project buildProject = new Project();
        buildProject.setAuthToken(authToken);
        buildProject.setDescription(description);
        buildProject.getBuilders().getShell().setCommand(StringUtils.join(command, IOUtils.LINE_SEPARATOR));
        if (params != null && !params.isEmpty()) {
            for (Param param : params) {
                StringParameterDefinition definition = new StringParameterDefinition();
                definition.setDefaultValue(param.getDefaultValue());
                definition.setDescription(param.getDescription());
                definition.setName(param.getName());
                definition.setTrim(true);
                buildProject.getProperties().getParametersDefinitionProperty().getParameterDefinitions()
                        .getStringParameterDefinition().add(definition);
            }
        } else {
            buildProject.setProperties(null);
        }
        return buildProject;
    }

    protected String getHttpAddress(HttpServletRequest request) {
        ServletContext servletContext = request.getServletContext();
        StringBuffer address = new StringBuffer();
        if (request.isSecure() && request.getServerPort() == 443) {
            address.append("https://").append(request.getServerName()).append(servletContext.getContextPath());
        } else if (!request.isSecure() && request.getServerPort() == 80) {
            address.append("http://").append(request.getServerName()).append(servletContext.getContextPath());
        } else {
            if (request.isSecure()) {
                address.append("https://");
            } else {
                address.append("http://");
            }
            address.append(request.getServerName()).append(":").append(request.getServerPort())
                    .append(servletContext.getContextPath());
        }
        String result = address.toString();
        if (result.endsWith("/")) {
            return result.substring(0, result.length() - 1);
        } else {
            return result;
        }
    }

}
