package com.angkorteam.cicd.ci.master.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "pipeline")
@XmlAccessorType(XmlAccessType.FIELD)
public class Pipeline {

    @XmlElement(name = "secret")
    private String secret;

    @XmlElement(name = "description")
    private String description;

    @XmlElement(name = "authToken")
    private String authToken;

    @XmlElement(name = "jenkin")
    private Jenkin jenkin;

    @XmlElement(name = "build")
    private Build build;

    @XmlElement(name = "release")
    private Release release;

    @XmlElement(name = "publish")
    private Publish publish;

    @XmlElement(name = "deploy")
    private Deploy deploy;

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Jenkin getJenkin() {
        return jenkin;
    }

    public void setJenkin(Jenkin jenkin) {
        this.jenkin = jenkin;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public Build getBuild() {
        return build;
    }

    public void setBuild(Build build) {
        this.build = build;
    }

    public Release getRelease() {
        return release;
    }

    public void setRelease(Release release) {
        this.release = release;
    }

    public Publish getPublish() {
        return publish;
    }

    public void setPublish(Publish publish) {
        this.publish = publish;
    }

    public Deploy getDeploy() {
        return deploy;
    }

    public void setDeploy(Deploy deploy) {
        this.deploy = deploy;
    }

}
