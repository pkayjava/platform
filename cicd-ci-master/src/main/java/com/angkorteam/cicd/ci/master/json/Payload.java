package com.angkorteam.cicd.ci.master.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class Payload {

    @Expose
    @SerializedName("eventKey")
    private String eventKey;

    @Expose
    @SerializedName("date")
    private Date date;

    @Expose
    @SerializedName("actor")
    private Actor actor;

    @Expose
    @SerializedName("repository")
    private Repository repository;

    @Expose
    @SerializedName("changes")
    private List<Change> changes;

    public String getEventKey() {
        return eventKey;
    }

    public void setEventKey(String eventKey) {
        this.eventKey = eventKey;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Actor getActor() {
        return actor;
    }

    public void setActor(Actor actor) {
        this.actor = actor;
    }

    public Repository getRepository() {
        return repository;
    }

    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    public List<Change> getChanges() {
        return changes;
    }

    public void setChanges(List<Change> changes) {
        this.changes = changes;
    }

}
