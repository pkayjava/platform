package com.angkorteam.cicd.ci.master.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
public class ParameterDefinitions {

    @XmlElement(name = "hudson.model.StringParameterDefinition")
    private List<StringParameterDefinition> stringParameterDefinition = new ArrayList<StringParameterDefinition>();

    public List<StringParameterDefinition> getStringParameterDefinition() {
        return stringParameterDefinition;
    }

    public void setStringParameterDefinition(List<StringParameterDefinition> stringParameterDefinition) {
        this.stringParameterDefinition = stringParameterDefinition;
    }

}
