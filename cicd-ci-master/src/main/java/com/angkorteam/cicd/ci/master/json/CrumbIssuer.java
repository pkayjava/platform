package com.angkorteam.cicd.ci.master.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CrumbIssuer {

    @Expose
    @SerializedName("crumb")
    private String crumb;

    @Expose
    @SerializedName("crumbRequestField")
    private String crumbRequestField;

    public String getCrumb() {
        return crumb;
    }

    public void setCrumb(String crumb) {
        this.crumb = crumb;
    }

    public String getCrumbRequestField() {
        return crumbRequestField;
    }

    public void setCrumbRequestField(String crumbRequestField) {
        this.crumbRequestField = crumbRequestField;
    }

}
