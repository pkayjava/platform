package com.angkorteam.cicd.ci.master.jenkin;

import com.angkorteam.cicd.ci.master.json.CrumbIssuer;
import com.angkorteam.cicd.ci.master.json.Job;
import com.angkorteam.cicd.ci.master.json.JobResponse;
import com.angkorteam.cicd.ci.master.xml.Jenkin;
import com.angkorteam.cicd.ci.master.xml.Project;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class JenkinClient {

    private final CloseableHttpClient client;

    private final Gson gson;

    private final Marshaller marshaller;

    public JenkinClient(Marshaller marshaller, Gson gson, CloseableHttpClient client) {
        this.client = client;
        this.gson = gson;
        this.marshaller = marshaller;
    }

    protected Marshaller getMarshaller() {
        return this.marshaller;
    }

    protected Gson getGson() {
        return this.gson;
    }

    protected CloseableHttpClient getClient() {
        return this.client;
    }

    protected Header authorizationHeader(Jenkin jenkin) {
        String secret = jenkin.getLogin() + ":" + jenkin.getPassword();
        String authorization = "Basic " + java.util.Base64.getEncoder().encodeToString(secret.getBytes());
        return new BasicHeader("Authorization", authorization);
    }

    public CrumbIssuer findCrumbIssuer(Jenkin jenkin) throws ClientProtocolException, IOException {
        String uri = jenkin.getAddress() + "/crumbIssuer/api/json";
        HttpGet request = new HttpGet(uri);
        request.addHeader(authorizationHeader(jenkin));
        CrumbIssuer crumbIssuer = null;
        try (CloseableHttpResponse response = getClient().execute(request)) {
            crumbIssuer = getGson().fromJson(EntityUtils.toString(response.getEntity()), CrumbIssuer.class);
        }
        return crumbIssuer;
    }

    public void updateJob(Jenkin jenkin, CrumbIssuer crumbIssuer, String jobName, Project project)
            throws ClientProtocolException, IOException, JAXBException {
        StringWriter content = new StringWriter();
        getMarshaller().marshal(project, content);
        String uri = jenkin.getAddress() + "/job/" + jobName + "/config.xml";
        HttpPost request = new HttpPost(uri);
        request.addHeader(authorizationHeader(jenkin));
        request.addHeader("Content-Type", "application/xml");
        request.addHeader(crumbIssuer.getCrumbRequestField(), crumbIssuer.getCrumb());
        ((HttpPost) request).setEntity(new StringEntity(content.toString(), "UTF-8"));
        try (CloseableHttpResponse response = getClient().execute(request)) {
            EntityUtils.consume(response.getEntity());
        }
    }

    public void createJob(Jenkin jenkin, CrumbIssuer crumbIssuer, String jobName, Project project)
            throws ClientProtocolException, IOException, JAXBException {
        StringWriter content = new StringWriter();
        getMarshaller().marshal(project, content);
        String uri = jenkin.getAddress() + "/createItem?name=" + jobName;
        HttpPost request = new HttpPost(uri);
        request.addHeader(authorizationHeader(jenkin));
        request.addHeader("Content-Type", "application/xml");
        request.addHeader(crumbIssuer.getCrumbRequestField(), crumbIssuer.getCrumb());
        ((HttpPost) request).setEntity(new StringEntity(content.toString(), "UTF-8"));
        try (CloseableHttpResponse response = getClient().execute(request)) {
            EntityUtils.consume(response.getEntity());
        }
    }

    public List<String> findJobs(Jenkin jenkin) throws JsonSyntaxException, ParseException, IOException {
        String uri = jenkin.getAddress() + "/api/json";
        HttpGet request = new HttpGet(uri);

        request.addHeader(authorizationHeader(jenkin));

        JobResponse jobResponse = null;
        try (CloseableHttpResponse response = getClient().execute(request)) {
            jobResponse = getGson().fromJson(EntityUtils.toString(response.getEntity()), JobResponse.class);
        }

        List<String> jobs = new ArrayList<>();
        if (jobResponse != null && jobResponse.getJobs() != null && !jobResponse.getJobs().isEmpty()) {
            for (Job job : jobResponse.getJobs()) {
                if (!jobs.contains(job.getName())) {
                    jobs.add(job.getName());
                }
            }
        }
        return jobs;
    }

    public String triggerJob(Jenkin jenkin, CrumbIssuer crumbIssuer, String jobName, String authToken)
            throws ClientProtocolException, IOException {
        return triggerJob(jenkin, crumbIssuer, jobName, authToken, null);
    }

    public String triggerJob(Jenkin jenkin, CrumbIssuer crumbIssuer, String jobName, String authToken,
                             Map<String, String> params) throws ClientProtocolException, IOException {
        String uri = null;
        if (params == null || params.isEmpty()) {
            uri = jenkin.getAddress() + "/job/" + jobName + "/build";
        } else {
            uri = jenkin.getAddress() + "/job/" + jobName + "/buildWithParameters";
        }
        HttpPost request = new HttpPost(uri);

        request.addHeader(authorizationHeader(jenkin));
        request.addHeader(crumbIssuer.getCrumbRequestField(), crumbIssuer.getCrumb());

        List<NameValuePair> parameters = new ArrayList<>();
        parameters.add(new BasicNameValuePair("token", authToken));
        if (params != null && !params.isEmpty()) {
            for (Entry<String, String> param : params.entrySet()) {
                parameters.add(new BasicNameValuePair(param.getKey(), param.getValue()));
            }
        }

        request.setEntity(new UrlEncodedFormEntity(parameters));

        Header location = null;
        try (CloseableHttpResponse response = client.execute(request)) {
            EntityUtils.consume(response.getEntity());
            location = response.getFirstHeader("Location");
        }

        if (location != null) {
            return location.getValue();
        }
        return null;
    }

}
