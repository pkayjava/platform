package com.angkorteam.cicd.ci.master;

import com.angkorteam.cicd.ci.master.boot.AppProperties;
import com.angkorteam.cicd.ci.master.jenkin.JenkinClient;
import com.angkorteam.cicd.ci.master.xml.Pipeline;
import com.angkorteam.cicd.ci.master.xml.Project;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

@SpringBootApplication
public class BootApplication {

    public static void main(String[] args) {
        SpringApplication.run(BootApplication.class, args);
    }

    @Bean
    public Gson createGson(AppProperties cibuildProperties) {
        Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation()
                .setDateFormat(cibuildProperties.getDatetime()).create();
        return gson;
    }

    @Bean
    public CloseableHttpClient createCloseableHttpClient() {
        return HttpClientBuilder.create().build();
    }

    @Bean
    public JenkinClient createJenkinClient(Marshaller marshaller, Gson gson, CloseableHttpClient client) {
        return new JenkinClient(marshaller, gson, client);
    }

    @Bean
    public JAXBContext createJAXBContext() throws JAXBException {
        return JAXBContext.newInstance(Pipeline.class, Project.class);
    }

    @Bean
    public Unmarshaller createUnmarshaller(JAXBContext context) throws JAXBException {
        return context.createUnmarshaller();
    }

    @Bean
    public Marshaller createMarshaller(JAXBContext context) throws JAXBException {
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        return marshaller;
    }

}
