package com.angkorteam.cicd.ci.master.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "project")
@XmlAccessorType(XmlAccessType.FIELD)
public class Project {

    @XmlElement(name = "description")
    private String description;

    @XmlElement(name = "keepDependencies")
    private boolean keepDependencies = false;

    @XmlElement(name = "properties")
    private Properties properties = new Properties();

    @XmlElement(name = "scm")
    private Scm scm = new Scm();

    @XmlElement(name = "canRoam")
    private boolean canRoam = true;

    @XmlElement(name = "disabled")
    private boolean disabled = false;

    @XmlElement(name = "blockBuildWhenDownstreamBuilding")
    private boolean blockBuildWhenDownstreamBuilding = false;

    @XmlElement(name = "blockBuildWhenDownstreamBuilding")
    private boolean blockBuildWhenUpstreamBuilding = false;

    @XmlElement(name = "authToken")
    private String authToken;

    @XmlElement(name = "concurrentBuild")
    private boolean concurrentBuild = false;

    @XmlElement(name = "builders")
    private Builders builders = new Builders();

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isKeepDependencies() {
        return keepDependencies;
    }

    public void setKeepDependencies(boolean keepDependencies) {
        this.keepDependencies = keepDependencies;
    }

    public Scm getScm() {
        return scm;
    }

    public void setScm(Scm scm) {
        this.scm = scm;
    }

    public boolean isCanRoam() {
        return canRoam;
    }

    public void setCanRoam(boolean canRoam) {
        this.canRoam = canRoam;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public boolean isBlockBuildWhenDownstreamBuilding() {
        return blockBuildWhenDownstreamBuilding;
    }

    public void setBlockBuildWhenDownstreamBuilding(boolean blockBuildWhenDownstreamBuilding) {
        this.blockBuildWhenDownstreamBuilding = blockBuildWhenDownstreamBuilding;
    }

    public boolean isBlockBuildWhenUpstreamBuilding() {
        return blockBuildWhenUpstreamBuilding;
    }

    public void setBlockBuildWhenUpstreamBuilding(boolean blockBuildWhenUpstreamBuilding) {
        this.blockBuildWhenUpstreamBuilding = blockBuildWhenUpstreamBuilding;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public boolean isConcurrentBuild() {
        return concurrentBuild;
    }

    public void setConcurrentBuild(boolean concurrentBuild) {
        this.concurrentBuild = concurrentBuild;
    }

    public Builders getBuilders() {
        return builders;
    }

    public void setBuilders(Builders builders) {
        this.builders = builders;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

}
