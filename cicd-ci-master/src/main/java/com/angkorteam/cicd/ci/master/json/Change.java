package com.angkorteam.cicd.ci.master.json;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Change {

    @Expose
    @SerializedName("refId")
    private String refId;

    @Expose
    @SerializedName("fromHash")
    private String fromHash;

    @Expose
    @SerializedName("toHash")
    private String toHash;

    @Expose
    @SerializedName("type")
    private String type;

    @Expose
    @SerializedName("ref")
    private Ref ref;

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getFromHash() {
        return fromHash;
    }

    public void setFromHash(String fromHash) {
        this.fromHash = fromHash;
    }

    public String getToHash() {
        return toHash;
    }

    public void setToHash(String toHash) {
        this.toHash = toHash;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Ref getRef() {
        return ref;
    }

    public void setRef(Ref ref) {
        this.ref = ref;
    }

}
