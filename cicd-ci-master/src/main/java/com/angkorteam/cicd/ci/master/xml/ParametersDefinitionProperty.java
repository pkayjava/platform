package com.angkorteam.cicd.ci.master.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class ParametersDefinitionProperty {

    @XmlElement(name = "parameterDefinitions")
    private ParameterDefinitions parameterDefinitions = new ParameterDefinitions();

    public ParameterDefinitions getParameterDefinitions() {
        return parameterDefinitions;
    }

    public void setParameterDefinitions(ParameterDefinitions parameterDefinitions) {
        this.parameterDefinitions = parameterDefinitions;
    }

}
