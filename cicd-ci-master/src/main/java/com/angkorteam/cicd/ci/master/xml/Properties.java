package com.angkorteam.cicd.ci.master.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class Properties {

    @XmlElement(name = "hudson.model.ParametersDefinitionProperty")
    private ParametersDefinitionProperty parametersDefinitionProperty = new ParametersDefinitionProperty();

    public ParametersDefinitionProperty getParametersDefinitionProperty() {
        return parametersDefinitionProperty;
    }

    public void setParametersDefinitionProperty(ParametersDefinitionProperty parametersDefinitionProperty) {
        this.parametersDefinitionProperty = parametersDefinitionProperty;
    }

}
