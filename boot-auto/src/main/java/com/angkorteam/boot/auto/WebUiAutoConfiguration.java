package com.angkorteam.boot.auto;

import com.angkorteam.core.webui.WebUiProperties;
import org.apache.wicket.protocol.http.WicketFilter;
import org.apache.wicket.protocol.http.WicketServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication.Type;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnWebApplication(type = Type.SERVLET)
@AutoConfigureAfter({WebMvcAutoConfiguration.class})
public class WebUiAutoConfiguration {

    @ConditionalOnClass(com.angkorteam.boot.webui.WicketFactory.class)
    @EnableConfigurationProperties({WebUiProperties.class, WebMvcProperties.class})
    static class WebUi {

        private static final Logger LOGGER = LoggerFactory.getLogger(WebUiAutoConfiguration.class);

        private final WebMvcProperties webMvcProperties;

        private final WebUiProperties webUiProperties;

        public WebUi(WebUiProperties webUiProperties, WebMvcProperties webMvcProperties) {
            this.webUiProperties = webUiProperties;
            this.webMvcProperties = webMvcProperties;
        }

        @Bean
        @ConditionalOnMissingBean(WicketServlet.class)
        public ServletRegistrationBean<WicketServlet> createWicketServlet() {
            LOGGER.info("spring.mvc.servlet.loadOnStartup {}", this.webMvcProperties.getServlet().getLoadOnStartup());
            LOGGER.info("spring.mvc.servlet.path {}", this.webMvcProperties.getServlet().getPath());
            WicketServlet servlet = new WicketServlet();
            ServletRegistrationBean<WicketServlet> bean = new ServletRegistrationBean<>(servlet,
                    this.webUiProperties.getServlet().getPath());
            bean.setName(WicketServlet.class.getName());
            bean.setLoadOnStartup(this.webMvcProperties.getServlet().getLoadOnStartup() + 1);
            bean.addInitParameter(WicketFilter.APP_FACT_PARAM, com.angkorteam.boot.webui.WicketFactory.class.getName());
            bean.addInitParameter("filterMappingUrlPattern", this.webUiProperties.getServlet().getPath());
            bean.addInitParameter("ignorePaths", this.webMvcProperties.getServlet().getPath());
            return bean;
        }

    }

    @ConditionalOnClass(com.angkorteam.boot.webui.authentication.WicketFactory.class)
    @EnableConfigurationProperties({WebUiProperties.class, WebMvcProperties.class})
    static class WebUiAuthentication {

        private static final Logger LOGGER = LoggerFactory.getLogger(WebUiAutoConfiguration.class);

        private final WebMvcProperties webMvcProperties;

        private final WebUiProperties webUiProperties;

        public WebUiAuthentication(WebUiProperties webUiProperties, WebMvcProperties webMvcProperties) {
            this.webUiProperties = webUiProperties;
            this.webMvcProperties = webMvcProperties;
        }

        @Bean
        @ConditionalOnMissingBean(WicketServlet.class)
        public ServletRegistrationBean<WicketServlet> createAuthenticationWicketServlet() {
            LOGGER.info("spring.mvc.servlet.loadOnStartup {}", this.webMvcProperties.getServlet().getLoadOnStartup());
            LOGGER.info("spring.mvc.servlet.path {}", this.webMvcProperties.getServlet().getPath());
            WicketServlet servlet = new WicketServlet();
            ServletRegistrationBean<WicketServlet> bean = new ServletRegistrationBean<>(servlet,
                    this.webUiProperties.getServlet().getPath());
            bean.setName(WicketServlet.class.getName());
            bean.setLoadOnStartup(this.webMvcProperties.getServlet().getLoadOnStartup() + 1);
            bean.addInitParameter(WicketFilter.APP_FACT_PARAM,
                    com.angkorteam.boot.webui.authentication.WicketFactory.class.getName());
            bean.addInitParameter("filterMappingUrlPattern", this.webUiProperties.getServlet().getPath());
            bean.addInitParameter("ignorePaths", this.webMvcProperties.getServlet().getPath());
            return bean;
        }

    }

}
