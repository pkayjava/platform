package com.angkorteam.pki.openssl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PkiOpensslApplication {

    public static void main(String[] args) {
        SpringApplication.run(PkiOpensslApplication.class, args);
    }
}
