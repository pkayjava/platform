# PKI OpenSSL

Public Key Infrastructure - OpenSSL

it is http-cli for openssl which are running inside docker 

## Install Docker

```
#!bash
sudo apt-get remove docker docker-engine docker.io
sudo apt-get update
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce
sudo usermod -aG docker `whoami`
```

## Provision Docker Instance

```
#!bash
docker run -it -d --name pki-openssl ubuntu:18.10 /bin/sh
docker exec -it pki-openssl /bin/bash
```

## Compile OpenSSL

```
#!bash
cd
apt-get update
apt-get install aptitude
aptitude full-upgrade
apt-get install openssh-client zip unzip rar unrar p7zip wget gnupg nano lsb-release iputils-ping net-tools build-essential
wget https://www.openssl.org/source/openssl-1.1.1.tar.gz
tar -zxvf openssl-1.1.1.tar.gz
cd openssl-1.1.1
./config no-shared
./Configure
make
make install
echo "LD_LIBRARY_PATH=/usr/local/lib" >> /etc/environment
export LD_LIBRARY_PATH=/usr/local/lib
ldconfig
cd /usr/local/bin
zip openssl.zip openssl
```
# copy openssl.zip outside, can remove docker instance

## Compile Project

## Start Project

## Remove Docker Instance

```
#!bash
docker stop pki-openssl
docker rm pki-openssl
```