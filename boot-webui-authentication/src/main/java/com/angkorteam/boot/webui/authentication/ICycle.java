package com.angkorteam.boot.webui.authentication;

import com.angkorteam.core.webui.WebUiProperties;
import org.apache.wicket.Application;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.http.WebRequest;
import org.apache.wicket.util.io.IClusterable;

public interface ICycle extends IClusterable {

    default void onInitializedApplication(WebUiProperties webUiProperties, AuthenticatedWebApplication application) {
    }

    default void onDestroyedApplication(WebUiProperties webUiProperties, AuthenticatedWebApplication application) {
    }

    default void onInitializedSession(WebUiProperties webUiProperties, WebSession session) {
    }

    default void onDestroyedSession(WebUiProperties webUiProperties, String sessionId) {
    }

    Class<? extends WebPage> getHomePageClass(WebUiProperties webUiProperties, WebSession session);

    Class<? extends WebPage> getLoginPageClass(WebUiProperties webUiProperties, WebSession session);

    Roles authenticateSession(WebUiProperties webUiProperties, WebRequest request, String username, String password);

    default void onLogout(WebUiProperties webUiProperties, RequestCycle requestCycle, WebSession session) {
        requestCycle.setResponsePage(Application.get().getHomePage());
        System.out.println("test");
    }

}
