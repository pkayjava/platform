package com.angkorteam.boot.webui.authentication;

import com.angkorteam.core.webui.WebUiProperties;
import org.apache.wicket.Session;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.http.WebRequest;

public class WebSession extends AuthenticatedWebSession {

    /**
     *
     */
    private static final long serialVersionUID = 254092450549419323L;

    private WebRequest request;

    private WebUiProperties webUiProperties;

    private ICycle cycle;

    private Roles roles;

    public WebSession(WebUiProperties webUiProperties, Request request, ICycle cycle) {
        super(request);
        this.request = (WebRequest) request;
        this.cycle = cycle;
        this.webUiProperties = webUiProperties;
    }

    @Override
    public Roles getRoles() {
        return this.roles;
    }

    @Override
    protected boolean authenticate(String username, String password) {
        Roles roles = getCycle().authenticateSession(getWebUiProperties(), getRequest(), username, password);
        this.roles = roles;
        return roles != null;
    }

    public static WebSession get() {
        return (WebSession) Session.get();
    }

    protected WebRequest getRequest() {
        return this.request;
    }

    protected WebUiProperties getWebUiProperties() {
        return this.webUiProperties;
    }

    protected ICycle getCycle() {
        return this.cycle;
    }

    @Override
    public void signOut() {
        super.signOut();
        getCycle().onLogout(getWebUiProperties(), RequestCycle.get(), this);
    }
}
