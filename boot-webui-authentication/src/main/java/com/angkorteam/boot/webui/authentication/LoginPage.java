package com.angkorteam.boot.webui.authentication;

import com.angkorteam.core.function.VersionExtension;
import com.angkorteam.core.webui.AdminLTEOption;
import com.angkorteam.core.webui.ReferenceUtilities;
import com.angkorteam.core.webui.wicket.markup.html.form.Button;
import com.angkorteam.core.webui.wicket.markup.html.form.Form;
import com.angkorteam.core.webui.wicket.widget.ReadOnlyView;
import com.angkorteam.core.webui.wicket.widget.TextFeedbackPanel;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.protocol.http.WebApplication;

import java.io.File;

public class LoginPage extends WebPage {

    /**
     *
     */
    private static final long serialVersionUID = -6918736909012882008L;

    private TextField<String> loginField;
    private String loginValue;

    private TextField<String> passwordField;
    private String passwordValue;

    protected String versionValue;
    protected ReadOnlyView versionView;

    private Form<Void> form;
    private Button loginButton;

    @Override
    protected void onInitialize() {
        super.onInitialize();
        this.form = new Form<>("form");
        this.add(this.form);

        String version = VersionExtension.getVersionManifest(WebApplication.get().getServletContext());
        if (version == null) {
            version = VersionExtension.getVersionPom(new File("pom.xml"));
        }
        this.versionValue = "VERSION : " + version;

        this.loginField = new TextField<>("loginField", new PropertyModel<>(this, "loginValue"));
        this.loginField.setLabel(Model.of("login"));
        this.loginField.setRequired(true);
        this.form.add(this.loginField);
        TextFeedbackPanel loginFeedback = new TextFeedbackPanel("loginFeedback", this.loginField);
        this.form.add(loginFeedback);

        this.passwordField = new PasswordTextField("passwordField", new PropertyModel<>(this, "passwordValue"));
        this.passwordField.setRequired(true);
        this.passwordField.setLabel(Model.of("password"));
        this.form.add(this.passwordField);
        TextFeedbackPanel passwordFeedback = new TextFeedbackPanel("passwordFeedback", this.passwordField);
        this.form.add(passwordFeedback);

        this.loginButton = new Button("loginButton");
        this.form.add(this.loginButton);
        this.loginButton.setOnSubmit(this::loginButtonClick);

        this.versionView = new ReadOnlyView("versionView", new PropertyModel<String>(this, "versionValue"));
        add(this.versionView);

    }

    protected void loginButtonClick(Button button) {
        boolean signIn = getSession().signIn(this.loginValue, this.passwordValue);
        if (signIn) {
            setResponsePage(getApplication().getHomePage());
        }
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);
        AdminLTEOption option = new AdminLTEOption();
        option.Bootstrap = true;
        option.FontAwesome = true;
        option.Ionicons = true;
        option.AdminLTE = true;
        option.ICheck = true;
        option.HTML5ShimAndRespondJS = true;
        option.GoogleFont = true;
        option.JQuery = true;
        ReferenceUtilities.render(option, response);
    }

    @Override
    public WebSession getSession() {
        return (WebSession) super.getSession();
    }
}