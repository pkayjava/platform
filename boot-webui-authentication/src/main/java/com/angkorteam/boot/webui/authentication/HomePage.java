package com.angkorteam.boot.webui.authentication;

import com.angkorteam.core.webui.Emoji;
import com.angkorteam.core.webui.models.*;
import com.angkorteam.core.webui.wicket.DashboardPage;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;

import java.util.ArrayList;
import java.util.List;

@AuthorizeInstantiation(value = {"ADMIN"})
public class HomePage extends DashboardPage {

    /**
     *
     */
    private static final long serialVersionUID = 2044769273333050131L;

    @Override
    protected void initData() {
    }

    @Override
    protected void initComponent() {
    }

    @Override
    protected void configureMetaData() {
    }

    @Override
    protected IModel<PageLogo> buildPageLogo() {
        return null;
    }

    @Override
    protected IModel<PageFooter> buildPageFooter() {
        return null;
    }

    @Override
    protected IModel<PageHeader> buildPageHeader() {
        return null;
    }

    @Override
    protected IModel<List<PageBreadcrumb>> buildPageBreadcrumb() {
        return null;
    }

    @Override
    protected IModel<List<SideMenu>> buildSideMenu() {
        return null;
    }

    @Override
    protected IModel<List<NavBarMenu>> buildNavBarMenu() {
        NavBarMenu logoutMenu = new NavBarMenu.Builder().withIcon(Emoji.ion_log_out, "Logout", LogoutPage.class)
                .build();
        List<NavBarMenu> menus = new ArrayList<>();
        menus.add(logoutMenu);
        return Model.ofList(menus);
    }

    @Override
    protected IModel<UserInfo> buildUserInfo() {
        return null;
    }

    @Override
    protected IModel<Boolean> hasSearchForm() {
        return null;
    }

    @Override
    protected void onSearchClick(String searchValue) {
    }

}
