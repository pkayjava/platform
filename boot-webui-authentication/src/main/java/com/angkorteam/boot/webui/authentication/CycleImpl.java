package com.angkorteam.boot.webui.authentication;

import com.angkorteam.core.webui.WebUiProperties;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.http.WebRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CycleImpl implements ICycle {

    /**
     *
     */
    private static final long serialVersionUID = 8857383030611740807L;

    private static final Logger LOGGER = LoggerFactory.getLogger(CycleImpl.class);

    @Override
    public void onInitializedApplication(WebUiProperties webUiProperties, AuthenticatedWebApplication application) {
        application.mountPage("/login", LoginPage.class);
        application.mountPage("/logout", LogoutPage.class);
    }

    @Override
    public Class<? extends WebPage> getLoginPageClass(WebUiProperties webUiProperties, WebSession session) {
        return LoginPage.class;
    }

    @Override
    public Class<? extends WebPage> getHomePageClass(WebUiProperties webUiProperties, WebSession session) {
        return HomePage.class;
    }

    @Override
    public Roles authenticateSession(WebUiProperties webUiProperties, WebRequest request, String username,
                                     String password) {
        Roles role = new Roles();
        role.add("ADMIN");
        return role;
    }

}
