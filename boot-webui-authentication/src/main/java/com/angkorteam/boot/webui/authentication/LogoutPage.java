package com.angkorteam.boot.webui.authentication;

import org.apache.wicket.markup.html.WebPage;

public class LogoutPage extends WebPage {

    @Override
    protected void onInitialize() {
        super.onInitialize();
        getSession().signOut();
    }

    @Override
    public WebSession getSession() {
        return (WebSession) super.getSession();
    }

}