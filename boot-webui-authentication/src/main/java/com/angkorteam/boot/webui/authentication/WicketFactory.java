package com.angkorteam.boot.webui.authentication;

import com.angkorteam.core.webui.ReferenceUtilities;
import com.angkorteam.core.webui.ResourceScope;
import com.angkorteam.core.webui.WebUiProperties;
import org.apache.wicket.*;
import org.apache.wicket.authroles.authentication.AbstractAuthenticatedWebSession;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.markup.html.IPackageResourceGuard;
import org.apache.wicket.markup.html.SecurePackageResourceGuard;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.IWebApplicationFactory;
import org.apache.wicket.protocol.http.WicketFilter;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.Response;
import org.apache.wicket.request.resource.PackageResourceReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class WicketFactory extends AuthenticatedWebApplication
        implements IWebApplicationFactory, ISessionListener, IApplicationListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(WicketFactory.class);

    private transient static ApplicationContext applicationContext;

    private ICycle cycle;

    private WebUiProperties webUiProperties;

    public WicketFactory() {
    }

    @Override
    protected void init() {
        super.init();
        getApplicationListeners().add(this);
        getSessionListeners().add(this);
        IPackageResourceGuard packageResourceGuard = this.getResourceSettings().getPackageResourceGuard();
        if (packageResourceGuard instanceof SecurePackageResourceGuard) {
            SecurePackageResourceGuard guard = (SecurePackageResourceGuard) packageResourceGuard;
            guard.addPattern("+*.ogg");
            guard.addPattern("+*.mp3");
        }
        getJavaScriptLibrarySettings()
                .setJQueryReference(new PackageResourceReference(ResourceScope.class, ReferenceUtilities.J_QUERY_JS));

        getMarkupSettings().setStripWicketTags(true);
    }

    @Override
    protected Class<? extends AbstractAuthenticatedWebSession> getWebSessionClass() {
        return WebSession.class;
    }

    @Override
    public Session newSession(Request request, Response response) {
        return new WebSession(getWebUiProperties(), request, getCycle());
    }

    protected ICycle getCycle() {
        return cycle;
    }

    public static void main(String[] args) {
        System.out.println(ICycle.class.isAssignableFrom(CycleImpl.class));
    }

    @Override
    public org.apache.wicket.protocol.http.WebApplication createApplication(WicketFilter filter) {
        applicationContext = WebApplicationContextUtils
                .getRequiredWebApplicationContext(filter.getFilterConfig().getServletContext());
        WebUiProperties webUiProperties = getApplicationContext().getBean(WebUiProperties.class);
        if (webUiProperties.getCycleClass() == null) {
            this.cycle = new CycleImpl();
        } else {
            if (ICycle.class.isAssignableFrom(webUiProperties.getCycleClass())) {
                try {
                    this.cycle = (ICycle) webUiProperties.getCycleClass().newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    throw new WicketRuntimeException(e);
                }
            } else {
                throw new WicketRuntimeException("webui.cycleClass is not instance of " + ICycle.class.getName());
            }
        }
        this.webUiProperties = webUiProperties;
        return this;
    }

    public static ApplicationContext getApplicationContext() {
        if (applicationContext == null) {
            throw new WicketRuntimeException("NPE");
        }
        return applicationContext;
    }

    @Override
    protected Class<? extends WebPage> getSignInPageClass() {
        return getCycle().getLoginPageClass(getWebUiProperties(), WebSession.get());
    }

    @Override
    public Class<? extends WebPage> getHomePage() {
        return getCycle().getHomePageClass(getWebUiProperties(), WebSession.get());
    }

    protected WebUiProperties getWebUiProperties() {
        return webUiProperties;
    }

    @Override
    public void onCreated(Session session) {
        getCycle().onInitializedSession(getWebUiProperties(), (WebSession) session);
    }

    @Override
    public void onUnbound(String sessionId) {
        getCycle().onDestroyedSession(getWebUiProperties(), sessionId);
    }

    @Override
    public void onAfterInitialized(Application application) {
        getCycle().onInitializedApplication(getWebUiProperties(), (AuthenticatedWebApplication) application);
    }

    @Override
    public void onBeforeDestroyed(Application application) {
        getCycle().onDestroyedApplication(getWebUiProperties(), (AuthenticatedWebApplication) application);
    }

    @Override
    public void destroy(WicketFilter filter) {
    }

}
